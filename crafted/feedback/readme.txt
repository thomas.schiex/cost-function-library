
Feedback arc set problem from Resende

and replaced with submodular cost functions ("arcsub" suffix) as described in
 
M. Cooper, S. de Givry, M. Sanchez, T. Schiex, M. Zytnicki, and T. Werner.
Soft arc consistency revisited.
Artificial Intelligence, (7-8):449-478, 2010.

