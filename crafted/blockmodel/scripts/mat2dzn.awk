BEGIN {
    j = 0;
}

NF>0 {
    N = NF;
    j++;
    if (j==1) {
        print "N=",N ";";
        printf("Matrix = [");
    }
    printf("|");
    for (i=1; i<=NF; i++) {
        printf("%d", 0+$i);
        if (i < NF || j < N) printf(",");
    }
    print "";
}

END {
    print "|];";
}
