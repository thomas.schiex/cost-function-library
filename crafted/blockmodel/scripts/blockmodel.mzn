%%% Blockmodeling problem
%%% Simon de Givry@2020
include "globals.mzn";

% number of blocks
int: K = 3;

% number of nodes
int: N;

% input matrix
array[1..N, 1..N] of 0..1: Matrix;

% Variables declaration
array[1..K, 1..K] of var 0..1: M;
array[1..N] of var 1..K: F;
var int: obj;

% clustering objective function
constraint obj = sum(u in 1..K, v in 1..K, i in 1..N, j in 1..N where i != j) (bool2int(F[i]=u /\ F[j]=v /\ M[u,v] != Matrix[i,j])) + sum(u in 1..K, i in 1..N) (bool2int(F[i]=u /\ M[u,u] != Matrix[i,i]));

%  partial symmetry breaking constraint
constraint forall(i in 1..(K-1)) (F[i] <= i);

% solve minimize objective;
solve minimize obj;

output
[
  "M: " ++ show(M) ++ "\n" ++
  "F: " ++ show(F) ++ "\n" ++
  "objective: " ++ show(obj)
];

