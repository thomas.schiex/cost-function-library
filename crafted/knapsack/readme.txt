
Knapsack original instances in Minizinc format taken from 

Geoffrey Chu and Peter J. Stuckey. Dominance driven search. In Proceedings of the 19th International Conference on Principles and Practice of Constraint Programming. LNCS 8124, pages 217-229, Springer, 2013

https://people.eng.unimelb.edu.au/pstuckey/dom-jump

Warning, instances in wcsp format have their optima shifted by total sum of profits. The correct optimum should be the profit sum minus wcsp optimum.

Usage:
> ./dzn2wcsp.sh knapsack-100-1.dzn
> toulbar2 knapsack-100-1.wcsp

New solution: 2444 (60 backtracks, 260 nodes, depth 1)
 0 0 1 1 1 0 0 0 1 1 1 1 0 0 0 1 1 1 0 1 1 1 0 1 1 1 0 1 0 0 0 0 1 1 1 1 1 0 1 0 0 1 1 1 1 0 1 1 1 0 0 0 1 0 1 1 0 0 1 1 1 0 0 1 0 1 1 1 1 0 0 1 0 0 1 0 0 1 0 1 0 0 1 1 1 0 0 1 0 1 1 1 0 0 0 1 1 1 0 1
Optimality gap: [2443, 2444] 0.041 % (70 backtracks, 428 nodes)
Optimality gap: [2444, 2444] 0.000 % (1111 backtracks, 3292 nodes)
Node redundancy during HBFS: 32.503 %
Optimum: 2444 in 1111 backtracks and 3292 nodes ( 0 removals by DEE) and 0.145 seconds.

