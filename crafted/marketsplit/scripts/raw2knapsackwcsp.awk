BEGIN {
}

FNR==1 {
N=$2
E=$1
print "knapsack",N,2,N+E*2,1000000;
for (i=0;i<N;i++) printf  2 " ";
print "";
}

FNR==2 {
for (i=1;i<=N;i++) {
  print 1,i-1,0,1,1,$i;
}
}

FNR>2 {
printf N;
for (i=1;i<=N;i++) {
  printf " " i-1;
}
printf " -1 knapsack " $NF;
for (i=1;i<=N;i++) {
  printf " " $i;
}
print "";

printf N;
for (i=1;i<=N;i++) {
  printf " " i-1;
}
printf " -1 knapsack " 0-($NF);
for (i=1;i<=N;i++) {
  printf " " 0-($i);
}
print "";
}
