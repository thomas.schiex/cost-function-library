function case_fold_compare(i1, v1, i2, v2,    l, r)
{
    l = 0+v1

    r = 0+v2

    if (l < r)
        return -1
    else if (l == r)
        return 0
    else
        return 1
}

BEGIN {
N = 0;
}

FNR==1 {
N=$2
E=$1
}

FNR==2 {
for (i=1;i<=N;i++) {
  cost[i-1] = 0+$i;
}
}

FNR>2 {
for (i=1;i<=N;i++) {
  weight[i-1] += 0+$i;
}
}

END {
for (i=1;i<=N;i++) {
  order[i-1] = cost[i-1]/weight[i-1];
}
asorti(order, result, "case_fold_compare");
for (i=1;i<=N;i++) {
  printf " " result[i];
}
print "";
}

