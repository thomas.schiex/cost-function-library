function ternary1(x,y,z,i) {
	nb = 0;
	for (vx=0; vx<=1; vx++) {
		for (vy=0; vy<=1; vy++) {
			if (a[i "," 1] * vx + a[i "," 2] * vy <= b[i]) nb++;
		}
	}
	print 3, x, y ,z, top, nb >> "mso_" m "_" n "_" C "_" R ".wcsp";
	for (vx=0; vx<=1; vx++) {
		for (vy=0; vy<=1; vy++) {
			if (a[i "," 1] * vx + a[i "," 2] * vy <= b[i]) print (reverseval)?1-vx:vx, (reverseval)?1-vy:vy, a[i "," 1] * vx + a[i "," 2] * vy, 0 >> "mso_" m "_" n "_" C "_" R ".wcsp";
		}
	}
}

function ternary(x,y,z,xmax,i,j) {
	nb = 0;
	for (vx=0; vx<=xmax; vx++) {
		for (vy=0; vy<=1; vy++) {
			if (vx + a[i "," j] * vy <= b[i]) nb++;
		}
	}
	print 3, x, y ,z, top, nb >> "mso_" m "_" n "_" C "_" R ".wcsp";
	for (vx=0; vx<=xmax; vx++) {
		for (vy=0; vy<=1; vy++) {
			if (vx + a[i "," j] * vy <= b[i]) print vx, (reverseval)?1-vy:vy, vx + a[i "," j] * vy, 0 >> "mso_" m "_" n "_" C "_" R ".wcsp";
		}
	}
}

function ternaryn(x,y,z,xmax,i) {
	print 3, x, y ,z, top, (xmax+1)*4 >> "mso_" m "_" n "_" C "_" R ".wcsp";
	for (vx=0; vx<=xmax; vx++) {
		for (vy=0; vy<=1; vy++) {
			for (vz=0; vz<=1; vz++) {
				print vx, (reverseval)?1-vy:vy, (reverseval)?1-vz:vz, (vx + a[i "," n-1] * vy + a[i "," n] * vz == b[i])?0:top >> "mso_" m "_" n "_" C "_" R ".wcsp";
			}
		}
	}
}

function comp_func(i1, v1, i2, v2)
     {
         return (v1 - v2);
     }

BEGIN {
	# parameter settings
	m = 0+ ARGV[1];
	n = 0+ ARGV[2]; # 10*(m-1);
	C = 0+ ARGV[3];
	R = 0+ ARGV[4];
	srand(R);
	obj = 1;  # if true, add a linear objective function
	dynvar = 1; # if true, insert decision variables first
	reverseval = 0; # if true, reverse value ordering: value 0 means item type selected and value 1 unselected
	sortedvar = 1; # if true, decision variables are ordered by decreasing cost / sum-of-weights
	top = (obj)?(n*C+1):1;

	# generate random costs and random weights
	if (obj) {
		for (j=1; j<=n; j++) {
			o[j] = int(C*rand());
		}
	}
	for (j=1; j<=n; j++) sumi[j]=0;
	for (i=1; i<=m; i++) {
		for (j=1; j<=n; j++) {
			a[i "," j] = int(C*rand());
			sumi[j] += a[i "," j];
			b[i] += a[i "," j];
		}
		b[i] = int(0.5 * b[i]);
		if (b[i] > bmax) bmax = b[i];
	}

	# reorder a[] and o[] values according to a decreasing cost / sum-of-weights 
	if (sortedvar) {
		for (j=1; j<=n; j++) sumi[j] = - o[j] / (sumi[j]+1);
		for (j=1; j<=n; j++) invsumi[sumi[j]] = "";
		for (j=1; j<=n; j++) invsumi[sumi[j]] = invsumi[sumi[j]] " " j;
		asort(sumi, dest);
		neworder = "";
		for (j=1; j<=n; j++) if (!(invsumi[dest[j]] in alreadyseen)) {neworder = neworder "" invsumi[dest[j]]; alreadyseen[invsumi[dest[j]]]=1;}
#	print neworder;
		nn = split(neworder,order);
		if (nn != n) print "ERROR" > "/dev/stderr";
#	for (j=1; j<=n; j++) print j,sumi[j],dest[j],invsumi[dest[j]],order[j];
		for (j=1; j<=n; j++) {
			o1[j] = o[j];
			for (i=1; i<=m; i++) {
				a1[i "," j] = a[i "," j];
			}
		}
		for (j=1; j<=n; j++) {
			o[j] = o1[order[j]];
			for (i=1; i<=m; i++) {
				a[i "," j] = a1[i "," order[j]];
			}
		}
	}

	# output in raw format
	print m,n,C,R > "mso_" m "_" n "_" C "_" R ".raw";
	if (obj) {
		for (j=1; j<=n; j++) printf("%d ",o[j]) >> "mso_" m "_" n "_" C "_" R ".raw";
		print "" >> "mso_" m "_" n "_" C "_" R ".raw";
	}
	for (i=1; i<=m; i++) {
		for (j=1; j<=n; j++) {
			printf("%d ",a[i "," j]) >> "mso_" m "_" n "_" C "_" R ".raw";
		}
		print b[i] >> "mso_" m "_" n "_" C "_" R ".raw";
	}
#	exit(0);

	# output for CPLEX file
	printf("") > "mso_" m "_" n "_" C "_" R ".cpx";
	if (obj) {
		printf("Min") >> "mso_" m "_" n "_" C "_" R ".cpx";
		for (j=1; j<=n; j++) {
			printf(" +%d x%d",o[j],j) >> "mso_" m "_" n "_" C "_" R ".cpx";
		}
		print "\nSubject To" >> "mso_" m "_" n "_" C "_" R ".cpx";
	}
	for (i=1; i<=m; i++) {
		for (j=1; j<=n; j++) {
			printf(" +%d x%d",a[i "," j],j) >> "mso_" m "_" n "_" C "_" R ".cpx";
		}
		print " = " b[i] >> "mso_" m "_" n "_" C "_" R ".cpx";
	}
	print "Bounds" >> "mso_" m "_" n "_" C "_" R ".cpx";
	for (j=1; j<=n; j++) {
		print "x" j " <= 1" >> "mso_" m "_" n "_" C "_" R ".cpx";
	}
	print "Integer" >> "mso_" m "_" n "_" C "_" R ".cpx";
	for (j=1; j<=n; j++) {
		print "x" j >> "mso_" m "_" n "_" C "_" R ".cpx";
	}
	print "End" >> "mso_" m "_" n "_" C "_" R ".cpx";

	# output for Pseudo-Boolean Optimization (OPB) file
	print "* #variable= " n " #constraint= " m > "mso_" m "_" n "_" C "_" R ".opb";
	if (obj) {
		printf("min:") >> "mso_" m "_" n "_" C "_" R ".opb";
		printf(" %d x%d",o[1],1) >> "mso_" m "_" n "_" C "_" R ".opb";
		for (j=2; j<=n; j++) {
			printf(" +%d x%d",o[j],j) >> "mso_" m "_" n "_" C "_" R ".opb";
		}
		print ";" >> "mso_" m "_" n "_" C "_" R ".opb";
	}
	for (i=1; i<=m; i++) {
		printf(" %d x%d",a[i "," 1],1) >> "mso_" m "_" n "_" C "_" R ".opb";
		for (j=2; j<=n; j++) {
			printf(" +%d x%d",a[i "," j],j) >> "mso_" m "_" n "_" C "_" R ".opb";
		}
		print " = " b[i] ";" >> "mso_" m "_" n "_" C "_" R ".opb";
	}

	# output for WCSP and DAC variable ordering files
	print "knapsack" m "_" n "_" C "_" R, n + (n-3)*m, bmax+1, (n-2)*m + obj*n, top > "mso_" m "_" n "_" C "_" R ".wcsp";
	printf("") > "mso_" m "_" n "_" C "_" R ".order";
	revdac = "";
	v = 0;
	varimplied = n;
	for (j=1; j<=n; j++) {
		revdac = v " " revdac;
		var[j "," 0] = v++;
		printf(" 2") >> "mso_" m "_" n "_" C "_" R ".wcsp";
		if (j>=2 && j <=n-2) {
			for (i=1; i<=m; i++) {
				if (dynvar) {
					revdac = varimplied " " revdac;
					varimplied++;
				} else {
					revdac = v " " revdac;
					var[j "," i] = v++;
					printf(" %d", b[i]+1) >> "mso_" m "_" n "_" C "_" R ".wcsp";
				}
			}
		}
	}
	if (dynvar) {
		for (j=1; j<=n; j++) {
			if (j>=2 && j <=n-2) {
				for (i=1; i<=m; i++) {
					var[j "," i] = v++;
					printf(" %d", b[i]+1) >> "mso_" m "_" n "_" C "_" R ".wcsp";
				}
			}
		}
	}
	print revdac >> "mso_" m "_" n "_" C "_" R ".order";
	print "" >> "mso_" m "_" n "_" C "_" R ".wcsp";
	for (i=1; i<=m; i++) {
		ternary1(var[1 "," 0], var[2 "," 0], var[2 "," i], i);
		tot = a[i "," 1];
		for (j=2; j<n-2; j++) {
			tot +=a[i "," j];
			ternary(var[j "," i], var[(j + 1) "," 0], var[(j + 1) "," i], (tot<b[i])?tot:b[i], i, j+1);
		}
		tot += a[i "," n-2];
		ternaryn(var[n-2 "," i], var[n-1 "," 0], var[n "," 0], (tot<b[i])?tot:b[i], i);
	}
	if (obj) {
		for (j=1; j<=n; j++) {
			print 1,var[j "," 0],0,1 >> "mso_" m "_" n "_" C "_" R ".wcsp";
			print (reverseval)?0:1,o[j] >> "mso_" m "_" n "_" C "_" R ".wcsp";
		}
	}
	exit(0);
}
