BEGIN {
	RS = "@";
}

FNR==1{
	N=0+$1;
	i=3;
	n=0;
    while (i<=NF) {
		LT0[n]=0+$(i+1);
		LT1[n]=0+$(i+2);
		LT2[n]=0+$(i+3);
		PC0[n]=0+$(i+4);
		PC1[n]=0+$(i+5);
		for (j=0; j<N; j++) {
			ST[n "," j] = 0+$(i+6+j);
		}
		i += 6 + N;
		n++;
	}
}

END {
	for (i=0; i<N; i++) {
		print LT0[i],i+1, LT2[i] - LT0[i], 0, LT1[i];
	}
}

