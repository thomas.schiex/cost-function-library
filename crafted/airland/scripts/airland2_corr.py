
import sys
import pytoulbar2

f = open(sys.argv[1], 'r').readlines()

# number of runways
K = int(sys.argv[2])  

tokens = []
for l in f:
    tokens += l.split()

pos = 0

def token():
   global pos, tokens
   if (pos == len(tokens)):
      return None
   s = tokens[pos]
   pos += 1
   return int(float(s)) 

# number of airplanes
N = token()
token() # skip freeze time

LT = []
PC = []
ST = []

for i in range(N):
   token()  # skip appearance time
# Times per plane: {earliest landing time, target landing time, latest landing time}
   LT.append([token(), token(), token()])

# Penalty cost per unit of time per plane:
# [for landing before target, after target]
   PC.append([token(), token()])

# Separation time required after i lands before j can land
   ST.append([token() for j in range(N)])

top = 99999

Problem = pytoulbar2.CFN(top, vac=1)
pytoulbar2.tb2.option.showSolutions = 3
#pytoulbar2.tb2.option.allSolutions = 1000
pytoulbar2.tb2.option.verbose = -1
pytoulbar2.tb2.check()

# airplane x_i lands at time t_i on runway r_i
for i in range(N):
    Problem.AddVariable('x' + str(i), ['r' + str(r) + '_t' + ('+' if t>=LT[i][1] else '') + str(t - LT[i][1]) for r in range(K) for t in range(LT[i][0],LT[i][2]+1)])

# penalty cost for landing before or after target landing time
for i in range(N):
    Problem.AddFunction([i], [(PC[i][0] if t < LT[i][1] else PC[i][1]) * abs(t-LT[i][1]) for r in range(K) for t in range(LT[i][0], LT[i][2]+1)])

# non overlap constraint between every pair of airplanes
count = 0
waste = 0
for i in range(N):
    for j in range(i+1,N):
        if LT[i][2]+1+ST[i][j] > LT[j][0] and LT[j][2]+1+ST[j][i] > LT[i][0]:
            count += 1
            Problem.AddFunction([i, j], [(0 if (r_i != r_j or t_i + ST[i][j] <= t_j or t_j + ST[j][i] <= t_i) else top) for r_i in range(K) for t_i in range(LT[i][0], LT[i][2]+1) for r_j in range(K) for t_j in range(LT[j][0], LT[j][2]+1)])            
        else:
            waste += 1

# symmetry breaking
for i in range(K-1):
    Problem.AddFunction([i], [(top if r > i else 0) for r in range(K) for t in range(LT[i][0], LT[i][2]+1)])

print("count:", count)
print("waste:", waste)

#Problem.Dump(sys.argv[1].replace('.txt','_' + str(K) + '.cfn'))
Problem.NoPreprocessing()
pytoulbar2.tb2.option.useRASPS = True
pytoulbar2.tb2.option.RASPSreset = True
if len(sys.argv) > 3 and int(sys.argv[3]) == 1:
    print("RASPS using VAC and soft AC...")
    pytoulbar2.tb2.option.backtrackLimit = 1000
    pytoulbar2.tb2.option.LcLevel = 1
    Problem.SolveFirst()
    depth=Problem.Depth()
    Problem.Store()
    rasps=Problem.SolveNext()   # returns (solution vector, optimum, None)
    Problem.Restore(depth)
    print("Complete search using EDAC...")
    pytoulbar2.tb2.option.useRASPS = False
    pytoulbar2.tb2.option.backtrackLimit = 9223372036854775807
    pytoulbar2.tb2.option.LcLevel = 4
    if rasps:
        Problem.SetUB(rasps[1] + 1)
    res=Problem.SolveNext()   # returns (solution vector, optimum, None)
    if res is None:
        res = rasps
else:
    # RASPS using VAC and EDAC
    res=Problem.Solve()   # returns (solution vector, optimum, number of solutions found)
print("Optimum:", res[1], "in", Problem.GetNbBacktracks(), "backtracks and", Problem.GetNbNodes(), "nodes.")

