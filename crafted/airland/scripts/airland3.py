
# another model using explicit disjunctive variables b_i_j such that b_i_j=1 => t_i <= t_j

import sys
import pytoulbar2

f = open(sys.argv[1], 'r').readlines()

# number of runways
K = int(sys.argv[2])  

tokens = []
for l in f:
    tokens += l.split()

pos = 0

def token():
   global pos, tokens
   if (pos == len(tokens)):
      return None
   s = tokens[pos]
   pos += 1
   return int(float(s)) 

# number of airplanes
N = token()
token() # skip freeze time

LT = []
PC = []
ST = []

for i in range(N):
   token()  # skip appearance time
# Times per plane: {earliest landing time, target landing time, latest landing time}
   LT.append([token(), token(), token()])

# Penalty cost per unit of time per plane:
# [for landing before target, after target]
   PC.append([token(), token()])

# Separation time required after i lands before j can land
   ST.append([token() for j in range(N)])

top = 99999

Problem = pytoulbar2.CFN(top, vac=1)
pytoulbar2.tb2.option.showSolutions = 3
#pytoulbar2.tb2.option.allSolutions = 1000
pytoulbar2.tb2.option.verbose = -1
pytoulbar2.tb2.check()

# airplane disjunctive variables b_i_j
B=0
for i in range(N):
    for j in range(i+1,N):
        B += 1
        Problem.AddVariable('b' + str(i) + '_' + str(j), range(2))

# transitive closure on disjunctive variables
for i in range(N):
    for j in range(N):
        for k in range(N):
            if i != j and i != k and j != k:
                Problem.AddFunction(['b' + str(i if i<j else j) + '_' + str(j if i<j else i), 'b' + str(j if j<k else k) + '_' + str(k if j<k else j), 'b' + str(i if i<k else k) + '_' + str(k if i<k else i)], [top if ((bij if i<j else not bij) and (bjk if j<k else not bjk) and not(bik if i<k else not bik)) else 0 for bij in range(2) for bjk in range(2) for bik in range(2)])
        
# airplane x_i lands at time t_i on runway r_i
for i in range(N):
    Problem.AddVariable('#x' + str(i), ['r' + str(r) + '_t' + ('+' if t>=LT[i][1] else '') + str(t - LT[i][1]) for r in range(K) for t in range(LT[i][0],LT[i][2]+1)])

# penalty cost for landing before or after target landing time
for i in range(N):
    Problem.AddFunction([B+i], [(PC[i][0] if t < LT[i][1] else PC[i][1]) * abs(t-LT[i][1]) for r in range(K) for t in range(LT[i][0], LT[i][2]+1)])

# non overlap constraint between every pair of airplanes
count = 0
waste = 0
ij=0
for i in range(N):
    for j in range(i+1,N):
        if LT[i][2]+1+ST[i][j] > LT[j][0] and LT[j][2]+1+ST[j][i] > LT[i][0]:
            count += 1
            Problem.AddFunction([ij, B+i, B+j], [(0 if r_i != r_j or (b and (t_i + ST[i][j] <= t_j)) or (not b and (t_j + ST[j][i] <= t_i)) else top) for b in range(2) for r_i in range(K) for t_i in range(LT[i][0], LT[i][2]+1) for r_j in range(K) for t_j in range(LT[j][0], LT[j][2]+1)])
        else:
            Problem.AddFunction([ij],[top * (LT[i][2]+1+ST[i][j] <= LT[j][0]), top * (1-(LT[i][2]+1+ST[i][j] <= LT[j][0]))])
            waste += 1
        ij += 1

# symmetry breaking
for i in range(K-1):
    Problem.AddFunction([B+i], [(0 if r <= i else top) for r in range(K) for t in range(LT[i][0], LT[i][2]+1)])

print("count:", count)
print("waste:", waste)

#Problem.Dump(sys.argv[1].replace('.txt','_' + str(K) + 'b.cfn'))
Problem.NoPreprocessing()
res=Problem.Solve() # returns (solution vector, optimum, number of solutions found)
print("Optimum:", res[1], "in", Problem.GetNbBacktracks(), "backtracks and", Problem.GetNbNodes(), "nodes.")
