%%% Airplane landing problem
%%% translated from Choco tutorial by Simon de Givry@2020
include "globals.mzn";

% number of runways
int: K=2;

% number of planes
int: N;

% Times per plane: {earliest landing time, target landing time, latest landing time}
array[1..N, 1..3] of int: LT;

% penalty cost penalty cost per unit of time per plane:
% for landing [before target, after target]
array[1..N, 1..2] of int: PC;

% Separation time required after i lands before j can land
array[1..N, 1..N] of int: ST;

% Variables declaration
array[1..N] of var 1..K: runways;
array[1..N] of var int: planes;
array[1..N] of var int: earliness;
array[1..N] of var int: tardiness;
var int: tot_dev;

% Constraint posting
% one plane per runway at a time:
constraint alldifferent(planes);

% for each plane 'i'
constraint forall(i in 1..N) (
    % time window
    (planes[i] >= LT[i,1]) /\
    (planes[i] <= LT[i,3]) /\
    % maintain earliness
    (earliness[i] >= 0) /\
    (earliness[i] <= LT[i,2]-LT[i,1]) /\
    (earliness[i] + planes[i] >= LT[i,2]) /\
    % and tardiness
    (tardiness[i] >= 0) /\
    (tardiness[i] <= LT[i,3]-LT[i,2]) /\
    (tardiness[i] - planes[i] >= -LT[i,2])
);

    % disjunctions: 'i' lands before 'j' or 'j' lands before 'i'
constraint forall(i in 1..N, j in i + 1..N) (
        (runways[i] != runways[j]) \/ (planes[i] + ST[i,j] <= planes[j]) \/ (planes[j] + ST[j,i] <= planes[i])
);

constraint tot_dev = sum(i in 1..N)(PC[i,1] * earliness[i] + PC[i,2] * tardiness[i]);

% solve minimize objective;
solve :: int_search(planes, first_fail, indomain_min, complete) minimize tot_dev;

output
[
  "planes: " ++ show(planes) ++ "\n" ++
  "runways: " ++ show(runways) ++ "\n" ++
  "objective: " ++ show(tot_dev)
];
