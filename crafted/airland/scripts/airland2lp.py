
import sys

####################################################################
#
# Integer Linear Program basic interface
#
####################################################################

class PLNE:

    def __init__(self, name):
        self.name = name
        self.var01 = []
        self.varcont = []
        self.objective = ''
        self.constraints =  []

    def Dump(self):
        print('min %s' % (self.objective))
        print('Subject To')
        for i,c in enumerate(self.constraints):
            print('c%d: %s' % (i,c))
        print('Bounds')
        for x,lowest,greatest in self.varcont:
            print('%d <= %s <= %d' % (lowest, x, greatest))
        print('Binaries')
        for x in self.var01:
            print('%s ' % (x,))
        print('End')

    def AddBinaryVariable(self, s):
        self.var01.append(s)

    def AddContinuousVariable(self, s, lowest, greatest):
        self.varcont.append((s, lowest, greatest))

    def AddLinearConstraint(self, coefs, vars, cmp_operator, right_coef):
        s = ''
        for i in range(len(vars)):
            if coefs[i]>=0 and i>0:
                s = s + ' +'
            s = s + ' ' + str(coefs[i]) + ' ' + vars[i]
        s = s + ' ' + cmp_operator + ' ' + str(right_coef)
        self.constraints.append(s)

    def AddLinearObjective(self, coefs, vars):
        s = ''
        for i in range(len(vars)):
            if coefs[i]>=0 and (i>0 or len(self.objective)>0):
                s = s + ' +'
            s = s + ' ' + str(coefs[i]) + ' ' + vars[i]
        self.objective = self.objective + s

####################################################################
#
# Read data file
#
####################################################################

f = open(sys.argv[1], 'r').readlines()

tokens = []
for l in f:
    tokens += l.split()

pos = 0

def token():
   global pos, tokens
   if (pos == len(tokens)):
      return None
   s = tokens[pos]
   pos += 1
   return int(float(s))

N = token()
token() # skip freeze time

LT = []
PC = []
ST = []

for i in range(N):
   token()  # skip appearance time
    # Times per plane: {earliest landing time, target landing time, latest landing time}
   LT.append([token(), token(), token()])

   # Penalty cost per unit of time per plane:
   # [for landing before target, after target]
   PC.append([token(), token()])

   # Separation time required after i lands before j can land
   ST.append([token() for j in range(N)])

####################################################################
#
# Build output problem file
#
####################################################################

# number of runways
K = int(sys.argv[2])

M = 100000

Problem = PLNE(sys.argv[1].replace('.txt','.lp'))

# t_i landing time of each airplane (continuous variable)
for i in range(N):
    Problem.AddContinuousVariable('t_' + str(i+1), LT[i][0], LT[i][2])

# x_i_u is equal to 1 if airplane i lands on runway u (binary variable)
for i in range(N):
    for u in range(K):
        Problem.AddBinaryVariable('x_' + str(i+1) + '_' + str(u+1))
    #only one variable x_i_u for all u should be equal to 1
    Problem.AddLinearConstraint([1 for u in range(K)], ['x_' + str(i+1) + '_' + str(u+1) for u in range(K)], '=', 1)

# binary variables for ordering airplanes
for i in range(N):
    for j in range(i+1, N):
        # b_i_j is equal to 1 if airplane i lands before airplane j (binary variable)
        Problem.AddBinaryVariable('b_' + str(i+1) + '_' + str(j+1))
        # b_j_i is equal to 1 if airplane j lands before airplane i (binary variable)
        Problem.AddBinaryVariable('b_' + str(j+1) + '_' + str(i+1))
        # either airplane i lands before airplane j or vice et versa
        Problem.AddLinearConstraint([1,1], ['b_' + str(i+1) + '_' + str(j+1), 'b_' + str(j+1) + '_' + str(i+1)], '=', 1)

#binary variables for two airplanes being on the same runway
for i in range(N):
    for j in range(i+1,N):
        # z_i_j equal to 1 if airplane i and airplane j land on the same runway
        Problem.AddBinaryVariable('z_' + str(i+1) + '_' + str(j+1))
        # for all runway u, if x_i_u = 1 and x_j_u = 1 then z_i_j =1
        for u in range(K):
            Problem.AddLinearConstraint([1, 1, -1], ['x_' + str(i+1) + '_' + str(u+1), 'x_' + str(j+1) + '_' + str(u+1), 'z_' + str(i+1) + '_' + str(j+1)], '<=', 1)

#separation time between airplanes if on the same runway
for i in range(N):
    for j in range(N):
        if i < j:
            Problem.AddLinearConstraint([1,ST[i][j],-1,-M], ['t_' + str(i+1), 'z_' + str(i+1)  + '_' + str(j+1), 't_' + str(j+1), 'b_' + str(j+1) + '_' + str(i+1)], '<=', 0)
        elif i > j:
            Problem.AddLinearConstraint([1,ST[i][j],-1,-M], ['t_' + str(i+1), 'z_' + str(j+1)  + '_' + str(i+1), 't_' + str(j+1), 'b_' + str(j+1) + '_' + str(i+1)], '<=', 0)

#minimize sum of early and late times
for i in range(N):
    Problem.AddContinuousVariable('early' + '_' + str(i+1), 0, LT[i][1] - LT[i][0])
    Problem.AddContinuousVariable('late' + '_' + str(i+1), 0, LT[i][2] - LT[i][1])
    Problem.AddLinearConstraint([1,1], ['early' + '_' + str(i+1), 't_' + str(i+1)], '>=', LT[i][1])
    Problem.AddLinearConstraint([1,-1], ['late' + '_' + str(i+1), 't_' + str(i+1)], '>=', -LT[i][1])

#minimize sum of early and late times
Problem.AddLinearObjective([PC[i][0] for i in range(N)] + [PC[i][1] for i in range(N)], ['early' + '_' + str(i+1) for i in range(N)] + ['late' + '_' + str(i+1) for i in range(N)])

Problem.Dump()
