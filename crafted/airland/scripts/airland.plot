
N = `awk '{print 1+$1 ; exit}' @ARG1`

set yrange [0:N]

plot "< awk -f ./airland2plot.awk " . ARG1 u 1:2:3:4 with vectors head notitle, "< awk -f ./airland2plot.awk " . ARG1 u 5:2 ls 2 notitle

