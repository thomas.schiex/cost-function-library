
Airplane Landing Problem

See problem description in CHOCO tutorial

https://choco-solver.org/tutos/aircraft-landing-problem/

Instances taken from Beasley et al, 2000, 2004

http://people.brunel.ac.uk/~mastjjb/jeb/orlib/airlandinfo.html


Usage
-----

# compile toulbar2 using PYTB2 flag ON in order to generate its Python library
> cd scripts
> cp ~/toulbar2/lib/Linux/pytoulbar2.cpython-35m-x86_64-linux-gnu.so .
> python3 airland.py ../data/airland1.txt 
...
New solution: 700 (64 backtracks, 154 nodes, depth 3)
 165 258 98 106 118 134 126 142 150 180
...
Optimum: 700 in 70 backtracks and 175 nodes ( 462 removals by DEE) and 3.442 seconds.

