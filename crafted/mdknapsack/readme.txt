Multi-Demand Multidimensional Knapsack problems

See instance description from OR-Library:

http://people.brunel.ac.uk/~mastjjb/jeb/orlib/mdmkpinfo.html

Original instances come from:

P. Cappanera and M. Trubian. A local-search-based heuristic for the demand-
constrained multidimensional knapsack problem. INFORMS Journal on Com-
puting, 17(1):82-98, 2005.

Warning, instances in wcsp format have their optima shifted by total sum of profits (corresponding to the initial upperbound given in wcsp header).
The correct optimum should be the profit sum minus wcsp optimum.

OPB instances have been produced from lp instances using SCIP. There are in minimization. Use option -precision=1 to accelerate solving time.
