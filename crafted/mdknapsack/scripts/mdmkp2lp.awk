
BEGIN {
	RS = "@"; # special character that should never appear in the wcsp file
}

{
	name = FILENAME;
	sub("[.]txt","",name);
	p=1;
	K=$p;
	p++;
	for (k=1; k<=K; k++) {
		n=$p;
		p++;
		m=$p;
		p++;
		for (i=1; i<=m; i++) {
			for (j=1; j<=n; j++) {
				aleq[i,j] = $p;
				p++;
			}
		}
		for (i=1; i<=m; i++) {
			bleq[i] = $p;
			p++;
		}
		for (i=1; i<=m; i++) {
			for (j=1; j<=n; j++) {
				ageq[i,j] = $p;
				p++;
			}
		}
		for (i=1; i<=m; i++) {
			bgeq[i] = $p;
			p++;
		}
		for (v=1; v<=6; v++) {
			for (j=1; j<=n; j++) {
				c[v,j] = $p;
				p++;
			}
		}

		print NF,p;
		
		for (v=1; v<=6; v++) {
			top = 0;
			for (j=1; j<=n; j++) {
				top += c[v,j];
			}
			if (((v-1) % 3)==0) {
				mm=1;
			} else if (((v-1) % 3)==1) {
				mm = int(m/2);
			} else {
				mm = m;
			}
			printf("Max ") > name "_" k "_" v ".lp";
			for (j=1; j<=n; j++) {
				printf(" + %d x%d", c[v,j], j) >> name "_" k "_" v ".lp";
			}
			print "" >> name "_" k "_" v ".lp";
			print "Subject To" >> name "_" k "_" v ".lp";
			for (i=1; i<=m; i++) {
				for (j=1; j<=n; j++) {
					printf(" + %d x%d", aleq[i,j], j) >> name "_" k "_" v ".lp";
				}
				printf(" <= %d", bleq[i]) >> name "_" k "_" v ".lp";
				print "" >> name "_" k "_" v ".lp";
			}
			for (i=1; i<=mm; i++) {
				for (j=1; j<=n; j++) {
					printf(" + %d x%d", ageq[i,j], j) >> name "_" k "_" v ".lp";
				}
				printf(" >= %d", bgeq[i]) >> name "_" k "_" v ".lp";
				print "" >> name "_" k "_" v ".lp";
			}
			print "Binary" >> name "_" k "_" v ".lp";			
			for (j=1; j<=n; j++) {
				print "x" j >> name "_" k "_" v ".lp";
			}
			print "End" >> name "_" k "_" v ".lp";
			close(name "_" k "_" v ".lp");
		}
	}
}
