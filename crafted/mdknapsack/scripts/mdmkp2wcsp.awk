
BEGIN {
	RS = "@"; # special character that should never appear in the wcsp file
}

{
	name = FILENAME;
	sub("[.]txt","",name);
	p=1;
	K=$p;
	p++;
	for (k=1; k<=K; k++) {
		n=$p;
		p++;
		m=$p;
		p++;
		for (i=1; i<=m; i++) {
			for (j=1; j<=n; j++) {
				aleq[i,j] = $p;
				p++;
			}
		}
		for (i=1; i<=m; i++) {
			bleq[i] = $p;
			p++;
		}
		for (i=1; i<=m; i++) {
			for (j=1; j<=n; j++) {
				ageq[i,j] = $p;
				p++;
			}
		}
		for (i=1; i<=m; i++) {
			bgeq[i] = $p;
			p++;
		}
		for (v=1; v<=6; v++) {
			for (j=1; j<=n; j++) {
				c[v,j] = $p;
				p++;
			}
		}

		print NF,p;
		
		for (v=1; v<=6; v++) {
			top = 0;
			for (j=1; j<=n; j++) {
				top += c[v,j];
			}
			if (((v-1) % 3)==0) {
				mm=1;
			} else if (((v-1) % 3)==1) {
				mm = int(m/2);
			} else {
				mm = m;
			}
			print "MDMKP_" k "_" v,n,2,m+mm+n,top > name "_" k "_" v ".wcsp";
			for (j=1; j<=n; j++) {
				printf(" %d", 2) >> name "_" k "_" v ".wcsp";
			}
			print "" >> name "_" k "_" v ".wcsp";
			for (i=1; i<=m; i++) {
				printf("%d",n) >> name "_" k "_" v ".wcsp";
				for (j=1; j<=n; j++) {
					printf(" %d", j-1) >> name "_" k "_" v ".wcsp";
				}
				printf(" -1 knapsack %d", -bleq[i]) >> name "_" k "_" v ".wcsp";
				for (j=1; j<=n; j++) {
					printf(" %d", -aleq[i,j]) >> name "_" k "_" v ".wcsp";
				}
				print "" >> name "_" k "_" v ".wcsp";
			}
			for (i=1; i<=mm; i++) {
				printf("%d",n) >> name "_" k "_" v ".wcsp";
				for (j=1; j<=n; j++) {
					printf(" %d", j-1) >> name "_" k "_" v ".wcsp";
				}
				printf(" -1 knapsack %d", bgeq[i]) >> name "_" k "_" v ".wcsp";
				for (j=1; j<=n; j++) {
					printf(" %d", ageq[i,j]) >> name "_" k "_" v ".wcsp";
				}
				print "" >> name "_" k "_" v ".wcsp";
			}
			for (j=1; j<=n; j++) {
				if (c[v,j]>=0) {
					print 1,j-1,0,1,0,c[v,j] >> name "_" k "_" v ".wcsp";
				} else {
					print 1,j-1,0,1,1,-c[v,j] >> name "_" k "_" v ".wcsp";
				}
			}
			close(name "_" k "_" v ".wcsp");
		}
	}
}
