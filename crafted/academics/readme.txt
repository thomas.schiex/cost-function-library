This directory contains some academic problems:

4queens			4 Queens problem (satisfaction problem)

4wqueens,8wqueens	Weighted queen problems (optimization problem)
			(see ../generators/wqueens.c)

zebra, zebre-ext	Zebra problem (satisfaction problem) 
			("zebre-ext" has alldiff translated into binary constraints)

donald, send		Crypto-arithmetic puzzles (satisfaction problem)

vacnotosac		A tiny problem which is virtual AC but not Optimal Soft AC

warehouses		Uncapacitated warehouse location problem (optimization problem)
