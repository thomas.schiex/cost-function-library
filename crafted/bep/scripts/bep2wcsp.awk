
# convert a BEP instance into a weighted CSP

# usage: awk -f bep2wcsp.awk instances/bEpInstance_A_15_1 > problem.wcsp

NR == 3 {
	N = $1;
	maxd = 0;
	top = 1;
}

NR >= 4 && NR < N + 4 {
	duration[$1] = $4;
	if ($5 > 0) earliest[$1] = $5;
	else earliest[$1] = 0;
	latest[$1] = $6 - $4;
	d = latest[$1] - earliest[$1] + 2;
	if (d > maxd) maxd = d;
	revenue[$1] = $7;
	top += revenue[$1] * (N-1);
}

NR >= N+4 {
	for (i=1;i<=N;i++) {
		delay[NR-N-3,i] = $i;
	}
}

END {
	print "bep",N,maxd,N*(N-1)/2,top;
	printf("%d",latest[1] - earliest[1] + 2);
	for (i=2;i<=N;i++) {
		printf(" %d",latest[i] - earliest[i] + 2);
	}
	print "";
	for (i=1;i<=N;i++) {
		for (j=i+1;j<=N;j++) {
			print 2,i-1,j-1,0,(latest[i]-earliest[i]+2)*(latest[j]-earliest[j]+2);
			for (a=earliest[i];a<=latest[i]+1;a++) {
				for (b=earliest[j];b<=latest[j]+1;b++) {
					if (a>latest[i] && b>latest[j]) print a-earliest[i],b-earliest[j],revenue[i]+revenue[j];
					else if (a>latest[i]) print a-earliest[i],b-earliest[j],revenue[i];
					else if (b>latest[j]) print a-earliest[i],b-earliest[j],revenue[j];
					else print a-earliest[i],b-earliest[j],((a>=b+duration[j]+delay[j,i])||(b>=a+duration[i]+delay[i,j]))?0:top;
				}
			}
		}
	}
}
