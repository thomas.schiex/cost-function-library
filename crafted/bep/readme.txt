// A simplified version of a problem of selecting and scheduling observations for agile satellites.
// 
// The satellite has a pool of candidate photographs to take, and must select and schedule
// a subset of them, at each pass above a strip of the earth territory.
// The satellite can only take one photograph at a time. A photograph can only be taken
// during a given time window, depending on its coordinates on the earth surface.
// Minimal time manoeuvres are required between two consecutive photographs.
// All physical constraints (time windows, time manoeuvres) must be met, and the sum of 
// the revenues of the selected photographs must be maximized.
//
// This problem is a mix between a TSP with time windows, and a Knapsack problem.
//
// The main components of an instance are :
//   1) a pool of photo candidates, each with a duration, a time window and a revenue,
//   2) a 2D array of manoeuvre times beetween each pair of photo candidate.
//
// Original version by Fran�ois Laburthe as a bench for CHOCO (oct 2000).
// Adaptation for an EOLE bench by Michel Lema�tre (june 2001).
//
// See also: Selecting and scheduling observations for agile satellites: 
//           some lessons from the constraint reasoning community point of view
//           G. Verfaillie and M. Lemaitre, CP-2001.

__________________________________
Description of the problem format:
__________________________________

# general information
<general comment> \n
<problem name> \n

# configuration parameters
<N (number of candidates)> <random seed> <maxXCoord> <maxYCoord> <minDuration> <maxDuration> <minMoney> <maxMoney> <maxYOffset> <speedFactor> \n

# description of each candidate
1 <xcoord> <ycoord> <duration> <earliestArrival> <latestDeparture> <revenue> \n
2 <xcoord> <ycoord> <duration> <earliestArrival> <latestDeparture> <revenue> \n
...
N <xcoord> <ycoord> <duration> <earliestArrival> <latestDeparture> <revenue> \n

# matrix of time maneuvers
<time maneuver from 1 to 1> <time maneuver from 1 to 2> ... <time maneuver from 1 to N> \n
<time maneuver from 2 to 1> <time maneuver from 2 to 2> ... <time maneuver from 2 to N> \n
...
<time maneuver from N to 1> <time maneuver from N to 2> ... <time maneuver from N to N> \n
