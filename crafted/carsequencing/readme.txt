
See problem description in

Tractability-preserving Transformations of Global Cost Functions David Allouche, Christian Bessiere, Patrice Boizumault, Simon de Givry, Patricia Gutierrez, Jimmy HM. Lee, Ka Lun Leung, Samir Loudni, Jean-Philippe Métivier, Thomas Schiex, Yi Wu. Artificial Intelligence, 238:166-189, 2016.

See also

CSPLib Car Sequencing

http://csplib.org/Problems/prob001/

