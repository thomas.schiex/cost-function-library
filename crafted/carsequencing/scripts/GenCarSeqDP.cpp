#include<cstdio>
#include<cstdlib>
#include<cmath>
#include <string>
#include <cstring>

#define TOP 2000000


int main(int argc, char **argv) {

	if (argc < 4 || argc > 5) {
		printf("Usage : carSeq <number of cars> <filename> <number of types(=domain size)> (seed)\n");
		exit(0);
	}
	if (argc == 4){
		srand(time(NULL));
	} else {
	  srand(atoi(argv[4]));
	}
	int n = atoi(argv[1]); // number of cars (variables)
	int violation = 10; // the cost of each amount of violation
	int opt = 5; // number of assembly line (each assembly line is responsible for one option)
	int cls = atoi(argv[3]); // number of car types
	
	FILE* file = fopen(argv[2], "w+");

	int block[opt]; // size of an assembly line
	int windowsize[opt]; // available number of cars to be assemblyed within a block size
	// every 'windowsize[i]' out of 'block[i]' cars can have the option 'i'.
	int optsize[opt]; // number of car types with this option
	
	int cars[cls][opt+1];
	
	int nc = 0;
	
	// assigning the size and limitation of each assembly line
	
	for (int i = 0; i < opt; i++){
		block[i] = 4 + rand() % 4;
		if (block[i] > n) block[i] = n;
		nc += (n + 1) - block[i];
		windowsize[i] = 1 + (rand() % (block[i]-1));
		optsize[i] = 0;
	}
	
	// assigning the components to each type of cars
	
	for (int i = 0; i < cls; i++){
		int optcnt = 0;
		cars[i][opt] = 1; // each car type has at least one car to be produced
		do {
			for (int j = 0; j < opt; j++){
				cars[i][j] = rand() % 2;
				if (cars[i][j] == 1){
					optsize[j]++;
					optcnt++;
				}
			}
		} while (optcnt == 0);
	}
	
	for (int i = cls; i < n; i++){
	  cars[rand()%cls][opt]++; // number of cars to be produced per type
	}
	
	
	// header of the WCSP problem
	
	fprintf(file, "car%d %d %d %d %d\n", n, n, cls, nc+cls+n, TOP);
	
	for (int i=0;i<n;i++) {
		fprintf(file, "%d ", cls);
	} 
	fprintf(file, "\n");
	
	// restricting the number of each type of cars
	
	for (int i = 0; i < cls; i++){
	  fprintf(file, "%d ", n);
	  for (int j = 0; j < n; j++){
		fprintf(file, "%d ", j);
	  }
	  fprintf(file, "-1 samong var %i %i %i 1 %d\n", TOP, cars[i][opt], cars[i][opt], i);	
	}
		
	for (int i = 0; i < opt; i++){
		for (int j = 0; j < n + 1 - block[i]; j++){
			fprintf(file, "%d ", block[i]); 
			for (int k = j; k < j+block[i]; k++){
				fprintf(file, "%d ", k);
			}
			fprintf(file, "-1 samong var %i 0 %i %i ", violation, windowsize[i], optsize[i]);
			for (int k = 0; k < cls; k++){
				if (cars[k][i] == 1){
					fprintf(file, "%d ", k);
				}
			}
			fprintf(file, "\n");
		}
	}

	// unary costs
	
	for (int i=0;i<n;i++) {
			fprintf(file, "1 %d 0 %d\n", i, cls);
			for (int v=0;v<cls;v++){
				int cost = rand()%10;
				fprintf(file, "%d %d\n", v, cost);
			}
	} 
	
	fclose(file);
	return 0;
}
