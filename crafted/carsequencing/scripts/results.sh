#!/bin/bash

MINCELL=$1
MAXCELL=$2
INCCELL=$3
TIMEOUT=$4
NBEXPE=$5

echo "Parameters : ";
echo "Minimal number of cells (height or width) = "$MINCELL
echo "Maximal number of cells (height or width) = "$MAXCELL
echo "Increment in the size = "$INCCELL
echo "Time out = "$TIMEOUT
echo "Number of experiments = "$NBEXPE

echo "$MINCELL $MAXCELL $INCCELL $TIMEOUT" > res.exp 
ulimit -t $TIMEOUT;

for i in `seq  $MINCELL $INCCELL $MAXCELL`; do
	for run in `seq 1 $NBEXPE`; do
		seed=$(($run))
		echo  -en "\r$i\t$run        "
		echo  -en "$i\t$seed" >> res.exp
		
		### SOFT CLASSIC ###
		
		file="./instances/carseqflow_"$i"_"$seed".wcsp"
		trace="trace/trace_carseq_"$i"_"$seed"_flow"
		end=$(grep end. $trace | wc -l)
		#echo $end
		if [[ $end == 1 ]] ; then
			line=$(tail -n 2 $trace	| head -n 1)
			bt=$(echo $line | cut -d' ' -f4)
			node=$(echo $line | cut -d' ' -f7)
			runtime=$(echo $line | cut -d' ' -f15)
			echo -en "\t$bt\t$node\t$runtime" >> res.exp
		else 
			echo -en "\t-\t-\t$TIMEOUT" >> res.exp
		fi
		
		
		### SOFT DP ###
		
		file="./instances/carseqdp_"$i"_"$seed".wcsp"
		trace="trace/trace_carseq_"$i"_"$seed"_dp"
		end=$(grep end. $trace | wc -l)
		#echo $end
		if [[ $end == 1 ]] ; then
			line=$(tail -n 2 $trace	| head -n 1)
			bt=$(echo $line | cut -d' ' -f4)
			node=$(echo $line | cut -d' ' -f7)
			runtime=$(echo $line | cut -d' ' -f15)
			echo -en "\t$bt\t$node\t$runtime" >> res.exp
		else 
			echo -en "\t-\t-\t$TIMEOUT" >> res.exp
		fi
		
		### WEIGHTED ###
		
		file="./instances/carseqtern_"$i"_"$seed".wcsp"
		trace="trace/trace_carseq_"$i"_"$seed"_tern"
		end=$(grep end. $trace | wc -l)
		#echo $end
		if [[ $end == 1 ]] ; then
			line=$(tail -n 2 $trace	| head -n 1)
			bt=$(echo $line | cut -d' ' -f4)
			node=$(echo $line | cut -d' ' -f7)
			runtime=$(echo $line | cut -d' ' -f15)
			echo -en "\t$bt\t$node\t$runtime" >> res.exp
		else 
			echo -en "\t-\t-\t$TIMEOUT" >> res.exp
		fi
		
		echo "" >> res.exp
	done
done

echo -e "\r"

for i in `seq  $MINCELL $INCCELL $MAXCELL`; do

	echo -en "$i\t|\t"
	
	nbline=$((1+($NBEXPE*(($i-$MINCELL)+1))))

	#if [[ $i -eq 0 ]] ; then start_line=0; fi
	head -n $nbline res.exp | tail -n $NBEXPE > temp.extract
		
	bt_SR=$(cat temp.extract | awk 'BEGIN{sum=0;n=0} { if ($3!="-") {sum+=$3; n++}} END {if (n>0) print sum/n; else print "-"}')
#	time_SR=$(cat temp.extract | awk 'BEGIN{sum=0} { sum+=$5} END {print sum/'$NBEXPE'}')
	time_SR=$(cat temp.extract | awk 'BEGIN{sum=0;n=0} { if ($3!="-") {sum+=$5; n++}} END {if (n>0) print sum/n; else print "-"}')
	success_SR=$(cat temp.extract | awk 'BEGIN{sum=0} { if ($3!="-") { sum+=1} } END { print sum }')
	echo -en "$success_SR\t"
	echo -en "$bt_SR\t"
	echo -en "$time_SR"
	echo -en "\t|\t"
	
	bt_DP=$(cat temp.extract | awk 'BEGIN{sum=0;n=0} {if ($6!="-") {sum+=$6; n++}} END {if (n>0) print sum/n; else print "-"}')
#	time_DP=$(cat temp.extract | awk 'BEGIN{sum=0} { sum+=$8} END {print sum/'$NBEXPE'}')
	time_DP=$(cat temp.extract | awk 'BEGIN{sum=0;n=0} { if ($6!="-") {sum+=$8; n++}} END {if (n>0) print sum/n; else print "-"}')
	success_DP=$(cat temp.extract | awk 'BEGIN{sum=0} { if ($6!="-") { sum+=1} } END { print sum }')
	echo -en "$success_DP\t"
	echo -en "$bt_DP\t"
	echo -en "$time_DP"
	echo -en "\t|\t"
	
	bt_WR=$(cat temp.extract | awk 'BEGIN{sum=0;n=0} {if ($9!="-") {sum+=$9; n++}} END {if (n>0) print sum/n; else print "-"}')
#	time_WR=$(cat temp.extract | awk 'BEGIN{sum=0} { sum+=$11} END {print sum/'$NBEXPE'}')
	time_WR=$(cat temp.extract | awk 'BEGIN{sum=0;n=0} { if ($9!="-") {sum+=$11; n++}} END {if (n>0) print sum/n; else print "-"}')
	success_WR=$(cat temp.extract | awk 'BEGIN{sum=0} { if ($9!="-") { sum+=1} } END { print sum }')
	echo -en "$success_WR\t"
	echo -en "$bt_WR\t"
	echo -en "$time_WR"
	echo -en "\t|\t"
	
	
	echo ""
	
	rm temp.extract
done

mv res.exp myresults.txt
