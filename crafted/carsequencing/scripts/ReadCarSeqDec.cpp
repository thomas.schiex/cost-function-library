#include<cstdio>
#include<cstdlib>
#include<cmath>
#include <string>
#include <cstring>

#define TOP 2000000


int main(int argc, char **argv) {
	

	if (argc != 3) {
		printf("Usage : carSeq <inputfilename> <outputfilename>\n");
		exit(0);
	}
	
	FILE* file = fopen(argv[1], "r");
	int n = 0; // number of cars (variables)
	fscanf(file, "%d", &n);
	int opt = 0; // number of assembly line (each assembly line is responsible for one option)
	fscanf(file, "%d", &opt);
	int cls = 0; // number of car types
	fscanf(file, "%d", &cls);
	int violation = 1; // the cost of each amount of violation
	
	int block[opt]; // size of an assembly line
	int windowsize[opt]; // available number of cars to be assemblyed within a block size
	// every 'windowsize[i]' out of 'block[i]' cars can have the option 'i'.
	int optsize[opt]; // number of car types with this option
	
	int cars[cls][opt+1];
	
	int nc = 0;
	
	// assigning the size and limitation of each assembly line
	for (int i = 0; i < opt; i++){
		fscanf(file, "%d", &windowsize[i]);
	}
	for (int i = 0; i < opt; i++){
		fscanf(file, "%d", &block[i]);
		nc += (n + 1) - block[i];
		optsize[i] = 0;
	}
	
	// assigning the components to each type of cars
	for (int i = 0; i < cls; i++){
	    int icls = 0;
		fscanf(file, "%d", &icls);
		fscanf(file, "%d", &cars[i][opt]);
		for (int j = 0; j < opt; j++){
		  fscanf(file, "%d", &cars[i][j]);
		  if (cars[i][j] == 1){
			optsize[j]++;
		  }
		}
	}
	fclose(file);

	// header of the WCSP problem
	file = fopen(argv[2], "w+");
	fprintf(file, "car%d %d %d %d %d\n", n, n+2, cls+2, nc+cls+2, TOP); // add 2 dummy vars for arity 2 or 3 global cost functions
	
	for (int i=0;i<n;i++) {
		fprintf(file, "%d ", cls);
	} 
	fprintf(file, "%d %d\n", cls+1, cls+2);
		
	for (int i = 0; i < opt; i++){
		for (int j = 0; j < n + 1 - block[i]; j++){
			fprintf(file, "%d ", (block[i]<4)?4:block[i]); 
			if (block[i]<=3) fprintf(file, "%d ", n);
			if (block[i]<=2) fprintf(file, "%d ", n+1);
			for (int k = j; k < j+block[i]; k++){
				fprintf(file, "%d ", k);
			}
			fprintf(file, "-1 wamong lin %i %i ", violation, optsize[i]);
			for (int k = 0; k < cls; k++){
				if (cars[k][i] == 1){
					fprintf(file, "%d ", k);
				}
			}
			fprintf(file, " 0 %i\n", windowsize[i]);
		}
	}
	
	// restricting the number of each type of cars
	
	for (int i = 0; i < cls; i++){
	  fprintf(file, "%d ", n);
	  for (int j = 0; j < n; j++){
		fprintf(file, "%d ", j);
	  }
	  fprintf(file, "-1 wamong hard %i 1 %d %i %i\n", TOP, i, cars[i][opt], cars[i][opt]);	
	}

	// unary costs
	
	// for (int i=0;i<n;i++) {
	// 		fprintf(file, "1 %d 0 %d\n", i, cls);
	// 		for (int v=0;v<cls;v++){
	// 			int cost = rand()%10;
	// 			fprintf(file, "%d %d\n", v, cost);
	// 		}
	// } 
	
	// dummy vars
	fprintf(file, "1 %d %d 1\n", n, TOP);
	fprintf(file, "%d 0\n", cls);
	fprintf(file, "1 %d %d 1\n", n+1, TOP);
	fprintf(file, "%d 0\n", cls+1);

	fclose(file);
	return 0;
}
