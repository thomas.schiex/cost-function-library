
Introduction
============

The file gcc.tar.gz contains an implementation of the
algorithm for bounds consistency propagation of the generalized
cardinality constraint (gcc) presented in:

	C.-G. Quimper, P. van Beek, A. Lopez-Ortiz, A. Golynski, and
	S.B. Sadjad. An efficient bounds consistency algorithm for the
	global cardinality constraint. CP-2003.

Also included are some example benchmark and random problems that use
the gcc. All of the code relies on the ILOG Solver Library.

	Peter van Beek
	vanbeek@uwaterloo.ca


Directory Contents
==================

./README.txt			  -- This file.

Bounds consistency propagator for the gcc:

./Propagator/gcc.cpp
./Propagator/gcc.h

Example uses of the propagator:

./golomb.cpp			  -- Golomb rulers
./pathological.cpp		  -- Pathological problems
./random.cpp		  	  -- Random problems
./holes.cpp			  -- Random problems with holes in the domains

./CarSequencing/carseq.cpp	  -- Car sequencing problems
./CarSequencing/Data/gen.c	  -- Random generator for car sequencing problems
./CarSequencing/Data/carseq_10
./CarSequencing/Data/carseq_15
./CarSequencing/Data/carseq_20
./CarSequencing/Data/carseq_25
./CarSequencing/Data/carseq_30
./Pathological/pathological.cpp	  -- Pathological problems
./Sport/sport.cpp		  -- Sport league scheduling problems
./Random/random1.cpp		  -- Random problems
./Random/random3.cpp		  -- Random problems
./Random/holes.cpp		  -- Random problems with holes in the domains

