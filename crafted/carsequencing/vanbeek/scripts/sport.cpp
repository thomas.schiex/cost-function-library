/*
 *  Sport league scheduling problem
 */

#include <ilsolver/ilcint.h>
#include "../Propagator/gcc.h"


IlcIntTupleSet
getTupleSet( IlcManager m, IlcInt n )
{
  IlcInt         i, j, k;
  IlcIntTupleSet set(m, 3);

  k = 0;
  for( i = 0;   i < n-1; i++ )
  for( j = i+1; j < n;   j++ ) {
    IlcIntArray value( m, 3 );
    value[0] = i;
    value[1] = j;
    value[2] = k;
    set.add( value );
    k++;
  }
  set.close();
  return( set );
}


IlcInt
main( int argc, char** argv )
{
  IlcManager m(IlcEdit);
  IlcInt     i, j, k, count, n, flag;

  IlcInt   currfail=m.getNumberOfFails();
  IlcFloat currtime=m.getTime();

  if( argc != 3 ) {
    m.out() << "Usage: sport n flag" << endl;
    exit( 0 );
  }

  n    = atoi( argv[1] );
  flag = atoi( argv[2] );

  if( n != (2*(n/2)) ) {
    m.out() << "n must be even" << endl;
    exit( 0 );
  }

/*
  if( (flag < 0) || (flag > 3) ) {
    m.out() << "Invalid flag (0,1,2,3)" << endl;
    exit( 0 );
  }
*/

  m.setTimeLimit( 600 );

  IlcIntVarArray* Schedule = new (m.getHeap()) IlcIntVarArray[n/2];

  for( i = 0; i < n/2; i++ ) {
    Schedule[i] = IlcIntVarArray( m, 2*n, 0, n-1 );
  }

  /*
   *  Break symmetry.
   */
  for( i = 0; i < n/2; i++ ) {
    for( j = 0; j < 2*n; j += 2 ) {
      m.add( Schedule[i][j] < Schedule[i][j+1] );
    }
  }

  /*
   *  No team plays more than twice in the same period.
   */
  int* minOccurrences = new int[n];
  int* maxOccurrences = new int[n];
  for( j = 0; j < n; j++ ) {
    minOccurrences[j] = 2;
    maxOccurrences[j] = 2;
  }

  for( i = 0; i < n/2; i++ ) {
    IlcIntVarArray gamesInPeriod( m, n, 2, 2 );
    switch( flag ) {
    case 0:
      m.add( IlcDistribute( gamesInPeriod, Schedule[i], IlcBasic ) );
      break;
    case 1:
      m.add( IlcDistribute( gamesInPeriod, Schedule[i], IlcExtended ) );
      break;
    case 2:
      m.add( IlcNewGCC( Schedule[i], WithOutValueRemoval, 0, n-1,
			    minOccurrences, maxOccurrences ) );
      break;
    case 3:
      m.add( IlcNewGCC( Schedule[i], WithValueRemoval, 0, n-1,
			    minOccurrences, maxOccurrences ) );
      break;
    case 4:
      m.add( IlcDistribute( gamesInPeriod, Schedule[i], IlcBasic ) );
      m.add( IlcNewGCC( Schedule[i], WithOutValueRemoval, 0, n-1,
			    minOccurrences, maxOccurrences ) );
      break;
    }
  }

  /*
   *  Every team plays exactly one game per week.
   */
  for( j = 0; j < 2*n; j += 2 ) {
    IlcIntVarArray col( m, n, 0, n-1 );
    k = 0;
    for( i = 0; i < n/2; i++ ) {
      col[k] = Schedule[i][j];
      k++;
      col[k] = Schedule[i][j+1];
      k++;
    }
    m.add( IlcAllDiff( col, IlcWhenDomain ) );
  }

  IlcIntVarArray matches( m, (n*(n-1))/2, 0, (n*(n-1))/2 - 1 );

  IlcIntTupleSet s = getTupleSet( m, n );

  k = 0;
  for( i = 0; i < n/2; i++ ) {
    for( j = 0; j < 2*(n-1); j += 2 ) {
      IlcIntVarArray vars(m,3);
      vars[0] = Schedule[i][j];
      vars[1] = Schedule[i][j+1];
      vars[2] = matches[k];
      m.add( IlcTableConstraint( vars, s, IlcTrue ) );
      k++;
    }
  }

  m.add( IlcAllDiff( matches, IlcWhenDomain ) );
  m.add( IlcGenerate( matches, IlcChooseMinSizeInt ) );

  m.out() << "n = " << n << "  ";

  count = 0;
  if( m.nextSolution() ) {
    count++;
/*
    for( i = 0; i < n/2; i++ ) {
      for( j = 0; j < 2*(n-1); j += 2 ) {
        m.out() << "  "   << Schedule[i][j].getValue()
                << " vs " << Schedule[i][j+1].getValue();
      }
      m.out() << endl;
    }
    m.out() << endl;
*/
  }

  m.out() << "fails:    " << (m.getNumberOfFails() - currfail) << "  ";
  m.out() << "CPU time: " << (m.getTime() - currtime)          << endl;

  m.end();
  return( 0 );
}

