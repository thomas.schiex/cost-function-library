/******************************************************************************
 File: alldiff.cpp

 Implementation of the algorithm for bounds consistency of the
 generalized cardinality constraint described in:

	C.-G. Quimper, P. van Beek, A. Lopez-Ortiz, A. Golynski, and
	S.B. Sadjad. An efficient bounds consistency algorithm for the
	global cardinality constraint. CP-2003.

 By: Claude-Guy Quimper
 ******************************************************************************/

#include <stdlib.h>
#include <ilsolver/ilcint.h>

#include "gcc.h"


const int INCONSISTENT = 0;
const int CHANGES = 1;
const int NO_CHANGES = 2;


IlcNewGCCI::IlcNewGCCI(
  IlcManager m,
  IlcIntVarArray vars,
  PropType prop,
  int firstDomainValue, 
  int lastDomainValue,
  int* minOccurrences,
  int* maxOccurrences )
  : IlcConstraintI( m ),
    _vars( vars ),
    _prop( prop )
{
  int i, range;

  n = vars.getSize();
  range = lastDomainValue - firstDomainValue + 1;

  currentLevel.setValue( m, 1 );
  lastLevel = -1;

  if( _prop == WithValueRemoval ) {
    _maxOccur = new (m.getHeap()) IlcRevInt[range];
    for( i = 0; i < range; i++ ) {
      _maxOccur[i].setValue( _vars.getManager(), maxOccurrences[i] );
    }
  }

  iv        = (interval  *)calloc(n, sizeof(interval  ));
  minsorted = (interval **)calloc(n, sizeof(interval *));
  maxsorted = (interval **)calloc(n, sizeof(interval *));
  bounds    = (int *)calloc(2*n+2, sizeof(int));

  for( i = 0; i < n; i++ ) {
    minsorted[i] = maxsorted[i] = &iv[i];
  }

  t = (int *)calloc(2*n+2, sizeof(int));
  d = (int *)calloc(2*n+2, sizeof(int));
  h = (int *)calloc(2*n+2, sizeof(int));

  stableInterval      = (int *)calloc(2*n+2, sizeof(int));
  potentialStableSets = (int *)calloc(2*n+2, sizeof(int));
  newMin              = (int *)calloc(  n,   sizeof(int));

  l = initializePartialSum(firstDomainValue, range, minOccurrences);
  u = initializePartialSum(firstDomainValue, range, maxOccurrences);
}


IlcNewGCCI::~IlcNewGCCI()
{
  free(bounds);
  free(maxsorted);
  free(minsorted);
  free(iv);
  free(h);
  free(d);
  free(t);
  free(newMin);
  free(potentialStableSets);
  free(stableInterval);
  destroyPartialSum(u);
  destroyPartialSum(l);
}


ILCDEMON1(propValue, IlcNewGCCI*, ct)
{
  ct->propagateValue();
}


void
IlcNewGCCI::post()
{
  if( _prop == WithValueRemoval ) {
    _vars.whenValue(propValue(getManager(),this));
  }
  _vars.whenRangeInterval(this);
}


void
IlcNewGCCI::propagateValue()
{
  IlcInt i, j, v, cap;

  i   = _vars.getIndexValue();
  v   = _vars[i].getValue();
  cap = _maxOccur[v].getValue() - 1;
  _maxOccur[v].setValue( _vars.getManager(), cap );

  if( cap == 0 ) {
    for( j = 0; j < n; j++ ) {
      if( !_vars[j].isBound() ) {
        _vars[j].removeValue( v );
      }
    }
  }
}


void
sortmin( interval *v[], int n )
{
  int i, current;
  bool sorted;
  interval *t;

  current = n-1;
  sorted = false;
  while( !sorted ) {
    sorted = true;
    for( i = 0; i < current; i++ ) {
      if( v[i]->min > v[i+1]->min ) {
        t = v[i];
        v[i] = v[i+1];
        v[i+1] = t;
        sorted = false;
      }
    }
    current--;
  }
}

void
sortmax( interval *v[], int n )
{
  int i, current;
  bool sorted;
  interval *t;

  current = 0;
  sorted = false;
  while( !sorted ) {
    sorted = true;
    for( i = n-1; i > current; i-- ) {
      if( v[i]->max < v[i-1]->max ) {
        t = v[i];
        v[i] = v[i-1];
        v[i-1] = t;
        sorted = false;
      }
    }
    current++;
  }
}

void
IlcNewGCCI::sortit()
{
  int i,j,nb,min,max,last;

  sortmin(minsorted, n);
  sortmax(maxsorted, n);

  min = minsorted[0]->min;
  max = maxsorted[0]->max + 1;
  //MODIFIED: bounds[0] = last = min-2;
  bounds[0] = last = l->firstValue + 1;

  for (i=j=nb=0;;) { // merge minsorted[] and maxsorted[] into bounds[]
    if (i<n && min<=max) {	// make sure minsorted exhausted first
      if (min != last)
        bounds[++nb] = last = min;
      minsorted[i]->minrank = nb;
      if (++i < n)
        min = minsorted[i]->min;
    } else {
      if (max != last)
         bounds[++nb] = last = max;
      maxsorted[j]->maxrank = nb;
      if (++j == n) break;
      max = maxsorted[j]->max + 1;
    }
  }
  IlcNewGCCI::nb = nb;
  //MODIFIED: bounds[nb+1] = bounds[nb] + 2;
  bounds[nb+1] = u->lastValue + 1;
}


void
pathset(int *t, int start, int end, int to)
{
  int k, l;
  for (l=start; (k=l) != end; t[k]=to) {
    l = t[k];
  }
}

int
pathmin(int *t, int i)
{
  for (; t[i] < i; i=t[i]) {
    ;
  }
  return i;
}

int
pathmax(int *t, int i)
{
  for (; t[i] > i; i=t[i]) {
    ;
  }
  return i;
}

/* 
 * Shrink the lower bounds for the max occurrences problem.
 */
int
IlcNewGCCI::filterLowerMax()
{
  int i,j,w,x,y,z;
  int changes = 0;

  for (i=1; i<=nb+1; i++) {
    t[i] = h[i] = i-1;
    d[i] = sum(u, bounds[i-1], bounds[i]-1);
  }
  for (i=0; i<n; i++) { // visit intervals in increasing max order
    // get interval bounds
    x = maxsorted[i]->minrank; y = maxsorted[i]->maxrank;
    j = t[z = pathmax(t, x+1)];
    if (--d[z] == 0) {
      t[z = pathmax(t, t[z]=z+1)] = j;
    }
    pathset(t, x+1, z, z);
    if (d[z] < sum(u, bounds[y], bounds[z] - 1)) {
      return INCONSISTENT; // no solution
    }
    if (h[x] > x) {
      maxsorted[i]->min = bounds[w = pathmax(h, h[x])];
      pathset(h, x, w, w);
      changes = 1;
    }
    if (d[z] == sum(u, bounds[y], bounds[z] - 1)) {
      pathset(h, h[y], j-1, y); // mark hall interval
      h[y] = j-1; //("hall interval [%d,%d)\n",bounds[j],bounds[y]);
    }
  }
  if( changes )
    return CHANGES;
  else
    return NO_CHANGES;
}

/*
 * Shrink the upper bounds for the max occurrences problem.
 */
int
IlcNewGCCI::filterUpperMax()
{
  // Assertion: filterLowerMax returns true
  int i,j,w,x,y,z;
  int changes = 0;

  for (i=0; i<=nb; i++) {
    d[i] = sum(u, bounds[i], bounds[t[i]=h[i]=i+1]-1);
  }
  for (i=n; --i>=0; ) { // visit intervals in decreasing min order
    // get interval bounds
    x = minsorted[i]->maxrank; y = minsorted[i]->minrank;
    j = t[z = pathmin(t, x-1)];
    if (--d[z] == 0) {
      t[z = pathmin(t, t[z]=z-1)] = j;
    }
    pathset(t, x-1, z, z);
    if (d[z] < sum(u, bounds[z], bounds[y]-1)) {
      return INCONSISTENT; // no solution
    }
    if (h[x] < x) {
      minsorted[i]->max = bounds[w = pathmin(h, h[x])] - 1;
      pathset(h, x, w, w);
      changes = 1;
    }
    if (d[z] == sum(u, bounds[z], bounds[y]-1)) {
      pathset(h, h[y], j+1, y);
      h[y] = j+1;
    }
  }
  if( changes )
    return CHANGES;
  else
    return NO_CHANGES;
}


/*
 * Shrink the lower bounds for the min occurrences problem.
 * called as: filterLowerMin(t, d, h, stableInterval, potentialStableSets, newMin);
 */
int
IlcNewGCCI::filterLowerMin(int *tl, int *c,
		int* stableAndUnstableSets,
		int* stableInterval,
		int* potentialStableSets,
		int* newMin)
{
  int i,j,w,x,y,z,v;
  int changes = 0;

  for (w=i=nb+1; i>0; i--) {
    //c[i] = sum(l, bounds[potentialStableSets[i]=stableInterval[i]=i-1], bounds[i]-1);
    potentialStableSets[i]=stableInterval[i]=i-1;
    c[i] = sum(l, bounds[i-1], bounds[i]-1);
    // If the capacity between both bounds is zero, we have
    // an unstable set between these two bounds.
    if (c[i] == 0) {
      stableAndUnstableSets[i-1] = w;
    }
    else {
      w = stableAndUnstableSets[w] = i - 1;
    }
  }

  for (i = w = nb + 1; i >= 0; i--) {
    if (c[i] == 0)
      tl[i] = w;
    else
      w = tl[w] = i;
  }

  for (i = 0; i < n; i++) { // visit intervals in increasing max order
    // Get interval bounds
    x = maxsorted[i]->minrank; y = maxsorted[i]->maxrank;
    j = tl[z = pathmax(tl, x+1)];
    if (z != x+1) {
      // if bounds[z] - 1 belongs to a stable set,
      // [bounds[x], bounds[z]) is a sub set of this stable set
      v = potentialStableSets[w = pathmax(potentialStableSets, x + 1)];
      pathset(potentialStableSets, x+1, w, w); // path compression
      w = y < z ? y : z;
      pathset(potentialStableSets, potentialStableSets[w], v , w);
      potentialStableSets[w] = v;
    }

    if (c[z] <= sum(l, bounds[y], bounds[z] - 1)) {
      // (potentialStableSets[y], y] is a stable set
      w = pathmax(stableInterval, potentialStableSets[y]);
      pathset(stableInterval, potentialStableSets[y], w, w); // Path compression
      pathset(stableInterval, stableInterval[y], v=stableInterval[w], y);
      stableInterval[y] = v;
    }
    else {
      // Decrease the capacity between the two bounds
      if (--c[z] == 0) {
	tl[z = pathmax(tl, tl[z]=z+1)] = j;
      }

      // If the lower bound belongs to an unstable or a stable set,
      // remind the new value we might assigned to the lower bound
      // in case the variable doesn't belong to a stable set.
      if (stableAndUnstableSets[x] > x) {
	w = newMin[i] = pathmax(stableAndUnstableSets, x);
	pathset(stableAndUnstableSets, x, w, w); // path compression
      }
      else {
	newMin[i] = x; // Do not shrink the variable
      }

      // If an unstable set is discovered
      if (c[z] == sum(l, bounds[y], bounds[z] - 1)) {
 	if (stableAndUnstableSets[y] > y) // Consider stable and unstable sets beyong y
 	  y = stableAndUnstableSets[y]; // Equivalent to pathmax since the path is fully compressed
	pathset(stableAndUnstableSets, stableAndUnstableSets[y], j-1, y); // mark the new unstable set
	stableAndUnstableSets[y] = j-1;
      }
    }
    pathset(tl, x+1, z, z); // path compression
  }

  // If there is a failure set
  if (stableAndUnstableSets[nb] != 0) {
    return INCONSISTENT; // no solution
  }

  // Perform path compression over all elements in
  // the stable interval data structure. This data
  // structure will no longer be modified and will be
  // accessed n or 2n times. Therefore, we can afford
  // a linear time compression.
  for (i = nb+1; i > 0; i--) {
    if (stableInterval[i] > i)
      stableInterval[i] = w;
    else
      w = i;
  }

  // For all variables that are not a subset of a stable set, shrink the lower bound
  for (i=n-1; i>=0; i--) {
    x = maxsorted[i]->minrank; y = maxsorted[i]->maxrank;
    if ((stableInterval[x] <= x) || (y > stableInterval[x])) {
      maxsorted[i]->min = skipNonNullElementsRight(l, bounds[newMin[i]]);
      changes = 1;
    }
  }

  if( changes )
    return CHANGES;
  else
    return NO_CHANGES;
}


/*
 * Shrink the upper bounds for the min occurrences problem.
 * called as: filterUpperMin(t, d, h, stableInterval, newMin);
 */
int 
IlcNewGCCI::filterUpperMin(int *tl, int *c,
		int* stableAndUnstableSets,
		int* stableInterval,
		int* newMax)
{
  // ASSERTION: filterLowerMin returns true
  int i,j,w,x,y,z;
  int changes = 0;

  for (w=i=0; i<=nb; i++) {
    //    d[i] = bounds[t[i]=h[i]=i+1] - bounds[i];
    c[i] = sum(l, bounds[i], bounds[i+1]-1);
    if (c[i] == 0)
      tl[i]=w;
    else
      w=tl[w]=i;
  }
  tl[w]=i;
  for (i = 1, w = 0; i<=nb; i++) {
    if (c[i-1] == 0)
      stableAndUnstableSets[i] = w;
    else
      w = stableAndUnstableSets[w] = i;
  }
  stableAndUnstableSets[w] = i;

  for (i=n; --i>=0; ) { // visit intervals in decreasing min order
    // Get interval bounds
    x = minsorted[i]->maxrank; y = minsorted[i]->minrank;

    // Solve the lower bound problem
    j = tl[z = pathmin(tl, x-1)];

    // If the variable is not in a discovered stable set
    // Possible optimization: Use the array stableInterval to perform this test
    if (c[z] > sum(l, bounds[z], bounds[y]-1)) {
      if (--c[z] == 0) {
	tl[z = pathmin(tl, tl[z]=z-1)] = j;
      }
      if (stableAndUnstableSets[x] < x) {
	newMax[i] = w = pathmin(stableAndUnstableSets, stableAndUnstableSets[x]);
	pathset(stableAndUnstableSets, x, w, w); // path compression
      }
      else {
	newMax[i] = x;
      }
      if (c[z] == sum(l, bounds[z], bounds[y]-1)) {
	if (stableAndUnstableSets[y] < y) {
	  y = stableAndUnstableSets[y];
        }
	pathset(stableAndUnstableSets, stableAndUnstableSets[y], j+1, y);
	stableAndUnstableSets[y] = j+1;
      }
    }
    pathset(tl, x-1, z, z);
  }

  // For all variables that are not subsets of a stable set, shrink the lower bound
  for (i=n-1; i>=0; i--) {
    x = minsorted[i]->minrank; y = minsorted[i]->maxrank;
    if ((stableInterval[x] <= x) || (y > stableInterval[x]))
      minsorted[i]->max = skipNonNullElementsLeft(l, bounds[newMax[i]]-1);
      changes = 1;
  }

  if( changes )
    return CHANGES;
  else
    return NO_CHANGES;
}


void
IlcNewGCCI::propagate()
{
  int statusLower, statusUpper;
  int statusLowerMin, statusUpperMin;
  int statusLowerMax, statusUpperMax;
  int i, a, b;
  int dl, du;

  a = _vars.getRangeIndexMin();
  b = _vars.getRangeIndexMax();

/*
  if( _prop == WithValueRemoval && (a == (b-1)) && _vars[a].isBound() ) {
    return;
  }
*/

  currentLevel.setValue( _vars.getManager(), currentLevel.getValue() + 1 );

  if( lastLevel != (currentLevel.getValue()-1) ) {
    // not incremental
    statusLower = CHANGES;
    statusUpper = CHANGES;
    IlcIntVarArrayIterator iter(_vars);
    IlcIntVar v;
    i = 0;
    while (iter.next(v)) {
      iv[i].min = v.getMin();
      iv[i].max = v.getMax();
      i++;
    }
  }
  else {
    // incremental
    statusLower = NO_CHANGES;
    statusUpper = NO_CHANGES;
    for( i = a; i < b; i++ ) {
      dl = iv[i].min;
      du = iv[i].max;
      iv[i].min = _vars[i].getMin();
      iv[i].max = _vars[i].getMax();
      if( dl != iv[i].min ) statusLower = CHANGES;
      if( du != iv[i].max ) statusUpper = CHANGES;
    }
  }

  lastLevel = currentLevel.getValue();

  if( statusLower == NO_CHANGES && statusUpper == NO_CHANGES ) {
    return;
  }

  sortit();

  // The variable domains must be inside the domain defined by
  // the lower bounds (l) and the upper bounds (u).
  //assert(minValue(l) == minValue(u));
  //assert(maxValue(l) == maxValue(u));
  //assert(minValue(l) <= minsorted[0]->min);
  //assert(maxsorted[n-1]->max <= maxValue(u));

  // Checks if there are values that must be assigned before the
  // smallest interval or after the last interval. If this is
  // the case, there is no solution to the problem
  // This is not an optimization since
  // filterLower{Min,Max} and
  // filterUpper{Min,Max} do not check for this case.
  if ((sum(l, minValue(l), minsorted[0]->min - 1) > 0) ||
      (sum(l, maxsorted[n-1]->max + 1, maxValue(l)) > 0)) {
    _vars.getManager().fail();
  }

  statusLowerMax = filterLowerMax();
  if( statusLowerMax != INCONSISTENT ) {
    statusLowerMin = filterLowerMin(t, d, h,
			stableInterval, potentialStableSets, newMin);
  }

  if( (statusLowerMax == INCONSISTENT) || (statusLowerMin == INCONSISTENT) ) {
    _vars.getManager().fail();
  }
  else {

    statusUpperMax = filterUpperMax();
    statusUpperMin = filterUpperMin(t, d, h, stableInterval, newMin);

    if( (statusLowerMax == CHANGES) || (statusLowerMin == CHANGES) ||
        (statusUpperMax == CHANGES) || (statusUpperMin == CHANGES) ) {
      IlcIntVarArrayIterator iter(_vars);
      IlcIntVar v;
      i = 0;
      while (iter.next(v)) {
        v.setRange( iv[i].min, iv[i].max );
        i++;
      }
    } // if
  } // else
}

// Create a partial sum data structure adapted to the
// filterLower{Min,Max} and filterUpper{Min,Max} functions.
// Two elements before and after the element list will be added
// with a weight of 1. 
partialSum*
IlcNewGCCI::initializePartialSum(int firstValue, int count, int* elements)
{
  int i,j;
  partialSum* res = new partialSum;
  res->sum = new int[count+1+2+2];
  res->firstValue = firstValue - 3; // We add three elements at the beginning
  res->lastValue = firstValue + count + 1;
  res->sum[0] = 0;
  res->sum[1] = 1;
  res->sum[2] = 2;
  for (i = 2; i < count+2; i++) {
    res->sum[i+1] = res->sum[i] + elements[i-2];
  }
  res->sum[i+1] = res->sum[i] + 1;
  res->sum[i+2] = res->sum[i+1] + 1;

  res->ds = new int[count+1+2+2];

  for (j=(i=count+3)+1; i > 0;) {
    while (res->sum[i] == res->sum[i-1])
      res->ds[i--]=j;
    j=res->ds[j]=i--;
  }
  res->ds[j]=0;
  return res;
}

void
IlcNewGCCI::destroyPartialSum(partialSum *p)
{
  delete p->ds;
  delete p->sum;
  delete p;
}

int
IlcNewGCCI::sum(partialSum *p, int from, int to)
{
  if (from <= to) {
    //assert((p->firstValue <= from) && (to <= p->lastValue));
    return p->sum[to - p->firstValue] - p->sum[from - p->firstValue - 1];
  }
  else {
    //assert((p->firstValue <= to) && (from <= p->lastValue));
    return p->sum[to - p->firstValue - 1] - p->sum[from - p->firstValue];
  }
}

int
IlcNewGCCI::minValue(partialSum *p) {
  return p->firstValue + 3;
}

int
IlcNewGCCI::maxValue(partialSum *p) {
  return p->lastValue - 2;
}

int
IlcNewGCCI::skipNonNullElementsRight(partialSum *p, int value) {
  value -= p->firstValue;
  return (p->ds[value] < value ? value : p->ds[value]) + p->firstValue;
}

int
IlcNewGCCI::skipNonNullElementsLeft(partialSum *p, int value) {
  value -= p->firstValue;
  return (p->ds[value] > value ? p->ds[p->ds[value]] : value) + p->firstValue;
}


IlcConstraint
IlcNewGCC( IlcIntVarArray vars,
	   PropType prop, 
	   int firstDomainValue,
	   int lastDomainValue, 
	   int* minOccurrences,
	   int* maxOccurrences )
{
  IlcManager m = vars.getManager();
  return new (m.getHeap())
	IlcNewGCCI( m,
	   vars,
	   prop,
	   firstDomainValue,
	   lastDomainValue,
	   minOccurrences,
	   maxOccurrences );
}

/*
 *  End of user defined propagator for enforcing bounds consistency
 *=================================================================*/
