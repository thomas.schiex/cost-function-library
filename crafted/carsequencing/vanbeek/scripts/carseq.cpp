
#include <ilsolver/ilcint.h>
#include <fstream.h>
#include <string.h>
#include "../Propagator/gcc.h"

IlcIntArray*
readData( IlcManager m,
	  char* example,
	  IlcInt& nCars,
	  IlcInt& nOptions,
	  IlcInt& nConfigs,
	  IlcIntArray& seqMax,
	  IlcIntArray& seqWidth,
	  IlcIntArray& nCarsPerConfig )
{
  IlcInt i;
  char   globalName[50];

  sprintf(globalName, "Data/%s", example);
  ifstream fin(globalName,ios::in);
  if( !fin ) {
    m.out() << "problem with file:" << globalName << endl;
    exit( 6 );
  }

  // first line of data
  fin >> nCars >> nOptions >> nConfigs;
  cout << nCars << " " << nOptions << " " << nConfigs << endl;

  // second line of data
  seqMax = IlcIntArray(m, nOptions);
  for( i = 0; i < nOptions; i++ ) {
    fin >> seqMax[i];
  }

  // third line of data
  seqWidth = IlcIntArray(m, nOptions);
  for( i = 0; i < nOptions; i++ ) {
    fin >> seqWidth[i];
  }

  IlcIntArray* confsByOption=new (m.getHeap()) IlcIntArray[nConfigs];
  for (i=0;i<nConfigs;i++){
    confsByOption[i] = IlcIntArray(m,nOptions);
  }
  IlcIntArray nbConfsByOption(m,nOptions);
  for (i=0;i<nOptions;i++){
    nbConfsByOption[i]=0;
  }

  // read the options required by each configuration
  nCarsPerConfig=IlcIntArray(m,nConfigs);
  IlcInt dummy;
  IlcInt j;
  for(i=0;i<nConfigs;i++){
    fin >> dummy;
    fin >> nCarsPerConfig[i];
    for (j=0;j<nOptions;j++){
      fin >> confsByOption[i][j];
      if (confsByOption[i][j] == 1){
	nbConfsByOption[j]++;
      }
    }
  }

  // compute the configurations required by each option
  IlcIntArray* optConf=new (m.getHeap()) IlcIntArray[nOptions];
  for(i=0;i<nOptions;i++){
    optConf[i]=IlcIntArray(m,nbConfsByOption[i]);
  }
  IlcIntArray ind(m,nOptions);
  for(i=0;i<nOptions;i++){
    ind[i]=0;
  }
  for(i=0;i<nConfigs;i++){
    for (j=0;j<nOptions;j++){
      if (confsByOption[i][j] == 1){
	optConf[j][ind[j]]=i;
	ind[j]++;
      }
    }
  }

/*
  m.out() << "number of cars: " << nCars << endl;
  m.out() << "number of options: " << nOptions << endl;
  m.out() << "number of configurations: " << nConfigs << endl;

  m.out() << "number of times each configuration is required:" << endl;
  for(i=0;i<nConfigs;i++){
    m.out() << nCarsPerConfig[i] << " ";
  }
  m.out() << endl;
  m.out() << "configuration required by each option:" << endl;
  for(i=0;i<nOptions;i++){
    m.out() << "option: " << i << " ";
    for (j=0;j<optConf[i].getSize();j++){
      m.out() << optConf[i][j] << " ";
    }
    m.out() << endl;
  }
  m.out() << "number of times each option is required:" << endl;
  for(i=0;i<nOptions;i++){
    IlcInt cpt=0;
    for(j=0;j<optConf[i].getSize();j++){
      cpt += nCarsPerConfig[optConf[i][j]];
    }
    m.out() << "option: " << i << " " << cpt << " x ";
    m.out() << seqMax[i] << "/" << seqWidth[i] << endl;
  }
*/

  return optConf;
}

IlcConstraint
IlcCarSequencing( IlcManager m,
		  IlcInt flag,
		  IlcInt nConfigs,
		  IlcInt maxCar,
		  IlcIntArray maxConfs,
		  IlcIntArray config,
		  IlcIntVarArray carsseq )
{
  IlcInt i;
  const IlcInt valMax=carsseq.getMaxMax();
  IlcIntVarArray nvars=IlcAbstraction(carsseq,config,valMax+1);
  const IlcInt size=config.getSize();

  IlcIntArray nval(m, size+1);
  IlcIntVarArray ncard(m, size+1);
  for(i=0;i<size;i++){
    nval[i+1] = config[i];
    if( maxConfs[i] > maxCar ) {
      ncard[i+1] = IlcIntVar( m, 0, maxCar );
    }
    else {
      ncard[i+1] = IlcIntVar( m, 0, maxConfs[i] );
    }
  }
  nval[OL]=valMax+1;
  ncard[OL]=IlcIntVar(m, carsseq.getSize() - maxCar, carsseq.getSize());

  int* minOccurrences = new int[nConfigs+1];
  int* maxOccurrences = new int[nConfigs+1];
  for( i = 0; i <= nConfigs; i++ ) {
    minOccurrences[i] = 0;
    maxOccurrences[i] = 99999;
  }
//printf("size = %d\n", nConfigs+1);
  for( i = 0; i < size+1; i++ ) {
    minOccurrences[nval[i]] = ncard[i].getMin();
    maxOccurrences[nval[i]] = ncard[i].getMax();
//printf("[%d, %d]\n", minOccurrences[nval[i]], maxOccurrences[nval[i]]);
  }

  switch(flag) {
  case 0:
    return( IlcDistribute( ncard, nval, nvars, IlcBasic ) );
  case 1:
    return( IlcDistribute( ncard, nval, nvars, IlcExtended ) );
  case 2:
    return( IlcNewGCC( nvars, WithOutValueRemoval, 0, nConfigs,
			      minOccurrences, maxOccurrences ) );
  case 3:
    return( IlcNewGCC( nvars, WithValueRemoval, 0, nConfigs,
			      minOccurrences, maxOccurrences ) );
  }

}

void
firstModel( char* example, IlcInt flag )
{
  IlcInt i, j, size;
  IlcManager m(IlcEdit);

  m.setTimeLimit( 7200 );

  IlcInt nCars;
  IlcInt nOptions;
  IlcInt nConfigs;
  IlcIntArray seqMax;
  IlcIntArray seqWidth;
  IlcIntArray nCarsPerConfig;

  IlcInt   currfail=m.getNumberOfFails();
  IlcFloat currtime=m.getTime();

  IlcIntArray* optConf = readData( m,
				   example,
				   nCars,
				   nOptions,
				   nConfigs,
				   seqMax,
				   seqWidth,
				   nCarsPerConfig );

  IlcIntVarArray cars( m, nCars, 0, nConfigs-1 );

  int* minOccurrences = new int[nConfigs];
  int* maxOccurrences = new int[nConfigs];
  IlcIntVarArray cards(m, nConfigs);
//printf("size = %d\n", nConfigs);
  for( i = 0; i < nConfigs; i++ ) {
    minOccurrences[i] = nCarsPerConfig[i];
    maxOccurrences[i] = nCarsPerConfig[i];
//printf("[%d, %d]\n", minOccurrences[i], maxOccurrences[i]);
    cards[i] = IlcIntVar( m, nCarsPerConfig[i], nCarsPerConfig[i] );
  }

  switch(flag) {
  case 0:
    m.add( IlcDistribute( cards, cars, IlcBasic ) );
    break;
  case 1:
    m.add( IlcDistribute( cards, cars, IlcExtended ) );
    break;
  case 2:
    m.add( IlcNewGCC( cars, WithOutValueRemoval, 0, nConfigs-1,
			minOccurrences, maxOccurrences ) );
    break;
  case 3:
    m.add( IlcNewGCC( cars, WithValueRemoval, 0, nConfigs-1,
			minOccurrences, maxOccurrences ) );
    break;
  }

  IlcIntArray * optCard = new (m.getHeap()) IlcIntArray[nOptions];
  for( i = 0; i < nOptions; i++ ) {
    size = optConf[i].getSize();
    optCard[i] = IlcIntArray(m, size);
    for( j = 0; j < size; j++ ) {
      optCard[i][j] = nCarsPerConfig[optConf[i][j]];
    }
  }

  for (IlcInt opt=0; opt < nOptions; opt++) {
    for( i = 0; i < nCars-seqWidth[opt]+1; i++ ) {
      IlcIntVarArray carsseq=cars.getImpl()->extract(i,seqWidth[opt]);
      m.add( IlcCarSequencing( m,
			       flag,
			       nConfigs,
			       seqMax[opt],
			       optCard[opt],
			       optConf[opt],
			       carsseq ) );
    }
  }

  m.add( IlcGenerate( cars, IlcChooseMinSizeInt ) );
  if( m.nextSolution() ) m.out() << "cars = " << cars << endl;
  else                   m.out() << "no solution" << endl;

  m.out() << "fails: " << (m.getNumberOfFails() - currfail) << "  ";
  m.out() << "CPU time: " << (m.getTime() - currtime)          << endl;

  m.end();
}


int
main( int argc, char** argv )
{
  int flag;

  if( argc != 3 ) {
    cout << "Usage: carseq flag example" << endl;
    exit( 0 );
  }

  flag = atoi( argv[1] );

  firstModel( argv[2], flag );

  return( 0 );
}

