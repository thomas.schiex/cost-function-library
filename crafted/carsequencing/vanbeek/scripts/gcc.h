
/*============================================================
 *  User defined propagator for enforcing bounds consistency
 *  on the restricted gcc constraint when bounds on
 *  occurrences are [a_i,b_i].
 */

typedef struct {
  int min, max;		// start, end of interval
  int minrank, maxrank; // rank of min & max in bounds[]
} interval;

typedef struct {
  int firstValue;
  int lastValue;
  int* sum;
  int* ds;
} partialSum;

enum PropType { WithOutValueRemoval, WithValueRemoval };

class IlcNewGCCI : public IlcConstraintI
{
  public:
    // A value "v" must be assigned to at least
    // minOccurrences[v - firstDomainValue] variables and at most
    // maxOccurrences[v - firstDomainValue] variables
    IlcNewGCCI( IlcManager m,
		IlcIntVarArray vars,
		PropType prop,
		int firstDomainValue,
		int lastDomainValue,
		int* minOccurrences,
		int* maxOccurrences);
    ~IlcNewGCCI();
    void post();
    void propagate();
    void propagateValue();
  private:
    IlcIntVarArray _vars;
    IlcRevInt currentLevel;
    int lastLevel;
    PropType _prop;
    IlcRevInt *_maxOccur;
    int n;
    int *t;			// tree links
    int *d;			// diffs between critical capacities
    int *h;			// hall interval links
    int *stableInterval;	// stable sets
    int *potentialStableSets;	// links elements that potentialy belong to same stable set
    int *newMin;
    interval *iv;
    interval **minsorted;
    interval **maxsorted;
    int *bounds;  // bounds[1..nb] hold set of min & max of the n intervals
                  // while bounds[0] and bounds[nb+1] allow sentinels
    int nb;

    partialSum* l; 
    partialSum* u;
    partialSum* initializePartialSum(int firstValue, int count, int* elements);
    void destroyPartialSum(partialSum *p);
    int  sum(partialSum *p, int from, int to);
    int  searchValue(partialSum *p, int value);
    int  minValue(partialSum *p);
    int  maxValue(partialSum *p);
    int  skipNonNullElementsRight(partialSum *p, int value);
    int  skipNonNullElementsLeft(partialSum *p, int value);

    void sortit();
    int  filterLowerMax();
    int  filterUpperMax();
    int  filterLowerMin(int *tl, int *c,
		int* stableAndUnstableSets,
		int* stableInterval,
		int* potentialStableSets,
		int* newMin);
    int  filterUpperMin(int *tl, int *c,
		int* stableAndUnstableSets,
		int* stableInterval,
		int* newMax);
};

IlcConstraint IlcNewGCC(
		IlcIntVarArray vars,
		PropType prop,
		int firstDomainValue,
		int lastDomainValue,
		int* minOccurrences,
		int* maxOccurrences );

