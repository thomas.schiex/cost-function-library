BEGIN { RS = "@"; } # FS = "[ \t\n]+"; }

{ n = $1;
  l = $2;
  m = $3;
  pos = 4;
  top = 1;
  shift = 0;
  for (i=1; i<=m; i++) {
    cap[i] = $pos;
    pos += 1;
  }
  for (i=1; i<=n; i++) {
    pos += 1;
    maxp = -1000000;
    minp = 1000000;
    for (u=1; u<=l; u++) {
      if (-$pos > maxp) maxp = -int($pos);
      if (-$pos < minp) minp = -int($pos);
      profit[i,u] = -int($pos);
      pos += 1;
      for (j=1; j<=m; j++) {
        weight[i,u,j] = $pos;
        pos += 1;
      }
    }
    for (u=1; u<=l; u++) {
      profit[i,u] -= minp;
    }
    top += maxp - minp;
    shift += minp;
  }
}

END {
  print "mmkp" shift,n,l,m+n,top;
  for (i=1; i<=n; i++) {
    printf " " l;
  }
  print "";
  for (i=1; i<=n; i++) {
    print 1 " " i-1 " " 0 " " l;
    for (u=1; u<=l; u++) {
      print u-1 " " profit[i,u];
    }
  }
  
  for (j=1; j<=m; j++) {
    printf n ;
    for (i=1; i<=n; i++) {
      printf " " i-1;
    }
    printf " -1 knapsackp %d", -cap[j];
    for (i=1; i<=n; i++) {
      printf " " l;
      for (u=1; u<=l; u++) {
        printf " %d %d", u-1, -weight[i,u,j];
      }
    }
    print "";
  }
}

