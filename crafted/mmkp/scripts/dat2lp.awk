BEGIN { RS = "@"; } # FS = "[ \t\n]+"; }

{ n = $1;
  l = $2;
  m = $3;
  pos = 4;
  for (i=1; i<=m; i++) {
    cap[i] = $pos;
    pos += 1;
  }
  for (i=1; i<=n; i++) {
    pos += 1; 
    for (u=1; u<=l; u++) {
      profit[i,u] = $pos;
      pos += 1;
      for (j=1; j<=m; j++) {
        weight[i,u,j] = $pos;
        pos += 1;
      }
    }
  }
}

END {
  printf "Max";
  for (i=1; i<=n; i++) {
    for (u=1; u<=l; u++) {
      if ((i>1 || u>1) && profit[i,u,j] >= 0) printf " +";
      printf " " int(profit[i,u]) " x" (i-1)*l+(u-1);
    }
  }
  print "";
  print "Subject To";
  for (j=1; j<=m; j++) {
    for (i=1; i<=n; i++) {
      for (u=1; u<=l; u++) {
        if ((i>1 || u>1) && weight[i,u,j] >= 0) printf " +";
        printf " " weight[i,u,j] " x" (i-1)*l+(u-1);
      }
    }
    print " <= " cap[j];
  }
  
  for (i=1; i<=n; i++) {
    for (u=1; u<=l; u++) { 
      for (v=u+1; v<=l; v++) {
        print "x" (i-1)*l+(u-1) " + x" (i-1)*l+(v-1) " <= 1";
      }
    }
  }
  
  print "Binary";
  for (i=1; i<=n; i++) {
    for (u=1; u<=l; u++) {
      printf " x" (i-1)*l+(u-1);
    }
  }
  print "";
  print "End";
}

