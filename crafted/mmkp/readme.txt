Multiple-choice Multidimensional Knapsack Problem

Instances I01-I13 and INST01-INST20 are classical benchmark instances which can be downloaded from 

ftp://cermsem.univ-paris1.fr/pub/CERMSEM/hifi/OR-Benchmark.html

Khan, S., Li, K. F., Manning, E. G., & Akbar, M. M. (2002). Solving the knapsack problem for adaptive multimedia systems. Stud. Inform. Univ., 2(1), 157-178.

M. Hifi, M. Michrafy, and A. Sbihi (2006) A reactive local search-based algorithm for the multiple-choice multi-dimensional knapsack problem, Comput. Optim. Appl. 33(2–3): 271–285.

https://leria-info.univ-angers.fr/~jinkao.hao/mmkp.html

Yuning Chen and Jin-Kao Hao. "A "reduce and solve" approach for the multiple-choice multidimensional knapsack problem". European Journal of Operational Research. 239(2): 313-322, 2014.

Warning! Solves opb instances using -precision=0 or -precision=1

Warning!! wcsp instances are in minimization and their optimum should be shifted by a constant given in the name of the problem (see first line first field in the problem file)

