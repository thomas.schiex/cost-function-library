#!/bin/tcsh
# usage:
# qpbo2cfn.sh G1.xz G1.qpbo G1.cfn

set M = `xzcat $1 | wc -l`
@ M -= 1
set N = `xzgrep '[-]1' $1 | wc -l`
toulbar2 -qpmult=0.5 $2 -precision=1 -z=3 -k=0 -nopre -z=problem.cfn
sed -E 's/<[0-9]+/>0/' problem.cfn | sed -E 's/^0,0,1/0,0,-1/' | sed -E 's/^0,1,1/0,1,-1/' | sed -E 's/^1,0,1/1,0,-1/' | sed -E 's/^1,1,1/1,1,-1/' | awk '/"F":/{gsub("[[]","[ ",$0);gsub("[]]"," ]",$0); $3 = -$3 + '${M}'/2 - '${N}'} {print $0}' > $3
rm -f problem.cfn

