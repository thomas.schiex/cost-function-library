71 DIMACS (weighted) max-cut instances (http://dimacs.rutgers.edu/programs/challenge)
downloaded from Yinyu Ye homepage (https://web.stanford.edu/~yyye/yyye/Gset)

Warning! the following instances have edge weights in {-1,1}
data/G10
data/G11
data/G12
data/G13
data/G18
data/G19
data/G20
data/G21
data/G27
data/G28
data/G29
data/G30
data/G31
data/G32
data/G33
data/G34
data/G39
data/G40
data/G41
data/G42
data/G56
data/G57
data/G59
data/G6
data/G61
data/G62
data/G64
data/G65
data/G66
data/G67
data/G7
data/G72
data/G77
data/G8
data/G81
data/G9
all the other instances have edge weights always equal to 1. 

Warning! qpbo files are in minimization and option -qpmult=0.5 must be used when solving them with toulbar2.
let O be the optimum obtained with toulbar2 on qpbo instances, P the number of edges with a positive weight (1), and N the number of edges with a negative weight (-1),
such that P + N = M the total number of edges,
then the original problem has optimum equal to -(O - P/2 + N/2) = -O + P - M/2.

Files in cfn format should have the correct optimum without requiring any transformation.
