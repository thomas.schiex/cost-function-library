Definition (extracted from http://www.informatik.uni-koeln.de/ls_juenger/research/spinglass/index.html):
-----------
A spin-glass instance in the prominent Ising model is given by n spins that can either point up or down. Spin i and j are coupled with coupling strengh Jij. We mainly consider diluted systems, e.g. d-dimensional systems in which spins are located on the sites of a d-dimensional lattice with nearest neighbor interactions being present. The Hamiltonian H is given by 

H = - SUM Jij Si Sj,

where the sum runs over all coupled spins. The couplings Jij  are often either chosen according to a gaussian distribution or are +1 or -1, with 50% negative interactions. A ground state is a spin configuration that attains the global minimum of the energy function H. In contrast to heuristic algorithms used by many physicists, we determine exact ground states by formulating the problem as a maximum cut problem in the graph of interactions.

Instances:
----------

All the instances were sent by Frauke Liers. We are very grateful to Frauke.
