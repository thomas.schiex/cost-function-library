# Submitter: Jirka Vomlel and Petr Savicky (Academy of Sciences of the Czech Republic)‏
# Domain: two-layer noisy-or Bayesian networks
# Type: Bayes for MAR/PRE

    * 18 instances
    * All variables binary
    * 45, 50, or 55 variables 

# Treewidth: ~24-27

* some exact PE solvers run out of memory (given 3GB) 
  
