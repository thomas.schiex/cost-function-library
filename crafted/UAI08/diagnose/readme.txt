# Submitter: John M. Agosta (Intel Corp.)‏
# Domain: diagnostic Bayesian networks, hand-built
# Type: Bayes for MAR

    * 2 Instances, each with 50 different sets of randomly generated evidence (leaf nodes only)‏
    * I 203 and 359 variables, respectively
    * Max. domain size 7 and 6, respectively
    * nodes assume causal independence (e.g., noisy-max)
    * relatively large for networks constructed by hand
          o ~200-300 nodes, ~300-600 edges 

# Treewidth ~11-18: still easy for exact solvers 
