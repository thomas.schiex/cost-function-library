Relational

    * Submitter: UCLA
    * Domain: Relational Bayesian networks constructed from the Primula tool
    * Type: Bayes for MAR/PRE
    * 251 networks, with binary variables
          o 150 Blockmap: 700 to 59,404 variables
          o 80 Mastermind: 1,220 to 3,692 variables

            11 Friends & Smoker: 10 to 76,212 variables
          o 10 Students: 376 variables 
    * Large networks with large treewidths, but with high levels of determinism
    *

      networks, list of networks used for exact PR/MAR, and a subset of networks used for the other tasks. 
