BEGIN {
	TRUE = 1;
	FALSE = 0;

	grammar = -1000;

	nb_extra_var = 0;

	grammar = -1000;
	nbrules = 0;
	costmultiplier = 0;
	nb_non_terminal = 0;
	nb_terminal = 0;
	start = -1;
	nb_encoding_non_terminal_expand  = 0;
	nb_encoding_terminal = 0;
	nb_expand_rule = 0;
	nb_terminal_rule = 0;
	output = "/tmp/grammar.wcsp";
	print "" > output;
}

FNR==1 {
	problem = $1;
	nb_var = $2;
	max_dom_size = $3;
	nb_constr = $4
	top = $5;
}

FNR==2 {
	domains = $0;
}

FNR >2 && !/sgrammar/ && (FNR<grammar || FNR>=grammar+4+nbrules)  {
	print $0 > output;
}

FNR==grammar+1 {
	costmultiplier = $2;
}

FNR==grammar+2 {
	nb_non_terminal = $1;
	nb_terminal = $2;
	start = $3;
}

FNR==grammar+3 {
	nbrules = $1;
}

FNR>=grammar+4 && FNR<grammar+4+nbrules {
	if ($1==1) {
		if (!($2 in encoding_non_terminal_expand)) {
			encoding_non_terminal_expand[$2] = 0+nb_encoding_non_terminal_expand;
			rev_encoding_non_terminal_expand[0+nb_encoding_non_terminal_expand] = $2;
			nb_encoding_non_terminal_expand++;
		}
		if (($3 "," $4) in expand_rule) expand_rule[$3 "," $4] = -1;
		else expand_rule[$3 "," $4] = $2;
		expand_ruleA[0+nb_expand_rule] = $2;
		expand_ruleB[0+nb_expand_rule] = $3;
		expand_ruleC[0+nb_expand_rule] = $4;
		nb_expand_rule++;
	} else {
		if (!($2 in encoding_terminal)) {
			encoding_terminal[$2] = 0+nb_encoding_terminal;
			rev_encoding_terminal[0+nb_encoding_terminal] = $2;
			nb_encoding_terminal++;
		}
		terminal_ruleA[0+nb_terminal_rule] = $2;
		terminal_ruleB[0+nb_terminal_rule] = $3;
		nb_terminal_rule++;
	}
}

FNR==grammar+4+nbrules-1 {
	nb_non_terminal = nb_encoding_non_terminal_expand;
	nb_terminal = nb_encoding_terminal;

	# create extra variables on the lower-left triangle of a nxn matrix (i column index and j row index)
	# with an order compatible with DAC
	for (j=1;j<=n;j++) {
		for (i=1;i<=n-j+1;i++) {
			var[i "," j] = nb_var+nb_extra_var;
			dom[i "," j] = ((j==1)?nb_terminal:((nb_non_terminal)*(j-1)+1));
			dom_extra_var[nb_extra_var] = dom[i "," j];
			if (dom[i "," j] > max_dom_size) {
				max_dom_size = dom[i "," j];
			}
			nb_extra_var++;
		}
	}
	# level 1 constraints
	for (i=1;i<=n;i++) {
		print 2,var[i "," 1],var[i],costmultiplier,nb_terminal_rule > output;
		nb_constr++;
		for (k=0;k<nb_terminal_rule;k++) {
			print encoding_terminal[terminal_ruleA[k]],terminal_ruleB[k],0 > output;
		}
	}
	# higher levels
	print 1,var[1 "," n],top,n-1 > output;
	nb_constr++;
	for (i=1;i<n;i++) {
		print nb_non_terminal*(i-1)+encoding_non_terminal_expand[start], 0 > output;
	}
	for (j=2;j<=n;j++) {
		for (i=1;i<=n-j+1;i++) {
			for (k=1;k<j;k++) {
				print 3,var[i "," j],var[i "," k],var[i+k "," j-k],0, dom[i "," j]*dom[i "," k]*dom[i+k "," j-k] > output;
				nb_constr++;
				for (a=0; a<dom[i "," j]; a++) {
					for (b=0; b<dom[i "," k]; b++) {
						B = dom[i "," k];
						if (k==1) B = rev_encoding_terminal[b];
						else if (b<dom[i "," k]-1) B = rev_encoding_non_terminal_expand[b % nb_non_terminal];
						for (c=0; c<dom[i+k "," j-k]; c++) {
							C = dom[i+k "," j-k]-1;
							if (j-k==1) C = rev_encoding_terminal[c];
							else if (c<dom[i+k "," j-k]-1) C = rev_encoding_non_terminal_expand[c % nb_non_terminal];
							if (a < nb_non_terminal*(k-1) || a >= nb_non_terminal*k) {
								if (a==dom[i "," j]-1 && (k==1 || b<dom[i "," k]-1) && (j-k==1 || c<dom[i+k "," j-k]-1) && ((B "," C) in expand_rule)) print a,b,c,top > output;
								else  print a,b,c,0 > output;
							} else if ((k==1 || b<dom[i "," k]-1) && (j-k==1 || c<dom[i+k "," j-k]-1) &&
									   ((B "," C) in expand_rule)) {
								if (expand_rule[B "," C]>=0) {
									if (a == nb_non_terminal*(k-1)+encoding_non_terminal_expand[expand_rule[B "," C]]) print a,b,c,0 > output;
									else  print a,b,c,top > output;
								} else {
									isinexpand = FALSE;
									for (r=0;!isinexpand && r<nb_expand_rule;r++) {
										if (B == expand_ruleB[r] && C == expand_ruleC[r] && a == nb_non_terminal*(k-1)+encoding_non_terminal_expand[expand_ruleA[r]]) isinexpand = TRUE;
									} 
									if (isinexpand) print a,b,c,0 > output;
									else  print a,b,c,top > output;
								}
							} else print a,b,c,top > output;
						}
					}
				}
			}
		}
	}

	grammar = -1000;
	nbrules = 0;
	costmultiplier = 0;
	nb_non_terminal = 0;
	nb_terminal = 0;
	start = -1;
	nb_encoding_non_terminal_expand  = 0;
	nb_encoding_terminal = 0;
	nb_expand_rule = 0;
	nb_terminal_rule = 0;
	delete encoding_non_terminal_expand;
	delete rev_encoding_non_terminal_expand;
	delete encoding_terminal;
	delete rev_encoding_terminal;
	delete expand_ruleA;
	delete expand_ruleB;
	delete expand_ruleC;
	delete terminal_ruleA;
	delete terminal_ruleB;
	delete expand_rule;
	delete var;
	delete dom;
}

/sgrammar/ {
	nb_constr--;
	grammar=FNR;
	n = $1;
	for (i=2;i<=n+1;i++) {
		var[i-1] = $i;
	}
}

END {
	print problem,nb_var+nb_extra_var,max_dom_size,nb_constr,top;
	printf("%s", domains);
	for (i=0;i<nb_extra_var;i++) {
		printf(" %d", 	dom_extra_var[i]);
	}
	close(output);
	system("cat " output);
}
