#!/bin/tcsh

g++ -o parentheses parentheses.cpp -g -Wall
g++ -o parenthesehard parenthesehard.cpp -g -Wall

cd instances
foreach i (7 8 9)
  echo "n = " $i
  foreach j (1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30)
    ../parenthesehard $i wellparhard${i}_${j}.wcsp $j
    awk -f ../grammar.awk wellparhard${i}_${j}.wcsp >! wellparhardtern${i}_${j}.wcsp
	awk 'FNR==1{for(i=0;i<$2;i++) printf " " i; exit}' wellparhardtern${i}_${j}.wcsp >! wellparhardtern${i}_${j}.order
    ../parentheses $i wellpar${i}_${j}.wcsp $j
    awk -f ../grammar.awk wellpar${i}_${j}.wcsp >! wellpartern${i}_${j}.wcsp
	awk 'FNR==1{for(i=0;i<$2;i++) printf " " i; exit}' wellpartern${i}_${j}.wcsp >! wellpartern${i}_${j}.order
  end
end
