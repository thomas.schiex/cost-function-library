#!/bin/bash

MINCELL=$1
MAXCELL=$2
INCCELL=$3
TIMEOUT=$4
NBEXPE=$5

for i in `seq  $MINCELL $INCCELL $MAXCELL`; do
	for run in `seq 1 $NBEXPE`; do
		seed=$(($run+0))
		echo  -en "\r$i\t$run        "
		echo  -en "$i\t$seed" >> reshard.exp
		
		### SOFT CLASSIC ###
				
		
		### SOFT DP ###
		
		trace="trace/trace_wellparhard_"$i"_"$seed
		end=$(grep end. $trace | wc -l)
		#echo $end
		if [[ $end == 1 ]] ; then
			line=$(tail -n 2 $trace	| head -n 1)
			bt=$(echo $line | cut -d' ' -f4)
			node=$(echo $line | cut -d' ' -f7)
			runtime=$(echo $line | cut -d' ' -f15)
			echo -en "\t$bt\t$node\t$runtime" >> reshard.exp
		else 
			echo -en "\t-\t-\t$TIMEOUT" >> reshard.exp
		fi
		
		### WEIGHTED ###
		
		trace="trace/trace_wellparhardtern_"$i"_"$seed
		end=$(grep end. $trace | wc -l)
		#echo $end
		if [[ $end == 1 ]] ; then
			line=$(tail -n 2 $trace	| head -n 1)
			bt=$(echo $line | cut -d' ' -f4)
			node=$(echo $line | cut -d' ' -f7)
			runtime=$(echo $line | cut -d' ' -f15)
			echo -en "\t$bt\t$node\t$runtime" >> reshard.exp
		else 
			echo -en "\t-\t-\t$TIMEOUT" >> reshard.exp
		fi
		
		echo "" >> reshard.exp
	done
done

echo -e "\r"

for i in `seq  $MINCELL $INCCELL $MAXCELL`; do

	echo -en "$i\t|\t"
	
	nbline=$((1+($NBEXPE*(($i-$MINCELL)+1))))
	#if [[ $i -eq 0 ]] ; then start_line=0; fi
	head -n $nbline reshard.exp | tail -n $NBEXPE > temphard.extract

#DP		
	bt_SR=$(cat temphard.extract | awk 'BEGIN{sum=0;n=0} { if ($3!="-") {sum+=$3; n++}} END {if (n>0) print sum/n; else print "-"}')
#	time_SR=$(cat temphard.extract | awk 'BEGIN{sum=0} { sum+=$5} END {print sum/'$NBEXPE'}')
	time_SR=$(cat temphard.extract | awk 'BEGIN{sum=0;n=0} { if ($3!="-") {sum+=$5; n++}} END {if (n>0) print sum/n; else print "-"}')
	success_SR=$(cat temphard.extract | awk 'BEGIN{sum=0} { if ($3!="-") { sum+=1} } END { print sum }')
	echo -en "$success_SR\t"
	echo -en "$bt_SR\t"
	echo -en "$time_SR"
	echo -en "\t|\t"

#WR		
	bt_DP=$(cat temphard.extract | awk 'BEGIN{sum=0;n=0} {if ($6!="-") {sum+=$6; n++}} END {if (n>0) print sum/n; else print "-"}')
#	time_DP=$(cat temphard.extract | awk 'BEGIN{sum=0} { sum+=$8} END {print sum/'$NBEXPE'}')
	time_DP=$(cat temphard.extract | awk 'BEGIN{sum=0;n=0} { if ($6!="-") {sum+=$8; n++}} END {if (n>0) print sum/n; else print "-"}')
	success_DP=$(cat temphard.extract | awk 'BEGIN{sum=0} { if ($6!="-") { sum+=1} } END { print sum }')
	echo -en "$success_DP\t"
	echo -en "$bt_DP\t"
	echo -en "$time_DP"
	echo -en "\t|\t"
	
	echo ""
	
	rm temphard.extract
done

mv reshard.exp results_hard.txt
