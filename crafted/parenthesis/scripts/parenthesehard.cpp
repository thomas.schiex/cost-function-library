#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <set>
#include <algorithm>
#include <ctime>

#define TOP 2000000

using namespace std;

#define abs(x) ((x)>0)?(x):-(x)

const char* grammarStr = 
"var 2000000\n\
10 6 0\n\
16\n\
1 0 0 0\n\
1 0 1 7\n\
1 7 0 2\n\
1 0 3 8\n\
1 8 0 4\n\
1 0 5 9\n\
1 9 0 6\n\
1 0 1 2\n\
1 0 3 4\n\
1 0 5 6\n\
0 1 0\n\
0 2 1\n\
0 3 2\n\
0 4 3\n\
0 5 4\n\
0 6 5";

int randInt(int low, int high) {
	return (int)((rand()*1.0/RAND_MAX)*(high-low+1)) + low;
}

int main (int argc, char** argv) {

	if (argc < 3) {
		printf("Usage: parenthesehard <n> <filename> (seed)\n");
		exit(0);
	}


	if (argc == 4) {
	printf("%s\n", argv[3]);
		srand(atoi(argv[3]));
	} else {
		srand(time(NULL));
	}

	int n = 2*atoi(argv[1]);
	int nconstr = 1+((n*(n-1))>>1);

	//sprintf(filename, "parentheses_%d.wcsp", n);
	const char* filename = argv[2];

	FILE* file = fopen(filename,"w+");

	fprintf(file, "parenthesehard%d %d %d %d %d\n", n, n, 6, nconstr, TOP);
	for (int i=0;i<n;i++) {
		fprintf(file, "%d ", 6);
	}
	fprintf(file, "\n");
		
	fprintf(file, "%d ", n);
	for (int j=0;j<n;j++) fprintf(file, "%d ", j);
	fprintf(file, "-1 sgrammar\n");
	fprintf(file, "%s\n", grammarStr);
	
	for (int i=0;i<n;i++) {
    	for (int j=i+1;j<n;j++) {
      		fprintf(file, "2 %d %d 0 3\n", i, j);
      		fprintf(file, "0 1 %d\n", randInt(0, 10));             
      		fprintf(file, "2 3 %d\n", randInt(0, 10));             
      		fprintf(file, "4 5 %d\n", randInt(0, 10));                          		
		}
	}

	fclose(file);
	return 0;
}
