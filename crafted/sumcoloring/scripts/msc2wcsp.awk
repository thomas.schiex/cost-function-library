
BEGIN {
	n = 0;
	x = 0;
	k = ARGV[2];
	ARGV[2] = "";
}

FNR == NR && /^e +[0-9]+ [0-9]+ *$/{
	if ($2 < $3) G[$2 ":" $3] = 1;
	else if ($2 > $3) G[$3 ":" $2] = 1;
	if ($2 > n) n = $2;
	if ($3 > n) n = $3;
}

END {
	for (e in G) x++;
	top = n*k+1;
	print "sum_coloring",n,k,x+n,top;
	for (i=0;i<n;i++) {
		printf(" %d", k);
	}
	print "";
	for (i=0;i<n;i++) {
		print 1,i,0,k;
		for (u=0;u<k;u++) {
			print u,u+1;
		}
	}
	for (e in G) {
		a = split(e,s,":");
		print 2,s[1]-1,s[2]-1,0,k;
		for (u=0;u<k;u++) {
			print u,u,top;
		}
	}
}
