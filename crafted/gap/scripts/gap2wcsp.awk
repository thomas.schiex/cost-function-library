BEGIN {
	RS = "@";
}

{
	top=1;
	m = 0+$1;
	n = 0+$2;
	pos=3;
	for (i=1;i<=m;i++) {
		for (j=1;j<=n;j++) {
			c[i "_" j] = 0+$pos;
			top += c[i "_" j];
			pos++;
		}
	}
	for (i=1;i<=m;i++) {
		for (j=1;j<=n;j++) {
			a[i "_" j] = 0+$pos;
			pos++;
		}
	}
	for (i=1;i<=m;i++) {
		b[i] = 0+$pos;
		pos++;
	}	
}

END {
	print "gap",n,m,m+n,top;
	for (j=1;j<=n;j++) {
		printf("%d ",m);
	}
	print "";
	for (j=1;j<=n;j++) {
		print 1,j-1,0,m;
		for (i=1;i<=m;i++) {
			print i-1,c[i "_" j];
		}
	}
	for (i=1;i<=m;i++) {
		printf("%d",n);
		for (j=1;j<=n;j++) {
			printf(" %d",j-1);
		}
		print " ",-1,"knapsackp",-b[i];
		for (j=1;j<=n;j++) {
			printf(" 1 %d %d", i-1, -a[i "_" j]);
		}
		print "";
	}		
}
