
# convert generalized assignment problem in original format to WCSP instance

# add an extra linear constraint combining all the original linear constraints

BEGIN {
	RS = "@";
}

{
	top=1;
	m = 0+$1;
	n = 0+$2;
	sumb = 0;
	pos=3;
	for (i=1;i<=m;i++) {
		for (j=1;j<=n;j++) {
			c[i "_" j] = 0+$pos;
			top += c[i "_" j];
			pos++;
		}
	}
	for (i=1;i<=m;i++) {
		for (j=1;j<=n;j++) {
			a[i "_" j] = 0+$pos;
			pos++;
		}
	}
	for (i=1;i<=m;i++) {
		b[i] = 0+$pos;
		sumb += b[i];
		pos++;
	}	
}

END {
	print "gap",n,m,m+n+1,top;
	for (j=1;j<=n;j++) {
		printf("%d ",m);
	}
	print "";
	for (j=1;j<=n;j++) {
		print 1,j-1,0,m;
		for (i=1;i<=m;i++) {
			print i-1,c[i "_" j];
		}
	}
	printf("%d",n);
	for (j=1;j<=n;j++) {
		printf(" %d",j-1);
	}
	print " ",-1,"knapsackp",-sumb;
	for (j=1;j<=n;j++) {
		printf(" %d", m);
		for (i=1;i<=m;i++) {
			printf(" %d %d", i-1, -a[i "_" j]);
		}
	}
	print "";
	for (i=1;i<=m;i++) {
		printf("%d",n);
		for (j=1;j<=n;j++) {
			printf(" %d",j-1);
		}
		print " ",-1,"knapsackp",-b[i];
		for (j=1;j<=n;j++) {
			printf(" 1 %d %d", i-1, -a[i "_" j]);
		}
		print "";
	}		
}
