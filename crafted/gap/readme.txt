Instances from

M. Yagiura, T. Yamaguchi and T. Ibaraki,
  A Variable Depth Search Algorithm with Branching Search for the Generalized
  Assignment Problem, Optimization Methods and Software, 10 (1998) 419-441.

http://www-or.amp.i.kyoto-u.ac.jp/~yagiura/gap

