
Simple Assembly Line Balancing Problem

Benchmark at https://assembly-line-balancing.de/salbp/benchmark-data-sets-2013

Usage: 
 gawk -f ./scripts/salbp.awk "./data/instance.alb" > ./instance.py
 python3 ./scripts/salbp.py ./instance.py ./instances/instance.cfn 15 1
 toulbar2 ./instances/instance.cfn -d:

Known results in xlsx file coming from:
 David R. Morrison, Edward C. Sewell, Sheldon H. Jacobson,
 An application of the branch, bound, and remember algorithm to a new simple assembly line balancing dataset,
 European Journal of Operational Research, Volume 236, Issue 2, 2014, Pages 403-409,
 https://doi.org/10.1016/j.ejor.2013.11.033

