BEGIN {
  task = 0;
  prec = 0;
  list = 0;
}

/<end>/{
  print "]";
  exit;
}

/</{
  sub("<","",$0);
  gsub(" ","_",$0);
  sub(">","=",$0);
  if (list) {
    print "]";
    printf $0 "[";
  } else {
    printf $0;
  }
}

task && NF==2 {
  if (task > 1)
    print "," $2;
  else
    print $2;
  task += 1;
}

prec && NF==1 {
  if (prec > 1)
    print ",[" $1 "]";
  else
    print "[" $1 "]";
  prec += 1;
}

/task_times/{
  task = 1;
  print "[";
  list = 1;
}

/precedence_relations/{
  task = 0;
  prec = 1;
}

NF>0 && task==0 && prec==0 && !/=/ {
  print $0;
}

