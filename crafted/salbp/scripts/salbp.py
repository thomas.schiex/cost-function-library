
# Simple Assembly Line Balancing Problem

# Benchmark at https://assembly-line-balancing.de/salbp/benchmark-data-sets-2013

# Usage: 
# gawk -f ./scripts/salbp.awk ./data/instance.alb > ./instances/instance.py
# python3 ./scripts/salbp.py ./instances/instance.py ./instances/instance.cfn [initgap|0] [optimize|0]
# toulbar2 ./instances/instance.cfn -d:

# Known results coming from:
# David R. Morrison, Edward C. Sewell, Sheldon H. Jacobson,
# An application of the branch, bound, and remember algorithm to a new simple assembly line balancing dataset,
# European Journal of Operational Research, Volume 236, Issue 2, 2014, Pages 403-409,
# https://doi.org/10.1016/j.ejor.2013.11.033

import sys
import pytoulbar2 as tb2

f = open(sys.argv[1])
source = f.read()
exec(source)

dag_levels=0  # TODO: compute DAG number of levels (maximum depth in precedence graph)

extra_number_of_stations = (int(sys.argv[3]) if len(sys.argv) >= 4 else 0)

# compute a greedy upper bound
N = max((sum(task_times) // cycle_time) + (1 if (sum(task_times) % cycle_time) else 0), dag_levels) + extra_number_of_stations
print(sum(task_times) / cycle_time, dag_levels, N)

top = 1000000

Problem = tb2.CFN(top, vac=1, verbose=0)

for i in range(number_of_tasks):
	Problem.AddVariable('t' + str(i+1), range(1,N+1))

if len(sys.argv) >= 5 and int(sys.argv[4]) > 0:
	Problem.AddVariable('n', range(1,N+1))
	Problem.AddFunction(['n'], [a for a in range(1,N+1)])
	for i in range(number_of_tasks):
		Problem.AddFunction(['t' + str(i+1), 'n'], [(0 if a <= b else top) for a in range(1,N+1) for b in range(1,N+1)])

for a in range(1,N+1):
	List = []
	for i in range(number_of_tasks):
		List.append(('t' + str(i+1), a, task_times[i]))
	Problem.AddGeneralizedLinearConstraint(List, operand = '<=', rightcoef = cycle_time)

for e in precedence_relations:
	Problem.AddFunction(['t' + str(e[0]), 't' + str(e[1])], [(0 if a <= b else top) for a in range(1,N+1) for b in range(1,N+1)])
	
Problem.Dump(sys.argv[2])

if True:
	res = Problem.Solve(showSolutions=3, timeLimit=300)
	if res:
		print(res)
		print(max(res[0]))

