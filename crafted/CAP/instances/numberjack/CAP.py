#! /usr/bin/env python

from Numberjack import *

def solve(param):
    data = CAP_Parser(param['data'])

    lib = __import__(param['solver'])
    
    S0 = 12                # size of a plot
    S = data.get("unit")   # size of a landunit
    B = data.get("block")  # number of blocks
    N = data.get("plots")  # total number of landunits
    H = data.get("horizon")  # horizon size
    h = data.get("historic") # number of historic values per landunit
    C = data.get("crop")     # number of crops
    
    if (sum([float(data.get("plot_hst_top")[p][2]) != S for p in range(N)]) != 0):
        print "ERROR: ALL PLOTS MUST HAVE THE SAME SIZE EQUAL TO UNIT=",S
        exit(1)

    blocks = [[p for p in range(N) if data.get("plot_hst_top")[p][1] == b] for b in range(data.get("block"))]

    Vars = [Variable("x"+str(p)+"."+str(t)+"."+str(c))  for p in range(N) for t in range(H) for c in range(C)]

    CSQVars = [Variable("csq"+str(p)+"."+str(t)+"."+str(c1)+"."+str(c2)) for p in range(N) for t in range(H) for c1 in range(C) for c2 in range(C)]

    TOPVars = [Variable("top"+str(p)+"."+str(t)+"."+str(c))  for p in range(N) for t in range(H) for c in range(C)]

    SBCblock = list(set([b for (b,c,lb,ub) in data.get("s_SBC")]))
    SBCVarsOvf = [Variable(0,1000000,"sbcovf"+str(t)+"."+str(b)+"."+str(c))  for t in range(h,H) for b in range(B) for c in range(C)]
    SBCVarsUnf = [Variable(0,1000000,"sbcunf"+str(t)+"."+str(b)+"."+str(c))  for t in range(h,H) for b in range(B) for c in range(C)]
    SBCVars = [Variable(0,1000000,"sbc"+str(t)+"."+str(b)) for t in range(h,H) for b in range(B) ]

    TBCblock = list(set([b for (b,c,lb,ub) in data.get("s_TBC")]))
    TBCVarsOvf = [Variable(0,1000000,"tbcovf"+str(p)+"."+str(c)) for p in range(N) for c in range(C)]
    TBCVarsUnf = [Variable(0,1000000,"tbcunf"+str(p)+"."+str(c)) for p in range(N) for c in range(C)]
    TBCVars = [Variable(0,1000000,"tbc"+str(p)) for p in range(N)]

    SGBCVarsOvf = [Variable(0,1000000,"sgbcovf"+str(t)+"."+str(c))  for t in range(h,H) for c in range(C)]
    SGBCVarsUnf = [Variable(0,1000000,"sgbcunf"+str(t)+"."+str(c))  for t in range(h,H) for c in range(C)]
    SGBCVars = [Variable(0,1000000,"sgbc"+str(t)) for t in range(h,H)]

    # s-CSQ (binary cost functions for each pair of time t and t+1 and for the same plot)
    left_s_CSQ = [CSQVars[p * H * C * C + t * C * C + c1 * C + c2] for b in data.get("s_CSQ") for p in range(N) for t in range(H-1) for c1 in range(C) for c2 in range(C) if data.get("plot_hst_top")[p][1] == b]
    right_s_CSQ = [int(data.get("KP")[c1][c2])                     for b in data.get("s_CSQ") for p in range(N) for t in range(H-1) for c1 in range(C) for c2 in range(C) if data.get("plot_hst_top")[p][1] == b]
    s_CSQ = Sum(left_s_CSQ, right_s_CSQ)
    s_TOP =  Sum([TOPVars[p * H * C + t * C + c] for b in data.get("s_TOP")[1:] for p in range(N) for t in range(h,H) for c in range(C) if data.get("plot_hst_top")[p][1] == b], [int(data.get("s_TOP")[0]) for b in data.get("s_TOP")[1:] for p in range(N) for t in range(h,H) for c in range(C) if data.get("plot_hst_top")[p][1] == b]) - int(data.get("s_TOP")[0])*(C-1)*N*(H-h)
    s_SBC = Sum(SBCVars, [int(data.get("s_SBC_cost")) for elt in SBCVars])
    s_TBC = Sum(TBCVars, [int(data.get("s_TBC_cost")) for elt in TBCVars])
    s_SGBC = Sum(SGBCVars, [int(data.get("s_SGBC_cost")) for elt in SGBCVars])

    obj = Variable(0,1000000,"objective")

    model = Model(
        # Objective function
        Minimise(obj),

        obj == s_CSQ + s_TOP + s_SBC + s_TBC + s_SGBC,

        # integrity constraints
        [Sum([Vars[p * H * C + t * C + c] for c in range(C)]) == 1 for p in range(N) for t in range(H)],

        # h-SCC
        [Vars[p * H * C + t * C + c] == 0 for b in data.get("h_SCC") for p in range(N) for t in range(h,H) for c in range(C) if data.get("plot_hst_top")[p][1] == b and data.get("plot_crops")[b][3:].count(c)<=0],

        # h-EQU
        [Vars[p1 * H * C + t * C + c] == Vars[p2 * H * C + t * C + c]  for (p1,p2) in data.get("h_EQU") for t in range(h,H) for c in range(C)],

        # h-HST
        [Vars[p * H * C + t * C + int(data.get("plot_hst_top")[p][t+3])] == 1 for b in data.get("h_HST") for p in range(N) for t in range(h) if data.get("plot_hst_top")[p][1] == b],

        # s-CSQ (channeling constraints between Vars and CSQVars)
        [Sum([CSQVars[p * H * C * C + t * C * C + c1 * C + c2] for c2 in range(C)]) == Vars[p * H * C + t * C + c1] for p in range(N) for t in range(H-1) for c1 in range(C)],
        [Sum([CSQVars[p * H * C * C + t * C * C + c1 * C + c2] for c1 in range(C)]) == Vars[p * H * C + (t+1) * C + c2] for p in range(N) for t in range(H-1) for c2 in range(C)],

        # s-TOP
        [Sum([Vars[pi * H * C + t * C + c] for pi in ([p]+(data.get("plot_hst_top")[p][4+h:]))] + [TOPVars[p * H * C + t * C + c]], [1 for i in range(1+int(data.get("plot_hst_top")[p][3+h]))] + [1+int(data.get("plot_hst_top")[p][3+h])]) >= 1+int(data.get("plot_hst_top")[p][3+h]) for b in data.get("s_TOP")[1:] for p in range(N) for t in range(h,H) for c in range(C) if data.get("plot_hst_top")[p][1] == b],

        # h-SCA
        [Sum([Vars[blocks[b][0] * H * C + t * C + c] for t in range(h,H)]) == Sum([Vars[p * H * C + t * C + c] for t in range(h,H)]) for b in data.get("h_SCA") for p in range(N) for c in range(C) if data.get("plot_hst_top")[p][1] == b and p != blocks[b][0]],
        
        # h-TSC
        [Vars[p * H * C + t1 * C + c] + Vars[p * H * C + t2 * C + c] <= 1 for b in data.get("h_TSC") for p in blocks[b] for c in range(C) for t1 in range(h-int(data.get("h_TSC_rt")[c]),H-1) for t2 in range(max(h,t1+1), min(H, t1+int(data.get("h_TSC_rt")[c])))],

        # h-CCS
        [Vars[p * H * C + t1 * C + c] + Vars[p * H * C + t2 * C + c] <= 1 for b in data.get("h_CCS") for p in blocks[b] for c in range(C) for t1 in range(max(h,H-int(data.get("h_TSC_rt")[c])),H) for t2 in range(h, min(H, h+int(data.get("h_TSC_rt")[c])-(H-t1)))],
        
        # h-RSC
        [Sum([Vars[p * H * C + t * C + c] for p in blocks[b]]) <= int((float(cap)/float(need))/float(data.get("plot_hst_top")[p][2]))  for t in range(h,H) for (b,c,need,cap) in data.get("h_RSC_cap")],

        # s-SBC
        [SBCVarsOvf[(t-h) * B * C + b * C + c] >= (Sum([Vars[p * H * C + t * C + c] for p in blocks[b]], [int(float(data.get("plot_hst_top")[p][2])/S) for p in blocks[b]]) - int(ub/S)) for t in range(h,H) for b in SBCblock for (b_,c,lb,ub) in data.get("s_SBC") if b==b_],
        [SBCVarsUnf[(t-h) * B * C + b * C + c] >= (int(lb/S) - Sum([Vars[p * H * C + t * C + c] for p in blocks[b]], [int(float(data.get("plot_hst_top")[p][2])/S) for p in blocks[b]])) for t in range(h,H) for b in SBCblock for (b_,c,lb,ub) in data.get("s_SBC") if b==b_],
        [SBCVars[(t-h) * B + b_] >= Sum([SBCVarsOvf[(t-h) * B * C + b * C + c] for (b,c,lb,ub) in data.get("s_SBC") if b==b_]) for t in range(h,H) for b_ in SBCblock],
        [SBCVars[(t-h) * B + b_] >= Sum([SBCVarsUnf[(t-h) * B * C + b * C + c] for (b,c,lb,ub) in data.get("s_SBC") if b==b_]) for t in range(h,H) for b_ in SBCblock],

        # s-TBC
        [TBCVarsOvf[p * C + c] >= (Sum([Vars[p * H * C + t * C + c] for t in range(h,H)], [int(float(data.get("plot_hst_top")[p][2])/S) for t in range(h,H)]) - min(H-h, int(ub/S0))) for b in TBCblock for p in range(N) for (b_,c,lb,ub) in data.get("s_TBC") if b==b_ and  data.get("plot_hst_top")[p][1] == b],
        [TBCVarsUnf[p * C + c] >= (min(H-h, int(lb/S0)) - Sum([Vars[p * H * C + t * C + c] for t in range(h,H)], [int(float(data.get("plot_hst_top")[p][2])/S) for t in range(h,H)])) for b in TBCblock for p in range(N) for (b_,c,lb,ub) in data.get("s_TBC") if b==b_ and  data.get("plot_hst_top")[p][1] == b],
        [TBCVars[p] >= Sum([TBCVarsOvf[p * C + c] for (b,c,lb,ub) in data.get("s_TBC") if b==b_]) for b_ in TBCblock for p in range(N) if data.get("plot_hst_top")[p][1] == b_],
        [TBCVars[p] >= Sum([TBCVarsUnf[p * C + c] for (b,c,lb,ub) in data.get("s_TBC") if b==b_]) for b_ in TBCblock for p in range(N) if data.get("plot_hst_top")[p][1] == b_],
    )
    # s-SGBC
    if len(data.get("s_SGBC")) > 0:
        model.add([SGBCVarsOvf[(t-h) * C + c] >= (Sum([Vars[p * H * C + t * C + c] for p in range(N)], [int(float(data.get("plot_hst_top")[p][2])/S) for p in range(N)]) - int(ub/S)) for t in range(h,H) for (c,lb,ub) in data.get("s_SGBC")])
        model.add([SGBCVarsUnf[(t-h) * C + c] >= (int(lb/S) - Sum([Vars[p * H * C + t * C + c] for p in range(N)], [int(float(data.get("plot_hst_top")[p][2])/S) for p in range(N)])) for t in range(h,H) for (c,lb,ub) in data.get("s_SGBC")])
        model.add([SGBCVars[(t-h)] >= Sum([SGBCVarsOvf[(t-h) * C + c] for (c,lb,ub) in data.get("s_SGBC")]) for t in range(h,H)])
        model.add([SGBCVars[(t-h)] >= Sum([SGBCVarsUnf[(t-h) * C + c] for (c,lb,ub) in data.get("s_SGBC")]) for t in range(h,H)])

    # initial solution if available
#    model.add([(Vars[i * C + int(v)] == 1) for (i,v) in zip(range(N*H), "".join(open("data/sol-farm1234star.txt")).split() ) if v != '*'])

    print model

    solver = lib.Solver(model)
    solver.setNodeLimit(param['cutoff'])
    solver.setVerbosity(param['verbose'])
    solver.setTimeLimit(param['tcutoff'])
    solver.solve()

    for p in range(N):
       for t in range(H):
          for c in range(C):
              if Vars[p * H * C + t * C + c].get_value() == 1:
                 print("%d" % c),
       print "" 
    print "\nFINAL COST: " + str(obj.get_value())
    print eval(s_CSQ.solution())
    print eval(s_TOP.solution())
    print eval(s_SBC.solution())
    print eval(s_TBC.solution())
    print eval(s_SGBC.solution())
    return "end."
    
class CAP_Parser:
    
    def __init__(self, file):
        lines = open(file).readlines() 
        self.name = lines[0]
        self.unit = float(lines[1][5:])
        self.horizon = int(lines[2][8:])
        self.historic = int(lines[3][9:])
        self.crop = int(lines[4][5:])
        self.block = int(lines[5][6:])
        self.plot_crops = eval("["+
                               " ".join((map((lambda a: a[:-1]+","),
                                                lines[6:(6+self.block)]))) +
                               "]")
        print "crops",self.plot_crops
        self.plots = 0
        for row in self.plot_crops:
            self.plots += row[1]
        self.plot_hst_top = eval("["+
                               " ".join((map((lambda a: a[:-1]+","),
                                                lines[(6+self.block):(6+self.block)+self.plots]))) +
                               "]")
        print "neighbors",self.plot_hst_top
        pos = 6+self.block+self.plots
        self.h_SCC = [int(elt) for elt in lines[pos][6:].split()]
        print "SCC",self.h_SCC
        pos += 1
        self.h_EQU = eval("["+
                               " ".join((map((lambda a: a[:-1]+","),
                                                lines[(pos+1):(pos+1+int(lines[pos][6:]))]))) +
                               "]")
        print "EQU",self.h_EQU
        pos += 1+len(self.h_EQU)
        self.h_HST = [int(elt) for elt in lines[pos][6:].split()]
        print "HST",self.h_HST
        pos += 1
        self.h_SCA = [int(elt) for elt in lines[pos][6:].split()]
        print "SCA",self.h_SCA
        pos += 1
        self.h_TSC = [int(elt) for elt in lines[pos][6:].split()]
        print "TSC",self.h_TSC
        pos += 1
        self.h_TSC_rt = [int(elt) for elt in lines[pos].split()]
        print "rt",self.h_TSC_rt
        pos += 1
        self.h_CCS = [int(elt) for elt in lines[pos][6:].split()]
        print "CCS",self.h_CCS
        pos += 1
        self.h_RSC_cap = eval("["+
                               " ".join((map((lambda a: a[:-1]+","),
                                                lines[(pos+1):(pos+1+int(lines[pos][6:]))]))) +
                               "]")
        print "capacity",self.h_RSC_cap
        pos += 1+len(self.h_RSC_cap)
        self.s_CSQ = [int(elt) for elt in lines[pos][6:].split()]
        print "CSQ",self.s_CSQ
        pos += 1
        self.KP = [[0 for j in range(self.crop)] for i in range(self.crop)]
        for i in range(self.crop*self.crop):
             (left,right,cost) =  lines[pos].split()
             print left,right,cost
             self.KP[int(left)][int(right)] = cost
             pos += 1
        print self.KP
        self.s_TOP = [int(elt) for elt in lines[pos][6:].split()]
        print "TOP",self.s_TOP
        pos += 1
        self.s_SBC_cost = 0
        (s,self.s_SBC_cost,n) = lines[pos].split()
        self.s_SBC = eval("["+
                               " ".join((map((lambda a: a[:-1]+","),
                                                lines[(pos+1):(pos+1+int(n))]))) +
                               "]")
        print "SBC",self.s_SBC_cost,self.s_SBC
        pos += 1+len(self.s_SBC)
        self.s_TBC_cost = 0
        (s,self.s_TBC_cost,n) = lines[pos].split()
        self.s_TBC = eval("["+
                               " ".join((map((lambda a: a[:-1]+","),
                                                lines[(pos+1):(pos+1+int(n))]))) +
                               "]")
        print "TBC",self.s_TBC_cost,self.s_TBC
        pos += 1+len(self.s_TBC)
        self.s_SGBC_cost = 0
        (s,self.s_SGBC_cost,n) = lines[pos].split()
        self.s_SGBC = eval("["+
                               " ".join((map((lambda a: a[:-1]+","),
                                                lines[(pos+1):(pos+1+int(n))]))) +
                               "]")
        print "SGBC",self.s_SGBC_cost,self.s_SGBC
        pos += 1+len(self.s_SGBC)
        
    def get(self, name):
        if hasattr(self, name):
            return getattr(self, name)
        print name + " \t No Such Data!"
        return None
    

solvers = ['Mistral', 'SCIP', 'MiniSat', 'toulbar2']
default = {'solver':'SCIP', 'data':'data/virtual-farm-col-120.txt', 'cutoff':500000, 'verbose':1, 'tcutoff':60000}

if __name__ == '__main__':
    param = input(default) 
    print solve(param)
