==============================
Crop allocation problem CAP
==============================

Introduction 
-------------

Crop allocation is a spatio-temporal planning problem  in which  crops are 
assigned to landunits x_{b,i} over a fixed horizon H of time. A Landunit is defined 
as a piece of indivisible and homogeneous land whose historic and biophysical properties 
are identical. The landunits are spatial sampling of the farmland where x_{b,i} denotes the 
landunit $i$ of block $b$.  Blocks are characterized by one cropping system 
defined by the same sequence of crops and by the use of a coherent set of 
production techniques applied to these crops (e.g. fertilizer, irrigation  water).  
The delimitation of blocks are not reshaped in the CAP considered in this work. 


More  formally,  the  Crop allocation problem  is  defined  by a tuple 
<X,D,W> where 
  - X a set of variables x_{b,i}^{t} that define the landunits
   i in block b (i \in [1, N_b, b \in [1, B]. Each landunit is described by H variables that
   represent the landunit occupation at each date. 
  - D the domains D_{b,i} of variables x_{b,i}^{t} is the set of
  possible  crop over the block b. 
  -W the cost functions W_i that define the hard and soft constraints. W_i can be (1) simple cost functions, 
  (2) same constraints,  (3) global constraints regular, (4) global cardinality constraints, 
  (5) soft global cardinality constraints. These constraints are precisely define according the the CAP. 
  
  
CAP instances  
-------------
The CAP described in this benchmark we consider a 4 blocks sampled into N landunits 
Four crops are produced over the all blocks : winter wheat (BH), spring barley (OP),
maize (MA) and winter rape (CH). Each block has a fixed area (areablock(1) = 48ha, 
areablock(2) = 24ha, areablock(3) = 48ha, areablock(4) = 60ha). The blocks 1 and 
3 have an access to irrigation equipments r_1 and r_2. The annual quota of
irrigation water over the blocks is 6000 m^{3} (respectively 4000 m^3) 
for r_1 (respectively r_2). Only the maize (MA) can be irrigated.  
There are two different types of soil : A (block 1, 3) and B (block 2, 4). 

The number of landunits N is increased from 15 to 120. For the CAP instance with  
15 landunits N_1=N_3=4, N_2=2 and N_4=5 where N_i is the number of landunits in the block i. 
Note that in this instance the area of each landunit is 12ha. 
These landunits are gradually refined by splitting them into 2, 4 and 8 smaller 
ones, to respectively build the instances with 30, 60 and 120 landunits. 
These sampling are chosen to be representative of different farm size. 
The planning horizon H=9. According to the minimum return time (winter wheat rt(BH)=2, 
spring barley rt(OP)=3, maize rt(MA)=2 and  winter rape rt(CH)=3) the four last years are dedicated to the future variable 
while the five first are the historic.


benchmark organization: 
./Bi-LUj.wcsp ==> 16 instances in toulbar2 format associated to the block i  
 	(where j denote the number of landunit in the block)
./B1234-LU*.wcsp ==> 8 instances in toulbar2 format associated to the the all 
	block 1 2 3 4
*.ub ==> optimal solution obtained by DFBB using the default option. 
*.sol ==> a complete  assignment


question : makploga@toulouse.inra.fr





