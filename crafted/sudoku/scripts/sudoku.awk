BEGIN {
	n = 9;
	p = int(sqrt(n));

	top = 999999;

	# wcsp header
	print "sudoku",n*n,n,3*n,1

	# domain sizes
	for (i = 0 ; i<n*n; i++) printf("%d ",n);
	print "";

	# list of constraints
	# alldifferent on lines
	for (i = 0 ; i<n; i++) {
		printf("%d", n);
		for (j = 0 ; j<n; j++) {
			printf(" %d", i*n+j);
		}
		print "",-1,"salldiff","var",top;
	}
	# alldifferent on column
	for (i = 0 ; i<n; i++) {
		printf("%d", n);
		for (j = 0 ; j<n; j++) {
			printf(" %d", j*n+i);
		}
		print "",-1,"salldiff","var",top;
	}
	# alldifferent on interior square
	for (i = 0 ; i<n; i++) {
		printf("%d", n);
		for (j = 0 ; j<n; j++) {
			printf(" %d", (i % p)*p+int(i / p)*p*n+(j % p)+int(j / p)*n);
		}
		print "",-1,"salldiff","var",top;
	}
}
