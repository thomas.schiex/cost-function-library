
# usage: awk -f sol2sudoku.awk sol

{
	n = int(sqrt(NF));
	p = int(sqrt(n));
	for (i = 0 ; i<n; i++) {
		if (i % p == 0) {
			for (j = 0 ; j<p; j++) printf("_");
			for (j = 0 ; j<n; j++) printf("___");
			print "_";
		}
		for (j = 0 ; j<n; j++) {
			if (j % p == 0) printf("|");
			printf(" %2d", $(i*n+j+1)+1);
		}
		print "|";
	}
	for (j = 0 ; j<p; j++) printf("_");
	for (j = 0 ; j<n; j++) printf("___");
	print "_";
}
