
# usage: 
# cp sudoku.wcsp sudoku_hard.wcsp
# awk -f sudoku2x.awk sudoku_hard.txt >> sudoku_hard.wcsp

# warning: you must change the number of constraints in sudoku_hard.wcsp by hand!
# awk -f sudoku2x.awk sudoku_hard.txt | wc -l

BEGIN {
	FS="|";
}

{
	for(i=1;i<=NF;i++) {
		if ($i >= 1 && $i <= 9) {
			printf("1 %d 99999 1\n",p);
			printf("%d 0\n",($i)-1);
		}
		p++;
	}
}
