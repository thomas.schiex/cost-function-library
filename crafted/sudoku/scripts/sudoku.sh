#!/usr/bin/tcsh

# usage: ./sudoku.sh sudoku_easy.txt

cp sudoku.wcsp ${1:r}.wcsp
set evid = `awk -f ./sudoku2x.awk $1 | wc -l | awk '{printf("%d",27+$1/2)}'`
head -1 ${1:r}.wcsp | sed "s/27/${evid}/" >! ${1:r}.wcsp.tmp
tail --lines=+2 ${1:r}.wcsp >> ${1:r}.wcsp.tmp
mv -f ${1:r}.wcsp.tmp ${1:r}.wcsp
awk -f ./sudoku2x.awk $1 >> ${1:r}.wcsp
toulbar2 ${1:r}.wcsp -w
awk -f ./sol2sudoku.awk sol
