(Un)capacitated warehouse location problem

These data files come from the test problem sets VII, X, XIII and A to
C  in  Table 2  of  J.E.Beasley  "Lagrangean  heuristics for  location
problems"  European  Journal of  Operational  Research, vol.65,  1993,
pp383-399. (see http://www.brunel.ac.uk/depts/ma/research/jeb/orlib/uncapinfo.html)

See also the capXX.dat files  and a problem description as proposed by
Brahim Hnich  in the CSPLib  (see http://4c.ucc.ie/~tw/csplib/ problem
#34).

warehouse1.dat comes from an Eclipse solver benchmark.
warehouse0.dat comes from Brahim Hnich's problem description.

Some optimal values of the original test problem sets:
Data file   Optimal solution value
cap71       932615.750
cap72       977799.400
cap73       1010641.450
cap74       1034976.975
cap101      796648.437
cap102      854704.200
cap103      893782.112
cap104      928941.750
cap131      793439.562
cap132      851495.325
cap133      893076.712
cap134      928941.750
capa        17156454.478
capb        12979071.582
capc        11505594.329

Note that the original instances use real numbers for the supply costs
that have been converted into integers by multiplying every cost by 10
(except for  capa, capb  and capc by 1, and capm* by 1000) and  then, removing  the fractional
part.   Thus,  the resulting  optimum  of  capXX.wcsp,  divided by  10
(except for  capa, capb and  capc by 1, and capm* by 1000), is a  lower bound of  the original
instance.

Minizinc instances have all their costs multiplied by 1.

By default, instances are uncapacitated WLP. 
Instances with capacity constraints have the extension symbol kp in their name. Their costs have been multiplied by 1. 

****** OR-Library ******

http://www.brunel.ac.uk/depts/ma/research/jeb/jeb.html

***** J_E_Beasley *****
OR-Library is a collection of test data sets for a variety of OR problems.
A full list of the test data sets available in OR-Library can be found here: http://www.brunel.ac.uk/depts/ma/research/jeb/info.html.
===============================================================================
***** Uncapacitated warehouse location *****
There are currently 15 data files.



These data files are the test problem sets VII, X, XIII and
A to C in Table 2 of J.E.Beasley "Lagrangean heuristics for
location problems" European Journal of Operational
Research, vol.65, 1993, pp383-399.

The following table gives the relationship between test
problem set and the appropriate files:

Problem set        Files
VII                cap71, ..., cap74
X                  cap101, ..., cap104
XIII               cap131, ..., cap134
A                  capa
B                  capb
C                  capc

The format of these data files is:
number of potential warehouse locations (m), number of
customers (n)
for each potential warehouse location i (i=1,...,m):
capacity, fixed cost
for each customer j (j=1,...,n): demand, cost of allocating
all of the demand of j to warehouse i (i=1,...,m)

As these test problems are derived from test problems for
the capacitated warehouse location problem they include
capacity figures for each potential warehouse location
and demand figures for each customer. These figures should
(obviously) be ignored when solving these problems
as uncapacitated warehouse location problems.

The value of the optimal solution for each of these data
files is given in the file uncapopt.

The largest file is capc of size 1300Kb (approximately).
The entire set of files is of size 4500Kb (approximately).

Click here: http://www.brunel.ac.uk/depts/ma/research/jeb/orlib/files   to access these files
