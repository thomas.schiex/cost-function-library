
# Translate original uncapacitated warehouse location problem into LP_SOLVE format
# Usage: awk -f cap2lp.awk problem.txt > problem.lp

BEGIN {
  RS = "@";
  floatmultiplier = 1; # = 10, except for capa, capb and capc (= 1)
}

{
  nbw = $1; # number of potential warehouse locations 
  nbs = $2; # number of stores (or customers) 
  pos = 2 + 1;
  for (w=1; w<=nbw; w++) {
      pos++;
      fixedcosts[w] = int($pos * floatmultiplier);
      pos++;
  }
  for (s=1; s<=nbs; s++) {
      pos++; # demand 
      for (w=1; w<=nbw; w++) {
	  costs[w,s] = int($pos * floatmultiplier);
	  pos++;
      }
  }
  printf("min:");
  for (w=1; w<=nbw; w++) {
      printf(" + %d y%d", fixedcosts[w], w);
  }
  for (s=1; s<=nbs; s++) {
      for (w=1; w<=nbw; w++) {
	  printf(" + %d x%d_%d", costs[w,s], w, s);
      }
  }
  print ";";
  for (s=1; s<=nbs; s++) {
      for (w=1; w<=nbw; w++) {
	  printf(" + x%d_%d", w, s);
      }
      print " = 1;";
  }
  for (s=1; s<=nbs; s++) {
      for (w=1; w<=nbw; w++) {
	  printf("x%d_%d - y%d <= 0;\n", w, s, w);
      }
  }
  for (w=1; w<=nbw; w++) {
      print "y" w " <= 1;";
  }
  printf("int y1");
  for (w=2; w<=nbw; w++) {
      printf(",y%d", w);
  }
  print ";";
}
