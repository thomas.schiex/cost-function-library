
# Translate original uncapacitated warehouse location problem into CPLEX format
# Usage: awk -f cap2cpx.awk problem.txt > problem.cpx

BEGIN {
  RS = "@";
  floatmultiplier = 1; # = 10, except for capa, capb and capc (= 1)
}

{
  nbw = $1; # number of potential warehouse locations 
  nbs = $2; # number of stores (or customers) 
  pos = 2 + 1;
  for (w=1; w<=nbw; w++) {
      capacity[w] = int($pos);
      pos++;
      fixedcosts[w] = int($pos * floatmultiplier);
      pos++;
  }
  totaldemand = 0;
  for (s=1; s<=nbs; s++) {
      demand[s] = int($pos);
      totaldemand += demand[s];
      pos++; # demand 
      for (w=1; w<=nbw; w++) {
	  costs[w,s] = int($pos * floatmultiplier);
	  pos++;
      }
  }
  print "Minimize";
  printf("obj:");
  for (w=1; w<=nbw; w++) {
      printf(" +%d y%d", fixedcosts[w], w);
  }
  for (s=1; s<=nbs; s++) {
      for (w=1; w<=nbw; w++) {
	  printf(" +%d x%d_%d", costs[w,s], w, s);
      }
  }
  print "";
  print "Subject To";
  for (s=1; s<=nbs; s++) {
      printf("c%d:", s);
      for (w=1; w<=nbw; w++) {
	  printf(" +x%d_%d", w, s);
      }
      print " = 1";
  }
  for (s=1; s<=nbs; s++) {
      for (w=1; w<=nbw; w++) {
	  printf("e%d: x%d_%d - y%d <= 0\n", ++e, w, s, w);
      }
  }
# print hard capacity constraints on warehouses
  for (w=1; w<=nbw; w++) {
      printf("k%d:", w);
      for (s=1; s<=nbs; s++) {
         printf(" +%d x%d_%d",demand[s],w,s);
      }
      print " <= " capacity[w];
  }
# print redundant constraint on warehouse selections in order to satisfy the total demand
  printf("r:");
  for (w=1; w<=nbw; w++) {
      printf(" +%d y%d", capacity[w], w);
  }
  print " >= " totaldemand;
  print "Binary"
  for (w=1; w<=nbw; w++) {
      print "y" w;
  }
  for (s=1; s<=nbs; s++) {
      for (w=1; w<=nbw; w++) {
	  print "x" w "_" s;
      }
  }
  print "End";
}
