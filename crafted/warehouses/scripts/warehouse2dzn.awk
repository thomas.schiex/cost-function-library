
# Usage: awk -f warehouse2dzn.awk problem.txt >> problem.dzn

BEGIN {
  RS = "@";
  floatmultiplier = 1; # = 10, except for capa, capb and capc (= 1)
}

{
  nbw = $1; # number of potential warehouse locations 
  nbs = $2; # number of stores (or customers) 

  for (w=1; w<=nbw; w++) {
      if (int($(2+w*2) * floatmultiplier) > 0) {
		  buildcost[w] = int($(2+w*2) * floatmultiplier);
	  }
	  capacity[w] = int($(1+w*2));
	  nbc++;
  }
  pos = 2 + nbw * 2 + 1;
  for (s=1; s<=nbs; s++) {
      demand[s] = int($pos);
      pos++; # demand 
      for (w=1; w<=nbw; w++) {
		  cost[s,w] = int($pos * floatmultiplier);
		  pos++;
      }
  }
  print "n_suppliers = " nbw ";"
  print "n_stores = " nbs ";"

  printf("building_cost = [");
  for (w=1; w<=nbw; w++) {
      if (w>1) printf(",");
      printf("%d", buildcost[w]);
  }
  print "];";

  printf("cost_matrix = [");
  for (s=1; s<=nbs; s++) {
      print("|");
      for (w=1; w<=nbw; w++) {
          if (w>1) printf(",");
          printf("%d", cost[s,w]);
      }
  }
  print "|];";

  # print capacity on warehouses
  printf("capacity = [");
  for (w=1; w<=nbw; w++) {
      if (w>1) printf(",");
      printf("%d", capacity[w]);
  }
  print "];";
  # print demand on stores
  printf("demand = [");
  for (s=1; s<=nbs; s++) {
      if (s>1) printf(",");
      printf("%d", demand[s]);
  }
  print "];";
}

