
# Usage: awk -f warehouse.awk problem.txt > problem.wcsp

BEGIN {
  RS = "@";
  floatmultiplier = 1; # = 10, except for capa, capb and capc (= 1)
}

{
  nbw = $1; # number of potential warehouse locations 
  nbs = $2; # number of stores (or customers) 
  nbc = nbs*nbw + nbs + 1;
  infinity = 1;
  for (w=1; w<=nbw; w++) {
      if (int($(2+w*2) * floatmultiplier) > 0) {
		  nbc++;
		  infinity += int($(2+w*2) * floatmultiplier);
	  }
	  capacity[w] = int($(1+w*2));
	  nbc++;
  }
  pos = 2 + nbw * 2 + 1;
  totaldemand = 0;
  for (s=1; s<=nbs; s++) {
	  demand[s] = int($pos);
	  totaldemand += demand[s];
      pos++; # demand 
      maxcost = 0;
      for (w=1; w<=nbw; w++) {
		  if (int($pos * floatmultiplier) > maxcost) {
			  maxcost = int($pos * floatmultiplier);
		  }
		  pos++;
      }
      infinity += maxcost;
  }
  print nbw "warehouses_" nbs "stores_" floatmultiplier "fltmult",nbw+nbs,nbw,nbc,infinity;
  # print domains
  printf("2");
  for (w=2; w<=nbw; w++) {
      printf(" 2");
  }
  for (s=1; s<=nbs; s++) {
      printf(" %d", nbw);
  }
  print "";
  # print soft unary constraint on warehouses
  for (w=1; w<=nbw; w++) {
      if (int($(2+w*2) * floatmultiplier) > 0) {
		  print 1,w-1,0,1;
		  print 1,int($(2+w*2) * floatmultiplier); # fixed cost 
      }
  }
  pos = 2 + nbw * 2 + 1;
  for (s=1; s<=nbs; s++) {
      # print hard channeling constraints for stores/warehouses
      for (w=1; w<=nbw; w++) {
		  print 2,nbw+s-1,w-1,0,1;
		  print w-1,0,infinity;
      }
      pos++; # demand 
      # print soft unary constraint on stores
      print 1,nbw+s-1,0,nbw;
      for (w=1; w<=nbw; w++) {
		  print w-1,int($pos * floatmultiplier); # cost of allocating all of the demand of s to warehouse w 
		  pos++;
      }
  }
  # print hard capacity constraints on warehouses
  for (w=1; w<=nbw; w++) {
      printf("%d", nbs);
	  for (s=1; s<=nbs; s++) {
		 printf(" %d", nbw+s-1);
	  }
	  printf(" -1 knapsackp ");
	  printf("%d", -capacity[w]);
	  for (s=1; s<=nbs; s++) {
		  printf(" 1 %d %d", w-1, -demand[s]);
	  }
	  print "";
  }
  # print redundant constraint on warehouse selections in order to satisfy the total demand
  printf("%d", nbw);
  for (w=1; w<=nbw; w++) {
	  printf(" %d", w-1);
  }
  printf(" -1 knapsack ");
  printf("%d", totaldemand);
  for (w=1; w<=nbw; w++) {
	  printf(" %d", capacity[w]);
  }
}
