
BEGIN {
	K = 10;
}

NF==3 {	
	i = 0+$1 - 1;
	x[i] = 0+$2;
	y[i] = 0+$3;
	n++;
}

END {
	print "{";
	print "\"problem\": { \"name\": \"pdispersionsum\", \"mustbe\": \">0.000\"},";
	printf("\"variables\": {");
	printf("\"i%d\": 2",1);
        for (i=1; i<n; i++) printf(", \"i%d\": 2",i+1);
	print "},";
	print "\"functions\": {";
	for (i=0; i<n; i++) {
		for (j=i+1; j<n; j++) {
			c++;
			dij = sqrt((x[i]-x[j])**2 + (y[i]-y[j])**2);
			print "\"c" c "\": { \"scope\": [\"i" i+1 "\", \"i" j+1 "\"], \"costs\": [0, 0, 0, " dij "]},";
		}
	}
	printf("\"knapsack\": { \"scope\": [");
	printf("\"i%d\"",1);
	for (i=1; i<n; i++) printf(", \"i%d\"", i+1);
	printf("], \"type\": \"knapsack\", \"params\": { \"capacity\": %d, \"weights\": [-1", -K);
	for (i=1; i<n; i++) printf(", %d", -1);
	print "] } },";
	printf("\"knapsack\": { \"scope\": [");
	printf("\"i%d\"",1);
	for (i=1; i<n; i++) printf(", \"i%d\"", i+1);
	printf("], \"type\": \"knapsack\", \"params\": { \"capacity\": %d, \"weights\": [1", K);
	for (i=1; i<n; i++) printf(", %d", 1);
	print "] } } } }";
}

