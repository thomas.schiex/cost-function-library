
BEGIN {
	MULT = 1000;
	K = 10;
}

NF==3 {	
	i = 0+$1 - 1;
	x[i] = 0+$2;
	y[i] = 0+$3;
	n++;
	if (0+$2 > maxx) maxx = 0+$2;
	if (0+$3 > maxy) maxy = 0+$3;
}

END {
	print "pdispersionsum",n,2,2+n*(n-1)/2,int(sqrt(maxx*maxx+maxy*maxy)*MULT*n*(n-1)/2);
	for (i=1; i<n; i++) printf("%d ",2);
	print 2;
	for (i=0; i<n; i++) {
		for (j=i+1; j<n; j++) {
			print 2,i,j,int(sqrt((x[i]-x[j])**2 + (y[i]-y[j])**2)*MULT+0.5),1;
			print 1,1,0;
		}
	}
	printf("%d",n);
	for (i=0; i<n; i++) printf(" %d", i);
	printf(" -1 knapsack %d", -K);
	for (i=0; i<n; i++) printf(" %d", -1);
	print "";
	printf("%d",n);
	for (i=0; i<n; i++) printf(" %d", i);
	printf(" -1 knapsack %d", K);
	for (i=0; i<n; i++) printf(" %d", 1);
	print "";
}

