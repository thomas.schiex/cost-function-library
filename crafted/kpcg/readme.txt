Quadratic Knapsack Problems

See http://or.dei.unibo.it/library/knapsack-problem-conflict-graph-kpcg

A Branch-and-Bound Algorithm for the Knapsack Problem with Conflict Graph
Andrea Bettinelli, Valentina Cacchiani, Enrico Malaguti
INFORMS Journal, 2017

Translate into wcsp minimization problems by script kpcg2wcsp.awk
