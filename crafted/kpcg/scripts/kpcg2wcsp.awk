
# Translate Quadratic Knapsack Problems into WCSP format

# Usage:
# awk -f ./kpcg2wcsp.awk problem.dat > problem.wcsp

BEGIN {
        TOP = 1000000000;
	ok = 0;
	e = 0;
	k = 0;

	scope = "";
	weight = "";
	profit = "";
	binary = "";
}

/param n := / {
	n = 0+$NF;
}

/param c := / {
	capacity = 0+$NF;
}

/;/ {
	ok = 0;
}

ok && NF==3 {
	weight = weight " " (-$3);
	profit = profit "\n1 " $1 " 0 1 0 " $2;
	k++;
	scope = scope " " $1;
}

ok && NF==2 {
   binary = binary "\n" 2 " " $1 " " $2 " 0 1 1 1 " TOP;
   e++;
}
	
/param : V : p w :=/ {
	ok = 1;
}

/set E :=/ {
	ok = 1;
}

END {
	print "KPCG",n,2,1+e+n,TOP;
	for (i=0; i < n; i++) printf("  2");
	print "";
	print n,scope,-1,"knapsack",-capacity,weight;
	print profit;
	print binary;
}

