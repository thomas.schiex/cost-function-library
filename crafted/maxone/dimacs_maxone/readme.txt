Definition:
-----------
Given a satisfiable CNF formula, max-one is the problem of finding a model with a maximum number of variables set to true. Note that solving this problem is much harder than solving the usual SAT problem, because the search cannot stop as soon as a model is found. The optimal model must be found and its optimality must be proved.

Instances:
----------


Dimacs: We have generated maxone instances coming from some satisfiable dimacs instances, including aim, ii, jnh etc.
