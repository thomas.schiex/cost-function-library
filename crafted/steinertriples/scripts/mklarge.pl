#!/usr/bin/perl
#
$firstcol = 0;
#
$initmode = 1;
while(<>){
  if($initmode == 1){
    split;
    $ncol = $_[$firstcol];
    $nrow = $_[$firstcol + 1];
    $initmode = 0;
    printf("%d %d\n", $ncol * 3, $ncol + 9 * $nrow);
    for($i = 1; $i <= $ncol; $i ++){
      $temp = 3 * ($i - 1);
      printf("%d %d %d\n", $temp + 1, $temp + 2, $temp + 3);
    }
  }
  else{
    split;
    $i = $_[$firstcol];
    $j = $_[$firstcol + 1];
    $k = $_[$firstcol + 2];
    for($r = 1; $r <= 3; $r ++){
      printf("%d %d %d\n",
	     3*($i - 1) + $r, 3*($j - 1) + $r, 3*($k - 1) + $r);
      $s = ($r % 3) + 1;
      $t = ( ($r + 1) % 3) + 1;
      #printf("(r s t) = ($r $s $t)\n", $r, $s, $t);
      printf("%d %d %d\n",
	     3*($i - 1) + $r, 3*($j - 1) + $s, 3*($k - 1) + $t);
      printf("%d %d %d\n",
	     3*($i - 1) + $r, 3*($j - 1) + $t, 3*($k - 1) + $s);
    }
  }
}
