The data files are Steiner triple covering problem (STCP) instances,
which were taken from OR-Library.

Large instances come from 

http://www-or.amp.i.kyoto-u.ac.jp/~yagiura/scp/stcp/

The file "mklarge.pl" is a perl script to generate larger STCP
instances. To use this, first modify the first line of mklarge.pl,
where the path to the perl command is written, and type, for example,

  cat data.135 | mklarge.pl

then the STCP instance of size 405 is generated.

The file "steiner2scp.pl" is a perl script to convert STCP format to
SCP (set covering problem) format, which is also adopted in OR-Library.
Usage:

  cat data.135 | steiner2scp.pl

---------------------------------------------------------------------------

You can generate MAXSAT formulation of these instances by using "scp2msat"
and CNF formulation by using "msat2cnf".

Compile:

  gcc -O2 -o scp2msat scp2msat.c
  gcc -O2 -o msat2cnf msat2cnf.c

Usage:

  cat data.135 | steiner2scp.pl | scp2msat
  cat data.135 | steiner2scp.pl | scp2msat | msat2cnf keepweight 1


---------------------------------------------------------------------------

the file format is:

one line: number-of-variables   number-of-triples

number-of-triple lines:  3 triple indices: T(i,1), T(i,2), T(i,3)

---------------------------------------------------------------------------

The set covering problem is: 

min x(1)+x(2)+...+x(number-of-variables)

s.t.   x(T(i,1))+x(T(i,2))+x(T(i,3)) >= 1, for i =1,...,number-of-triples

       0<=x(j)<=1, x(j) integer, for j=1,...,number-of-variables

