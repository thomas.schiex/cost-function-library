
# Resource-Constrained Project Scheduling Problem

import sys
import pytoulbar2

# replace data.py by some instances/python/*.py
from data import *

N = len(job_durations)

top = horizon+1 # give a better initial upper-bound if known

Problem = pytoulbar2.CFN(top)

for i in range(N):
    Problem.AddVariable('x' + str(i+1), range(horizon+1))

# first job starts at 0
Problem.AddFunction([0], [0 if a==0 else top for a in range(horizon+1)])

# precedence constraints 
for i in range(N):
    for j in job_successors[i]:
        Problem.AddFunction([i, j], [(0 if a + job_durations[i] <= b else top) for a in range(horizon+1) for b in range(horizon+1)])

# for each ressource and each time slot, we post a linear constraint on all the jobs that require this ressource to not overcome the ressoure capacity
for k, capacity in enumerate(capacities):
  for a in range(horizon+1):
    params = ''
    realscope = []
    for i in range(N):
      if job_requirements[i][k] > 0:
        paramval = ''
        nbval = 0
        for b in range(horizon+1):
          if a >= b and a < b + job_durations[i]:
            nbval += 1
            paramval += ' ' + str(b) + ' ' + str(-job_requirements[i][k])
        if nbval > 0:
          params += ' ' + str(nbval) + paramval
          realscope.append(i)
    if len(params) > 0:
      Problem.CFN.wcsp.postKnapsackConstraint(realscope, str(-capacity) + params, False, True)

# minimize makespan, i.e., the completion time of the last job
Problem.AddFunction([N-1], [a for a in range(horizon+1)])

Problem.Dump('rcpsp.wcsp')
# returns (optimal solution, optimum value, number of solutions found)
print(Problem.Solve(showSolutions=1))

