
# Convert original RCPSP format (sm) into python data
# Usage: xzcat ../data/j3029_5.sm.xz | awk -f ./rcpsp2py.awk - > ./data.py

BEGIN {
  H = 0;
  N = 0;
  p = 0;
  r = 0;
  d = "job_durations = [";
  c = 0;
}

/^jobs / {
  N = 0+$NF
}

/^horizon / {
  H = 0+$NF
  print "horizon = " H;
}

p>0 && $1 >= 1 && $1 <= N {
 if (p<N) printf(", ");
 printf("[");
 for (i=4; i<=NF; i++) {
   if (i>4) printf(", ");
   printf("%d", $i - 1); # jobs starts at 0 in python
 }
 printf("]");
 if (p==1) print "]";
 p--;
}

r>0 && $1 >= 1 && $1 <= N {
 if (r<N) {
   printf(", ");
   d = d ", ";
 }
 d = d "" $3;
 printf("[");
 for (i=4; i<=NF; i++) {
   if (i>4) printf(", ");
   printf("%d", $i);
 }
 printf("]");
 if (r==1) {
   print "]";
   d = d "]";
 }
 r--;
}

c && !/R/ {
 printf("capacities = [");
 for (i=1; i<=NF; i++) {
  if (i>1) printf(", ");
   printf("%d", $i);
 }
 print "]";
 exit;
}

/^PRECEDENCE RELATIONS:/ {
 p = N;
 printf("job_successors = [");
}

/^REQUESTS/ {
 r = N;
 printf("job_requirements = [");
}

/^RESOURCEAVAILABILITIES/ {
 c = 1;
}

END {
 print d;
}

