


    Resource-Constrained Project Scheduling Problem (RCPSP)

Original instances from http://www.om-db.wi.tum.de/psplib/library.html

Kolisch, R. and A. Sprecher (1996): 
PSPLIB - A project scheduling library, European Journal of Operational Research,
Vol. 96,
pp. 205--216.

Data files have been translated into python using scripts/rcpsp2py.awk. Each instance.py can be copied into scripts/data.py and solved using pytoulbar2 scripts/rcpsp.py.

