#include<cstdio>
#include<cstdlib>
#include<cmath>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <list>
#include <vector>

#define TOP 1000

using namespace std;

int **sol;

void genHeader(int n, ofstream& file) {
	file << "nonogram " << n*n << " " << 2 << " " << 2*n << " " << TOP << endl;
	for (int i = 0 ; i < n*n ; ++i) file << 2  << " "; file << endl;
}

void genSoftRegular(int n, string line, int position, vector<int> number, ofstream& file) {
	file << n << " ";
	for (int i = 0 ; i < n ; i++) {
		int pos_var = 0;
		if (line=="line") pos_var = n*position + i;
		else pos_var = position + i*n;
		file << pos_var << " ";
	}
	file << "-1 sregular var 1" << endl;
	
	int list_length = number.size();
	int nbStates = max(int(0),int(list_length - 1));
	for (int i : number) {
		nbStates += i;
	}
	++nbStates;
	file << nbStates << endl;
	file << 1 << " " << 0 << endl;
	file << 1 << " " << nbStates - 1 << endl;
	
	int nbTransitions = (nbStates - 1) + (list_length + 1);
	file << nbTransitions << endl;
	
	int position_state = 0;
	int position_list = 0;
	file << position_state << " " << 0 << " " << position_state << endl;
	for (int i : number) {
		position_list++;
		for (int current = 0 ; current < i ; ++current) {
			file << position_state << " " << 1 << " " << (position_state+1) << endl;
			position_state++;
		}
		if (position_list != list_length) {
			file << position_state << " " << 0 << " " << (position_state+1) << endl;
			position_state++;		
		}
		file << position_state << " " << 0 << " " << position_state << endl;
	}
}

void genSoftRegularDP(int n, string line, int position, vector<int> number, ofstream& file) {
	file << n << " ";
	for (int i = 0 ; i < n ; i++) {
		int pos_var = 0;
		if (line=="line") pos_var = n*position + i;
		else pos_var = position + i*n;
		file << pos_var << " ";
	}
	file << "-1 sregulardp var 1" << endl;
	
	int list_length = number.size();
	int nbStates = max(int(0),int(list_length - 1));
	for (int i : number) {
		nbStates += i;
	}
	++nbStates;
	file << nbStates << endl;
	file << 1 << " " << 0 << endl;
	file << 1 << " " << nbStates - 1 << endl;
	
	int nbTransitions = (nbStates - 1) + (list_length + 1);
	file << nbTransitions << endl;
	
	int position_state = 0;
	int position_list = 0;
	file << position_state << " " << 0 << " " << position_state << endl;
	for (int i : number) {
		position_list++;
		for (int current = 0 ; current < i ; ++current) {
			file << position_state << " " << 1 << " " << (position_state+1) << endl;
			position_state++;
		}
		if (position_list != list_length) {
			file << position_state << " " << 0 << " " << (position_state+1) << endl;
			position_state++;		
		}
		file << position_state << " " << 0 << " " << position_state << endl;
	}
}

void genWeightedRegular(int n, string line, int position, vector<int> number, ofstream& file) {
	file << n << " ";
	for (int i = 0 ; i < n ; i++) {
		int pos_var = 0;
		if (line=="line") pos_var = n*position + i;
		else pos_var = position + i*n;
		file << pos_var << " ";
	}
	file << "-1 wregular" << endl;
	
	int list_length = number.size();
	int nbStates = max(int(0),int(list_length - 1));
	for (int i : number) {
		nbStates += i;
	}
	++nbStates;
	file << nbStates << endl;
	file << 1 << " " << 0 << " " << 0 << endl;
	file << 1 << " " << nbStates - 1 << " " << 0 << endl;
	
	int nbTransitions = ((nbStates - 1) + (list_length + 1)) * 2;
	file << nbTransitions << endl;
	
	int position_state = 0;
	int position_list = 0;
	file << position_state << " " << 0 << " " << position_state << " " << 0 << endl;
	file << position_state << " " << 1 << " " << position_state << " " << 1 << endl;
	for (int i : number) {
		position_list++;
		for (int current = 0 ; current < i ; ++current) {
			file << position_state << " " << 1 << " " << (position_state+1) << " " << 0 << endl;
			file << position_state << " " << 0 << " " << (position_state+1) << " " << 1 << endl;
			position_state++;
		}
		if (position_list != list_length) {
			file << position_state << " " << 0 << " " << (position_state+1) << " " << 0 << endl;
			file << position_state << " " << 1 << " " << (position_state+1) << " " << 1 << endl;
			position_state++;		
		}
		file << position_state << " " << 0 << " " << position_state << " " << 0 << endl;
		file << position_state << " " << 1 << " " << position_state << " " << 1 << endl;
	}
}

int main(int argc, char **argv) {

	bool display = false;

	if (argc < 3 || argc > 4) {
		printf("Usage : nonogram nbLine seed\n");
		exit(0);
	}
	int seed = atoi(argv[2]);
	srand(seed);
	int n = atoi(argv[1]); 

	int **table = new int*[n];
	for (int i=0;i<n;i++) table[i] = new int[n];

	sol = new int*[n];
	for (int i=0;i<n;i++) sol[i] = new int[n];

	int nVar = 0;
	for (int i=0;i<n;i++) {
		for (int j=0;j<n;j++) {
			sol[i][j] = rand()%2;
			table[i][j] = nVar;
			nVar++;
		}
	}
	
	if (display)  {
	cout << "--------------------------" << endl;
	for (int i = 0; i < n ; i++) {
		for (int j = 0; j < n ; j++) {
			if (sol[i][j]) cout << "#"; else cout << ".";
		}
		cout << endl;
	}
	}
	
	///GENERATE LIST
	//line
	if (display)  cout << "--------------------------" << endl;
	vector< vector<int> > lines; lines.reserve(n);
	for (int i = 0 ; i < n ; i++) {
		vector<int> line; line.reserve(n);
		int current_length = 0;
		for (int j = 0 ; j < n ; j++) {
			if (sol[i][j]) current_length++;
			else {
				if (current_length != 0) {
					//if (display) cout << current_length << " ";
					line.push_back(current_length);
					current_length = 0;
				}
			}
		}
		if (current_length != 0) {
			//if (display)  cout << current_length;
			line.push_back(current_length);
		}
		//if (display) cout << endl;
		lines.push_back(line);
	}
	//cols
	//if (display)  cout << "--------------------------" << endl;
	vector< vector<int> > cols; cols.reserve(n);
	for (int i = 0 ; i < n ; i++) {
		vector<int> col; col.reserve(n);
		int current_length = 0;
		for (int j = 0 ; j < n ; j++) {
			if (sol[j][i]) current_length++;
			else {
				if (current_length != 0) {
					//if (display) cout << current_length << " ";
					col.push_back(current_length);
					current_length = 0;
				}
			}
		}
		if (current_length != 0) {
			//if (display)  cout << current_length;
			col.push_back(current_length);
		}
		//if (display) cout << endl;
		cols.push_back(col);
	}
	
	if (display) {
		for (vector<int>& line : lines) {
			for (int value : line) {
				cout << value << " ";
			}
			cout << endl;
		}
		cout << " ----------------- " << endl;
		for (vector<int>& col : cols) {
			for (int value : col) {
				cout << value << " ";
			}
			cout << endl;
		}
	}
	
	///CHAOS
	for (int i=0; i < n ; i++) {
		if (lines[i].size()) {
			random_shuffle(lines[i].begin(), lines[i].end());
		} else {
			int rand_val = rand() % n;
			if (rand_val) lines[i].push_back(rand_val);
		}
	}
	for (int i=0; i < n ; i++) {
		if (cols[i].size()) {
			random_shuffle(cols[i].begin(), cols[i].end());
		} else {
			int rand_val = rand() % n;
			if (rand_val) cols[i].push_back(rand_val);
		}
	}
	
	///GENERATE WCSP
	if (display)   cout << "--------------------------" << endl;
	string prefix = "nonogram_" +string(argv[1]) + "_" + string(argv[2]) + "_";
	//if (display)   cout << "prefix=" << prefix << endl;
	
	
	string filename_sr = prefix + "softregular.wcsp";
	if (display)  cout << "generate the SoftRegular file " << filename_sr << endl;
	
	ofstream file_sr(filename_sr.c_str());
	genHeader(n,file_sr);
	
	for (int i = 0 ; i < n ; i++) {
		//list<int> number = {2, 1};
		genSoftRegular(n,"line",i,lines[i], file_sr);
	}
	for (int i = 0 ; i < n ; i++) {
		//list<int> number = {};
		genSoftRegular(n,"col",i,cols[i], file_sr);
	}
	file_sr.close();
	
	string filename_srdp = prefix + "softregulardp.wcsp";
	if (display)  cout << "generate the SoftRegularDP file " << filename_sr << endl;
	
	ofstream file_srdp(filename_srdp.c_str());
	genHeader(n,file_srdp);
	
	for (int i = 0 ; i < n ; i++) {
		//list<int> number = {2, 1};
		genSoftRegularDP(n,"line",i,lines[i], file_srdp);
	}
	for (int i = 0 ; i < n ; i++) {
		//list<int> number = {};
		genSoftRegularDP(n,"col",i,cols[i], file_srdp);
	}
	file_srdp.close();
	
	string filename_wr = prefix + "weightedregular.wcsp";
	if (display)  cout << "generate the WeightedRegular file " << filename_wr << endl;
	ofstream file_wr(filename_wr.c_str());
	genHeader(n,file_wr);
	
	for (int i = 0 ; i < n ; i++) {
		//list<int> number = {2, 1};
		genWeightedRegular(n,"line",i,lines[i], file_wr);
	}
	for (int i = 0 ; i < n ; i++) {
		list<int> number = {};
		genWeightedRegular(n,"col",i,cols[i], file_wr);
	}
	
	file_wr.close();
	
	
	return 0;
}
	
