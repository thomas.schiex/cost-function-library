#!/bin/bash

MINCELL=$1
MAXCELL=$2
INCCELL=$3
TIMEOUT=$4
NBEXPE=$5

for i in `seq  $MINCELL $INCCELL $MAXCELL`; do
	for run in `seq 1 $NBEXPE`; do
		seed=$(($run+1000))
		echo  -en "\r$i\t$run        "
		echo  -en "$i\t$seed" >> res.exp
		
		### SOFT CLASSIC ###
		
		trace="trace/trace_nonogram_"$i"_"$seed"_softregular"		
		end=$(grep end. $trace | wc -l)
		#echo $end
		if [[ $end == 1 ]] ; then
			line=$(tail -n 2 $trace	| head -n 1)
			opt=$(echo $line | cut -d' ' -f2)
			bt=$(echo $line | cut -d' ' -f4)
			node=$(echo $line | cut -d' ' -f7)
			runtime=$(echo $line | cut -d' ' -f15)
			echo -en "\t$opt\t$bt\t$node\t$runtime" >> res.exp
		else 
			best=$(awk 'BEGIN{b=1000} /New solution: /{b=$3} END{printf("%d",b)}' $trace)
			echo -en "\t$best\t-\t-\t$TIMEOUT" >> res.exp
		fi
		
		
		### SOFT DP ###
		
		trace="trace/trace_nonogram_"$i"_"$seed"_softregulardp"
		end=$(grep end. $trace | wc -l)
		#echo $end
		if [[ $end == 1 ]] ; then
			line=$(tail -n 2 $trace	| head -n 1)
			opt=$(echo $line | cut -d' ' -f2)
			bt=$(echo $line | cut -d' ' -f4)
			node=$(echo $line | cut -d' ' -f7)
			runtime=$(echo $line | cut -d' ' -f15)
			echo -en "\t$opt\t$bt\t$node\t$runtime" >> res.exp
		else 
			best=$(awk 'BEGIN{b=1000} /New solution: /{b=$3} END{printf("%d",b)}' $trace)
			echo -en "\t$best\t-\t-\t$TIMEOUT" >> res.exp
		fi
		
		### WEIGHTED ###
		
		trace="trace/trace_nonogram_"$i"_"$seed"_weightedregular"
		end=$(grep end. $trace | wc -l)
		#echo $end
		if [[ $end == 1 ]] ; then
			line=$(tail -n 2 $trace	| head -n 1)
			opt=$(echo $line | cut -d' ' -f2)
			bt=$(echo $line | cut -d' ' -f4)
			node=$(echo $line | cut -d' ' -f7)
			runtime=$(echo $line | cut -d' ' -f15)
			echo -en "\t$opt\t$bt\t$node\t$runtime" >> res.exp
		else 
			best=$(awk 'BEGIN{b=1000} /New solution: /{b=$3} END{printf("%d",b)}' $trace)
			echo -en "\t$best\t-\t-\t$TIMEOUT" >> res.exp
		fi
		
		echo "" >> res.exp
	done
done

echo -e "\r"

for i in `seq  $MINCELL $INCCELL $MAXCELL`; do

	echo -en "$i\t|\t"
	
	nbline=$((1+($NBEXPE*(($i-$MINCELL)+1))))
	#if [[ $i -eq 0 ]] ; then start_line=0; fi
	head -n $nbline res.exp | tail -n $NBEXPE > temp.extract
	
	
		
	best_SR=$(cat temp.extract | awk 'BEGIN{sum=0;n=0} {sum+=$3; n++} END {if (n>0) print sum/n; else print "-"}')
	bt_SR=$(cat temp.extract | awk 'BEGIN{sum=0;n=0} { if ($4!="-") {sum+=$4; n++}} END {if (n>0) print sum/n; else print "-"}')
#	time_SR=$(cat temp.extract | awk 'BEGIN{sum=0} { sum+=$6} END {print sum/'$NBEXPE'}')
	time_SR=$(cat temp.extract | awk 'BEGIN{sum=0;n=0} { if ($4!="-") {sum+=$6; n++}} END {if (n>0) print sum/n; else print "-"}')
	success_SR=$(cat temp.extract | awk 'BEGIN{sum=0} { if ($4!="-") { sum+=1} } END { print sum }')
	echo -en "$success_SR\t"
	echo -en "$best_SR\t"
	echo -en "$bt_SR\t"
	echo -en "$time_SR"
	echo -en "\t|\t"
	
	best_DP=$(cat temp.extract | awk 'BEGIN{sum=0;n=0} {sum+=$7; n++} END {if (n>0) print sum/n; else print "-"}')
	bt_DP=$(cat temp.extract | awk 'BEGIN{sum=0;n=0} {if ($8!="-") {sum+=$8; n++}} END {if (n>0) print sum/n; else print "-"}')
#	time_DP=$(cat temp.extract | awk 'BEGIN{sum=0} { sum+=$10} END {print sum/'$NBEXPE'}')
	time_DP=$(cat temp.extract | awk 'BEGIN{sum=0;n=0} { if ($8!="-") {sum+=$10; n++}} END {if (n>0) print sum/n; else print "-"}')
	success_DP=$(cat temp.extract | awk 'BEGIN{sum=0} { if ($8!="-") { sum+=1} } END { print sum }')
	echo -en "$success_DP\t"
	echo -en "$best_DP\t"
	echo -en "$bt_DP\t"
	echo -en "$time_DP"
	echo -en "\t|\t"
	
	best_WR=$(cat temp.extract | awk 'BEGIN{sum=0;n=0} {sum+=$11; n++} END {if (n>0) print sum/n; else print "-"}')
	bt_WR=$(cat temp.extract | awk 'BEGIN{sum=0;n=0} {if ($12!="-") {sum+=$12; n++}} END {if (n>0) print sum/n; else print "-"}')
#	time_WR=$(cat temp.extract | awk 'BEGIN{sum=0} { sum+=$14} END {print sum/'$NBEXPE'}')
	time_WR=$(cat temp.extract | awk 'BEGIN{sum=0;n=0} { if ($12!="-") {sum+=$14; n++}} END {if (n>0) print sum/n; else print "-"}')
	success_WR=$(cat temp.extract | awk 'BEGIN{sum=0} { if ($12!="-") { sum+=1} } END { print sum }')
	echo -en "$success_WR\t"
	echo -en "$best_WR\t"
	echo -en "$bt_WR\t"
	echo -en "$time_WR"
	echo -en "\t|\t"
	
	
	echo ""
	
	rm temp.extract
done

mv res.exp results.txt
