
# a faire: ajouter symetrie breaking pour x_i <= max(x_(j<i))+1

BEGIN {
	C = 1000000;
	top = 100000000000;
	k = ARGV[2];
	ARGV[2] = "";
}

NF==2 && $1 != $2 &&  (!(($1 ":" $2) in edges)){
	if ($1>n) n = $1;
	if ($2>n) n = $2;
	edge[$1 ":" $2] = 1;
	edge[$2 ":" $1] = 1;
	degree[$1]++;
	degree[$2]++;
	e++;
}

END {
    for (i=1;i<=n;i++) {
		for (j=i+1;j<=n;j++) {
				modularite[i ":" j] = (edge[i ":" j] - ((degree[i]*degree[j])/(2*e)))/e;
#				print i,j,modularite[i ":" j];
				if (modularite[i ":" j] > maxm) maxm = modularite[i ":" j];
				if (modularite[i ":" j] < minm) minm = modularite[i ":" j];
		}
	}
#	k=n;
	print "Newman",n,k,n*(n-1)/2+k,top;
	for (i=0;i<n;i++) printf("%d ",k);
	print "";
	
    for (i=1;i<=k;i++) {
		print 1,i-1,top,i;
		for (v=0;v<i;v++) {
			print v,0;
		}
	}
    for (i=1;i<=n;i++) {
		for (j=i+1;j<=n;j++) {
			if (modularite[i ":" j]>0) {
				print 2,i-1,j-1,int(C*modularite[i ":" j]),k;
				for (v=0;v<k;v++) {
					print v,v,0;
				}
			} else {
				print 2,i-1,j-1,0,k;
				for (v=0;v<k;v++) {
					print v,v,int(-C*modularite[i ":" j]);
				}
			}
		}
	}
}
