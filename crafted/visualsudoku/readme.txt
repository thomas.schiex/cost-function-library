Visual Sudoku

toulbar2_visual_sudoku_puzzle.py written by Simon de Givry (@ INRAE, 2022),
adapted from a tutorial by Adrian Rosebrock (@ PyImageSearch, 2022),
in order to use toulbar2 solver.

Usage (in case of digit_classifier.h5) :

>cd scripts
>python ./train_digit_classifier.py --model ../data/digit_classifier.h5
>python ./toulbar2_visual_sudoku_puzzle.py -m ../data/digit_classifier.h5 -i ../data/sudoku.jpg -o solution.jpg -k 40 -b 15 -t 5

Options:
-t toulbar2 CPU time limit
-k during image processing, in each cell, keep a given percentage of the image in the center of the cell untouched by border filtering operations (useful when digits overlap cell boundaries)
-b enlarge cell dimension by a given percentage (also useful when digits overlap cell boundaries)
-d 1 debug mode (show various image processing steps)

To see this tutorial :
https://pyimagesearch.com/2020/08/10/opencv-sudoku-solver-and-ocr

Models history :
- data/digit_classifier.h5
- data/mixed_classifier.h5 : improvements, Model for both MNIST & font digit

Required, to run into a virtual python environment : see requirements.txt

