#/bin/csh -f

# elim-dup.csh
# Author: Allen Van Gelder, UC Santa Cruz.
# After saving the file, remember to do "chmod a+x elim-dup.csh" to
# make it executable.
# This script makes 3 passes on the input to avoid using temporary files.

if ($#argv != 2) then
	echo Usage: $0 in.col out.col
	echo '	' out.col may be '"-"' for stdout.
	echo '	' out.col will have no duplicate edges.
	echo '	' out.col will be be undirected: e x y means '(x,y) and (y,x)'.
	exit $#argv
	endif

set inf = $1
set outf = $2

if (x$inf == x$outf) then
	echo Infile and outfile may not be the same for $0.
	exit 1
	endif

# ecnt is number of unique undirected edges in in.col.
set ecnt = `cat $inf | awk '/^e/{x=$2+0;y=$3+0;if(x<y)print $1,x,y;if(y<x)print $1,y,x;}' | sort -u | wc -l`

# write correct preamble.
cat $inf |\
  awk '/^c/{print;next;} /^p/{printf "%s %s %s ", $1, $2, $3;exit;}' |\
  cat > $outf
echo $ecnt >> $outf

# write unique undirected edges.
cat $inf |\
  awk '/^e/{x=$2+0;y=$3+0;if(x<y)print $1,x,y;if(y<x)print $1,y,x;}' |\
  sort -u |\
  sort -n +1 -n +2 >> $outf
