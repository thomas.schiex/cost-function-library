
# elim-dup.csh problem.col problem_nodup.col
# awk -f problem_nodup.col <numberofcolor> > problem.wcsp

BEGIN {
    D = ARGV[2];
    ARGV[2] = "";
}

/^p / {
    N = $3;
    C = $4;
    print FILENAME,N,D,C,C+1;
    printf("%d",D);
    for (i=1;i<N;i++) printf(" %d", D);
    print "";
}

/^e / {
    print 2,$2 - 1,$3 - 1,0,D;
    for (i=0;i<D;i++) print i,i,1;
}

   
