Graph coloring instances cast into Minimum Coloring instances.

The instances (*.col DIMACS format) come from COLOR02/03/04
Graph Coloring and its Generalizations web site:
   http://mat.gsia.cmu.edu/COLORING02/benchmarks

---
Useful scripts:

elim-dup.csh removes duplicate edges

color2wcsp.awk transforms a graph coloring instance in DIMACS format into a minimum coloring instance in wcsp format
