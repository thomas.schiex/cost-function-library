Organisation des benchmarks dans /trunk.

les benchmarks sont organisés de la facons suivante:

Nous avons 3 categories de Benchmark: 
	-crafted
	-random
	-real

Dans chaques categories, nous avons des sous categories de benchmarks:
	-crafted :academics;coloring;kbtree;maxclique;maxcut;maxone;planning;warehouse;auction
	-random  :jnh;Maxcsp;randomksat;translators;generators;dimacs
	-real    :ergo;protein;pedigree;spot;celar;tagsnp

Pour chaque dossier racine de benchmarks, nous pouvons observer:

	-un fichier nommé « readme.txt », il definit chaques benchmarks;
	-un fichier instances.txt contient une liste des instances du benchmark ainsi que : le nbre de variable, la taille de domaine max, nbre de contraintes, arité max, ainsi que 		upper bound.


	-un répertoire nommé « data ». Il contient les données brutes des problèmes:
		ce même répertoire contient ensuite des répertoires classés selon les extensions de 		
		fichiers rencontrés. Les extension rencontrés pour les répertoires « data » sont
			→ .ds ; .txt ; .dat; .col; .asc; .sat; .spot

	-un répertoire nommé « instances ». Il contient les solvers:
		ce même répertoire contient ensuite des répertoires classés selon les extensions de 		
		fichiers rencontrés. Les extension rencontrés pour les répertoires « instances » sont
			→ .wcsp (qui sont groupés avec les .ub) ; .cnf (qui sont groupés avec les .lb);.wcnf; .erg; .cp; .lb; .ub; .ergo

	-un répertoire nommé « scripts ». Il contient les scripts de conversions de format:
		ce même répertoire contient ensuite des répertoires classés selon les extensions de 		
		fichiers rencontrés. Les extension rencontrés pour les répertoires « scripts  » sont
			→ .awk; .sh; .c; .ps; .h; .py; .csh

	-un répertoire nommé « solution ». Il contient les scripts de conversions de format:
		ce même répertoire contient ensuite des répertoires classés selon les extensions de 		
		fichiers rencontrés. Les extension rencontrés pour les répertoires « solution » sont
			→ .sol; 

	Ces repertoires n'existent que s'ils contiennent les extensions qui les concernent dans chaque benchmark.

