
Quadratic Assignment Problem Library (QAPLIB)

See https://www.opt.math.tugraz.at/qaplib/inst.html

or https://coral.ise.lehigh.edu/data-sets/qaplib/

Script qap2wcsp.awk generates .wcsp files with domain variables of size N and binary cost functions encoding the objective and the permutation constraint (with ALLDIFF=0).
Additionnal linear inequality (at most one) / equality (exactly one) / inequality (at least one) constraints per value can be added with ALLDIFF=1/ALLDIFF=2/ALLDIFF=3. We used ALLDIFF=3 to generate kpge.wcsp instances. 

Script qap2opb.awk generates .opb files (converted into .lp format by scip -c "read file.opb write problem file.lp quit") based on QAP mixed integer programming model of

Frieze, A.M., Yadegar, J., 1983. On the quadratic assignment problem. Discrete Applied Mathematics 5, 89–98.

as described in Eq.(2.7)-(2.16) in Loiola et al., EJOR 2007 (doi:10.1016/j.ejor.2005.09.032)

Warning! opb format made all variables 0/1 whereas y_ijkp can be continuous (integrality property)

