
BEGIN {
	RS = "@";
}

{
	N = $1;
	printf("min:");
	pos = 2;
	for (i=0;i<N;i++) {
		for (j=0;j<N;j++) {
			A[i,j] = $pos;
#			print pos,i,j,A[i,j];
			pos++;
		}
	}
	for (i=0;i<N;i++) {
		for (j=0;j<N;j++) {
			B[i,j] = $pos;
#			print pos,i,j,B[i,j];
			pos++;
		}
	}
	for (i=0;i<N;i++) {
		for (k=0;k<N;k++) {
			printf(" +0 x%d_%d",i,k); # ensure domain variables xik first in variable ordering
		}
	}
	for (i=0;i<N;i++) {
		for (j=0;j<N;j++) {
			for (k=0;k<N;k++) {
				for (p=0;p<N;p++) {
					if (A[i,j]*B[k,p] > 0) {
						printf(" +%d y%d_%d_%d_%d", A[i,j]*B[k,p], i, j, k, p);
					} else if (A[i,j]*B[k,p] < 0) {
						printf(" -%d y%d_%d_%d_%d", -A[i,j]*B[k,p], i, j, k, p);
					}
				}
			}
		}
	}
	print ";";
	for (k=0;k<N;k++) {
		for (i=0;i<N;i++) {
			printf(" +1 x%d_%d",i,k);
		}
		print " = 1;";
	}
	for (i=0;i<N;i++) {
		for (k=0;k<N;k++) {
			printf(" +1 x%d_%d",i,k);
		}
		print " = 1;";
	}
	for (j=0;j<N;j++) {
		for (k=0;k<N;k++) {
			for (p=0;p<N;p++) {
				for (i=0;i<N;i++) {
					printf(" +1 y%d_%d_%d_%d",i,j,k,p);
				}
				print " -1 x" j "_" p " = 0;";
			}
		}
	}
	for (i=0;i<N;i++) {
		for (k=0;k<N;k++) {
			for (p=0;p<N;p++) {
				for (j=0;j<N;j++) {
					printf(" +1 y%d_%d_%d_%d",i,j,k,p);
				}
				print " -1 x" i "_" k " = 0;";
			}
		}
	}
	for (i=0;i<N;i++) {
		for (j=0;j<N;j++) {
			for (p=0;p<N;p++) {
				for (k=0;k<N;k++) {
					printf(" +1 y%d_%d_%d_%d",i,j,k,p);
				}
				print " -1 x" j "_" p " = 0;";
			}
		}
	}
	for (i=0;i<N;i++) {
		for (j=0;j<N;j++) {
			for (k=0;k<N;k++) {
				for (p=0;p<N;p++) {
					printf(" +1 y%d_%d_%d_%d",i,j,k,p);
				}
				print " -1 x" i "_" k " = 0;";
			}
		}
	}
	for (i=0;i<N;i++) {
		for (k=0;k<N;k++) {
			printf(" +1 y%d_%d_%d_%d -1 x%d_%d = 0;\n", i, i, k, k, i, k);
		}
	}
}

