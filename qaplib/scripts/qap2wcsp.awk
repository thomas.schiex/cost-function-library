function abs(v) {v += 0; return v < 0 ? -v : v}

BEGIN {
	RS = "@";
	top = 1000000000;
	ALLDIFF = 0;
}

{
	N = $1;
	print FILENAME,N,N,N*(N-1)/2+N+((ALLDIFF==0)?0:1),top;
	pos = 2;
	for (i=0;i<N;i++) {
		for (j=0;j<N;j++) {
			A[i,j] = $pos;
#			print pos,i,j,A[i,j];
			pos++;
		}
	}
	for (i=0;i<N;i++) {
		for (j=0;j<N;j++) {
			B[i,j] = $pos;
#			print pos,i,j,B[i,j];
			pos++;
		}
	}
	for (i=0;i<N;i++) {
		printf(" %d",N);
	}
	print "";
	for (i=0;i<N;i++) {
		for (j=i+1;j<N;j++) {
			print 2,i,j,0,N*N;
			for (u=0;u<N;u++) {
				for (v=0;v<N;v++) {
					if (u==v) print u,v,((ALLDIFF >= 0)?top:0);
					else print u,v,A[i,j]*B[u,v] + A[j,i]*B[v,u];
				}
			}
		}
	}
	for (i=0;i<N;i++) {
		print 1,i,0,N;
		for (u=0;u<N;u++) {
			print u,A[i,i]*B[u,u];
		}
	}
	if (ALLDIFF) {
		printf("%d",N);
		for (i=0;i<N;i++) {
			printf(" %d",i);
		}
		if (abs(ALLDIFF)==1) print "",-1,"salldiffkp","hard",top;
		else if (abs(ALLDIFF)==2) print "",-1,"salldiffkp","hardeq",top;
		else print "",-1,"salldiffkp","hardge",top;
#		print "",-1,"salldiffdp","var",top;
#		print "",-1,"salldiff","var",top;
#		print "",-1,"walldifferent","lin",top;
	}
}
