#! /usr/bin/tcsh

# Translate the SPOT benchmark in wcsp format

# where is libcp.awk
setenv AWKPATH .

foreach f (../spot5/*.spot)
    awk -f spot2cp.awk $f >! ${f:r}.cp
    awk -f cp2wcsp.awk ${f:r}.cp >! ${f:r}.wcsp
end
