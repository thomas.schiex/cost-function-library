#!/usr/bin/env python

import os
import sys
import itertools

assert len(sys.argv) == 3, "Please specify INPUT and OUTPUT filenames."


class WidthFile(file):
    maxcol = 80

    def __init__(self, *x, **k):
        file.__init__(self, *x, **k)
        self.col = 0

    def write(self, x):
        lines = x.splitlines()
        #print "outputting", lines
        if (self.col + len(lines[0])) >= 80:
            file.write(self, "\n")
            self.col = 0
        map(lambda x: file.write(self, x + '\n'), lines[:-1])
        file.write(self, lines[-1])
        if len(lines) > 1:
            self.col = len(lines[-1])
        else:
            self.col += len(lines[-1])


def domain_var(n, v):
    return "d%i_%i" % (n, v)


def tuple_var(n1, v1, n2, v2):
    if (n1 > n2):
        nt,vt = n1,v1
        n1,v1 = n2,v2
        n2,v2 = nt,vt
    return "t%i_%i_%i_%i" % (n1, v1, n2, v2)


def read_num_vec(lit):
    return map(int, line_iter.next().strip().split(" "))

line_iter = open(sys.argv[1]).xreadlines()
#output = open(sys.argv[2], 'w')
output = WidthFile(sys.argv[2], 'w')

name, n_var, max_domain_size, n_fun, upper_bound = (line_iter.next()
                                                    .strip().split(" "))
domains = read_num_vec(line_iter)
n_fun = int(n_fun)
ub = int(upper_bound)

def read_fun(lit):
    stuff = read_num_vec(line_iter)
    n_var = stuff[0]
    vars_ = stuff[1:1 + n_var]
    defcost = stuff[-2]
    n_spec = stuff[-1]
    specs_vec = [read_num_vec(line_iter) for i in xrange(n_spec)]
    specs = dict((tuple(v[:-1]), v[-1]) for v in specs_vec)
    return vars_, defcost, specs


def iter_fun(vars_, defcost, specs):
    vardom = [xrange(domains[v]) for v in vars_]
    for t in itertools.product(*vardom):
        if t in specs:
            yield t, specs[t]
        else:
            yield t, defcost

print >> output, "Minimize"

#groBe_expr = []

all_fun = [read_fun(line_iter) for i in xrange(n_fun)]

for vars_, defcost, specs in all_fun:
    n_vars = len(vars_)
    for t, cost in iter_fun(vars_, defcost, specs):
        if cost == 0 or cost == ub:
            continue
        if n_vars == 0:
            output.write(' +%i ' % cost)
        elif n_vars == 1:
            output.write(' +%i %s ' % (cost, domain_var(vars_[0], t[0])))
        else:
            assert n_vars == 2, "Binary function expected!"
            output.write(' +%i %s ' % (cost, tuple_var(vars_[0], t[0],
                vars_[1], t[1])))

output.write("\n\nSubject to:\n\n")

for vars_, defcost, specs in all_fun:
    n_vars = len(vars_)
    for t, cost in iter_fun(vars_, defcost, specs):
        if cost != ub:
            continue
        if n_vars == 1:
            output.write('+1 %s = 0\n\n' %  domain_var(vars_[0], t[0]))
        else:
            assert n_vars == 2, "Binary function expected!"
            output.write('+1 %s = 0\n\n' %  tuple_var(vars_[0], t[0], vars_[1], t[1]))


for i, dom in enumerate(domains):
    map(lambda v: output.write("+1 %s " % domain_var(i, v)), xrange(dom))
    output.write("= 1\n\n")


for vars_, defcost, specs in (f for f in all_fun if len(f[0]) == 2):
    for va, vb in itertools.permutations(vars_):
        for a in xrange(domains[va]):
            map(lambda b: output.write("+1 %s " % tuple_var(va, a, vb, b)),
                xrange(domains[vb]))
            output.write("-1 %s = 0\n\n" % domain_var(va, a))

output.write("\n\nBounds\n\n")

for vars_, defcost, specs in (f for f in all_fun if len(f[0]) == 2):
    va, vb = vars_
    for a in xrange(domains[va]):
        map(lambda b: output.write("%s <= 1\n\n" % tuple_var(va, a, vb, b)),
            xrange(domains[vb]))

output.write("\n\nBinary\n\n")

for i, dom in enumerate(domains):
    map(lambda v: output.write("%s " % domain_var(i, v)), xrange(dom))


output.write("\n\nEnd")

#
