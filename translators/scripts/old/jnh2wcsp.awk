
# Translate in wcsp format from a JNH problem in sat format (with non-unit clause weights)

# See the file benchs/jnh/read.me for a description of the sat format

# Usage: awk -f jnh2wcsp.awk file.sat > file.wcsp

BEGIN {
  RS = "@";
}

{
  nbvar = $1;
  nbctr = $2;
  infinity = 0;
  pos = 3;
  for (c=0; c<nbctr; c++) {
    arity = $pos;
    infinity += $(pos+1);
    pos += arity + 2;
  }
  infinity++;
  print FILENAME,nbvar,2,nbctr,infinity;
  printf("2");
  for (i=1; i<nbvar; i++) {
    printf(" 2");
  }
  print "";
  pos = 3;
  for (c=0; c<nbctr; c++) {
    arity = $pos;
    printf("%d", arity);
    for (i=1; i<=arity; i++) {
	if ($(pos+i+1) < 0) printf(" %d", - $(pos+i+1) - 1);
	else printf(" %d", $(pos+i+1) - 1);
    }
    print " 0 1";
    for (i=1; i<=arity; i++) {
	if ($(pos+i+1) < 0) printf("1 ");
	else printf("0 ");
    }
    print $(pos+1);
    pos += arity + 2;
  } 
}
