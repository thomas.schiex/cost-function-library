
# Translate in wcsp format from an original SPOT instance

# Usage: awk -f spot2cp.awk 8.spot > 8.cp

BEGIN {
    nbv = 0;
    totalnbv = 0;
    totalnbc = 0;
    nbc = 0;
    isvariables = 0;
    isconstraints = 0;
}

isconstraints && nbc < totalnbc && NF >= $1 * 2 + 1 {
    nbc++;
    for (i=2; i <= $1 + 1; i++) {
	printf("x%d ", $i);
    }
    print "0";
    pos = $1 + 2;
    for (t=1; t <= (NF - $1 - 1) / $1; t++) {
	for (i=1; i <= $1; i++) {
	    printf("%d ", $pos);
	    pos++;
	}
	print -1;
    }
}

isvariables && nbv == totalnbv {
    isconstraints = 1;
    isvariables = 0;
    totalnbc = $1;
}

isvariables && nbv < totalnbv {
    nbv++;
    printf("x%d", $1);
    vars[nbv] = $1;
    costs[nbv] = $2;
    for (i=4; i<=NF; i+=2) {
	printf(" %d", $i);
    }
    print " -1";
}

NR == 1 {
    print FILENAME;
    totalnbv = $1;
    isvariables = 1;
}

END {
    # soft unary constraints
    for (i=1; i <= totalnbv; i++) {
	print "soft(" costs[i] ", x" vars[i] " != -1)";
    }
}
