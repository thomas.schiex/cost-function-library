#! /bin/tcsh
awk 'FNR==1{print "c PSEUDOBOOLEAN",$2,$1; print "p wcnf",$2,$2 + $2 * ($2 - 1) / 2 - $1,$2+1; n=$2; for(i=1;i<=n;i++)print 1,i,0} FNR>1{graph[0+$1 "_" 0+$2]=1;graph[0+$2 "_" 0+$1]=1} END{for(i=1;i<=n;i++){for(j=i+1;j<=n;j++){if (!((i "_" j) in graph)) {print n+1,-i,-j,0}}}}' $1 >! $1.wcnf
