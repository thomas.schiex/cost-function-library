BEGIN {
    lb = 0;
}

/numatom = / {
    gsub(",","");
    sum = $6;
}

NF == 7 && $6 == 100000 {
    if (sum - $1 > lb) lb = sum - $1;
}

NF == 9 && $1 == 0 {
    lb = sum;
}

END {
    print lb;
}
