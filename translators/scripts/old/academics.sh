#! /usr/bin/tcsh

# Generate some academic benchmarks in wcsp format

# where is libcp.awk
setenv AWKPATH .

foreach f (../academics/*.cp ../academics/*/*.cp)
    awk -f cp2wcsp.awk $f >! ${f:r}.wcsp
end
