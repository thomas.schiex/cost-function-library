#! /usr/bin/tcsh

# Generate all benchmarks in ds format from wcsp format

foreach f (../*/*.wcsp)
    echo "$f"
    awk -f wcsp2ds.awk $f >! ${f:r}.ds
end
