
# awk -f subproblem.awk file_with_a_list_of_variable_numbers problem.wcsp 

BEGIN {
    RS = "@"; # special character that should never appear in the wcsp file
    nbconstr = 0;
    nbvar = 0;
    max = -1;
    output = "";
}

FNR==NR {
  for (i=1; i<=NF; i++) {
    varsubset[$i] = nbvar;
    invvarsubset[nbvar] = $i;
    if ($i > max) max = $i;
    nbvar++;
  }
}

FNR!=NR {
  pbname = $1;
  nbvartot = $2;
  if (max >= nbvartot) {
    print "Error: selected variables not in the problem!";
    exit(1);
  }
  maxdomsize = $3;
  nbconstrtot = $4;
  ub = $5;
  pos = 5 + $2 + 1;
  for (c=1; c<=nbconstrtot; c++) {
    arity = $pos;
    defval = $(pos + arity + 1);
    nbtuples = $(pos + arity + 2);
    ok = 1;
    for (i=1; i<=arity; i++) {
      if (!($(pos+i) in varsubset)) {
	ok = 0;
	break;
      }
    }
    if (ok) {
      nbconstr++;
    }
    pos += arity + 3 + nbtuples * (arity+1);
  }

  print pbname, nbvar, maxdomsize, nbconstr, ub;
  printf("%d", $(6+invvarsubset[0]));
  for (i=1; i<nbvar; i++) {
    printf(" %d", $(6+invvarsubset[i]));
  }
  print "";
  pos = 5 + $2 + 1;
  for (c=1; c<=nbconstrtot; c++) {
    arity = $pos;
    defval = $(pos + arity + 1);
    nbtuples = $(pos + arity + 2);
    ok = 1;
    for (i=1; i<=arity; i++) {
      if (!($(pos+i) in varsubset)) {
	ok = 0;
	break;
      }
    }
    if (ok) {
      printf("%d", arity);
      for (i=1; i<=arity; i++) {
	printf(" %d", varsubset[$(pos+i)]);
      }
      print " " defval, nbtuples;
      for (t=0; t<nbtuples; t++) {
	printf("%d", $(pos + arity + 3 + t * (arity+1)));
	for (i=1; i<arity; i++) {
	  printf(" %d", $(pos + arity + 3 + t * (arity+1) + i));
	}
	print " " $(pos + arity + 3 + t * (arity+1) + arity);
      }
    }
    pos += arity + 3 + nbtuples * (arity+1);
  }
}
