#!/usr/bin/env python

import os
import sys
import itertools

assert len(sys.argv) == 3, "Please specify INPUT and OUTPUT filenames."


# classe pour écriture à largeur de texte contrôlée
class WidthFile(file):
    maxcol = 80

    def __init__(self, *x, **k):
        file.__init__(self, *x, **k)
        self.col = 0

    def write(self, x):
        lines = x.splitlines()
        #print "outputting", lines
        if (self.col + len(lines[0])) >= 80:
            file.write(self, "\n")
            self.col = 0
        map(lambda x: file.write(self, x + '\n'), lines[:-1])
        file.write(self, lines[-1])
        if len(lines) > 1:
            self.col = len(lines[-1])
        else:
            self.col += len(lines[-1])

# le nom des variables pour l'encodage des valeurs des domaines
def domain_var(n, v):
    return "d%i_%i" % (n, v)

# le nom des variables pour l'encodage des autres tuples
def tuple_var(tvar,tval):
    tvarval = map(lambda var,val: (var,val),tvar,tval)
    # normalize tuple
    st = sorted(tvarval, key=lambda x: x[0])
    name = "t"
    for x in st:
        name = name + ("_%i_%i" % x)
    return name

#le produit cartésien des séquences (stockées dans une séquence vlist).
def product(vlist):
    return apply(itertools.product,vlist)

#enumerate all "tuples" on tvar (for var, if it appears in tvar, a
#single value val is used instead of thh full domain) generating the
#set of support tuples.
def enum_tuples(tvar, var, val):
    return product(map(lambda ovar: [val] if (var == ovar) else xrange(domains[ovar]), tvar))

# reading numbers
def read_num_vec(lit):
    return map(int, line_iter.next().strip().split(" "))

# lire une définition de cost function. The cost table is a tuple based dictionary
def read_fun(lit):
    stuff = read_num_vec(line_iter)
    n_var = stuff[0]
    vars_ = stuff[1:1 + n_var]
    defcost = stuff[-2]
    n_spec = stuff[-1]
    specs_vec = [read_num_vec(line_iter) for i in xrange(n_spec)]
    specs = dict((tuple(v[:-1]), v[-1]) for v in specs_vec)
    return vars_, defcost, specs

# parcourir une cost function table
def iter_fun(vars_, defcost, specs):
    vardom = [xrange(domains[v]) for v in vars_]
    for t in itertools.product(*vardom):
        if t in specs:
            yield t, specs[t]
        else:
            yield t, defcost

# -------------  MAIN ---------------------

line_iter = open(sys.argv[1]).xreadlines()
output = WidthFile(sys.argv[2], 'w')

# reading parameters
name, n_var, max_domain_size, n_fun, upper_bound = (line_iter.next()
                                                    .strip().split(" "))
domains = read_num_vec(line_iter)
n_fun = int(n_fun)
ub = int(upper_bound)
print >> output, "Minimize"

all_fun = [read_fun(line_iter) for i in xrange(n_fun)]

# Output the criteria. Do not integrate zero or "infinite" cost
# components here. Zero is useless, "infinite" will be handled as
# linear constraints
for vars_, defcost, specs in all_fun:
    n_vars = len(vars_)
    for t, cost in iter_fun(vars_, defcost, specs):
        if cost == 0 or cost == ub:
            continue
        if n_vars == 0:
            output.write(' +%i ' % cost)
        elif n_vars == 1:
            output.write(' +%i %s ' % (cost, domain_var(vars_[0], t[0])))
        else:
            output.write(' +%i %s ' % (cost, tuple_var(vars, t)))

output.write("\n\nSubject to:\n\n")

# Hard constraints: for every value/tuple with cost >=ub, we forbid it
# explicitely.
for vars_, defcost, specs in all_fun:
    n_vars = len(vars_)
    for t, cost in iter_fun(vars_, defcost, specs):
        if cost < ub:
            continue
        if n_vars == 1:
            output.write('+1 %s = 0\n\n' %  domain_var(vars_[0], t[0]))
        else:
            output.write('+1 %s = 0\n\n' %  tuple_var(vars_, t))

# Direct encoding. Exactly one value constraint.
for i, dom in enumerate(domains):
    map(lambda v: output.write("+1 %s " % domain_var(i, v)), xrange(dom))
    output.write("= 1\n\n")

# marginal consistency: one value selected iff one associated tuple selected.
for vars_, defcost, specs in (f for f in all_fun if len(f[0]) >= 2):
    for va in vars_:
        for a in xrange(domains[va]):
            map(lambda b: output.write("+1 %s " % tuple_var(vars_, b)), enum_tuples(vars_,va,a))
            output.write("-1 %s = 0\n\n" % domain_var(va, a))

output.write("\n\nBounds\n\n")

# bound tuple variables to [0,1]
for vars_, defcost, specs in (f for f in all_fun if len(f[0]) >= 2):
    map(lambda b: output.write("%s <= 1\n\n" % tuple_var(vars_, b)),enum_tuples(vars_,-1,-1)) 

output.write("\n\nBinary\n\n")

# indicate 0/1 variables (direct encoding).
for i, dom in enumerate(domains):
    map(lambda v: output.write("%s " % domain_var(i, v)), xrange(dom))

output.write("\n\nEnd")
