BEGIN {
	N = 0;
	TOP = 51240955760304310;
}

/^*/ && /#variable=/ {
	N = $3;
	print "{\"problem\": {\"name\":\"" FILENAME "\", \"mustbe\": \"<" TOP "\"},";
	printf "\"variables\": {";
	for (i=1; i<N; i++) {
		printf "\"x" i "\": 2, ";   
	}
	print "\"x" N "\": 2},";
	print "\"functions\": {";
}

!/^*/ &&  !/min:/ {
	sub(" *; *$","",$0);
	sub("=","= ",$0);
	printf "{\"scope\": [";
	for (i=2; i<=NF-4; i+=2) {
		printf "\"" $i "\", ";
	}
	printf "\"" $i "\"], ";
	printf "\"type\": \"knapsack\", ";
	printf "\"params\": {\"capacity\": "
	printf 0+$NF ", \"weights\": [";
	for (i=1; i<=NF-4; i+=2) {
		printf 0+$i ", ";
	}
	printf 0+$i;
	print "]}},";	
	if ($(NF-1) == "=") {
		printf "{\"scope\": [";
		for (i=2; i<=NF-4; i+=2) {
			printf "\"" $i "\", ";
		}
		printf "\"" $i "\"], ";
		printf "\"type\": \"knapsack\", ";
		printf "\"params\": {\"capacity\": "
		printf 0-$NF ", \"weights\": [";
		for (i=1; i<=NF-4; i+=2) {
			printf 0-$i ", ";
		}
		printf 0-$i;
		print "]}},";	
	}
}

!/^*/ && /min:/ {
	sub("^ *min: *","",$0);
	sub(" *; *$","",$0);
	if (NF/2 > N) {
		print "Bad number of variables!",NF/2,N;
		exit(-1);
	}
	for (i=1; i<=NF; i+=2) {
		print "{\"scope\": [\"" $(i+1) "\"], \"defaultcost\": 0, \"costs\": [1, " 0+$i "]},";
	}
}

END {
	print "}";
}

