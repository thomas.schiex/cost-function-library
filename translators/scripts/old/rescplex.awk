# usage: runcplex.sh *.mps | awk -f rescplex.awk

# for benchmarking, cplex output filtering

# for each problem, returns:
# filename optimum number_of_nodes cpu_time_in_seconds

BEGIN {
    opt = "-";
    name = "";
    node = "0";
    time = "0";
}

/^Problem / {
    gsub("'","",$0);
    sub("[.]cpx","",$0);
    name = $2;
}

/^Integer optimal solution/ {
    opt = $6;
}

/^Integer optimal, tolerance / {
    opt = $8;
}

/^Time limit exceeded/ {
    opt = "-";
}

/^Solution time / {
    time = $4;
    node = $11;
    if (opt != "-") {
	printf("%s %d %d %.2f\n", name, opt, node, time);
    } else {
	printf("%s - %d %.2f\n", name, node, time);
    }
    name = "";
    opt = "-";
    time = "";
}
