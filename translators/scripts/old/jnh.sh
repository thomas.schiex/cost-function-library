#! /usr/bin/tcsh

# Translate the JNH benchmark in wcsp format

# where is libcp.awk
setenv AWKPATH .

foreach f (../jnh/*.sat)
    awk -f jnh2wcsp.awk $f >! ${f:r}.wcsp
    awk -f jnh2wcnf.awk $f >! ${f:r}.wcnf
end
