#! /usr/bin/tcsh

# Translate the DIMACS and random K-SAT benchmarks in wcsp format

# where is libcp.awk
setenv AWKPATH .

foreach f (../*/*.cnf)
    awk -f cnf2wcsp.awk $f >! ${f:r}.wcsp
end
