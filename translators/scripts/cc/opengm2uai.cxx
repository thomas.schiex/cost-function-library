#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>


#include <opengm/graphicalmodel/graphicalmodel.hxx>
#include <opengm/graphicalmodel/graphicalmodel_hdf5.hxx>
#include <opengm/operations/minimizer.hxx>
#include <opengm/operations/adder.hxx>
#include <opengm/functions/explicit_function.hxx>
#include <opengm/functions/potts.hxx>
#include <opengm/functions/pottsn.hxx>
#include <opengm/functions/pottsg.hxx>
#include "opengm/functions/truncated_absolute_difference.hxx"
#include "opengm/functions/truncated_squared_difference.hxx"


int
main(int argc, const char* argv[] ) {

   typedef double ValueType;
   typedef size_t IndexType;
   typedef size_t LabelType;
   typedef opengm::Adder OperatorType;
   typedef opengm::Minimizer AccumulatorType;
   typedef opengm::DiscreteSpace<IndexType, LabelType> SpaceType;

   // Set functions for graphical model
   typedef opengm::meta::TypeListGenerator<
      opengm::ExplicitFunction<ValueType, IndexType, LabelType>,
      opengm::PottsFunction<ValueType, IndexType, LabelType>,
      opengm::PottsNFunction<ValueType, IndexType, LabelType>,
      opengm::PottsGFunction<ValueType, IndexType, LabelType>,
      opengm::TruncatedSquaredDifferenceFunction<ValueType, IndexType, LabelType>,
      opengm::TruncatedAbsoluteDifferenceFunction<ValueType, IndexType, LabelType>
   >::type FunctionTypeList;


   typedef opengm::GraphicalModel<
      ValueType,
      OperatorType,
      FunctionTypeList,
      SpaceType
   > GmType;
   

   GmType gm; 
   std::string opengmfile = argv[1]; 
   std::string uaifile    = argv[2];
 
   opengm::hdf5::load(gm, opengmfile,"gm");
   std::ofstream myuaifile;
   myuaifile.open(uaifile.c_str());
   myuaifile << "MARKOV" << std::endl;
   myuaifile << gm.numberOfVariables() << std::endl;
   for(IndexType var=0; var<gm.numberOfVariables(); ++var){
      myuaifile << gm.numberOfLabels(var) << " ";
   }
   myuaifile << std::endl;
   myuaifile << gm.numberOfFactors() << std::endl;
   for(IndexType f=0; f<gm.numberOfFactors(); ++f){
      myuaifile << gm[f].numberOfVariables() << " " ;
      for(IndexType i=0; i<gm[f].numberOfVariables(); ++i){
         myuaifile << gm[f].variableIndex(i) << " "; 
      } 
      myuaifile << std::endl;
   }
   LabelType l[9] = {0,0,0,0,0,0,0,0,0};
   for(IndexType f=0; f<gm.numberOfFactors(); ++f){
      myuaifile << std::endl;
      myuaifile << gm[f].size() << std::endl;
      if(gm[f].numberOfVariables()==0){
         l[0]=0;
         myuaifile << expl((long double) -gm[f](l)) << std::endl;
      }
      else if(gm[f].numberOfVariables()==1){
         for(l[0]=0; l[0]<gm[f].numberOfLabels(0); ++l[0]){
            myuaifile << expl((long double) -gm[f](l)) << " "; 
         } 
         myuaifile << std::endl;

      } 
      else if(gm[f].numberOfVariables()==2){
         for(l[0]=0; l[0]<gm[f].numberOfLabels(0); ++l[0]){
            for(l[1]=0; l[1]<gm[f].numberOfLabels(1); ++l[1]){
               myuaifile << expl((long double) -gm[f](l)) << " "; 
            }
            myuaifile << std::endl;
         }
      }
      else if(gm[f].numberOfVariables()==3){
		for(l[0]=0; l[0]<gm[f].numberOfLabels(0); ++l[0]){
		  for(l[1]=0; l[1]<gm[f].numberOfLabels(1); ++l[1]){
			for(l[2]=0; l[2]<gm[f].numberOfLabels(2); ++l[2]){
			  myuaifile << expl((long double) -gm[f](l)) << " ";
			}
		  }
		  myuaifile << std::endl;
		}
      }
      else if(gm[f].numberOfVariables()==4){
		for(l[0]=0; l[0]<gm[f].numberOfLabels(0); ++l[0]){
		  for(l[1]=0; l[1]<gm[f].numberOfLabels(1); ++l[1]){
			for(l[2]=0; l[2]<gm[f].numberOfLabels(2); ++l[2]){
			  for(l[3]=0; l[3]<gm[f].numberOfLabels(3); ++l[3]){
			    myuaifile << expl((long double) -gm[f](l)) << " ";
			  }
			}
		  }
		  myuaifile << std::endl;
		}
      }
      else if(gm[f].numberOfVariables()==5){
		for(l[0]=0; l[0]<gm[f].numberOfLabels(0); ++l[0]){
		  for(l[1]=0; l[1]<gm[f].numberOfLabels(1); ++l[1]){
			for(l[2]=0; l[2]<gm[f].numberOfLabels(2); ++l[2]){
			  for(l[3]=0; l[3]<gm[f].numberOfLabels(3); ++l[3]){
			   for(l[4]=0; l[4]<gm[f].numberOfLabels(4); ++l[4]){
			      myuaifile << expl((long double) -gm[f](l)) << " ";
			   }
			  }
			}
		  }
		  myuaifile << std::endl;
		}
      }
      else if(gm[f].numberOfVariables()==6){
		for(l[0]=0; l[0]<gm[f].numberOfLabels(0); ++l[0]){
		  for(l[1]=0; l[1]<gm[f].numberOfLabels(1); ++l[1]){
			for(l[2]=0; l[2]<gm[f].numberOfLabels(2); ++l[2]){
			  for(l[3]=0; l[3]<gm[f].numberOfLabels(3); ++l[3]){
			   for(l[4]=0; l[4]<gm[f].numberOfLabels(4); ++l[4]){
			     for(l[5]=0; l[5]<gm[f].numberOfLabels(5); ++l[5]){
			       myuaifile << expl((long double) -gm[f](l)) << " ";
				 }
			    }
			   }
			}
		  }
		  myuaifile << std::endl;
		}
      }
      else if(gm[f].numberOfVariables()==7){
		for(l[0]=0; l[0]<gm[f].numberOfLabels(0); ++l[0]){
		  for(l[1]=0; l[1]<gm[f].numberOfLabels(1); ++l[1]){
			for(l[2]=0; l[2]<gm[f].numberOfLabels(2); ++l[2]){
			  for(l[3]=0; l[3]<gm[f].numberOfLabels(3); ++l[3]){
			   for(l[4]=0; l[4]<gm[f].numberOfLabels(4); ++l[4]){
			     for(l[5]=0; l[5]<gm[f].numberOfLabels(5); ++l[5]){
			       for(l[6]=0; l[6]<gm[f].numberOfLabels(6); ++l[6]){
			         myuaifile << expl((long double) -gm[f](l)) << " ";
				   }
				 }
			   }
			  }
			}
		  }
		  myuaifile << std::endl;
		}
      }
      else if(gm[f].numberOfVariables()==8){
		for(l[0]=0; l[0]<gm[f].numberOfLabels(0); ++l[0]){
		  for(l[1]=0; l[1]<gm[f].numberOfLabels(1); ++l[1]){
			for(l[2]=0; l[2]<gm[f].numberOfLabels(2); ++l[2]){
			  for(l[3]=0; l[3]<gm[f].numberOfLabels(3); ++l[3]){
			   for(l[4]=0; l[4]<gm[f].numberOfLabels(4); ++l[4]){
			     for(l[5]=0; l[5]<gm[f].numberOfLabels(5); ++l[5]){
			       for(l[6]=0; l[6]<gm[f].numberOfLabels(6); ++l[6]){
			         for(l[7]=0; l[7]<gm[f].numberOfLabels(7); ++l[7]){
			           myuaifile << expl((long double) -gm[f](l)) << " ";
					 }
				    }
				   }
				 }
			   }
			}
		  }
		  myuaifile << std::endl;
		}
      }
      else if(gm[f].numberOfVariables()==9){
		for(l[0]=0; l[0]<gm[f].numberOfLabels(0); ++l[0]){
		  for(l[1]=0; l[1]<gm[f].numberOfLabels(1); ++l[1]){
			for(l[2]=0; l[2]<gm[f].numberOfLabels(2); ++l[2]){
			  for(l[3]=0; l[3]<gm[f].numberOfLabels(3); ++l[3]){
			   for(l[4]=0; l[4]<gm[f].numberOfLabels(4); ++l[4]){
			     for(l[5]=0; l[5]<gm[f].numberOfLabels(5); ++l[5]){
			       for(l[6]=0; l[6]<gm[f].numberOfLabels(6); ++l[6]){
			         for(l[7]=0; l[7]<gm[f].numberOfLabels(7); ++l[7]){
			           for(l[8]=0; l[8]<gm[f].numberOfLabels(8); ++l[8]){
			             myuaifile << expl((long double) -gm[f](l)) << " ";
					   }
					  }
					 }
				   }
				 }
			   }
			}
		  }
		  myuaifile << std::endl;
		}
      }
       else{
         std::cout << "Factors of order higher than 9 are so far not supported !" <<std::endl;
         myuaifile.close();
         return 1;
      }

   }
   myuaifile.close();
   return 0;
}
