#!/bin/tcsh
foreach f ( $* )
   echo $f
   ${EVALWCSP}/scripts/wcsp2sat --encoding=direct  $f ${f:r}_direct.wcnf
   ${EVALWCSP}/scripts/wcsp2sat $f ${f:r}_support.wcnf
end
