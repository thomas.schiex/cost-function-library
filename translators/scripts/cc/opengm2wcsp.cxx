#include <string>
#include <vector>
#include <iostream>
#include <fstream>


#include <opengm/graphicalmodel/graphicalmodel.hxx>
#include <opengm/graphicalmodel/graphicalmodel_hdf5.hxx>
#include <opengm/operations/minimizer.hxx>
#include <opengm/operations/adder.hxx>
#include <opengm/functions/explicit_function.hxx>
#include <opengm/functions/potts.hxx>
#include <opengm/functions/pottsn.hxx>
#include <opengm/functions/pottsg.hxx>
#include "opengm/functions/truncated_absolute_difference.hxx"
#include "opengm/functions/truncated_squared_difference.hxx"

typedef long long Long;
const Long LONGLONG_MAX = LONG_LONG_MAX;
const Long MAX_COST (LONGLONG_MAX / 6);

typedef long double Double;
inline Double Pow(Double x, Double y) {return powl(x,y);}
inline Double Exp10(Double x) {return powl(10.l, x);}
inline Double Log10(Double x) {return log10l(x);}
inline Double Log(Double x) {return logl(x);}
inline Double Log1p(Double x) {return log1pl(x);}

int Precision = 8;
Double Mult = 1.0l;
Double MinValue = 0.0l;

bool invloglike2Long(double l, Long *intpart) {
   // ((std::modf(gm[f](l),&intpart) == 0.0) && (intpart >=0.0))
   Double res = (l-MinValue) * Mult;
   if (res <= (Double) MAX_COST && res >= 0.0) {
     *intpart = (Long) res;
     return true;
   } else {
     *intpart = MAX_COST;
     std::cout << l << " " << res << " (" << MinValue << "," << Mult << ")" << std::endl;
     if (res < 0) abort();
     return false;
   }
}

int
main(int argc, const char* argv[] ) {

   typedef double ValueType;
   typedef size_t IndexType;
   typedef size_t LabelType;
   typedef opengm::Adder OperatorType;
   typedef opengm::Minimizer AccumulatorType;
   typedef opengm::DiscreteSpace<IndexType, LabelType> SpaceType;

   // Set functions for graphical model
   typedef opengm::meta::TypeListGenerator<
      opengm::ExplicitFunction<ValueType, IndexType, LabelType>,
      opengm::PottsFunction<ValueType, IndexType, LabelType>,
      opengm::PottsNFunction<ValueType, IndexType, LabelType>,
      opengm::PottsGFunction<ValueType, IndexType, LabelType>,
      opengm::TruncatedSquaredDifferenceFunction<ValueType, IndexType, LabelType>,
      opengm::TruncatedAbsoluteDifferenceFunction<ValueType, IndexType, LabelType>
   >::type FunctionTypeList;


   typedef opengm::GraphicalModel<
      ValueType,
      OperatorType,
      FunctionTypeList,
      SpaceType
   > GmType;
   

   GmType gm; 
   std::string opengmfile = argv[1]; 
   std::string wcspfile   = argv[2];
   int precision = atoi(argv[3]);
   Precision = precision;

   opengm::hdf5::load(gm, opengmfile,"gm");
   std::ofstream mywcspfile;
   mywcspfile.open(wcspfile.c_str());
   mywcspfile << opengmfile << ".OpenGM2WCSP" << " ";
   mywcspfile << gm.numberOfVariables() << " ";
   IndexType maxnblabel = 0;
   for(IndexType var=0; var<gm.numberOfVariables(); ++var){
     if (gm.numberOfLabels(var) > maxnblabel) {
       maxnblabel = gm.numberOfLabels(var);
    }
   }
   mywcspfile << maxnblabel << " " << gm.numberOfFactors() << " ";

   LabelType l[4] = {0,0,0,0};

   Double summaxlog = 0.0l;
   Double minlog = 0.0l;
   bool integral = true;
   bool powerintegral[9] = {true,true,true,true,true,true,true,true,true};
   double intpartd = 0.0;
   for(IndexType f=0; f<gm.numberOfFactors(); ++f) {
      double fsum = gm[f].sum();
//      std::cout << gm[f](l) << " " << fsum << std::endl;
      if (std::modf(fsum,&intpartd) != 0.0) integral = false;
      for (int i=1;i<=8;i++) if (std::modf(fsum * pow(10., (double)i), &intpartd) != 0.0) powerintegral[i] = false;
      summaxlog += gm[f].max();
      double minv = gm[f].min();
      if (minv < minlog) minlog = minv;
   }
   summaxlog -= minlog * (gm.numberOfFactors()-1);
   if (integral) {
     std::cout << "Problem has integer potential values only!" << std::endl;
     Precision = 0;
   } else {
     for (int i=1;i<=std::min(8,Precision);i++) {
        if (powerintegral[i]) {
           Precision = i;
           std::cout << "Precision reduced to 10^-" << Precision << std::endl;
           break;
        }
     }
   }
   Double mult = Pow((Double)10., (Double) Precision);
   Mult = mult;
   MinValue = minlog;
   Long intpart = 0;
   if (invloglike2Long(summaxlog,&intpart)) {
     mywcspfile << intpart << std::endl;
   } else {
     std::cout << "Top has been rounded to " << MAX_COST << std::endl;
     mywcspfile << MAX_COST << std::endl;
   }

   for(IndexType var=0; var<gm.numberOfVariables(); ++var){
      mywcspfile << gm.numberOfLabels(var) << " ";
   }
   mywcspfile << std::endl;

//   for(IndexType f=0; f<gm.numberOfFactors(); ++f){
//
//      mywcspfile << std::endl;
//   }

 
   for(IndexType f=0; f<gm.numberOfFactors(); ++f){
      mywcspfile << gm[f].numberOfVariables() << " " ;
      for(IndexType i=0; i<gm[f].numberOfVariables(); ++i){
	mywcspfile << gm[f].variableIndex(i) << " "; 
      }
      mywcspfile << "0 "; // default Long
      
      mywcspfile << gm[f].size() << std::endl;
      if(gm[f].numberOfVariables()==0){
         l[0]=0;
	 if (invloglike2Long(gm[f](l),&intpart))
	   mywcspfile << (Long)intpart << std::endl;
	 else{
	   std::cout << "Non integer or negative Longs not supported !" <<std::endl;
	   mywcspfile.close();
	   return 1;
	 }
      }
      else if(gm[f].numberOfVariables()==1){
         for(l[0]=0; l[0]<gm[f].numberOfLabels(0); ++l[0]){
	   if (invloglike2Long(gm[f](l),&intpart))
	     mywcspfile << l[0] << " " << (Long)intpart << std::endl; 
	   else{
	     std::cout << "Non integer or negative Longs not supported !" <<std::endl;
	     mywcspfile.close();
	     return 1;
	   }
         } 
      }
      else if(gm[f].numberOfVariables()==2){
         for(l[0]=0; l[0]<gm[f].numberOfLabels(0); ++l[0]){
            for(l[1]=0; l[1]<gm[f].numberOfLabels(1); ++l[1]){
	      if (invloglike2Long(gm[f](l),&intpart))
		mywcspfile << l[0] << " " << l[1] << " " << (Long)intpart << std::endl;
	      else{
		std::cout << "Non integer or negative Longs not supported !" <<std::endl;
		mywcspfile.close();
		return 1;
	      }
            }
         }
      }
      else if(gm[f].numberOfVariables()==3){
		for(l[0]=0; l[0]<gm[f].numberOfLabels(0); ++l[0]){
		  for(l[1]=0; l[1]<gm[f].numberOfLabels(1); ++l[1]){
			for(l[2]=0; l[2]<gm[f].numberOfLabels(2); ++l[2]){
			      if (invloglike2Long(gm[f](l),&intpart))
				mywcspfile << l[0] << " " << l[1] << " " << l[2] << " " << (Long)intpart << std::endl;
	      		      else{
				std::cout << "Non integer or negative Longs not supported !" <<std::endl;
				mywcspfile.close();
				return 1;
	      		      }
			}
		  }
		}
      }
      else{
	std::cout << "Factors of order higher than 3 are so far not supported !" <<std::endl;
	mywcspfile.close();
	return 1;
      }
//      mywcspfile << std::endl;
   }
   mywcspfile.close();
   return 0;
}
