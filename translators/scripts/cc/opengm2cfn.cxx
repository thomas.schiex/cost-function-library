#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <iomanip>


#include <opengm/graphicalmodel/graphicalmodel.hxx>
#include <opengm/graphicalmodel/graphicalmodel_hdf5.hxx>
#include <opengm/operations/minimizer.hxx>
#include <opengm/operations/adder.hxx>
#include <opengm/functions/explicit_function.hxx>
#include <opengm/functions/potts.hxx>
#include <opengm/functions/pottsn.hxx>
#include <opengm/functions/pottsg.hxx>
#include "opengm/functions/truncated_absolute_difference.hxx"
#include "opengm/functions/truncated_squared_difference.hxx"

typedef long long Long;
const Long LONGLONG_MAX = LONG_LONG_MAX;
const Long MAX_COST (LONGLONG_MAX / 6);

typedef long double Double;
inline Double Pow(Double x, Double y) {return powl(x,y);}
inline Double Exp10(Double x) {return powl(10.l, x);}
inline Double Log10(Double x) {return log10l(x);}
inline Double Log(Double x) {return logl(x);}
inline Double Log1p(Double x) {return log1pl(x);}

int main(int argc, const char* argv[] ) {

  typedef double ValueType;
  typedef size_t IndexType;
  typedef size_t LabelType;
  typedef opengm::Adder OperatorType;
  typedef opengm::Minimizer AccumulatorType;
  typedef opengm::DiscreteSpace<IndexType, LabelType> SpaceType;

  // Set functions for graphical model
  typedef opengm::meta::TypeListGenerator<
	opengm::ExplicitFunction<ValueType, IndexType, LabelType>,
	opengm::PottsFunction<ValueType, IndexType, LabelType>,
	opengm::PottsNFunction<ValueType, IndexType, LabelType>,
	opengm::PottsGFunction<ValueType, IndexType, LabelType>,
	opengm::TruncatedSquaredDifferenceFunction<ValueType, IndexType, LabelType>,
	opengm::TruncatedAbsoluteDifferenceFunction<ValueType, IndexType, LabelType>
	>::type FunctionTypeList;


  typedef opengm::GraphicalModel<
	ValueType,
	OperatorType,
	FunctionTypeList,
	SpaceType
	> GmType;
   

  GmType gm; 
  std::string opengmfile = argv[1]; 
  std::string wcspfile   = argv[2];
  int precision = atoi(argv[3]);

  Double summaxlog = 0.0l;
  Double maxlog = 0.0l;
  opengm::hdf5::load(gm, opengmfile,"gm");
  std::replace( opengmfile.begin(), opengmfile.end(), '/', '.');
  std::ofstream mywcspfile;
  mywcspfile.open(wcspfile.c_str());
  mywcspfile << "{ \"problem\": { \"name\": \"" << opengmfile << ".OpenGM2CFN\", \"mustbe\": \"<";
  for(IndexType f=0; f<gm.numberOfFactors(); ++f) {
	Double max= gm[f].max();
	summaxlog += max;
	if (max>maxlog) maxlog = max;
  }
  mywcspfile << std::fixed << std::setprecision(precision) << maxlog << "\" }," << std::endl; // Warning!!! It assumes maximum corresponds to forbidden tuples
  mywcspfile << " \"variables\": {";
  IndexType maxnblabel = 0;
  for(IndexType var=0; var<gm.numberOfVariables(); ++var){
	if (var>0) mywcspfile << ", ";
	mywcspfile << "\"x" << var << "\": " << gm.numberOfLabels(var);
  }
  mywcspfile << " }," << std::endl;
  mywcspfile << " \"functions\": {" << std::endl;

  LabelType l[4] = {0,0,0,0};

  for(IndexType f=0; f<gm.numberOfFactors(); ++f){
	if (f>0) mywcspfile << "," << std::endl;
	mywcspfile << " { \"scope\": [ " ;
	for(IndexType i=0; i<gm[f].numberOfVariables(); ++i){
	  if (i>0) mywcspfile << ", ";
	  mywcspfile << "\"x" << gm[f].variableIndex(i) << "\""; 
	}
	mywcspfile << " ]," << std::endl;
      
	mywcspfile << "   \"costs\": [ ";
	if(gm[f].numberOfVariables()==0){
	  l[0]=0;
	  mywcspfile << gm[f](l);
	}
	else if(gm[f].numberOfVariables()==1){
	  for(l[0]=0; l[0]<gm[f].numberOfLabels(0); ++l[0]){
		if (l[0]>0) mywcspfile << ", ";
		mywcspfile << gm[f](l); 
	  }
	}
	else if(gm[f].numberOfVariables()==2){
	  for(l[0]=0; l[0]<gm[f].numberOfLabels(0); ++l[0]){
		for(l[1]=0; l[1]<gm[f].numberOfLabels(1); ++l[1]){
		  if (l[0]>0 || l[1]>0) mywcspfile << ", ";
		  mywcspfile << gm[f](l);
		}
	  }
	}
	else if(gm[f].numberOfVariables()==3){
	  for(l[0]=0; l[0]<gm[f].numberOfLabels(0); ++l[0]){
		for(l[1]=0; l[1]<gm[f].numberOfLabels(1); ++l[1]){
		  for(l[2]=0; l[2]<gm[f].numberOfLabels(2); ++l[2]){
			if (l[0]>0 || l[1]>0 || l[2]>0) mywcspfile << ", ";
			mywcspfile << gm[f](l);
		  }
		}
	  }
	}
	else{
	  std::cout << "Factors of order higher than 3 are so far not supported !" <<std::endl;
	  mywcspfile.close();
	  return 1;
	}
	mywcspfile << " ] }";
  }
  mywcspfile << " } }" << std::endl;
  mywcspfile.close();
  return 0;
}
