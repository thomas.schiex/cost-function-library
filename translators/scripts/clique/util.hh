#ifndef UTIL_HH
#define UTIL_HH

#include <iostream>
#include <vector>
#include <algorithm>

//--------------------------------------------------
// output operator for vector, pair
namespace std {

template <typename T> ostream& operator<<(ostream& os, vector<T> const& v)
{
    os << "v(sz=" << v.size() << ")[";
    bool first = true;
    for (auto&& t : v) {
        if (first)
            first = false;
        else
            os << ",";
        os << t;
    }
    os << "]";
    return os;
}

template <typename U, typename T>
ostream& operator<<(ostream& os, pair<U, T> const& p)
{
    return os << "p{" << p.first << "," << p.second << "}";
}

}

//--------------------------------------------------
// erase elements from a vector/vec, subject to a Pred
template<typename T, typename A, typename Pred>
void erase_if(std::vector<T, A>& v, Pred p)
{
    v.erase( remove_if(begin(v), end(v), p), end(v) );
}


#endif
