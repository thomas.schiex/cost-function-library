#ifndef __TRAIL_HH
#define __TRAIL_HH

#include <cassert>
#include <vector>

template <typename iterator> struct subtrail;

/* a stack-like class that supports numbered checkpoints. We can call
   x = checkpoint() and later restore(x) to get back to the state it
   was at the first call. This is append-only. It also supports
   iterating over the elements appended between different
   checkpoints. When empty, checkpoint 0 exists already. */
template <typename T> class trail_vector
{
public:
    typedef std::vector<T> storage_type;
    typedef std::size_t size_type;
    typedef typename storage_type::iterator iterator;
    typedef typename storage_type::const_iterator const_iterator;
    typedef T& reference;
    typedef T const& const_reference;

    void push_back(T t) { elems.push_back(t); }

    int checkpoint()
    {
        ckpts.push_back(elems.size());
        return current_checkpoint() - 1;
    }
    int current_checkpoint() const { return ckpts.size(); }
    void restore(int ckpt)
    {
        if (ckpt >= current_checkpoint())
            return;
        elems.resize(ckpts[ckpt]);
        ckpts.resize(ckpt);
    }
    bool incheckpoint(size_type idx, int ckpt) const
    {
#ifdef _GLIBCXX_DEBUG
        assert(ckpts.size() >= size_type(ckpt) && ckpt >= 0);
#endif
        size_type b{0}, e{elems.size()};
        if (ckpt > 0)
            b = ckpts[ckpt - 1];
        if (size_type(ckpt) < ckpts.size())
            e = ckpts[ckpt];
        return (b <= idx && idx < e);
    }

    iterator begin() { return elems.begin(); }
    iterator end() { return elems.end(); }
    const_iterator begin() const { return elems.begin(); }
    const_iterator end() const { return elems.end(); }

    subtrail<iterator> since(int ckpt) // [since, end)
    {
#ifdef _GLIBCXX_DEBUG
        assert(ckpts.size() >= size_type(ckpt));
#endif
        return subtrail<iterator>(
            elems.begin() + (ckpt > 0 ? ckpts[ckpt - 1] : 0), end());
    }
    subtrail<const_iterator> since(int ckpt) const // [since, end)
    {
#ifdef _GLIBCXX_DEBUG
        assert(ckpts.size() >= ckpt);
#endif
        return subtrail<const_iterator>(
            elems.begin() + (ckpt > 0 ? ckpts[ckpt - 1] : 0), end());
    }
    subtrail<iterator> until(int ckpt) // [0, ckpt]
    {
#ifdef _GLIBCXX_DEBUG
        assert(ckpts.size() >= ckpt);
#endif
        return subtrail<iterator>(
            begin(), (ckpts.size() > ckpt ? begin() + ckpts[ckpt] : end()));
    }
    subtrail<const_iterator> until(int ckpt) const // [0, ckpt]
    {
#ifdef _GLIBCXX_DEBUG
        assert(ckpts.size() >= ckpt);
#endif
        return subtrail<const_iterator>(
            begin(), (ckpts.size() > ckpt ? begin() + ckpts[ckpt] : end()));
    }
    subtrail<iterator> between(int from, int to); // [from, to)
    subtrail<const_iterator> between(int from, int to) const; // [from, to)
    subtrail<iterator> only(int ckpt); // [ckpt, ckpt]
    subtrail<const_iterator> only(int ckpt) const; // [ckpt, ckpt]

    reference operator[](size_type t) { return elems[t]; }
    const_reference operator[](size_type t) const { return elems[t]; }
    size_type size() const { return elems.size(); }
private:
    storage_type elems;
    std::vector<size_type> ckpts;
};

template <typename iterator> struct subtrail {
    subtrail(iterator ib, iterator ie)
        : b(ib)
        , e(ie)
    {
    }

    iterator begin() { return b; }
    iterator end() { return e; }

    iterator b, e;
};

/* As with trail_vector<T>, but this keeps track of a single value,
   which can change arbitrarily. Interface remains the same with
   respect to checkpoint(), restore(). Accessing intermediate values
   yields a single value.
 */
template <typename T> class trail
{
public:
    typedef std::size_t size_type;
    typedef typename std::vector<T>::const_iterator const_iterator;

    trail()
        : values({T()})
    {
    }

    trail(std::initializer_list<T> il)
        : values(il)
    {
        assert(!values.empty());
    }

    T& operator*() { return values.back(); }
    T const& operator*() const { return values.back(); }

    // no non-const operator[], we do not want to accidentally change
    // the past
    T const& operator[](size_type i) const
    {
#ifdef _GLIBCXX_DEBUG
        assert(i <= current_checkpoint());
#endif
        return values[i];
    }

    const_iterator begin() const { return values.begin(); }
    const_iterator end() const { return values.end(); }

    size_type checkpoint()
    {
        values.push_back(values.back());
        return values.size() - 1;
    }
    size_type current_checkpoint() const { return values.size() - 1; }
    void restore(size_type ckpt)
    {
        if (ckpt >= current_checkpoint())
            return;
        values.resize(ckpt + 1);
    }

private:
    std::vector<T> values;
};

/* A decreasing, unordered, trailing set. It must be initialized with
   a set of elements. We can remove elements, set checkpoints and
   restore to a previous checkpoint. */
template <typename T> class decreasing_vector
{
public:
    /* types */
    typedef std::vector<T> storage_type;
    typedef std::size_t size_type;
    typedef typename storage_type::iterator iterator;
    typedef typename storage_type::const_iterator const_iterator;
    typedef T& reference;
    typedef T const& const_reference;

    /* methods */
    decreasing_vector(std::initializer_list<T> init);

    template <typename Iter> decreasing_vector(Iter begin, Iter end);

    int checkpoint();
    int current_checkpoint() const;
    void restore(int ckpt);

    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;

    reference operator[](size_type t)
    {
#ifdef _GLIBCXX_DEBUG
        assert(0 <= t && t < size());
#endif
        return elems[t];
    }
    const_reference operator[](size_type t) const
    {
#ifdef _GLIBCXX_DEBUG
        assert(0 <= t && t < size());
#endif
        return elems[t];
    }
    size_type size() const;

private:
    std::vector<T> elems;
    std::vector<size_type> ckpts;
};

#endif
