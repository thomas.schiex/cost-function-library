#include "wcsp.hh"
#include "graph.hh"

#include <algorithm>
#include <utility>
#include <numeric>

#include "fmt/format.h"
#include "fmt/ostream.h"
#include <fstream>

#include <boost/variant.hpp>

#include <malloc.h>

using wcsp_graph = graph<std::pair<int,int>>;
using wcsp_btgraph = btgraph<std::pair<int,int>>;

using std::vector;

wcsp_graph make_graph(wcsp& w)
{
    fmt::print(
        "WCSP with {} variables, {} functions, ub = {}, constructing graph\n",
        w.nvars(), w.functions.size(), w.ub);

    wcsp_graph g;
    for (auto& vf : w.functions) {
        auto* fp = boost::get<wcsp_explicit_func>(&vf.function);
        if (!fp)
            continue;
        auto& f = *fp;
        if (vf.scope.size() != 2)
            continue;
        std::sort(begin(f.specs), end(f.specs), [&](auto&& t1, auto&& t2) {
            return std::lexicographical_compare(
                begin(t1.tup), end(t1.tup), begin(t2.tup), end(t2.tup));
        });
        auto handle_tuple = [&](auto&& tup, auto&& cost) {
            if (cost < w.ub)
                return;
            auto var1{vf.scope[0]}, var2{vf.scope[1]};
            auto val1{tup[0]}, val2{tup[1]};
            auto v1{g.get_vertex(std::make_pair(var1, val1))},
                v2{g.get_vertex(std::make_pair(var2, val2))};
            g.add_edge(v1, v2);
        };
        vector<int> tup;
        if (f.defcost >= w.ub)
            foreach_tuple(w, vf, tup, handle_tuple);
        else
            for (auto wt : f.specs)
                handle_tuple(wt.tup, wt.cost);
    }

    // make a clique for each var
    std::vector<std::vector<vertex_ref>> var_vertices(w.nvars());
    for (auto v : g.vertices()) {
        auto data = g.get_data(v);
        var_vertices[data.first].push_back(v);
    }
    for (auto vv : var_vertices) {
        for (auto v1 : vv)
            for (auto v2 : vv) {
                if (v1 == v2)
                    continue;
                g.add_edge(v1, v2);
            }
    }

    return g;
}

/* Given a clique, add the constraint that we can use at most 1 of the
   variable-value pairs involved, as an automaton.

   In the process, update a variable mapping new_to_old. We want to
   insert the automaton's state variable in between the original
   variables, so that the ordering x_1 q_1 x_2 ... agrees with the
   edac order. We can achieve this by renaming variables, but we do
   not do this here. We simply state in new_to_old what we would like
   the new ordering to be.

   Note we get a copy of clique because we want to sort it.
*/
struct unimplemented{};
bool add_clique_cut(wcsp& w, wcsp_btgraph const& G,
    std::vector<vertex_ref> clique, std::vector<int>& new_to_old, encoding enc,
    std::vector<std::tuple<int, int, int>>& transition)
{
    assert(!clique.empty());

    std::sort(begin(clique), end(clique), [&](vertex_ref u, vertex_ref v) {
        return G.get_data(u) < G.get_data(v);
    });

    // do nothing for cliques with at most 2 variables
    auto nv{1};
    auto lastvar{G.get_data(clique[0]).first};
    // which values are involved for each variable?
    std::vector<std::vector<int>> values{w.nvars()};
    for (auto v : clique) {
        int var, val;
        std::tie(var, val) = G.get_data(v);
        values[var].push_back(val);
        if (lastvar != var) {
            ++nv;
            lastvar = var;
        }
    }
    // we need at least 3 vars to say something meaningful
    if (nv <= 2)
        return false;

    // if true, the same set of values participates in the clique from
    // each variable involved
    bool uniform_values{true};
    auto const& firstset = values[G.get_data(clique[0]).first];
    for (auto const& s : values)
        if ( !s.empty() && !std::equal(begin(firstset), end(firstset), begin(s))) {
            uniform_values = false;
            break;
        }

    if (0) {
        std::vector<std::pair<int,int>> clqdata(clique.size());
        std::transform(begin(clique), end(clique), begin(clqdata),
            [&](vertex_ref u) { return G.get_data(u); });
        fmt::print("Found clique {}\n", clqdata);
    }

    std::vector<int> rscope(clique.size());
    std::transform(begin(clique), end(clique), begin(rscope),
        [&](vertex_ref u) { return G.get_data(u).first; });
    rscope.erase(std::unique(begin(rscope), end(rscope)), end(rscope));
    if (enc == DECOMP) {
        auto q0{w.add_var(1)};
        auto i{begin(new_to_old)};
        auto qprev{q0};
        for (auto var : rscope) {
            i = find(i, end(new_to_old), var);
            i = new_to_old.insert(i, qprev);
            int qnext{w.add_var(2)};
            wcspfunc transition{{qprev, var, qnext}, wcsp_explicit_func{w.ub}};
            auto& func = boost::get<wcsp_explicit_func>(transition.function);
            assert(!values[var].empty());
            auto curval = values[var].begin();
            for (int i = 0; i != w.domains[var]; ++i) {
                // for any value other than val, we stay at the state we
                // were at with 0 cost. for val, we go from 0 to 1 with 0
                // cost and from 1 to 1 with infinite cost.
                bool inclq = (curval != values[var].end() && *curval == i);
                if (inclq)
                    ++curval;
                wcsptuple t0{{0, i, inclq}, 0}, t1{{1, i, 1}, inclq ? w.ub : 0};
                func.specs.emplace_back(t0);
                func.specs.emplace_back(t1);
            }
            erase_if(func.specs, [&](auto& spec) {
                for (size_t i = 0; i != spec.tup.size(); ++i)
                    if (spec.tup[i] >= w.domains[transition.scope[i]])
                        return true;
                return false;
            });
            w.functions.emplace_back(transition);

            qprev = qnext;
        }
        new_to_old.push_back(qprev);
    } else if (enc == SREGULARDP) {
        // easier to construct the transition table and then put it in the
        // parameters
        transition.clear();
        size_t layers = uniform_values ? 1 : rscope.size();
        for (size_t q = 0; q != layers; ++q) {
            auto var = rscope[q];
            assert(!values[var].empty());
            auto curval = values[var].begin();
            for (int i = 0; i != w.domains[var]; ++i) {
                bool inclq = (curval != values[var].end() && *curval == i);
                int normalize = uniform_values ? 0 : 2 * q;
                int normalize_next = uniform_values ? 0 : 2 * (q + 1);
                if (inclq)
                    ++curval;
                transition.push_back(
                    std::tuple<int, int, int>(normalize, i, normalize_next + static_cast<int>(inclq)));
                if (!inclq)
                    transition.push_back(
                        std::tuple<int, int, int>(normalize + 1, i, normalize_next + 1));
            }
        }

        fmt::MemoryWriter funcparams;
        int accept_normalize = uniform_values ? 0 : 2 * rscope.size();
        funcparams.write("sregulardp var {} {} 1 0 2 {} {} {}", w.ub,
            // number of states
            uniform_values ? 2 : 2 * (rscope.size() + 1),
            // accepting states
            accept_normalize, accept_normalize + 1,
            // transitions
            transition.size());
        for (auto t : transition) {
            int q0, x, q1;
            std::tie(q0, x, q1) = t;
            funcparams.write(" {} {} {}", q0, x, q1);
        }

        w.functions.emplace_back(wcspfunc{rscope, wcsp_implicit_func{funcparams.str()}});
    } else {
        // clique constraint encoding
        fmt::MemoryWriter funcparams;
        funcparams.write("clique 1"); // rhs == 1
        for(auto var : rscope) {
            funcparams.write(" {}", values[var].size());
            for (auto val : values[var])
                funcparams.write(" {}", val);
        }
		vector<vector<int>> nonclqvals;
		for (unsigned int var=0; var < w.nvars(); var++) {
		  vector<int> nvals;
		  vector<bool> clqvals(w.domains[var], false);
		  for (auto v : values[var]) clqvals[v] = true;
		  for (int i = 0; i != w.domains[var]; ++i) {
			if (!clqvals[i]) nvals.push_back(i);  // Warning! It assumes value == index!!!
		  }
		  nonclqvals.push_back(nvals);
		}
        w.functions.emplace_back(wcspfunc{rscope, wcsp_implicit_func{funcparams.str(), nonclqvals}});
    }
    return true;
}

/* Renumber the variables in a WCSP to agree with the new2old
   ordering: new variable y was named new2old[y] before the
   renumbering. */
void renumber_variables(wcsp& w, std::vector<int> const& new2old)
{
    // construct the reverse mapping
    std::vector<int> old2new(new2old.size());
    for (size_t i = 0; i != new2old.size(); ++i)
        old2new[new2old[i]] = i;

    // update domains
    std::vector<int> newdomains(w.domains.size());
    for (size_t i = 0; i != w.domains.size(); ++i)
        newdomains[i] = w.domains[new2old[i]];
    w.domains = std::move(newdomains);

    // update functions
    for (auto& f : w.functions)
        for (auto& var : f.scope)
            var = old2new[var];
}

void add_clique_cuts(wcsp& w, encoding enc, int perbranch, int /*bound*/)
{
    auto g{make_graph(w)};
    auto bg = wcsp_btgraph{std::move(g)};

    static const bool write_graph{false};
    if (write_graph) {
        std::ofstream ofs("pp.gg");
        for (auto v : bg.vertices()) {
            fmt::print(ofs, "{} ", v.id);
            for (auto u : bg.neighbors(v)) {
                if (v < u)
                    continue;
                fmt::print(ofs, "{} ", u.id);
            }
            fmt::print(ofs, "\n");
        }
        ofs.close();
    }

    int ne{0};
    for(auto v: bg.vertices())
        ne += bg.neighbors(v).size();
    fmt::print("{} vertices, {} edges in graph\n", bg.vertices().size(), ne/2);

    auto mistart{mallinfo()};
    auto baseline{mistart.uordblks};
    auto cliquesizes{0};

    int numcliques{0};
    // declared here to avoid reallocating in add_clique_cut (and
    // potentially fragmenting memory?)
    std::vector<std::tuple<int, int, int>> transition;
    std::vector<int> new_to_old(w.nvars());
    std::iota(begin(new_to_old), end(new_to_old), 0);
    find_all_cliques(bg,
        [&](auto&& clique) {
            if (clique.size() <= 2)
                return;
            if (add_clique_cut(w, bg, clique, new_to_old, enc, transition)) {
                ++numcliques;
                auto mi = mallinfo();
                auto total = mi.uordblks - baseline;
                cliquesizes += clique.size();
                if (0)
                    fmt::print(
                        "total: {}, avg: {}, free blocks: {}, free bytes: "
                        "{}, avg clq size {}, avg free blocksize {}\n",
                        total, total / static_cast<double>(numcliques),
                        mi.ordblks, mi.fordblks,
                        cliquesizes / static_cast<double>(numcliques),
                        mi.fordblks / static_cast<double>(mi.ordblks));
            }
        },
        perbranch);
    fmt::print("{} cliques found\n", numcliques);
    renumber_variables(w, new_to_old);
}

