
# Convert a dot file (undirected graph) into a maximum clique problem in wcsp format

# Usage:
# awk -f ./clique2wcsp.awk problem.wcsp.dot > maxclique.wcsp

# Example: breaking symmetries in a graph coloring problem (gc_250_9 with at-most 92 colors)
# 1) generates a dot file (problem.wcsp.dot) from the original constraint graph
# toulbar2 gc_250_9.wcsp -z=1 -nopre -k=0
# 2) creates the corresponding maximum clique problem
# awk -f ./clique2wcsp.awk problem.wcsp.dot > gc_250_9_maxclq.wcsp
# 3) solves it with a time limit of 30 seconds
# toulbar2 gc_250_9_maxclq.wcsp -i -w=gc_250_9_maxclq.sol -vns -timer=30
# 4) uses it solution to assign the clique variables in the original graph coloring problem to break some symmetries
# toulbar2 gc_250_9.wcsp -vns -vnsini=-1 -l: -bt=10000 -kmin=125 -kmax=125 -L=10000 -d: -x=`awk '{for(i=2;i<=NF;i++) if ($i == 1) printf "," (i-1) "=" c++}' gc_250_9_maxclq.sol`

BEGIN{n=0} /--/{if ($1 in id) x=id[$1]; else {id[$1]=n;x=n;n++};if ($3 in id) y=id[$3]; else {id[$3]=n;y=n;n++}; edge[x "," y]=1; edge[y "," x]=1; e++;} END{t=n*(n-1)/2; print "clique",n,2,n+t-e,t+1; for (i=0;i<n;i++)  printf " " 2; print ""; for (i=0;i<n;i++) for (j=i+1; j<n; j++) if ((i "," j) in edge) nothing; else print 2,i,j,0,1,1,1,t+1; for (i=0;i<n;i++) print 1,i,0,1,0,1}
