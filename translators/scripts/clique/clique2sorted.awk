
## Warning! It assumes Boolean variables with no globals except cliques composed of forbidden pairs of truth values (1,1)

## Warning bis! cliques must be after unary cost functions

## Warning tern! cost functions start at line 3, each cost function in extention is written using 1+nbtuples lines, each clique using a single text line

FNR==1 {
	n=$2;
	d=$3;
	e=$4;
	k=$5;
	t=0;
	ok=1;
	var=-1;
	m=0;
	bestm=0;
}

var>=0 && NF==2 {
  if ($1==ZEROVALUE) ucost0[0+var] = $2;
  else if ($1==ONEVALUE) ucost1[0+var] = $2;
}

FNR==3+t {
	t += 1 + (($($1+3)=="clique")?0:$NF);
	if ($1==1 && $NF==2) {
		var=$2;
		ok=1;
	} else if ($($1+2)==-1 && $($1+3)=="clique") {    
		NF=$1+1;
		size[m] = $1;
		arity[m] = $1;
		cliques[m] = $0 " "; ## keep arity and scope only with one extra space for further pattern searches
		sumucost0[m]=0;
		maxucost0[m]=-1;
		posmaxucost0[m]=0;
		max2ucost0[m]=-1;
		posmax2ucost0[m]=0;
		minucost1[m]=k;
		for (i=1;i<=size[m];i++) {
			sumucost0[m]+=ucost0[0+$(i+1)];
			if (ucost0[0+$(i+1)] > 0 && ucost1[0+$(i+1)] > 0) { ## apply NC*
				if (ucost0[0+$(i+1)] > ucost1[0+$(i+1)]) {
					ucost0[0+$(i+1)] -= ucost1[0+$(i+1)];
					ucost1[0+$(i+1)]=0;
				} else {
					ucost1[0+$(i+1)] -= ucost0[0+$(i+1)];
					ucost0[0+$(i+1)]=0;
				}
			}
			if (ucost0[0+$(i+1)] > maxucost0[m]) {
				maxucost0[m] = ucost0[0+$(i+1)];
				posmaxucost0[m] = i;
			}
			if (ucost1[0+$(i+1)] < minucost1[m]) minucost1[m] = ucost1[0+$(i+1)];
		}
		for (i=1;i<=size[m];i++) {
			if (ucost0[0+$(i+1)] > max2ucost0[m] && i!=posmaxucost0[m]) {
				max2ucost0[m] = ucost0[0+$(i+1)];
				posmax2ucost0[m] = i;
			}
		}
		if (posmaxucost0[m]==0) {print "ERROR MAXUCOST0 NOT FOUND !!!" > "/dev/stderr"; exit 1}
		if (posmax2ucost0[m]==0) {print "ERROR SECOND MAXUCOST0 NOT FOUND !!!" > "/dev/stderr"; exit 1}
		if (maxucost0[m]>minucost1[m]) {
			if (minucost1[m]>0) {print "ERROR MINUCOST1>0 !!! NC NOT DONE???" > "/dev/stderr"; exit 1}
			lb[m] = sumucost0[m]-maxucost0[m];
		} else {
			lb[m] = 0;
		}
		if (size[m]*lb[m] > size[bestm]*lb[bestm]) bestm = m;
		m++;
		var=-1;
		ok=0;
	} else {
		var=-1;
		ok=1;
	}
}

ok {
  print $0;
}

END {
     c = m;
     for (u=1;bestm!=-1 && u<=c;u++) {
		 m1=bestm;
#		 print "SELECT",m1,size[m1],lb[m1],sumucost0[m1],maxucost0[m1],posmaxucost0[m1],cliques[m1] > "/dev/stderr";
		 # print bestm clique
		 printf("%s%d %s",cliques[m1],-1,"clique");
		 printf(" 1");
		 for (i=0;i<arity[m1];i++) {
			 printf(" 1");
			 printf(" %d",ONEVALUE);
		 }
		 print "";

		 arity1=split(cliques[m1], scope1, " ");
		 delete cliques[m1];
		 delete invscope1;
		 for (i=2;i<=arity1;i++) invscope1[0+scope1[i]]=i-1;

		 # update second maxcost0 for best clique
		 if (maxucost0[m1]>0) {
			 max2ucost0[m1]=-1;
			 for (i=2;i<=arity1;i++) if (ucost0[0+scope1[i]] > max2ucost0[m1] && i-1!=posmaxucost0[m1]) max2ucost0[m1] = ucost0[0+scope1[i]];
		 }

		 # update clique "effective" sizes and lower-bounds
		 bestm=-1;
		 for (m in cliques) {
			 redomax=0;
			 for (i=2;i<=arity1;i++) {
				 if (!(0+scope1[i] in cover) && match(cliques[m], " " scope1[i] " ")) size[m] -= 1;
				 if (!(0+scope1[i] in zero) && match(cliques[m], " " scope1[i] " ")) {
					 sumucost0[m] -= ucost0[0+scope1[i]];
					 if (i-1==posmaxucost0[m1]) sumucost0[m] += maxucost0[m1]-max2ucost0[m1];
					 if (ucost0[0+scope1[i]]==maxucost0[m]) redomax=1;
				 }
			 }
			 if (redomax) {
				 arity2=split(cliques[m], scope2, " ");
				 maxucost0[m]=-1;
				 posmaxucost0[m]=0;
				 for (i=2;i<=arity2;i++) {
					 cost0 = ucost0[0+scope2[i]];
					 if (0+scope2[i] in invscope1) {
						 if (invscope1[0+scope2[i]]==posmaxucost0[m1]) cost0 = maxucost0[m1]-max2ucost0[m1];
						 else cost0 = 0;
					 }
					 if (cost0 > maxucost0[m]) {
						 maxucost0[m] = cost0;
						 posmaxucost0[m] = i-1;
					 }
				 }
			 }
			 lb[m] = sumucost0[m]-maxucost0[m];
			 if (lb[m]==0 && arity[m]>3) { ## keep zero-cost ternary functions
#				 print "DEL",m,size[m],lb[m],sumucost0[m],maxucost0[m],posmaxucost0[m],cliques[m] > "/dev/stderr";
				 delete cliques[m];
			 } else if (bestm==-1 || size[m]*lb[m] > size[bestm]*lb[bestm] || (size[m]*lb[m] == size[bestm]*lb[bestm] && (size[m]>size[bestm] || lb[m]>lb[bestm]))) bestm = m;	 
		 }
      
		 # perform EPTs for previous best clique
		 if (maxucost0[m1]>0) {
			 for (i=2;i<=arity1;i++) {
				 if (i-1==posmaxucost0[m1]) ucost0[0+scope1[i]]=maxucost0[m1]-max2ucost0[m1];
				 else ucost0[0+scope1[i]]=0;
				 if (ucost0[0+scope1[i]]==0) zero[0+scope1[i]]=1;
			 }
		 }
		 for (i=2;i<=arity1;i++) {
			 cover[0+scope1[i]]=1;
		 }

         z=0;
		 for (v in zero) z+=1;
		 if (v == n) break; ## stop if all unary costs are zero
     }
	 # generate dummy cost functions to replace unused cliques
     for (;u<=c;u++) {
		 print 0,0,0;
	 }
}
