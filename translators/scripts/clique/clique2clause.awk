
## Warning! It assumes Boolean variables with no globals except cliques composed of forbidden pairs of truth values (1,1)

## Warning bis! cliques must be after unary cost functions

## Warning tern! cost functions start at line 3, each cost function in extention is written using 1+nbtuples lines, each clique using a single text line

FNR==1 {
	n=$2;
	d=$3;
	e=$4; $4++; ## add a supplementary global lower bound increment
	k=$5;
	t=0;
	ok=1;
	var=-1;
	lb=0;
}

var>=0 && NF==2 {
  if ($1==ZEROVALUE) ucost0[0+var] = $2;
  else if ($1==ONEVALUE) ucost1[0+var] = $2;
}

FNR==3+t {
	t += 1 + (($($1+3)=="clique")?0:$NF);
	if ($1==1 && $NF==2) {
		var=$2;
		ok=0;
	} else if ($($1+2)==-1 && $($1+3)=="clique") {    
		$($1+2)=0;
		$($1+3)=1;
		NF=$1+3;
		print $0;
		sumucost0=0;
		maxucost0=-1;
		posmaxucost0=0;
		max2ucost0=-1;
		posmax2ucost0=0;
		minucost1=k;
		for (i=1;i<=$1;i++) {
			printf("%d ",ZEROVALUE);
			sumucost0+=ucost0[0+$(i+1)];
			if (ucost0[0+$(i+1)] > 0 && ucost1[0+$(i+1)] > 0) { ## apply NC*
				if (ucost0[0+$(i+1)] > ucost1[0+$(i+1)]) {
					lb += ucost1[0+$(i+1)];
					ucost0[0+$(i+1)] -= ucost1[0+$(i+1)];
					ucost1[0+$(i+1)]=0;
				} else {
					lb += ucost0[0+$(i+1)];
					ucost1[0+$(i+1)] -= ucost0[0+$(i+1)];
					ucost0[0+$(i+1)]=0;
				}
			}
			if (ucost0[0+$(i+1)] > maxucost0) {
				maxucost0 = ucost0[0+$(i+1)];
				posmaxucost0 = i;
			}
			if (ucost1[0+$(i+1)] < minucost1) minucost1 = ucost1[0+$(i+1)];
		}
		for (i=1;i<=$1;i++) {
			if (ucost0[0+$(i+1)] > max2ucost0 && i!=posmaxucost0) {
				max2ucost0 = ucost0[0+$(i+1)];
				posmax2ucost0 = i;
			}
		}
		if (posmaxucost0==0) {print "ERROR MAXUCOST0 NOT FOUND !!!" > "/dev/stderr"; exit 1}
		if (posmax2ucost0==0) {print "ERROR SECOND MAXUCOST0 NOT FOUND !!!" > "/dev/stderr"; exit 1}
		if (maxucost0>minucost1) {
			if (minucost1>0) {print "ERROR MINUCOST1>0 !!! NC NOT DONE???" > "/dev/stderr"; exit 1}
			print max2ucost0;
			lb += sumucost0-maxucost0;
			for (i=1;i<=$1;i++) {
				if (i==posmaxucost0) {
					ucost0[0+$(i+1)]=maxucost0-max2ucost0;
				} else {
					ucost1[0+$(i+1)]+=max2ucost0-ucost0[0+$(i+1)];
					ucost0[0+$(i+1)]=0;
				}
			}
		} else {
			print 0; ## do nothing! zero cost function
		}
		var=-1;
		ok=0;
	} else {
		var=-1;
		ok=1;
	}
}

ok {
  print $0;
}

END {
	m=0;
	for (var in ucost0) {
		m++;
		print 1,var,0,2;
		print ZEROVALUE,ucost0[var];
		print ONEVALUE,ucost1[var];
	}
	print 0,lb,0;
}


