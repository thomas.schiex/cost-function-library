#include <vector>
#include <iostream>
#include <sstream>
#include <tuple>
#include <algorithm>
#include "wcsp.hh"

using std::vector;
using std::istream;
using std::ostream;
using std::string;
using std::tuple;

template <typename T> vector<T> read_vec(istream& is)
{
    vector<T> r;
    T s;
    is >> s;
    while (is) {
        r.push_back(s);
        is >> s;
    }
    return r;
}

template <typename T> vector<T> read_vec(string const& line)
{
    std::istringstream iss(line);
    return read_vec<T>(iss);
}

tuple<string, size_t, size_t, size_t, Cost> read_header(string const& line)
{
    std::istringstream iss(line);

    string name;
    size_t nvars;
    size_t domsize;
    size_t nfun;
    Cost ub;

    iss >> name >> nvars >> domsize >> nfun >> ub;
    return make_tuple(name, nvars, domsize, nfun, ub);
}

wcspfunc read_fun(istream& is)
{
    string line;
    getline(is, line);
    vector<Cost> hd = read_vec<Cost>(line);
    size_t arity = hd[0];
    Cost defcost = hd[hd.size() - 2];
    size_t nspec = hd[hd.size() - 1];

    vector<wcsptuple> specs;
    for (size_t i = 0; i != nspec; ++i) {
        getline(is, line);
        vector<Cost> v = read_vec<Cost>(line);
        specs.push_back(
            {vector<int>(v.begin(), v.begin() + arity), v[v.size() - 1]});
    }

    return {vector<int>(hd.begin() + 1, hd.begin() + 1 + arity),
        wcsp_explicit_func{defcost, specs}};
}

wcsp readwcsp(istream& is)
{
    wcsp w;

    string name;
    size_t nvars;
    size_t domsize;
    size_t nfun;

    string line;

    getline(is, line);
    tie(w.name, nvars, domsize, nfun, w.ub) = read_header(line);

    getline(is, line);
    w.domains = read_vec<int>(line);

    for (size_t i = 0; i != nfun; ++i)
        w.functions.push_back(read_fun(is));

    return w;
}

struct func_writer {
    wcsp const& w;
    ostream& ofs;

    void operator()(wcsp_explicit_func const& f) const
    {
        ofs << f.defcost << ' ' << f.specs.size() << "\n";
        for (auto& s : f.specs) {
            for (auto& v : s.tup)
                ofs << v << ' ';
            ofs << std::min(s.cost, w.ub) << "\n";
        }
    }

    void operator()(wcsp_implicit_func const& f) const
    {
        ofs << -1 << " " << f.desc << "\n";
    }
};

void writewcsp(wcsp const& w, ostream& ofs)
{
    size_t maxd = *std::max_element(w.domains.begin(), w.domains.end());
    ofs << w.name << ' ' << w.nvars() << ' ' << maxd << ' '
        << w.functions.size() << ' ' << w.ub << "\n";

    for (auto& d : w.domains)
        ofs << d << ' ';
    ofs << "\n";

    for (auto& f : w.functions) {
        ofs << f.arity() << ' ';
        for (auto& v : f.scope)
            ofs << v << ' ';
        apply_visitor(func_writer{w, ofs}, f.function);
    }
}

int wcsp::add_var(int domsize)
{
    domains.push_back(domsize);
    return domains.size()-1;
}
