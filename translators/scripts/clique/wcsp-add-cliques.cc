// -*- Mode: c++; c-basic-offset: 4 -*-

#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>

#include <boost/program_options.hpp>

#include "fmt/format.h"
#include "fmt/ostream.h"

#include "wcsp.hh"


using namespace std;

const bool debug = false;

Cost getmin(vector<Cost> &ucost, vector<int>& values) {
	Cost minc = ucost[values[0]];
	for (unsigned int i = 1; i < values.size(); i++) {
		if (ucost[values[i]]<minc) minc = ucost[values[i]];
	}
	return minc;
}

void extend(vector<Cost> &ucost, vector<int>& values, Cost cost) {
	for (unsigned int i = 0; i < values.size(); i++) {
		ucost[values[i]] -= cost;
	}
}

bool lexico(vector<int> &s1, vector<int> &s2)
{
	unsigned int n1 = s1.size();
	unsigned int n2 = s2.size();
	for (unsigned int i=0; i<min(n1,n2); i++) {
		if (s1[i]<s2[i]) return true;
		else if (s1[i]>s2[i]) return false;
	}
	return false;
}

int main(int argc, char* argv[])
{
    namespace po = boost::program_options;

    string encoding;
    int perbranch{0};
    int bound{1};
	int ternary{1};
    po::options_description desc("Allowed options");
    desc.add_options()
        //
        ("help", "produce help message")
        //
        ("input,i", po::value<string>(), "WCSP input file")
        //
        ("output,o", po::value<string>(), "WCSP output file")
        //
        ("perbranch", po::value<int>(&perbranch)->default_value(0),
            "maximum #cliques per branch")
        //
        ("bound", po::value<int>(&bound)->default_value(1),
            "Add edges for costs >= k/bound")
        //
        ("ternary", po::value<int>(&ternary)->default_value(1),
            "keep all generated ternary cliques if nonzero")
        //
        ("encoding", po::value<string>(&encoding)->default_value("clique-cons"),
            "encode automata with decomp/sregulardp/clique-cons")
        //
        ;

    po::positional_options_description p;
    p.add("input", 1).add("output",1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).
              options(desc).positional(p).run(), vm);
    po::notify(vm);

    if (vm.count("help") || !vm.count("input") || !vm.count("output")) {
        cout << desc << "\n";
        return 1;
    }

    enum encoding enc{CLIQUE_CONS};
    if (encoding == "decomp")
        enc = DECOMP;
    else if (encoding == "sregulardp")
        enc = SREGULARDP;
    else if (encoding != "clique-cons") {
        fmt::print("Unknown encoding \"{}\"\n", encoding);
        return 1;
    }

    auto ifn{vm["input"].as<string>()}, ofn{vm["output"].as<string>()};
    ifstream ifs(ifn);
    ofstream ofs(ofn);

    if (!ifs) {
        cout << "could not open " << ifn << "\n";
        return 1;
    }

    if (!ofs) {
        cout << "could not open " << ofn << "\n";
        return 1;
    }

	int original=0;	
    wcsp w = readwcsp(ifs);

    Cost newtop = 0;
    for (auto const& vf : w.functions) {
        auto* fp = get<wcsp_explicit_func>(&vf.function);
        if (!fp) {
            // do not know how to get max cost of an implicit cost
            // function in a generic fashion
            newtop = w.ub;
            break;
        }
        auto& f = *fp;
        if (f.specs.empty())
            newtop += f.defcost;
        else {
            auto me = std::max_element(f.specs.begin(), f.specs.end(),
                [&](wcsptuple const& m, wcsptuple const& c) {
                    return c.cost < w.ub && (c.cost > m.cost || m.cost >= w.ub);
                });
            newtop += me->cost;
        }
    }
    w.ub = std::min(w.ub, newtop);
	original = w.functions.size();
    add_clique_cuts(w, enc, perbranch, bound);

	// Select and sort cliques
	set<int> zero;
	set<int> cover;
    vector<wcspfunc> sortedcliques;
	set<int> cliques;
	vector<int> func;
	vector<int> size;
	vector< map<int,int> > invscope;
	vector<Cost> maxucost0;
	vector<Cost> max2ucost0;
	vector<Cost> sumucost0;
	vector<Cost> lb;
	// get unary costs
	vector<vector<Cost>> ucost;
    for (auto& d : w.domains) ucost.push_back(vector<Cost>(d, 0));
    for (auto& vf : w.functions) {
        auto* fp = boost::get<wcsp_explicit_func>(&vf.function);
        if (!fp)
            continue;
        auto& f = *fp;
        if (vf.scope.size() != 1)
            continue;
		for (auto& t : f.specs) {
			//            cout << "x" << vf.scope[0] << "." << t.tup[0] << ": " << t.cost << endl; 
			ucost[vf.scope[0]][t.tup[0]] += t.cost;
		}
	}
	// performs NC
	int i = 0;
	for (auto& d : w.domains)  {
		Cost minv = w.ub;
		for (int v=0; v<d; v++) {
			if (ucost[i][v] < minv) minv = ucost[i][v];
		}
		if (minv > 0) {
			for (int v=0; v<d; v++) {
				ucost[i][v] -= minv;
			}
		}
		i++;
	}
	// examine all cliques (and sort in ascending order their scope)
	int m=0;
	int bestm=-1;
    for (unsigned int j=0; j<w.functions.size(); j++) {
		auto &vf = w.functions[j];
        auto* fp = boost::get<wcsp_implicit_func>(&vf.function);
        if (!fp)
            continue;
        auto& f = *fp;
        if (vf.scope.size() <= 2)
            continue;
        if (f.desc.find("clique") == std::string::npos)
            continue;
		cliques.insert(m);
		func.push_back(j);
		size.push_back(vf.scope.size());
		sumucost0.push_back(0);
		maxucost0.push_back(0);
		max2ucost0.push_back(0);
		sort(vf.scope.begin(), vf.scope.end());
		invscope.push_back( map<int, int>() );
		for (int i=0;i<size[m];i++) {
			invscope[m][vf.scope[i]] = i;
			Cost ucost0 = getmin(ucost[vf.scope[i]], f.values[vf.scope[i]]);
			sumucost0[m]+=ucost0;
			if (ucost0 > maxucost0[m]) {
				max2ucost0[m] = maxucost0[m];
				maxucost0[m] = ucost0;
			} else if (ucost0 > max2ucost0[m]) {
				max2ucost0[m] = ucost0;
			}
		}
		if (maxucost0[m]>0) {
			lb.push_back(sumucost0[m]-maxucost0[m]);
		} else {
			lb.push_back(0);
		}
		if (bestm==-1 || size[m]*lb[m] > size[bestm]*lb[bestm] || (size[m]*lb[m] == size[bestm]*lb[bestm] && lexico(vf.scope, w.functions[func[bestm]].scope))) bestm = m;
		m++;
	}
	// dynamic selection of cliques
	int c = m;
	int nbternsel = 0;
	for (int u=1;bestm!=-1 && u<=c;u++) {
		int m1=bestm;
		auto &vf1 = w.functions[func[m1]];
        auto* fp1 = boost::get<wcsp_implicit_func>(&vf1.function);
        auto& f1 = *fp1;
		
		//		cout << "SELECT " << m1 << " " << size[m1] << " " << lb[m1] << " " << sumucost0[m1] << " " << maxucost0[m1] << " " << posmaxucost0[m1]  << endl;
		sortedcliques.push_back(vf1);
		cliques.erase(m1);
		int arity1=vf1.arity();
		if (arity1==3) nbternsel++;

		vector<vector<Cost>> newucostm1;
		for (int i=0;i<arity1;i++) {
			int var1 = vf1.scope[i];
			if (maxucost0[m1]>0) {
				vector<Cost> newucostvar1(ucost[var1]);
				Cost ucost0f1 = getmin(ucost[var1], f1.values[var1]);
				Cost extend0f1 = min(ucost0f1, max2ucost0[m1]);
				for (auto v : f1.values[var1]) newucostvar1[v] -= extend0f1;
				newucostm1.push_back(newucostvar1);
			} else newucostm1.push_back(ucost[var1]);
		}

		// update clique "effective" sizes and lower-bounds
		bestm=-1;
		set<int>::iterator iterm = cliques.begin();
		while (iterm != cliques.end()) {
			int m=*iterm;
			auto &vf2 = w.functions[func[m]];
			auto* fp2 = boost::get<wcsp_implicit_func>(&vf2.function);
			auto& f2 = *fp2;
			++iterm;
			bool redomax=false;
			for (int i=0;i<arity1;i++) {
				int var1 = vf1.scope[i];
				if (invscope[m].find(var1) != invscope[m].end()) {
					if (cover.find(var1)==cover.end())  {
						size[m] -= 1;
						//cout << var1 << ": size[" << m << "] = " << size[m] << endl;
					}
					if (zero.find(var1)==zero.end()) {
						Cost ucost0f2old = getmin(ucost[var1], f2.values[var1]);
						Cost ucost0f2new = getmin(newucostm1[i], f2.values[var1]);
						if (ucost0f2new != ucost0f2old) {
							sumucost0[m] += ucost0f2new - ucost0f2old;
							if (ucost0f2old==maxucost0[m] || ucost0f2old==max2ucost0[m])  {
								redomax=true;
							}
						}
					}
				}
			}
			if (redomax) {
				int arity2=vf2.arity();
				maxucost0[m]=0;
				max2ucost0[m]=0;
				for (int i=0;i<arity2;i++) {
					int var2 = vf2.scope[i];
					Cost ucost0f2 = 0;
					map<int,int>::iterator index1 = invscope[m1].find(var2);
					if (index1 != invscope[m1].end()) {
						ucost0f2 = getmin(newucostm1[index1->second], f2.values[var2]);
					} else {
						ucost0f2 = getmin(ucost[var2], f2.values[var2]);
					}
					if (ucost0f2 > maxucost0[m]) {
						max2ucost0[m] = maxucost0[m];
						maxucost0[m] = ucost0f2;
					} else if (ucost0f2 > max2ucost0[m]) {
						max2ucost0[m] = ucost0f2;
					}
				}
			}
			lb[m] = sumucost0[m]-maxucost0[m];
			if (lb[m]==0 && vf2.arity()>((ternary)?3:2)) { // keep zero-cost ternary functions
				//				cout << "DEL " << m << " " << size[m] << " " << lb[m] << " " << sumucost0[m] << " " << maxucost0[m] << " " << posmaxucost0[m] << endl;
				cliques.erase(m);
			} else if (bestm==-1 || size[m]*lb[m] > size[bestm]*lb[bestm] || (size[m]*lb[m] == size[bestm]*lb[bestm] && lexico(vf2.scope, w.functions[func[bestm]].scope))) bestm = m;	 
		}
		// perform EPTs for previous best clique
		if (maxucost0[m1]>0) {
			for (int i=0;i<arity1;i++) {
				int var1 = vf1.scope[i];
				// Cost ucost0f1 = getmin(ucost[var1], f1.values[var1]);
				// extend(ucost[var1], f1.values[var1], min(ucost0f1, max2ucost0[m1]));
				ucost[var1] = newucostm1[i];
				bool allzero = true;
				for (int v=0; v<w.domains[var1]; v++) if (ucost[var1][v]!=0) {allzero=false; break;}
				if (allzero) zero.insert(var1);
			}
		}
		for (int i=0;i<arity1;i++) cover.insert(vf1.scope[i]);
		if (zero.size() == w.nvars()) break; // stop if all unary costs are zero
	}
	// remove all cliques and add sorted ones
    w.functions.resize(original);
	for (auto& f : sortedcliques) w.functions.push_back(f);
	cout << sortedcliques.size() << " selected cliques, including " << nbternsel << " ternary constraints" << endl;
    writewcsp(w, ofs);

    return 0;
}
