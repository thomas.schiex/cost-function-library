#ifndef WCSP_HH
#define WCSP_HH

#include <vector>
#include <map>
#include <set>
#include <string>
#include <iosfwd>
#include <algorithm>
#include <boost/variant.hpp>

using Cost = long long;

struct wcsptuple {
    std::vector<int> tup;
    Cost cost;
};

struct wcsp_explicit_func {
    Cost defcost;
    std::vector<wcsptuple> specs;
};

struct wcsp_implicit_func {
    std::string desc;
    std::vector<std::vector<int>> values;
};

struct wcspfunc {
    std::vector<int> scope;
    boost::variant<wcsp_explicit_func, wcsp_implicit_func> function;

    size_t arity() const { return scope.size(); }
};

struct wcsp {
    std::string name;
    Cost ub;

    std::vector<int> domains;
    size_t nvars() const { return domains.size(); }

    std::vector<wcspfunc> functions;

    int add_var(int domsize);
};

void writewcsp(wcsp const& w, std::ostream& ofs);
wcsp readwcsp(std::istream& is);

/* Find cliques and add clique cuts using an automaton encoding. Will
   renumber variables so that state variables stay between their
   previous and next input variable, if encoding == DECOMP. Will
   generate at most perbranch cliques at each branch of the search
   tree and the graph will have edges for all tuples with cost >=
   k/bound */
enum encoding { CLIQUE_CONS, SREGULARDP, DECOMP };
void add_clique_cuts(wcsp& w, encoding enc, int perbranch, int bound);

// For each (explicit or implicit) tuple of f, call P with that tuple
// and its cost
template <typename P>
void foreach_tuple(
    wcsp const& w, wcspfunc const& vf, std::vector<int>& current, P proc)
{
    // if not explicit, will throw as it should
    auto const& fdata = boost::get<wcsp_explicit_func>(vf.function);
    if (current.size() == vf.arity()) {
        auto i = std::lower_bound(begin(fdata.specs), end(fdata.specs), current,
            [&](auto&& spec, auto&& tup) {
                return std::lexicographical_compare(
                    begin(spec.tup), end(spec.tup), begin(tup), end(tup));
            });
        if (i->tup == current) {
            auto cost = i->cost;
            proc(current, cost);
        } else
            proc(current, fdata.defcost);
        return;
    }
    int idx = current.size();
    current.push_back(0);
    int var = vf.scope[idx];
    for (int j = 0; j != w.domains[var]; ++j) {
        current[idx] = j;
        foreach_tuple(w, vf, current, proc);
    }
    current.pop_back();
}

#endif
