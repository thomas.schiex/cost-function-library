#ifndef GRAPH_HH
#define GRAPH_HH

#include <boost/compressed_pair.hpp>
#include <map>
#include <vector>
#include <list>
#include <cassert>
#include <iostream>

#include "fmt/format.h"
#include "fmt/ostream.h"

//----------------------------------------------------------------------
// Generic algorithms

//--------------------------------------------------
// erase elements from a vector/vec, subject to a Pred
template <typename T, typename A, typename Pred>
void erase_if(std::vector<T, A>& v, Pred p)
{
    v.erase(std::remove_if(v.begin(), v.end(), p), v.end());
}


//--------------------------------------------------
// move elements from one vector to another, subject to a Pred
template <typename T, typename A, typename Pred>
void move_if(std::vector<T, A>& from, std::vector<T, A>& to, Pred p)
{
    erase_if(from, [&](auto&& e) {
        if (p(e)) {
            to.emplace_back(e);
            return true;
        }
        return false;
    });
}

//--------------------------------------------------
// output operator for vector, pair
namespace std {

template <typename T> ostream& operator<<(ostream& os, vector<T> const& v)
{
    os << "v(sz=" << v.size() << ")[";
    bool first = true;
    for (auto&& t : v) {
        if (first)
            first = false;
        else
            os << ",";
        os << t;
    }
    os << "]";
    return os;
}

template <typename U, typename T>
ostream& operator<<(ostream& os, pair<U, T> const& p)
{
    return os << "p{" << p.first << "," << p.second << "}";
}

}

//----------------------------------------------------------------------
// a graph structure with optional storing of additional data for each
// vertex

struct vertex_ref {
    int id;
    vertex_ref()
        : id()
    {
    }
    explicit vertex_ref(int i)
        : id(i)
    {
    }
};

bool operator==(vertex_ref u, vertex_ref v) { return u.id == v.id; }
bool operator!=(vertex_ref u, vertex_ref v) { return u.id != v.id; }
bool operator<(vertex_ref u, vertex_ref v) { return u.id < v.id; }

std::ostream& operator<<(std::ostream& os, vertex_ref u)
{
    os << "v" << u.id;
    return os;
}

template<typename VertexData>
struct vertex : public boost::compressed_pair<vertex_ref, VertexData>
{
    using base_type = boost::compressed_pair<vertex_ref, VertexData>;
    using base_type::base_type;

    int ref() const { return this->first(); }
    VertexData& data() { return this->second(); }
    VertexData const& data() const { return this->second(); }
};

/* An undirected graph with both adjacency list
   representations. No data here. */
struct graph_base {
public:
    virtual ~graph_base() {}

    void add_edge(vertex_ref v1, vertex_ref v2);

    std::vector<vertex_ref> const& neighbors(vertex_ref v) const { return E[v.id]; }
    std::vector<vertex_ref> const& vertices() const { return V; }
protected:
    std::vector<std::vector<vertex_ref>> E;
    std::vector<vertex_ref> V;

    bool has_vertex(vertex_ref r)
    {
        return 0 <= r.id && r.id < static_cast<int>(vertices().size());
    }
};

inline void graph_base::add_edge(vertex_ref v1, vertex_ref v2)
{
    assert(has_vertex(v1));
    assert(has_vertex(v2));
    E[v1.id].push_back(v2);
    E[v2.id].push_back(v1);
}

/* Graph, stores VertexData for each vertex. Can be void and will then
   take no extra space */
template <typename VertexData> struct graph : public graph_base {
public:
    using vertex_type = vertex<VertexData>;
public:
    vertex_ref get_vertex(VertexData const& vd);
    VertexData const& get_data(vertex_ref u) const;
protected:
    std::vector<vertex_type> vertex_data;
    std::map<VertexData, vertex_ref> rmap;
};

template <typename VD> vertex_ref graph<VD>::get_vertex(VD const& vd)
{
    if (!rmap.count(vd)) {
        auto newid = int{static_cast<int>(vertices().size())};
        auto newref = vertex_ref{newid};
        vertex_data.push_back(vertex<VD>(newref, vd));
        V.push_back(newref);
        E.push_back({});
        rmap[vd] = newref;
        return newref;
    }
    return rmap[vd];
}

template <typename VD> VD const& graph<VD>::get_data(vertex_ref u) const
{
    assert(u.id >= 0 && u.id < static_cast<int>(vertex_data.size()));
    return vertex_data[u.id].data();
}

/* Allows removing nodes, setting checkpoints and restoring to a
   previous checkpoint, to support backtracking*/
template<typename VertexData>
struct btgraph : graph<VertexData> {
    using graph<VertexData>::graph;
    btgraph(graph<VertexData> const& pg)
        : graph<VertexData>(pg)
    {
        constructM();
    }
    btgraph(graph<VertexData>&& pg)
        : graph<VertexData>(pg)
    {
        constructM();
    }

    // remove a single vertex
    void remove_vertex(vertex_ref v);
    // remove all vertices which satisfy predicate 'p'
    template <typename Pred> void remove_vertices(Pred p);
    // check whether a vertex is still in the graph
    bool has_vertex(vertex_ref v) const { return ing[v.id]; }
    // check whether an edge exists
    bool connected(vertex_ref u, vertex_ref v) const { return M(u,v); }

    int checkpoint()
    {
        removed.push_back({});
        return removed.size();
    }
    int current_checkpoint() const { return removed.size(); }
    void restore(int ckpt);

private:
    using boolref = std::vector<bool>::reference;
    using constboolref = std::vector<bool>::const_reference;
    boolref M(vertex_ref u, vertex_ref v) {
        if (u.id > v.id)
            std::swap(u,v);
        return Mstorage[u.id][v.id-u.id];
    }
    constboolref M(vertex_ref u, vertex_ref v) const {
        if (u.id > v.id)
            std::swap(u,v);
        return Mstorage[u.id][v.id-u.id];
    }
    void constructM() {
        Mstorage.resize(this->vertices().size());
        for(auto v : this->vertices()) {
            Mstorage[v.id].resize(this->vertices().size() - v.id);
            for(auto u : this->neighbors(v))
                M(u,v)=true;
        }
        ing.resize(this->vertices().size(), true);
    }
    std::vector<std::vector<bool>> Mstorage;
    std::vector<bool> ing;
    std::vector<std::vector<vertex_ref>> removed;
};

template <typename VD>
template <typename Pred>
void btgraph<VD>::remove_vertices(Pred p)
{
    for (auto v : this->V) {
        if (!has_vertex(v))
            continue;
        if (!p(v))
            erase_if(this->E[v.id], p);
        else
            ing[v.id] = false;
    }
    auto ckpt{current_checkpoint()};
    if (ckpt > 0)
        move_if(this->V, removed[ckpt - 1], p);
    else
        erase_if(this->V, p);
}

template <typename VD>
void btgraph<VD>::remove_vertex(vertex_ref u)
{
    remove_vertices([u](vertex_ref v) { return v==u; });
}

template <typename VD> void btgraph<VD>::restore(int ckpt)
{
    assert(ckpt >= 0 && ckpt < static_cast<int>(removed.size()));
    for (int c = current_checkpoint(); c > ckpt; --c) {
        for (auto u : removed[c - 1]) {
            for (auto v : this->neighbors(u))
                if (has_vertex(v)) {
                    this->E[v.id].push_back(u);
                    assert(this->E[v.id].size()
                        <= this->V.size() + removed[c - 1].size());
                }
        }
        // have to do it in separate loops
        for (auto u : removed[c - 1]) {
            this->V.push_back(u);
            ing[u.id] = true;
        }
        removed.pop_back();
    }
}

//----------------------------------------------------------------------
// Find a degeneracy ordering of the graph: each vertex in the
// ordering has at most d neighbors ahead of it in the
// ordering. Return the ordering and d

inline
std::pair<std::vector<vertex_ref>, int> degeneracy_ordering(graph_base& g)
{
    std::pair<std::vector<vertex_ref>, int> rv;
    auto& order{rv.first};
    auto& d{rv.second};

    std::vector<std::list<vertex_ref>> buckets;
    std::vector<int> degrees(g.vertices().size());
    std::vector<std::list<vertex_ref>::iterator> iterators(g.vertices().size());
    std::vector<bool> ordered(g.vertices().size());
    for (auto v : g.vertices()) {
        auto vd = g.neighbors(v).size();
        if (vd >= buckets.size())
            buckets.resize(vd+1);
        buckets[vd].push_front(v);
        degrees[v.id] = vd;
        iterators[v.id] = buckets[vd].begin();
        ordered[v.id] = false;
    }

    while(true) {
        size_t i{0};
        for (; i != buckets.size(); ++i)
            if (!buckets[i].empty())
                break;
        if (i == buckets.size())
            break;
        d = std::max(d,static_cast<int>(i));
        auto v = buckets[i].back();
        order.push_back(v);
        buckets[i].pop_back();
        ordered[v.id] = true;
        for (auto u : g.neighbors(v)) {
            if (ordered[u.id])
                continue;
            auto &ud = degrees[u.id];
            buckets[ud].erase(iterators[u.id]);
            --ud;
            buckets[ud].push_front(u);
            iterators[u.id] = buckets[ud].begin();
        }
    }

    fmt::print("graph has degeneracy {}\n", d);
    return rv;
}

//----------------------------------------------------------------------
// helper for find_all_cliques
struct find_all_cliques_state {
    // current tentative clique
    std::vector<vertex_ref> clique;

    // parameters
    int perbranch{0};

    // stats
    int nnodes{0};
    int nclq{0};

    // cached arrays to avoid allocations
    struct level_cache {
        std::vector<vertex_ref> potential, forbidden, cand;
    };
    std::vector<level_cache> cache;
};

template <typename VD, typename Pred>
int find_all_cliques_(btgraph<VD>& g, find_all_cliques_state& state, Pred p)
{
    auto& clique{state.clique};
    auto lvl{clique.size()};
    if (lvl == 0)
        state.cache.resize(g.vertices().size() + 1);
    auto& cache = state.cache[lvl];
    auto& nextcache = state.cache[lvl+1];
    auto &potential{cache.potential}, &forbidden{cache.forbidden},
        &cand{cache.cand};

    if (lvl == 0)
        potential = g.vertices();
    cand = potential;

    ++state.nnodes;

    // fmt::print("clique: {}\n", clique);
    // fmt::print("candidates: {}\n", cand);
    // fmt::print("forbidden: {}\n", forbidden);
    if (cand.empty() && forbidden.empty()) {
        ++state.nclq;
        p(clique);
        return 1;
    } else if (cand.empty())
        return 0;

    std::sort(begin(cand), end(cand), [&](auto v1, auto v2) {
        return g.neighbors(v1).size() < g.neighbors(v2).size();
    });
    std::sort(begin(forbidden), end(forbidden), [&](auto v1, auto v2) {
        return g.neighbors(v1).size() < g.neighbors(v2).size();
    });

    auto pivot = cand.back();
    if (!forbidden.empty()
        && g.neighbors(forbidden.back()).size() > g.neighbors(pivot).size())
        pivot = forbidden.back();
    erase_if(cand, [&](vertex_ref u) { return g.connected(u, pivot); });

    int inbranch{0};
    while (!cand.empty()) {
        //fmt::print("candidates: {}\n", cand);
        auto v = cand.back();
        cand.pop_back();

        // fmt::print("Branching on {} (degree {}) at lvl {}\n", v,
        //     g.neighbors(v).size(), g.current_checkpoint() + 1);
        clique.push_back(v);
        auto &nextforbidden{nextcache.forbidden},
            &nextpotential{nextcache.potential};
        nextforbidden.clear();
        std::copy_if(begin(forbidden), end(forbidden),
            std::back_inserter(nextforbidden),
            [&](vertex_ref u) { return g.connected(u, v); });
        nextpotential.clear();
        std::copy_if(begin(potential), end(potential),
            std::back_inserter(nextpotential),
            [&](vertex_ref u) { return g.connected(u, v); });

        auto thisbranch = find_all_cliques_(g, state, p);

        clique.pop_back();

        inbranch += thisbranch;
        if (state.perbranch > 0 && inbranch > state.perbranch)
            break;
        forbidden.push_back(v);
        erase_if(potential, [&](vertex_ref u) { return u == v; });
    }
    return inbranch;
}

// branch using the degeneracy ordering at the root
template <typename VD, typename Pred>
int find_all_cliques_degeneracy_(btgraph<VD>& g, find_all_cliques_state& state,
    std::vector<vertex_ref> const& order, Pred p)
{
    auto& clique{state.clique};
    auto lvl{clique.size()};
    if (lvl == 0)
        state.cache.resize(g.vertices().size() + 1);
    auto& cache = state.cache[lvl];
    auto& nextcache = state.cache[lvl+1];
    auto &potential{cache.potential}, &forbidden{cache.forbidden};

    potential = g.vertices();

    int inbranch{0};
    for (vertex_ref v : order) {
        clique.push_back(v);
        auto &nextforbidden{nextcache.forbidden},
            &nextpotential{nextcache.potential};
        nextforbidden.clear();
        std::copy_if(begin(forbidden), end(forbidden),
            std::back_inserter(nextforbidden),
            [&](vertex_ref u) { return g.connected(u, v); });
        nextpotential.clear();
        std::copy_if(begin(potential), end(potential),
            std::back_inserter(nextpotential),
            [&](vertex_ref u) { return g.connected(u, v); });

        auto thisbranch = find_all_cliques_(g, state, p);

        clique.pop_back();

        inbranch += thisbranch;
        if (state.perbranch > 0 && inbranch > state.perbranch)
            break;
        forbidden.push_back(v);
        erase_if(potential, [&](vertex_ref u) { return u == v; });
    }
    return inbranch;
}

/* Find all maximal cliques of g and call p for each of them. If
   perbranch > 0, then report at most perbranch cliques from each top
   level branch. This is a way to get a relatively diverse population
   of maximal cliques without enumerating all of them. */
template <typename VD, typename Pred>
void find_all_cliques(btgraph<VD>& g, Pred p, int perbranch)
{
    find_all_cliques_state state;
    state.perbranch = perbranch;
    auto dg{degeneracy_ordering(g)};
    auto& order{dg.first};
    auto deg{dg.second};

    // arbitrarily chosen bound for degeneracy
    if (deg > 100)
        find_all_cliques_(g, state, p);
    else
        find_all_cliques_degeneracy_(g, state, order, p);
    std::cout << state.nnodes << " nodes\n";
}

#endif
