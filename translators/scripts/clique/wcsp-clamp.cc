// -*- Mode: c++; c-basic-offset: 4 -*-

#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <unordered_set>

#include <boost/program_options.hpp>

#include "fmt/format.h"
#include "fmt/ostream.h"

#include "wcsp.hh"
#include "util.hh"

using namespace std;

const bool debug = false;

int main(int argc, char* argv[])
{
    namespace po = boost::program_options;

    string encoding;
    po::options_description desc("Allowed options");
    desc.add_options()
        //
        ("help", "produce help message")
        //
        ("input,i", po::value<string>(), "WCSP input file")
        //
        ("output,o", po::value<string>(), "WCSP output file")
        //
        ("variable,v", po::value<vector<int>>(), "Which variables to include")
        //
        ;

    po::positional_options_description p;
    p.add("input", 1).add("output",1).add("variable", -1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).
              options(desc).positional(p).run(), vm);
    po::notify(vm);

    if (vm.count("help") || !vm.count("input") || !vm.count("output")) {
        cout << desc << "\n";
        return 1;
    }

    auto ifn{vm["input"].as<string>()}, ofn{vm["output"].as<string>()};
    ifstream ifs(ifn);
    ofstream ofs(ofn);

    if (!ifs) {
        cout << "could not open " << ifn << "\n";
        return 1;
    }

    if (!ofs) {
        cout << "could not open " << ofn << "\n";
        return 1;
    }

    auto vars = vm["variable"].as<vector<int>>();
    cout << "variables: " << vars << "\n";
    std::unordered_set<int> varset(begin(vars), end(vars));

    wcsp w = readwcsp(ifs);

    erase_if(w.functions, [&](auto&& f) {
        return std::any_of(f.scope.begin(), f.scope.end(),
            [&](int var) { return varset.count(var) == 0; });
    });
    auto maxvar = *std::max_element(begin(varset),end(varset));
    w.domains.resize(maxvar+1);

    Cost newtop = 0;
    for (auto const& vf : w.functions) {
        auto* fp = get<wcsp_explicit_func>(&vf.function);
        if (!fp) {
            // do not know how to get max cost of an implicit cost
            // function in a generic fashion
            newtop = w.ub;
            break;
        }
        auto& f = *fp;
        if (f.specs.empty())
            newtop += f.defcost;
        else {
            auto me = std::max_element(f.specs.begin(), f.specs.end(),
                [&](wcsptuple const& m, wcsptuple const& c) {
                    return c.cost < w.ub && (c.cost > m.cost || m.cost >= w.ub);
                });
            newtop += me->cost;
        }
    }
    w.ub = newtop;
    writewcsp(w, ofs);

    return 0;
}
