#include <stdio.h>
#include <stdlib.h>

#define MAX_CARS 500
#define TRUE 1
#define FALSE 0

typedef struct
{
	int num_nodes;
	int num_edges;
	int *nodes;
	int **matrix;
} problem;

void createProblem(problem *p)
{
	int i,j; 

	p->matrix=(int **)malloc(sizeof(int *)*p->num_nodes);
	if(p->matrix==NULL)
		exit(-1);

	for(i=0;i<p->num_nodes;i++) 
	{ 
		p->matrix[i]=(int *)malloc(sizeof(int)*p->num_nodes);
		if(p->matrix[i]==NULL) exit(-1);

		for (j=0;j<p->num_nodes;j++)
		{

			p->matrix[i][j]=FALSE;

		}
	}
	
	p->nodes=(int *)malloc(sizeof(int)*p->num_nodes);
	
	for(i=0;i<p->num_nodes;i++) 
	{
		p->nodes[i]=1;
	}

}

void destroyProblem(problem *p)
{
	int k;
	
	for(k=0; k<p->num_nodes; k++) 
	{ 
		free(p->matrix[k]);
	}

	free(p->matrix);
	
	free(p->nodes);
}

int readProblem(FILE * f, problem *p,char **argv)
{
	
	char line[MAX_CARS];
	
	char aux1[20],aux2[20],type;
	
	int i,j,v1,v2,c,w;
	
	int valMax;
	
	FILE *of;
	
	//type=0 MAXCLIQUE - type=1 MIN VERTEX COVERING
	type = strtol(argv[2],NULL,0);
	w = strtol(argv[3],NULL,0);
	
	sprintf(line,"%s.wcnf",argv[1]);
	
	of = fopen(line, "w");
	
	if(of == NULL)
	{
		printf("\n Error generating the output file...\n");
		exit(-1);
	}
	
	printf(" OUTPUT FILE: %s\n",line);

	
	fgets(line, MAX_CARS, f);
	
	
	while (line[0] != 'p')
	{	
		fputs(line,of);
		fgets(line, MAX_CARS, f);
	}
	
	
	// p edge NODES EDGES
	sscanf(line, "%s %s %d %d", aux1, aux2, &p->num_nodes, &p->num_edges);
	
	printf("\n NODES:%d EDGES:%d \n",p->num_nodes,p->num_edges);
	
	createProblem(p);
	
	printf("\n DATASTRUCTURES CREATED\n");
		
	fgets(line, MAX_CARS, f);
	
	// read nodes
	while (line[0] != 'e')
	{	
		sscanf(line, "%s %d %d", aux1,&v1,&v2);
		
		p->nodes[v1-1]=v2;
		
		fgets(line, MAX_CARS, f);
	}
	
	// read edges
	for(i=0;i<p->num_edges;i++)
	{
		sscanf(line, "%s %d %d", aux1,&v1,&v2);
		
		p->matrix[v1-1][v2-1]=TRUE;
		p->matrix[v2-1][v1-1]=TRUE;

		fgets(line, MAX_CARS, f);
	}
	
	
	// generate cnf/wcnf file
	
	c=0;
	for(i=0;i<p->num_nodes;i++)
	{
		for(j=i+1;j<p->num_nodes;j++)
		{
			
			if(type==0 && p->matrix[i][j]==FALSE)
			{
				c++;
			}
			if(type==1 && p->matrix[i][j]==TRUE)
			{
				c++;
			}

		}
	}
	
	c+=p->num_nodes;
	
	printf("\n GENERATING %d VARIABLES AND %d CLAUSES\n",p->num_nodes,c);
	
	sprintf(line,"c PSEUDOBOOLEAN %d\n",p->num_nodes);
	fputs(line, of);
	
	if(w==0) sprintf(line,"p wcnf %d %d\n",p->num_nodes,c);
	else sprintf(line,"p wcnf %d %d %d\n",p->num_nodes,c,p->num_nodes);
	fputs(line, of);
	
	for(i=0;i<p->num_nodes;i++)
	{
		sprintf(line,"%d -%d 0\n",p->nodes[i],i+1);
		fputs(line, of);
	}
	

	
	//valMax=9999; // Hard constraint/clause
	valMax=p->num_nodes; // Hard constraint/clause
	
	for(i=0;i<p->num_nodes;i++)
	{
		for(j=i+1;j<p->num_nodes;j++)
		{
			if(p->matrix[i][j]==FALSE && type==0) // CLIQUE
			{
				sprintf(line,"%d %d %d 0\n",valMax,i+1,j+1);
				fputs(line, of);
			}
			if(p->matrix[i][j]==TRUE && type==1) // VERTEX
			{
				sprintf(line,"%d %d %d 0\n",valMax,i+1,j+1);
				fputs(line, of);
			}

		}
	}
	
	fclose(of);
	
	destroyProblem(p);
	
	printf("\n TRANSLATION SUCCEED\n");
	
	return TRUE;

}

int main(int argc,char ** argv)
{
	FILE* fileIn;
	problem p;
	
	if(argc!=4)
	{
		printf("\n Incorrect number of parameters. Usage: cli2cnf [inputfile] [0-CLIQUE|1-VERTEX] [0:NW|1:W]\n");
		exit(-1);
	}
	
	printf("\n Input file: %s ... \n",argv[1]);
	fileIn = fopen(argv[1], "r");
	
	if(fileIn == NULL)
	{
		printf("\n Incorrect number of parameters. Usage: cli2cnf [inputfile] [0-CLIQUE|1-VERTEX] [0:NW|1:W]\n");
		exit(-1);
	}
		
	printf("\n Translating... \n\n");
	
	readProblem(fileIn,&p,argv);
	
	fclose(fileIn);
	
}
