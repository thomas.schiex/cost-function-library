#ifndef _ExpressionParser_h_
#define _ExpressionParser_h_

#include <iostream>
#include <iomanip>
#include <sstream>
#include <exception>
#include <stdexcept>
#include <string>
#include <map>
#include <vector>
#include <cctype>

/**
 * @file ExpressionParser.h
 * @brief Defines an Abstract Syntax Tree and a parser for
 * prefix/infix/postfix expressions.
 */

namespace CSPXMLParser
{

using namespace std;

/**
 * TODO
 *
 * - handle operator priorities (infix only)
 * - distinguish booleans from integers in the valuation of an AST
 */


/**
 * return value of a function represented in an abstract syntax tree
 * (AST).
 *
 * one day we'll switch to a union with an int/bool/...
 */
typedef
int FunctionValue;

/// possible types for a variable or an expression
enum VarType {TYPE_INT,TYPE_BOOL};

/// user provided information about a variable
struct VarInfo
{
  int id; // identifier corresponding to the variable name
  VarType type; // type of the variable
};

typedef
map<string,VarInfo> VariableInfo; // list of variables (when used)

/**
 * an interface to get the value of a variable
 */
class VariableValuation
{
public:
  virtual ~VariableValuation() {}

  /**
   * return the value of a variable in the current interpretation
   *
   * @param id: the identifier of the variable. Whether id is a global
   * or local identifier is dependent on what has been recorded in the
   * AST.
   */
  virtual FunctionValue getVarValue(int id) const=0;

  // ??? could we get rid of the virtual
};

/**
 * @brief An abstract node of an abstract syntax tree (AST).
 */
class AST
{
public:
  enum Syntax {TREE,PREFIX,INFIX_C,POSTFIX,MATHML};

  virtual ~AST() {}

  /**
   * compute the value of this expression
   *
   * @param varValuation: provides the value of each variable. This
   * parameter is needed so that the code be usable from several
   * threads or contexts.
   */
  virtual FunctionValue value(const VariableValuation &varValuation)=0;


  void expression(ostream &s, Syntax syntax=INFIX_C)
  {
    switch(syntax)
    {
    case TREE:
      throw runtime_error("can't use this method to get an AST");
      break;
    case PREFIX:
      prefixExpression(s);
      break;
    case INFIX_C:
      infixExpression(s);
      break;
    case POSTFIX:
      postfixExpression(s);
      break;
    case MATHML:
      mathmlExpression(s);
      break;
    default:
      throw runtime_error("undefined syntax for an expressioin");
    }
  }

  virtual void prefixExpression(ostream &s)=0;
  virtual void infixExpression(ostream &s)=0;
  virtual void postfixExpression(ostream &s)=0;

  void mathmlExpression(ostream &s)
  {
    s << "<math>" << endl;
    mathmlExpressionRec(s);
    s << "</math>" << endl;
  }

  virtual void mathmlExpressionRec(ostream &s)=0;

  virtual void setArg(int pos, AST *subExpr)=0;
};

/**
 * @brief A variable represented in the AST.
 */
class ASTVar : public AST
{
private:
  string name;
  int id;
public:
  ASTVar(const string &name) {this->name=name; id=-1;}

  ASTVar(const string &name, int id) {this->name=name; this->id=id;}

  const string &getName() {return name;}

  virtual FunctionValue value(const VariableValuation &varValuation) 
  {
    return varValuation.getVarValue(id);
  }

  virtual void setArg(int pos, AST *subExpr) 
  {
    throw runtime_error("a variable cannot have any argument");
  }

  virtual void prefixExpression(ostream &s)
  {
    s << name 
      << "/$" << id 
      ;
  }

  virtual void infixExpression(ostream &s)
  {
    s // << name << "/"
      //<< "$" << id
      << "parm[" << id << "]"
      ;
  }

  virtual void postfixExpression(ostream &s)
  {
    s << name
      << "/$" << id 
      ;
  }

  virtual void mathmlExpressionRec(ostream &s)
  {
    s << "  <ci>" << name << "</ci>" << endl;
  }
};

/**
 * @brief An integer constant represented in the AST.
 */
class ASTInteger : public AST
{
private:
  int val;
public:
  ASTInteger(const string &expr) 
  {
    istringstream f(expr);
    f >> val;
  }

  virtual FunctionValue value(const VariableValuation &varValuation) 
  {
    return val;
  }

  virtual void setArg(int pos, AST *subExpr) 
  {
    throw runtime_error("a constant cannot have any argument");
  }

  virtual void prefixExpression(ostream &s)
  {
    s << val;
  }

  virtual void infixExpression(ostream &s)
  {
    s << val;
  }

  virtual void postfixExpression(ostream &s)
  {
    s << val;
  }

  virtual void mathmlExpressionRec(ostream &s)
  {
    s << "  <cn>" << val << "</cn>" << endl;
  }
};

/**
 * @brief A boolean constant represented in the AST.
 */
class ASTBoolean : public AST
{
private:
  bool val;
public:
  ASTBoolean(bool v) 
  {
    val=v;
  }

  virtual FunctionValue value(const VariableValuation &varValuation) 
  {
    if (val) 
      return 1; 
    else 
      return 0;
  }

  virtual void setArg(int pos, AST *subExpr) 
  {
    throw runtime_error("a constant cannot have any argument");
  }

  virtual void prefixExpression(ostream &s)
  {
    s << boolalpha << val << noboolalpha;
  }

  virtual void infixExpression(ostream &s)
  {
    s << boolalpha << val << noboolalpha;
  }

  virtual void postfixExpression(ostream &s)
  {
    s << boolalpha << val << noboolalpha;
  }

  virtual void mathmlExpressionRec(ostream &s)
  {
    if (val)
      s << "  <true/>" << endl;
    else
      s << "  <false/>" << endl;
  }

};

/**
 * @brief A generic representation of a function inside the AST
 */
class ASTAbstractFunction : public AST
{
protected:
  int nbarg;
  AST **args;

  const char *prefixSymbol;
  const char *infixSymbol;
  const char *mathMLSymbol;

public:
  ASTAbstractFunction(int nbarg, 
		      const char *prefixSymbol, 
		      const char *infixSymbol,
		      const char *mathMLSymbol)
  {
    if (nbarg<0)
      throw runtime_error("number of arguments cannot be negative");

    this->nbarg=nbarg;
    this->prefixSymbol=prefixSymbol;
    this->infixSymbol=infixSymbol;
    this->mathMLSymbol=mathMLSymbol;

    if (nbarg==0)
      args=NULL;
    else
      args=new AST *[nbarg];
  }

  virtual ~ASTAbstractFunction()
  {
    if (args!=NULL)
    {
      for(int i=0;i<nbarg;++i)
	delete args[i];

      delete[] args;
    }
  }

  virtual AST *makeNode()=0;

  virtual void setArg(int pos, AST *subExpr) 
  {
    if (pos<0 || pos>=nbarg)
      throw runtime_error("incorrect argument number");

    args[pos]=subExpr;
  }

  const char *getPrefixSymbol() {return prefixSymbol;}
  const char *getInfixSymbol() {return infixSymbol;}

  void prefixExpression(ostream &s)
  {
    s << prefixSymbol << "(";

    if (nbarg!=0 && args[0]!=NULL)
      args[0]->prefixExpression(s);

    for(int i=1;i<nbarg;++i)
    {
      s << ",";
      if (args[i]!=NULL)
	args[i]->prefixExpression(s);
    }

    s << ")";
  }

  void infixExpression(ostream &s)
  {
    if (infixSymbol!=NULL)
    {
      s << "("; // be careful with priorities

      if (args[0]!=NULL)
	args[0]->infixExpression(s);
      
      s << infixSymbol;

      if (args[1]!=NULL)
	args[1]->infixExpression(s);

      s << ")";
    }
    else
    {
      s << prefixSymbol << "(";

      if (nbarg!=0 && args[0]!=NULL)
	args[0]->infixExpression(s);

      for(int i=1;i<nbarg;++i)
      {
	s << ",";
	if (args[i]!=NULL)
	  args[i]->infixExpression(s);
      }

      s << ")";
    }
  }

  void postfixExpression(ostream &s)
  {
    if (nbarg!=0 && args[0]!=NULL)
      args[0]->postfixExpression(s);

    for(int i=1;i<nbarg;++i)
    {
      s << " ";
      if (args[i]!=NULL)
	args[i]->postfixExpression(s);
    }

    s << " " << prefixSymbol;
  }

  virtual void mathmlExpressionRec(ostream &s)
  {
    s << "  <apply>" << endl;
    s << "  <" << mathMLSymbol << "/>" << endl;
    for(int i=0;i<nbarg;++i)
    {
      if (args[i]!=NULL)
	args[i]->mathmlExpressionRec(s);
    }
    s << "  </apply>" << endl;
  }

protected:
  /**
   * check that all args are defined (not NULL)
   */
  void checkargs()
  {
    for(int i=0;i<nbarg;++i)
      if (args[i]==NULL)
	throw runtime_error("missing argument for a function");
  }

};


class FunctionNeg : public ASTAbstractFunction
{
public:
  FunctionNeg() : ASTAbstractFunction(1,"neg",NULL,"minus") {}

  virtual AST *makeNode() {return new FunctionNeg;}

  FunctionValue value(const VariableValuation &varValuation)
  {
    checkargs();
    return -args[0]->value(varValuation);
  }

  void infixExpression(ostream &s)
  {
    s << "-";
    s << "("; // ??? avoid these parentheses unless we need them

    if (args[0]!=NULL)
      args[0]->infixExpression(s);
      
    s << ")";
  }
};

class FunctionAbs : public ASTAbstractFunction
{
public:
  FunctionAbs() : ASTAbstractFunction(1,"abs",NULL,"abs") {}

  virtual AST *makeNode() {return new FunctionAbs;}

  FunctionValue value(const VariableValuation &varValuation)
  {
    checkargs();
    return abs(args[0]->value(varValuation));
  }
};

class FunctionAdd : public ASTAbstractFunction
{
public:
  FunctionAdd() : ASTAbstractFunction(2,"add","+","plus") {}

  AST *makeNode() {return new FunctionAdd;}

  FunctionValue value(const VariableValuation &varValuation)
  {
    checkargs();
    return args[0]->value(varValuation)+args[1]->value(varValuation);
  }
};

class FunctionSub : public ASTAbstractFunction
{
public:
  FunctionSub() : ASTAbstractFunction(2,"sub","-","minus") {}

  AST *makeNode() {return new FunctionSub;}

  FunctionValue value(const VariableValuation &varValuation)
  {
    checkargs();
    return args[0]->value(varValuation)-args[1]->value(varValuation);
  }
};

class FunctionMul : public ASTAbstractFunction
{
public:
  FunctionMul() : ASTAbstractFunction(2,"mul","*","times") {}

  AST *makeNode() {return new FunctionMul;}

  FunctionValue value(const VariableValuation &varValuation)
  {
    checkargs();
    return args[0]->value(varValuation)*args[1]->value(varValuation);
  }
};

class FunctionDiv : public ASTAbstractFunction
{
public:
  FunctionDiv() : ASTAbstractFunction(2,"div","/","quotient") {}

  AST *makeNode() {return new FunctionDiv;}

  FunctionValue value(const VariableValuation &varValuation)
  {
    checkargs();

    int div=args[1]->value(varValuation);

    if (div==0)
      throw runtime_error("divide by 0");

    return args[0]->value(varValuation)/div;
  }
};

class FunctionMod : public ASTAbstractFunction
{
public:
  FunctionMod() : ASTAbstractFunction(2,"mod","%","rem") {}

  AST *makeNode() {return new FunctionMod;}

  FunctionValue value(const VariableValuation &varValuation)
  {
    checkargs();

    int div=args[1]->value(varValuation);


    if (div==0)
      throw runtime_error("modulo by 0");

    // ??? implementation defined !!
    return args[0]->value(varValuation)%div;
  }
};

class FunctionPow : public ASTAbstractFunction
{
public:
  FunctionPow() : ASTAbstractFunction(2,"pow",NULL,"power") {}

  AST *makeNode() {return new FunctionPow;}

  FunctionValue value(const VariableValuation &varValuation)
  {
    checkargs();

    long long prod=1;
    long long x=args[0]->value(varValuation);
    int n=args[1]->value(varValuation);
    int test;

    if (x==0 && n==0)
      throw runtime_error("pow(0,0) is undefined");
    
    while(n!=0)
    {
      if ((test=prod)!=prod || (test=x)!=x)
	throw runtime_error("overflow in pow(x,y) computation");

      if (n&0x01)
	prod*=x;

      x*=x;
    }

    if ((test=prod)!=prod) // ??? reliable ?
      throw runtime_error("overflow in pow(x,y) computation");

    return prod;
  }
};

class FunctionMin : public ASTAbstractFunction
{
public:
  FunctionMin() : ASTAbstractFunction(2,"min",NULL,"min") {}

  AST *makeNode() {return new FunctionMin;}

  FunctionValue value(const VariableValuation &varValuation)
  {
    checkargs();

    return std::min(args[0]->value(varValuation),args[1]->value(varValuation));
  }
};

class FunctionMax : public ASTAbstractFunction
{
public:
  FunctionMax() : ASTAbstractFunction(2,"max",NULL,"max") {}

  AST *makeNode() {return new FunctionMin;}

  FunctionValue value(const VariableValuation &varValuation)
  {
    checkargs();

    return std::max(args[0]->value(varValuation),args[1]->value(varValuation));
  }
};

class FunctionEQ : public ASTAbstractFunction
{
public:
  FunctionEQ() : ASTAbstractFunction(2,"eq","==","eq") {}

  AST *makeNode() {return new FunctionEQ;}

  FunctionValue value(const VariableValuation &varValuation)
  {
    checkargs();

    return args[0]->value(varValuation)==args[1]->value(varValuation);
  }
};

class FunctionNE : public ASTAbstractFunction
{
public:
  FunctionNE() : ASTAbstractFunction(2,"ne","!=","neq") {}

  AST *makeNode() {return new FunctionNE;}

  FunctionValue value(const VariableValuation &varValuation)
  {
    checkargs();

    return args[0]->value(varValuation)!=args[1]->value(varValuation);
  }
};

class FunctionGE : public ASTAbstractFunction
{
public:
  FunctionGE() : ASTAbstractFunction(2,"ge",">=","geq") {}

  AST *makeNode() {return new FunctionGE;}

  FunctionValue value(const VariableValuation &varValuation)
  {
    checkargs();

    return args[0]->value(varValuation)>=args[1]->value(varValuation);
  }
};

class FunctionGT : public ASTAbstractFunction
{
public:
  FunctionGT() : ASTAbstractFunction(2,"gt",">","gt") {}

  AST *makeNode() {return new FunctionGT;}

  FunctionValue value(const VariableValuation &varValuation)
  {
    checkargs();
    if (args[0]->value(varValuation)>args[1]->value(varValuation))
      return FunctionValue(1);
    else
      return FunctionValue(0);
  }
};

class FunctionLE : public ASTAbstractFunction
{
public:
  FunctionLE() : ASTAbstractFunction(2,"le","<=","leq") {}

  AST *makeNode() {return new FunctionLE;}

  FunctionValue value(const VariableValuation &varValuation)
  {
    checkargs();

    return args[0]->value(varValuation)<=args[1]->value(varValuation);
  }
};

class FunctionLT : public ASTAbstractFunction
{
public:
  FunctionLT() : ASTAbstractFunction(2,"lt","<","lt") {}

  AST *makeNode() {return new FunctionLT;}

  FunctionValue value(const VariableValuation &varValuation)
  {
    checkargs();
    if (args[0]->value(varValuation)<args[1]->value(varValuation))
      return FunctionValue(1);
    else
      return FunctionValue(0);
  }
};

class FunctionNot : public ASTAbstractFunction
{
public:
  FunctionNot() : ASTAbstractFunction(1,"not","!","not") {}

  AST *makeNode() {return new FunctionNot;}

  FunctionValue value(const VariableValuation &varValuation)
  {
    checkargs();

    return !args[0]->value(varValuation);
  }
};

class FunctionAnd : public ASTAbstractFunction
{
public:
  FunctionAnd() : ASTAbstractFunction(2,"and","&&","and") {}

  AST *makeNode() {return new FunctionAnd;}

  FunctionValue value(const VariableValuation &varValuation)
  {
    checkargs();

    return args[0]->value(varValuation)&&args[1]->value(varValuation);
  }
};

class FunctionOr : public ASTAbstractFunction
{
public:
  FunctionOr() : ASTAbstractFunction(2,"or","||","or") {}

  AST *makeNode() {return new FunctionOr;}

  FunctionValue value(const VariableValuation &varValuation)
  {
    checkargs();

    return args[0]->value(varValuation)||args[1]->value(varValuation);
  }
};

class FunctionXor : public ASTAbstractFunction
{
public:
  FunctionXor() : ASTAbstractFunction(2,"xor","^","xor") {}

  AST *makeNode() {return new FunctionXor;}

  FunctionValue value(const VariableValuation &varValuation)
  {
    checkargs();

    // ??? OK with booleans ?
    return args[0]->value(varValuation)^args[1]->value(varValuation);
  }
};




class ExpressionParser
{
private:
  vector<ASTAbstractFunction *> functionList;

  const VariableInfo *varInfo; // an optional map which provides some
			       // information on the variables that
			       // may occur in the expression

public:
  ExpressionParser()
  {
    varInfo=NULL;

    functionList.push_back(new FunctionNeg);
    functionList.push_back(new FunctionAbs);
    functionList.push_back(new FunctionAdd);
    functionList.push_back(new FunctionSub);
    functionList.push_back(new FunctionMul);
    functionList.push_back(new FunctionDiv);
    functionList.push_back(new FunctionMod);
    functionList.push_back(new FunctionPow);
    functionList.push_back(new FunctionMin);
    functionList.push_back(new FunctionMax);
    functionList.push_back(new FunctionEQ);
    functionList.push_back(new FunctionNE);
    functionList.push_back(new FunctionGE);
    functionList.push_back(new FunctionGT);
    functionList.push_back(new FunctionLE);
    functionList.push_back(new FunctionLT);
    functionList.push_back(new FunctionNot);
    functionList.push_back(new FunctionAnd);
    functionList.push_back(new FunctionOr);
    functionList.push_back(new FunctionXor);
  }

  void setVarInfo(const VariableInfo *varInfo=NULL)
  {
    this->varInfo=varInfo;
  }

  void unsetVarInfo()
  {
    this->varInfo=NULL;
  }

  AST *prefixParser(const string &expr)
  {
    string s;

    // remove spaces
    for(unsigned int i=0;i<expr.length();++i)
      if (!isspace(expr[i]))
	s+=expr[i];

    return recursivePrefixParser(s);
  }

  AST *infixParser(const string &expr)
  {
    return new ASTVar("infix parser unimplemented");
  }

  AST *postfixParser(const string &expr)
  {
    return new ASTVar("postfix parser unimplemented");
  }

private:
  AST *recursivePrefixParser(const string &f)
  {
    int level=0;
    int argNum=0;
    int subExprStart=0;

    AST *node=NULL;

    for(unsigned int i=0;i<f.length();++i)
    {
      if (f[i]=='(')
      {
	if (level==0)
	{
	  node=findPrefixFunction(f.substr(0,i));
	  subExprStart=i+1;
	}
	++level;
      }
      else
      {
	if (level==1 && (f[i]==',' || f[i]==')'))
	{
	  node->setArg(argNum,prefixParser(f.substr(subExprStart,
						    i-subExprStart)));
	  ++argNum;

	  subExprStart=i+1;
	}

        if (f[i]==')')
	  --level;
      }
    }

    if (level!=0)
      throw runtime_error("unbalanced parentheses");

    if (node==NULL)
    {
      // no opening parenthese found, this is a constant or a variable
      if (isalpha(f[0]))
      {
	if (f=="true")
	  node=new ASTBoolean(true);
	else
	  if (f=="false")
	    node=new ASTBoolean(false);
	  else
	  {
	    // a variable
	    if (varInfo==NULL)
	      node=new ASTVar(f);
	    else
	    {
	      VariableInfo::const_iterator it=varInfo->find(f);

	      if(it==varInfo->end())
		throw runtime_error("undefined variable found in expression");

	      node=new ASTVar(f,(*it).second.id);
	    }
	  }
      }
      else
      {
	// an int
	node=new ASTInteger(f);
      }
    }

    return node;
  }

  AST *findPrefixFunction(const string &name)
  {
    for(unsigned int i=0;i<functionList.size();++i)
      if (functionList[i]->getPrefixSymbol()==name)
	return functionList[i]->makeNode();

    throw runtime_error("unknown function symbol");
  }
};

} // namespace


// Local Variables:
// mode: C++
// End:


#endif
