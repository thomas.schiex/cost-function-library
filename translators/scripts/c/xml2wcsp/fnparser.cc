#include "ExpressionParser.h"

int main(int argc, char **argv)
{
  ExpressionParser parser;

  AST *expr=parser.prefixParser("gt(Y2,abs(sub(Y1, Y0)))");

  cout << "prefix:  ";
  expr->prefixExpression(cout);
  cout << endl;

  cout << "infix:   ";
  expr->infixExpression(cout);
  cout << endl;

  cout << "postfix: ";
  expr->postfixExpression(cout);
  cout << endl;

  cout << "mathml: " << endl;
  expr->mathmlExpression(cout);
  cout << endl;

  return 0;
}
