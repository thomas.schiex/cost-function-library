#ifndef _SampleCallback_h_
#define _SampleCallback_h_

#include "CSPParserCallback.h"

#define ShowNumericalIDS

#define MAXDOMS 500
#define MAXDOMSIZE 1000

#include <iostream>
#include <fstream>
#include <list>

using namespace std;

string fname;
bool intention = false;
bool convertWCSP = true;


namespace CSPXMLParser
{


// definition of the functions which are called by the parser when it
// reads new information in the file. These functions are in charge of
// creating the data structure that the solver needs to do his job
class SampleCallback : public CSPParserCallback
{
public:

  ofstream f,fsol,fsol1;	
  ifstream sol;	

  typedef struct {
  	int index;
  	int tdom;
  } var;


  typedef struct {
  	int arity;
  	int def;
  	list<int*> tuples;
  	bool suport;
  	int id;
  } relation;

  typedef struct {
  	int id;
  	AST* exp;
  	string expstr;
  } predicate;

  typedef struct {
	 list<int> scope;
     int arity;
     int type;
     string ref;
  } ctr;

   	 	
  map<int,var*> vars;
  map<string,relation*> rels;
  map<string,predicate*> preds;

  list<ctr*> ctrs;



  int currentDom;	
  relation* currentR;	
  predicate* currentP;	
  ctr* currentC;
  int currentIndexArity;	


  int nDoms[MAXDOMS];
  int Doms[MAXDOMS][MAXDOMSIZE];
  int DomsToIndex[MAXDOMS][MAXDOMSIZE];

  int maxDomSize;	  
  

  SampleCallback() 
  {
  }

  virtual void beginInstance(const string & name) 
  {
	 
	 if(!convertWCSP) 
	 {
	    sol.open("sol");	
	    if(!sol) { cout << "cannot open solution file to translate" << endl; exit(1); }
	    fsol.open("solution");	
	    fsol1.open("onesolution");	
	    cout << "solution file: sol to solution" << endl;  
	 }

  	 for(int i=0;i<MAXDOMS;i++) nDoms[i] = 0;
  	 maxDomSize = 0;

	 if(convertWCSP)  {
 	  	 int pos = fname.find_last_of(".");
	  	 string nameExt = fname.substr(0,pos) + ".wcsp";
	     f.open(nameExt.c_str());	

		 if (!f) {
	       cerr << "Could not open file " << endl;
		   exit(EXIT_FAILURE);
	     }	 	
	 	cout << "translate " << fname << " to " << nameExt << endl;
	 }

  }

  virtual void beginDomainsSection(int nbDomains) {}

  virtual void beginDomain(const string & name, int idDomain, int nbValue) 
  {
  	currentDom = idDomain;
  }

  void addDomainValue(int v) 
  {
  	Doms[currentDom][ nDoms[currentDom] ] = v;
  	DomsToIndex[currentDom][ v ] = nDoms[currentDom];
  	nDoms[currentDom]++;
  }

  virtual void addDomainValue(int first,int last) 
  {
  	for(int i=first;i<=last;i++) addDomainValue(i);
  }

  virtual void endDomain() {}

  virtual void endDomainsSection() { f << endl; }


  virtual void beginVariablesSection(int nbVariables) 
  {
  }
  
  virtual void addVariable(const string & name, int idVar,
			   const string & domain, int idDomain) 
  {
  	var* v = new var;
  	v->index = vars.size();
  	v->tdom = idDomain;
  	vars[idVar] = v;   // c'est un map pas un vecteur, donc pas souci
	if(maxDomSize < nDoms[idDomain]) maxDomSize = nDoms[idDomain];
  }

  virtual void endVariablesSection() 
  {
  }


  virtual void beginRelationsSection(int nbRelations) 
  {
  }

  
  virtual void beginRelation(const string & name, int idRel,
			     int arity, int nbTuples, bool isSupport) 
  {
		relation* r = new relation;
		r->id = idRel;
		r->arity = arity;
		r->suport = isSupport;
		currentR = r;
		rels[name] = r;
  }

  virtual void addRelationTuple(int arity, int tuple[]) 
  {
  		int* t = new int [arity]; 
	    for(int i=0;i<arity;++i)  t[i] = tuple[i];
	    currentR->tuples.push_back(t);
  }

  virtual void endRelation() 
  {
  }

  virtual void endRelationsSection() 
  {
  }

  virtual void beginPredicatesSection(int nbPredicates) 
  {
  }
  
  virtual void beginPredicate(const string & name, int idPred) 
  {
		predicate* p = new predicate;
		p->id = idPred;
		p->exp = NULL;
		preds[name] = p;
		currentP = p;
		
		intention = true;	  	  	  	    
  }

  virtual void addFormalParameter(int pos, const string & name, 
				  const string & type) 
  {
  }

  virtual void predicateExpression(AST *tree) 
  {
  	 //AST* newexp = new AST(*tree); 
     //currentP->exp = newexp;
  }

  virtual void predicateExpression(const string &expr) 
  {
     currentP->expstr = expr;
  }

  virtual void endPredicate() 
  {
  }

  virtual void endPredicatesSection() 
  {
  }

  virtual void beginConstraintsSection(int nbConstraints) 
  {
  }
  
  virtual void beginConstraint(const string & name, int idConstr,
			       int arity, 
			       const string & reference, 
			       CSPDefinitionType type, int id)
  {
  	 currentC = new ctr;
     currentC->arity = arity;
     currentC->type = type;
     currentC->ref = reference;
  }

  virtual void addVariableToConstraint(const string & name, int idVar) 
  {
  	currentC->scope.push_back(idVar);
  }


  virtual void addEffectiveParameter(int pos, const string & expr) 
  {
  }

  virtual void addEffectiveParameter(int pos, AST *tree) 
  {
  }

  virtual void addEffectiveParameterVariable(int pos, 
					     const string & name, 
					     int idVar) 
  {
  }

  virtual void addEffectiveParameterInteger(int pos, 
					    int value)
  {
  }

  virtual void addEffectiveParameterList(int pos) 
  {
  }

  /**
   * indicate the start of a list
   */
  virtual void beginList() 
  {
  }

  virtual void addListVariable(int pos, const string & name, int idVar) 
  {
  }

  virtual void addListInteger(int pos, int value) 
  {
  }

  virtual void endList() 
  {
  }

  virtual void endConstraint() 
  {
  	ctrs.push_back(currentC);
  }

  /**
   * end the definition of all constraints
   */
  virtual void endConstraintsSection() 
  {
  }

  /********************************************************************/


  void writeWCSP()
  {
  	int i,j;
  	
  	f << "traductionXMLaWCSP ";
  	f << vars.size() << " ";
  	f << maxDomSize << " ";
  	f << ctrs.size() << " ";
  	f << 1000000 << endl;
  	

	map<int,var*>::iterator  it = vars.begin();
	while(it != vars.end()) {
		int id = it->first;
		var* v =  it->second;		
		it++;
		
        f << nDoms[v->tdom] << " ";
	} 
	f << endl;

	list<ctr*>::iterator itc = ctrs.begin();
	while(itc != ctrs.end())
	{
		ctr* c = *itc;
		
		relation* r = NULL;
	  	if(c->type == RelationType) {
	
			f << c->arity << " ";

			int index = 0;
			int* varScopeIndex = new int [c->arity]; 

			list<int>::iterator its = c->scope.begin();
			while(its != c->scope.end()) {
				int id = *its;
				varScopeIndex[index] = id;
				f << vars[id]->index << " ";
				index++;
				its++;
			}
	
			map<string,relation*>::iterator it = rels.find(c->ref);
			if(it != rels.end()) {
				r = it->second;  				

				if(r->suport) f << 1 << " ";
				else f << 0 << " ";
				
				f << r->tuples.size() << endl;

	  			list<int*>::iterator  itl = r->tuples.begin(); 
	  			while(itl != r->tuples.end()) {
	  				int* t = *itl;
	  				for(i=0;i<r->arity;i++) 
	  				{ 
	  					int varId = varScopeIndex[i];
	 					var* v = vars[varId];
	  					f << DomsToIndex[v->tdom][ t[i] ] << " ";
	  				}
					if(r->suport) f << 0 << " " << endl;
					else f << 1 << " " << endl;
	  				itl++;
	  			}
			} 
			
			delete [] varScopeIndex;
	  	}
	  	
 	  	if(c->type == PredicateType) intention = true;	  	  	  	    
 
  	    itc++;
	 }
  	
  	 f.close();
  } 


  void convertSol()
  {
  	int indexv;
  	
  	fsol << "SOL ";
  	map<int,var*>::iterator  it = vars.begin();
	while(it != vars.end()) {
		int id = it->first;
		var* v =  it->second;		
		it++;

	  	sol >> indexv;
		
        fsol << Doms[v->tdom][indexv] << " ";
        fsol1 << Doms[v->tdom][indexv] << " ";
	} 
	fsol1 << endl;
	fsol << endl;
	
	sol.close();
	fsol.close();
	fsol1.close();
  }



  /**
   * signal the end of parsing
   */
  virtual void endInstance() 
  {
  	if(convertWCSP) writeWCSP();
  	else convertSol();
  }

};

} // namespace

#endif

