#include "CSPParserCallback.h"
#include "XMLParser.h"


#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

using namespace CSPXMLParser;

#include "SampleCallback.h"
extern string fname;
extern bool intention;
extern bool convertWCSP;
 


int main(int argc, char *argv[])
{
  struct rlimit rlim;
  getrlimit(RLIMIT_STACK,&rlim);
  //cout << "stacksize=" << (rlim.rlim_cur>>10) << " KB" << endl;
  //cout << "sizeof(cb)=" << (sizeof(cb)>>10) << "KB" << endl;

  if(argc > 1) {
	string argv_string(argv[1]);
	fname =  argv_string + ".xml";
  }
  else
  {
    cout << "usage:  xml2wcsp xml_file_with_no_extension" << endl; 
	return 0;
  }


  try {
  	SampleCallback cb; 
    XMLParser<SampleCallback> parser(&cb); // the parser that will decode the XML file
    parser.setPreferredExpressionRepresentation(AST::INFIX_C);
    parser.parse((char *) fname.c_str()); // parse the input file
  }
  catch (exception &e)
  {
    cout.flush();
    cerr << "\n\tUnexpected exception :\n";
    cerr << "\t" << e.what() << endl;
    exit(1);
  }

  int pos = fname.find_last_of(".");
	
	if(intention && convertWCSP) 
	{
	  	string fname_ext = fname.substr(0,pos) + "_ext.xml";
		string appel1 = "java -jar instanceChecker.jar " + fname + " 4"; 
		cout << "instance en intention" << endl;   	
		cout << appel1 << endl; std::system(appel1.c_str());
		fname = fname_ext;
	
		  try {
		  	SampleCallback cb; 
		    XMLParser<SampleCallback> parser(&cb); // the parser that will decode the XML file
		    parser.setPreferredExpressionRepresentation(AST::INFIX_C);
		    parser.parse((char *) fname.c_str()); // parse the input file
		  }
		  catch (exception &e)
		  {
		    cout.flush();
		    cerr << "\n\tUnexpected exception :\n";
		    cerr << "\t" << e.what() << endl;
		    exit(1);
		  }	
	}
	
	/*
	if(!convertWCSP) 
	{ 
		string appel1 = "java -jar solutionChecker.jar " + fname + " onesolution"; 
		cout << appel1 << endl; std::system(appel1.c_str());
	}*/
	

    return 0;
}
