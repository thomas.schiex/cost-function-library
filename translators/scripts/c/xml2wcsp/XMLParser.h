#ifndef _XMLParser_h_
#define _XMLParser_h_

#define LIBXERCES_C

/**
 * @file XMLParser.h
 * @brief defines the XMLParser class to parse a CSP instance in XML format
 */

/*
 * TODO
 *
 * - detect unused attributes
 * - add the necessary syntax checks
 * - check the number of elements per section
 * - check arity and type of formal/effective parameters
 */


#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <deque>
#include <sstream>
#include <stdexcept>

#ifdef LIBXERCES_C
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/TransService.hpp>
#include <xercesc/parsers/SAXParser.hpp>
#include <xercesc/util/OutOfMemoryException.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/framework/XMLFormatter.hpp>
#include <xercesc/util/XMLChar.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/XMLStringTokenizer.hpp>
#include <xercesc/sax/InputSource.hpp>
#include <xercesc/util/BinInputStream.hpp>
#endif

#ifdef LIBXML2
#include <libxml/parser.h>
#endif

#include "ExpressionParser.h"
#include "CSPParserCallback.h"

/**
 * @namespace CSPXMLParser
 * @brief this namespace encloses all definitions relative to the
 * CSP XML format parser.
 */
namespace CSPXMLParser
{

using namespace std;

#ifdef LIBXERCES_C
using namespace xercesc;

ostream &operator <<(ostream &s, const XMLCh *str)
{
  char *txt=XMLString::transcode(str);
  s << txt;
  XMLString::release(&txt);
  return s;
}

void copy(string &dest, const XMLCh *src)
{
  char *txt=XMLString::transcode(src);
  dest=txt;
  XMLString::release(&txt);
}

class StreamInputSource : public InputSource
{
private:
  istream &in;
public:
  class StreamBinInputStream : public BinInputStream
  {
  private:
    unsigned int curpos;
    istream &in;
  public:
    StreamBinInputStream(istream &in) : in(in)
    {
      curpos=0;
    }

    virtual unsigned int curPos() const
    {
      return curpos;
    }

    virtual unsigned int readBytes(XMLByte *const toFill, 
				   const unsigned int maxToRead)
    {
      in.read((char *)toFill,maxToRead);

      int n=in.gcount();

      if (n>=0)
	curpos+=n;

      return n;
    }
  };

  StreamInputSource(istream &in) : in(in) {}

  virtual BinInputStream *makeStream () const
  {
    return new StreamBinInputStream(in);
  }
};
#endif



/**
 * @brief contains a parser for the CSP XML format.
 *
 * This class proposes a parser in SAX mode (to use less memory than
 * in DOM mode)
 *
 * @param Callback the type of the class that will be used to transmit
 * information from the parser to the solver
 *
 */
template<class Callback>
class XMLParser
{
private:
  Callback *cb;
  AST::Syntax preferredSyntax; // preferred syntax for
                               // transmitting an expression
			       // to the solver

  ExpressionParser exprParser;

  /// stores some information on each symbol defined in the CSP instance
  struct SymbolInfo
  {
    CSPDefinitionType type; ///< kind of definition
    int id; ///< identifier

    SymbolInfo()
    {
      id=-1;
      type=UndefinedType;
    }

    SymbolInfo(int id, CSPDefinitionType type)
    {
      this->id=id;
      this->type=type;
    }
  };

  typedef
    map<string,SymbolInfo> SymbolDictionnary;

  SymbolDictionnary symbolDict;

  // stores the next available ID in each category
  int nextId[UndefinedType];

  /**
   * register a new symbol in its category.
   *
   * @return the identifier assigned to this symbol
   */
  int defineSymbol(const string &name, CSPDefinitionType type)
  {
    if (type==UndefinedType)
      throw runtime_error("Cannot register a symbol without a type");

    if (type<0 || type>UndefinedType)
      throw runtime_error("internal error: illegal CSPDefinitionType");

    SymbolInfo &info=symbolDict[name];

    if (info.type!=UndefinedType)
      throw runtime_error("symbol is defined twice");

    info.type=type;
    info.id=nextId[type]++;

    return info.id;
  }


  /**
   * get the identifier assigned to an already registered symbol
   *
   * @param name symbol to look up
   * @param expectedType if different from UndefinedType, check that
   * the symbol has the expected type given by this parameter
   * @return the identifier assigned to this symbol
   */
  int getSymbolId(const string &name, 
		  CSPDefinitionType expectedType=UndefinedType)
  {
    SymbolInfo &info=symbolDict[name];

    if (info.type==UndefinedType)
      throw runtime_error("symbol is undefined");

    if (expectedType!=UndefinedType && info.type!=expectedType)
      {
	cout<< name << endl;
      throw runtime_error("symbol doesn't have the expected type");
      }
    return info.id;
  }


  /**
   * get the information about an already registered symbol
   *
   * @param name symbol to look up
   * @return type and identifier associated to this symbol
   */
  SymbolInfo getSymbolInfo(const string &name)
  {
    typename SymbolDictionnary::iterator it=symbolDict.find(name);

    if (it==symbolDict.end())
      throw runtime_error("symbol is undefined");

    return (*it).second;
  }

  /**
   * transmit an expression to the solver
   *
   */
  void xmitExpressionToSolver(AST *tree)
  {
    if (preferredSyntax==AST::TREE)
    {
      getCallback()->
	predicateExpression(tree);
    }
    else
    {
      ostringstream tmp;
      
      tree->expression(tmp,preferredSyntax);

      getCallback()->
	predicateExpression(tmp.str());
	
      delete tree;
    }
  }

 /**
  * @brief the action to be performed (callback) when a tag is read in the
  * XML file
  */
  class TagAction
  {
  protected:
    bool activated;
    XMLParser *parser;

  public:
    TagAction(XMLParser *parser): parser(parser) {activated=false;}
    virtual ~TagAction() {}

    void activate() {activated=true;}
    void deactivate() {activated=false;}
    
    bool isActivated() {return activated;}

    /**
     * return the name of this tag (constant)
     */
    virtual const char *getTagName()=0;
    virtual void beginTag(AttributeList& attributes)=0;
    virtual void text(const XMLCh* const chars, const unsigned int length)=0;
    virtual void endTag()=0;

  protected:
    /**
     * check that the parent tag in the XML file has the indicated name
     *
     * when name is NULL, check that this tag has no parent in the XML file
     *
     * @param parentTag the expected name of the parent tag or NULL to
     * check that we have no parent
     * @param n 1 for parent, 2 for grand-parent and so on
     */
    void checkParentTag(const char *parentTag, int n=1)
    {
      if (this->parser->getParentTagAction(n)==NULL)
      {
	if (parentTag!=NULL)
	  throw runtime_error("tag has no parent but it should have one");
	else
	  return;
      }
 
      if (parentTag==NULL)
	throw runtime_error("tag should have no parent");

#ifdef debug
      cout << "parent tag=" 
	   << this->parser->getParentTagAction(n)->getTagName() << endl;
#endif
      if (strcmp(this->parser->getParentTagAction(n)->getTagName(),
		 parentTag)!=0)
	throw runtime_error("wrong parent for tag");
    }
  };

  typedef 
    map<string,TagAction *> TagActionList;

  TagActionList tagList;
  deque<TagAction *> actionStack;


  void registerTagAction(TagActionList &tagList, TagAction *action)
  {
    tagList[action->getTagName()]=action;
  }


  /**
   * reads the value of attribute "name" from the list
   * "attributes" and store the value in "value"
   *
   * @return true iff an integer could be read
   */
  static bool getXMLAttributeAsInt(const AttributeList &attributes, char *name,
			    int &value)
  {
    char *v=XMLString::transcode(attributes.getValue(name));
    if (v==NULL)
      return false;

    value=strtol(v,NULL,10); // FIXME ???

    XMLString::release(&v);
    return true;
  }

  /**
   * reads the value of attribute "name" from the list
   * "attributes" and store the value in "value"
   *
   * @return true iff a string could be read
   */
  static bool getXMLAttributeAsString(const AttributeList &attributes, char *name,
			       string &value)
  {
    char *v=XMLString::transcode(attributes.getValue(name));
    if (v==NULL)
      return false;

    value=v;

    XMLString::release(&v);
    return true;
  }


  class InstanceTagAction : public TagAction
  {
  public:
    InstanceTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "instance";}

    virtual void beginTag(AttributeList& attributes)
    {
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
      if (!XMLChar1_1::isAllSpaces(chars,length))
	throw runtime_error("<intance> tag should not have meaningful text");
    }

    virtual void endTag()
    {
      this->parser->getCallback()->endInstance();
    }
  };

  class PresentationTagAction : public TagAction
  {
  public:
    PresentationTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "presentation";}

    virtual void beginTag(AttributeList& attributes)
    {
      string name;

      this->checkParentTag("instance");

      if (!XMLParser::getXMLAttributeAsString(attributes,"name",name))
	throw runtime_error("expected attribute name for tag "
			    "<instance>");

      this->parser->getCallback()->beginInstance(name);
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
      // ignore
    }

    virtual void endTag()
    {
    }
  };

  class DomainsTagAction : public TagAction
  {
  public:
    DomainsTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "domains";}

    virtual void beginTag(AttributeList& attributes)
    {
      int nbDomains;

      this->checkParentTag("instance");

      if (!XMLParser::getXMLAttributeAsInt(attributes,"nbDomains",nbDomains))
	throw runtime_error("expected attribute nbDomains for tag "
			    "<domains>");

      this->parser->getCallback()->beginDomainsSection(nbDomains);
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
      if (!XMLChar1_1::isAllSpaces(chars,length))
	throw runtime_error("<domains> tag should not have meaningful text");
    }

    virtual void endTag()
    {
      this->parser->getCallback()->endDomainsSection();
    }
  };

  class DomainTagAction : public TagAction
  {
  private:
    XMLCh *doubledot;
  public:
    DomainTagAction(XMLParser *parser): TagAction(parser) 
    {
      doubledot=XMLString::transcode("..");
    }
      
    ~DomainTagAction()
    {
      XMLString::release(&doubledot);
    }
	
    virtual const char *getTagName() {return "domain";}

    virtual void beginTag(AttributeList& attributes)
    {
      string name;
      int nbValues;

      this->checkParentTag("domains");

      if (!XMLParser::getXMLAttributeAsString(attributes,"name",name))
	throw runtime_error("expected attribute name for tag "
			    "<domain>");

      if (!XMLParser::getXMLAttributeAsInt(attributes,"nbValues",nbValues))
	throw runtime_error("expected attribute nbValue for tag "
			    "<domain>");

      int idDomain=this->parser->defineSymbol(name,DomainType);

      this->parser->getCallback()->beginDomain(name,idDomain,nbValues);
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
      XMLStringTokenizer tokenizer(chars);

      while(tokenizer.hasMoreTokens())
      {
	XMLCh *token=tokenizer.nextToken();

	int pos=XMLString::patternMatch(token,doubledot);

	if (pos==-1)
	{
	  int val=XMLString::parseInt(token);
	  this->parser->getCallback()->addDomainValue(val);
	}
	else
	{
	  int first,last;

	  token[pos]=0;
	  first=XMLString::parseInt(token);
	  last=XMLString::parseInt(token+pos+2);	  

	  this->parser->getCallback()->addDomainValue(first,last);
	}
      }
    }

    virtual void endTag()
    {
      this->parser->getCallback()->endDomain();
    }
  };

  class VariablesTagAction : public TagAction
  {
  public:
    VariablesTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "variables";}

    virtual void beginTag(AttributeList& attributes)
    {
      int nbVariables;

      this->checkParentTag("instance");

      if (!XMLParser::getXMLAttributeAsInt(attributes,"nbVariables",
					   nbVariables))
	throw runtime_error("expected attribute nbVariables for tag "
			    "<variables>");

      this->parser->getCallback()->beginVariablesSection(nbVariables);
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
       if (!XMLChar1_1::isAllSpaces(chars,length))
	throw runtime_error("<variables> tag should not have meaningful text");
   }

    virtual void endTag()
    {
      this->parser->getCallback()->endVariablesSection();
    }
  };

  class VariableTagAction : public TagAction
  {
  public:
    VariableTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "variable";}

    virtual void beginTag(AttributeList& attributes)
    {
      string name;
      string domain;

      this->checkParentTag("variables");

      if (!XMLParser::getXMLAttributeAsString(attributes,"name",name))
	throw runtime_error("expected attribute name for tag "
			    "<variable>");

      if (!XMLParser::getXMLAttributeAsString(attributes,"domain",domain))
	throw runtime_error("expected attribute domain for tag "
			    "<variable>");

      int idVar=this->parser->defineSymbol(name,VariableType);
      int idDomain=this->parser->getSymbolId(domain,DomainType);

      this->parser->getCallback()->addVariable(name,idVar,domain,idDomain);
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
      if (!XMLChar1_1::isAllSpaces(chars,length))
	throw runtime_error("<variable> tag should not have meaningful text");
    }

    virtual void endTag()
    {
    }
  };

  class RelationsTagAction : public TagAction
  {
  public:
    RelationsTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "relations";}

    virtual void beginTag(AttributeList& attributes)
    {
      int nbRelations;

      this->checkParentTag("instance");

      if (!XMLParser::getXMLAttributeAsInt(attributes,
                                           "nbRelations",nbRelations))
        throw runtime_error("expected attribute nbRelation for tag "
                            "<relations>");

      this->parser->getCallback()->beginRelationsSection(nbRelations);
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
      if (!XMLChar1_1::isAllSpaces(chars,length))
	throw runtime_error("<relations> tag should not have meaningful text");
    }

    virtual void endTag()
    {
      this->parser->getCallback()->endRelationsSection();
    }
  };

  class RelationTagAction : public TagAction
  {
  private:
    int arity;
    int nbTuples;
  public:
    RelationTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "relation";}

    virtual void beginTag(AttributeList& attributes)
    {
      string name,semantics;

      this->checkParentTag("relations");

      if (!XMLParser::getXMLAttributeAsInt(attributes,
					   "arity",arity))
	throw runtime_error("expected attribute arity for tag "
			    "<relation>");

      if (!XMLParser::getXMLAttributeAsInt(attributes,
					   "nbTuples",nbTuples))
	throw runtime_error("expected attribute nbTuples for tag "
			    "<relation>");

      if (!XMLParser::getXMLAttributeAsString(attributes,
					   "name",name))
	throw runtime_error("expected attribute name for tag "
			    "<relation>");

      if (!XMLParser::getXMLAttributeAsString(attributes,
					   "semantics",semantics))
	throw runtime_error("expected attribute semantics for tag "
			    "<relation>");

      bool isSupport;
      if (semantics=="supports")
	isSupport=true;
      else
	if (semantics=="conflicts")
	  isSupport=false;
	else
	  throw runtime_error("unknown semantics for tag <relation>");

      int idRel=this->parser->defineSymbol(name,RelationType);
      this->parser->getCallback()->beginRelation(name,idRel,
						 arity,nbTuples,isSupport);
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
      XMLCh buffer[64];

      if (arity<=0)
	throw runtime_error("arity must be >0");

      int tab[arity]; // the tuple
      int i=0; // position in the tuple of the next value 
      int nb=0; // number of tuples read so far

      const XMLCh *p=chars;

      while(p<chars+length) // don't use *p!=0 here
      {
	if (XMLChar1_0::isWhitespace(*p))
	{
	  ++p;
	  continue;
	}

	if (i==arity)
	{
	  this->parser->getCallback()->addRelationTuple(arity,tab);
	  nb++;
	  i=0;

	  if (*p!='|')
	    throw runtime_error("pipe ('|') expected to separate tuples");

	  ++p;
	  continue;
	}

	XMLCh *cp=buffer;

	while (*p!=0 && *p!='|' && !XMLChar1_0::isWhitespace(*p) 
	       && cp<buffer+sizeof(buffer)-1)
	  *cp++=*p++;
	
	*cp=0;

	tab[i++]=XMLString::parseInt(buffer);
      }

      if (i!=arity)
	throw runtime_error("last tuple is incomplete !");

      this->parser->getCallback()->addRelationTuple(arity,tab);
      nb++;

      if (nb!=nbTuples)
	throw runtime_error("effective number of tuples doesn't match the announced number");
    }

    virtual void endTag()
    {
      this->parser->getCallback()->endRelation();
    }
  };

  class PredicatesTagAction : public TagAction
  {
  public:
    PredicatesTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "predicates";}

    virtual void beginTag(AttributeList& attributes)
    {
      int nbPredicates;

      this->checkParentTag("instance");

      if (!XMLParser::getXMLAttributeAsInt(attributes,
                                           "nbPredicates",nbPredicates))
        throw runtime_error("expected attribute nbPredicate for tag "
                            "<predicates>");

      this->parser->getCallback()->beginPredicatesSection(nbPredicates);
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
      if (!XMLChar1_1::isAllSpaces(chars,length))
	throw runtime_error("<predicates> tag should not have meaningful text");
    }

    virtual void endTag()
    {
      this->parser->getCallback()->endPredicatesSection();
    }
  };

  class PredicateTagAction : public TagAction
  {
  public:
    PredicateTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "predicate";}

    virtual void beginTag(AttributeList& attributes)
    {
      string name;

      this->checkParentTag("predicates");

      if (!XMLParser::getXMLAttributeAsString(attributes,
					   "name",name))
	throw runtime_error("expected attribute name for tag "
			    "<predicate>");

      int idPred=this->parser->defineSymbol(name,PredicateType);
      this->parser->getCallback()->beginPredicate(name,idPred);
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
    }

    virtual void endTag()
    {
      this->parser->getCallback()->endPredicate();
    }


    const VariableInfo *getParmInfo() 
    {
      return &parametersInfo;
    }

  private:
    VariableInfo parametersInfo; // information collected about the
				 // formal parameters
    
    // this class may alter parametersInfo
    friend class XMLParser<Callback>::ParametersTagAction;  
  };


  class ParametersTagAction : public TagAction
  {
  private:
    bool formalParameters; // formal or effective parameters ?
    int pos; // position of a parameter
       
  public:
    ParametersTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "parameters";}

    virtual void beginTag(AttributeList& attributes)
    {
      pos=0;

      const char *parentTag=this->parser->getParentTagAction()->getTagName();

      if (strcmp(parentTag,"predicate")==0)
      {
	formalParameters=true;
      }
      else
	if (strcmp(parentTag,"constraint")==0)
	{
	  formalParameters=false;
	}
	else
	  throw runtime_error("unexpected <parameters> tag");
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
      if (formalParameters)
	parseFormalParameters(chars,length);
      else
	parseEffectiveParameters(chars,length);
    }

    virtual void endTag()
    {
    }

    int getCurrentPos()
    {
      return pos;
    }

  private:
    void parseFormalParameters(const XMLCh* const chars, 
			       const unsigned int length)
    {
      XMLStringTokenizer tokenizer(chars);

      PredicateTagAction *parent=
	dynamic_cast<PredicateTagAction *>(this->parser
					   ->getParentTagAction());
      if (parent==NULL)
	throw runtime_error("internal error: couldn't get my parent or wasn't a <predicate>");

      VariableInfo &parmInfo=parent->parametersInfo;

      while(tokenizer.hasMoreTokens())
      {
	XMLCh *thetype=tokenizer.nextToken();

	if (!tokenizer.hasMoreTokens())
	  throw runtime_error("missing formal parameter name after type");

	XMLCh *thename=tokenizer.nextToken();

	string name,type;
	copy(name,thename);
	copy(type,thetype);

	parmInfo[name].id=pos;
	// ??? parmInfo[name].type=...

	this->parser->getCallback()->addFormalParameter(pos,name,type);

	++pos;
      }
    }


    void parseEffectiveParameters(const XMLCh* const chars, 
				  const unsigned int length)
    {
      XMLStringTokenizer tokenizer(chars);

      ConstraintTagAction *parent=
	dynamic_cast<ConstraintTagAction *>(this->parser
					    ->getParentTagAction());
      if (parent==NULL)
	throw runtime_error("internal error: couldn't get my parent or wasn't a <constraint>");

      const VariableInfo &varInfo=parent->getScopeInfo();

      while(tokenizer.hasMoreTokens())
      {
	XMLCh *thename=tokenizer.nextToken();

	if (isalpha(*thename))
	{
	  // variable
	  string name;
	  copy(name,thename);

	  VariableInfo::const_iterator it=varInfo.find(name);
	  if (it==varInfo.end())
	    throw runtime_error("variable given as effective parameter is unknown");

	  this->parser->getCallback()->
	    addEffectiveParameterVariable(pos,name,(*it).second.id);
	}
	else
	{
	  int value=XMLString::parseInt(thename);
	  this->parser->getCallback()->
	    addEffectiveParameterInteger(pos,value);	  
	}
      
	++pos;
      }
    }

    void incrementCurrentPos()
    {
      ++pos;
    }

    // this class may call incrementCurrentPos()
    friend class XMLParser<Callback>::ListTagAction;
  };

  class ConstraintTagAction;

  class ListTagAction : public TagAction
  {
  private:
    ParametersTagAction *parentParametersTag;
    ConstraintTagAction *parentConstraintTag;
    deque<int> pos; // position in the list (we may have nested lists)
    // pos.back() is the position in the inner most list

  public:
    ListTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "list";}

    virtual void beginTag(AttributeList& attributes)
    {
      pos.push_back(0);

      const char *parentTag=this->parser->getParentTagAction()->getTagName();

      if (strcmp(parentTag,"parameters")==0)
      {
	// list as an effective parameter of a constraint

	// grand-parent must be <constraint>
	this->checkParentTag("constraint",2);

	parentParametersTag=
	  dynamic_cast<ParametersTagAction *>(this->parser
					      ->getParentTagAction());
	if (parentParametersTag==NULL)
	  throw runtime_error("internal error: couldn't get my parent or wasn't a <parameters>");

	parentConstraintTag=
	  dynamic_cast<ConstraintTagAction *>(this->parser
					      ->getParentTagAction(2));
	if (parentConstraintTag==NULL)
	  throw runtime_error("internal error: couldn't get my grand-parent or wasn't a <constraint>");
	
	this->parser->getCallback()->
	  addEffectiveParameterList(parentParametersTag->getCurrentPos());

	parentParametersTag->incrementCurrentPos();
      }
      else
      {
	// list inside a list
	this->checkParentTag("list");

	// get my constraint ancestor

	// ??? will have to increment the position in the parent's list
	// ???
	throw runtime_error("lists inside lists are not handled yet");
      }

      this->parser->getCallback()->beginList();
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
      XMLStringTokenizer tokenizer(chars);

      const VariableInfo &varInfo=parentConstraintTag->getScopeInfo();

      while(tokenizer.hasMoreTokens())
      {
	XMLCh *thename=tokenizer.nextToken();

	if (isalpha(*thename))
	{
	  // variable
	  string name;
	  copy(name,thename);

	  VariableInfo::const_iterator it=varInfo.find(name);
	  if (it==varInfo.end())
	    throw runtime_error("variable found in list is unknown");

	  this->parser->getCallback()->
	    addListVariable(pos.back(),name,(*it).second.id);
	}
	else
	{
	  int value=XMLString::parseInt(thename);
	  this->parser->getCallback()->
	    addListInteger(pos.back(),value);	  
	}
      
	++pos.back();
      }
    }

    virtual void endTag()
    {
      this->parser->getCallback()->endList();

      pos.pop_back();
    }
  };

  class ExpressionTagAction : public TagAction
  {
  private:
    bool inPredicateDefinition;

  public:
    ExpressionTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "expression";}

    virtual void beginTag(AttributeList& attributes)
    {
      const char *parentTag=this->parser->getParentTagAction()->getTagName();

      if (strcmp(parentTag,"predicate")==0)
      {
	inPredicateDefinition=true;
      }
      else
	if (strcmp(parentTag,"constraint")==0)
	{
	  inPredicateDefinition=false;
	}
	else
	  throw runtime_error("unexpected <expression> tag");
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
      if (!XMLChar1_1::isAllSpaces(chars,length))
	throw runtime_error("<expression> tag should not have meaningful text");
    }

    virtual void endTag()
    {
    }

    bool isPredicateDefinition() {return inPredicateDefinition;}
  };

  class FunctionalTagAction : public TagAction
  {
  public:
    FunctionalTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "functional";}

    virtual void beginTag(AttributeList& attributes)
    {
      this->checkParentTag("expression");
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
      string expr;
      copy(expr,chars);

      ExpressionTagAction *parentExpressionTag=
	dynamic_cast<ExpressionTagAction *>(this->parser
					    ->getParentTagAction(1));
      if (parentExpressionTag==NULL)
	throw runtime_error("internal error: couldn't get my parent or wasn't an <expression>");

      bool predicateDefinition=parentExpressionTag->isPredicateDefinition();

      if (predicateDefinition)
      {
	PredicateTagAction *parentPredicateTag=
	  dynamic_cast<PredicateTagAction *>(this->parser
					     ->getParentTagAction(2));
	if (parentPredicateTag==NULL)
	  throw runtime_error("internal error: couldn't get my parent or wasn't a <predicate>");

	this->parser->exprParser.setVarInfo(parentPredicateTag->getParmInfo());
     
	AST *tree=this->parser->exprParser.prefixParser(expr);

	this->parser->exprParser.unsetVarInfo();

	this->parser->xmitExpressionToSolver(tree);
      }
      else
	throw runtime_error("expressions not yet suported as effective parameters"); // ???
    }

    virtual void endTag()
    {
    }
  };

  class InfixTagAction : public TagAction
  {
  public:
    InfixTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "infix";}

    virtual void beginTag(AttributeList& attributes)
    {
      this->checkParentTag("expression");
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
      string expr;
      copy(expr,chars);

      ExpressionTagAction *parentExpressionTag=
	dynamic_cast<ExpressionTagAction *>(this->parser
					    ->getParentTagAction(1));
      if (parentExpressionTag==NULL)
	throw runtime_error("internal error: couldn't get my parent or wasn't an <expression>");

      bool predicateDefinition=parentExpressionTag->isPredicateDefinition();

      if (predicateDefinition)
      {
	PredicateTagAction *parentPredicateTag=
	  dynamic_cast<PredicateTagAction *>(this->parser
					     ->getParentTagAction(2));
	if (parentPredicateTag==NULL)
	  throw runtime_error("internal error: couldn't get my parent or wasn't a <predicate>");

	this->parser->exprParser.setVarInfo(parentPredicateTag->getParmInfo());
     
	AST *tree=this->parser->exprParser.infixParser(expr);

	this->parser->exprParser.unsetVarInfo();

	ostringstream tmp;

	// prefix
	tree->expression(tmp);
	this->parser->getCallback()->predicateExpression(tmp.str());
	
	delete tree;
      }
      else
	throw runtime_error("expressions not yet suported as effective parameters"); // ???
    }

    virtual void endTag()
    {
    }
  };

  class PostfixTagAction : public TagAction
  {
  public:
    PostfixTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "postfix";}

    virtual void beginTag(AttributeList& attributes)
    {
      this->checkParentTag("expression");
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
      string expr;
      copy(expr,chars);

      ExpressionTagAction *parentExpressionTag=
	dynamic_cast<ExpressionTagAction *>(this->parser
					    ->getParentTagAction(1));
      if (parentExpressionTag==NULL)
	throw runtime_error("internal error: couldn't get my parent or wasn't an <expression>");

      bool predicateDefinition=parentExpressionTag->isPredicateDefinition();

      if (predicateDefinition)
      {
	PredicateTagAction *parentPredicateTag=
	  dynamic_cast<PredicateTagAction *>(this->parser
					     ->getParentTagAction(2));
	if (parentPredicateTag==NULL)
	  throw runtime_error("internal error: couldn't get my parent or wasn't a <predicate>");

	this->parser->exprParser.setVarInfo(parentPredicateTag->getParmInfo());
     
	AST *tree=this->parser->exprParser.postfixParser(expr);

	this->parser->exprParser.unsetVarInfo();

	ostringstream tmp;

	// prefix
	tree->expression(tmp);
	this->parser->getCallback()->predicateExpression(tmp.str());
	
	delete tree;
      }
      else
	throw runtime_error("expressions not yet suported as effective parameters"); // ???
    }

    virtual void endTag()
    {
    }
  };



  class ConstraintsTagAction : public TagAction
  {
  public:
    ConstraintsTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "constraints";}

    virtual void beginTag(AttributeList& attributes)
    {
      int nbConstraints;

      this->checkParentTag("instance");

      if (!XMLParser::getXMLAttributeAsInt(attributes,"nbConstraints",
					   nbConstraints))
	throw runtime_error("expected attribute nbConstraints for tag "
			    "<constraints>");

      this->parser->getCallback()->beginConstraintsSection(nbConstraints);
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
      if (!XMLChar1_1::isAllSpaces(chars,length))
	throw runtime_error("<constraints> tag should not have meaningful text");
    }

    virtual void endTag()
    {
      this->parser->getCallback()->endConstraintsSection();
    }
  };

  class ConstraintTagAction : public TagAction
  {
  public:
    ConstraintTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "constraint";}

    virtual void beginTag(AttributeList& attributes)
    {
      string name,reference;
      int arity;

      this->checkParentTag("constraints");

      if (!XMLParser::getXMLAttributeAsInt(attributes,
					   "arity",arity))
	throw runtime_error("expected attribute arity for tag "
			    "<constraint>");

      if (!XMLParser::getXMLAttributeAsString(attributes,
					      "name",name))
	throw runtime_error("expected attribute name for tag "
			    "<constraint>");

      if (!XMLParser::getXMLAttributeAsString(attributes,
					      "reference",reference))
	throw runtime_error("expected attribute reference for tag "
			    "<constraint>");

      SymbolInfo info=this->parser->getSymbolInfo(reference);

      int idConstr=this->parser->defineSymbol(name,ConstraintType);
      this->parser->getCallback()->beginConstraint(name,idConstr,arity,
						   reference,
						   info.type,
						   info.id);

      const XMLCh *scope=attributes.getValue("scope");
      if (scope==NULL)
	throw runtime_error("expected attribute scope for tag "
			    "<constraint>");

      XMLStringTokenizer tokenizer(scope);

      while(tokenizer.hasMoreTokens())
      {
	string var;

	copy(var,tokenizer.nextToken());
	int idVar=this->parser->getSymbolId(var,VariableType);
	this->parser->getCallback()->addVariableToConstraint(var,idVar);

	scopeInfo[var].id=idVar;
	//scopeInfo[var].type=...; ???
      }
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
    }

    virtual void endTag()
    {
      this->parser->getCallback()->endConstraint();
    }

    const VariableInfo &getScopeInfo()
    {
      return scopeInfo;
    }

  private:
    VariableInfo scopeInfo;
  };

  class ExtensionTagAction : public TagAction
  {
  public:
    ExtensionTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "extension";}

    virtual void beginTag(AttributeList& attributes)
    {
      // ignore
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
      // ignore
    }

    virtual void endTag()
    {
      // ignore
    }
  };

  /*************************************************************************
   *
   * MathML tags
   *
   *************************************************************************/

  class MathMLFunctionTagAction : public TagAction
  {
  public:
    MathMLFunctionTagAction(XMLParser *parser,
			    ASTAbstractFunction *ast): TagAction(parser) {}

    virtual const char *getTagName()=0;

    virtual void beginTag(AttributeList& attributes)
    {
      this->checkParentTag("apply");
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
       if (!XMLChar1_1::isAllSpaces(chars,length))
	throw runtime_error("MathML function tag should not have meaningful text");
    }

    virtual void endTag()
    {
    }
  };

  class MathTagAction : public TagAction
  {
  public:
    MathTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "math";}

    virtual void beginTag(AttributeList& attributes)
    {
      this->checkParentTag("expression");
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
    }

    virtual void endTag()
    {
    }
  };

  class ApplyTagAction : public TagAction
  {
  public:
    ApplyTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "apply";}

    virtual void beginTag(AttributeList& attributes)
    {
      //this->checkParentTag("expression");
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
    }

    virtual void endTag()
    {
    }
  };

  class CITagAction : public TagAction
  {
  public:
    CITagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "ci";}

    virtual void beginTag(AttributeList& attributes)
    {
      //this->checkParentTag("expression");

      // ??? don't allow subtags
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
      string name;
      copy(name,chars);
      new ASTVar(name);
    }

    virtual void endTag()
    {
    }
  };


  class CNTagAction : public TagAction
  {
  public:
    CNTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "cn";}

    virtual void beginTag(AttributeList& attributes)
    {
      //this->checkParentTag("expression");

      // ??? don't allow subtags
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
      int value=XMLString::parseInt(chars);

      new ASTInteger(value);
    }

    virtual void endTag()
    {
    }
  };

  class TrueTagAction : public TagAction
  {
  public:
    TrueTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "true";}

    virtual void beginTag(AttributeList& attributes)
    {
      //this->checkParentTag("expression");

      // ??? don't allow subtags
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
    }

    virtual void endTag()
    {
    }
  };

  class FalseTagAction : public TagAction
  {
  public:
    FalseTagAction(XMLParser *parser): TagAction(parser) {}

    virtual const char *getTagName() {return "false";}

    virtual void beginTag(AttributeList& attributes)
    {
      //this->checkParentTag("expression");

      // ??? don't allow subtags
    }

    virtual void text(const XMLCh* const chars, const unsigned int length)
    {
    }

    virtual void endTag()
    {
      //parent->nextParm(new ASTBoolean(false));
    }
  };


  /*************************************************************************
   *
   * SAX Handler
   *
   *************************************************************************/

  /**
   * @brief the SAX handler for the Xerces library
   */
  class SAXCSPHandler : public HandlerBase
  {
  protected:
    Callback &cb;
    XMLParser *parser;

  public:
    SAXCSPHandler(Callback &callback, XMLParser *parser): 
      cb(callback), parser(parser) 
    {
    }

    void endDocument()
    {
#ifdef debug
      cout << "Parsing ends" << endl;
#endif
    }

    void ignorableWhitespace(const XMLCh* const chars,
			     const unsigned int length) {}
    
    void processingInstruction(const XMLCh* const target, 
			       const XMLCh* const data) {}

    void startDocument()
    {
#ifdef debug
      cout << "Parsing begins" << endl;
#endif
    }

    void startElement(const XMLCh* const name, AttributeList& attributes) 
    {
      char *n=XMLString::transcode(name);
#ifdef debug
      cout << "  begin element " << n << endl;
      for(int i=0;i<attributes.getLength();++i)
      {
	char *n,*v;
	n=XMLString::transcode(attributes.getName(i));
	v=XMLString::transcode(attributes.getValue(i));
	cout << "    attribute " << n
	     << " = " << v << endl;
	XMLString::release(&n);
	XMLString::release(&v);
      }
#endif

      typename TagActionList::iterator iAction=parser->tagList.find(n);

      if (iAction!=parser->tagList.end())
      {
	TagAction *action=(*iAction).second;

	// ???
	//if (!action->isActivated())
	//  throw runtime_error("unexpected tag");

	parser->actionStack.push_front(action);
	action->beginTag(attributes);
      }
      else
	cerr << "unknown TAG: " << n << endl;

      XMLString::release(&n);
    }

    void characters(const XMLCh* const chars, const unsigned int length) 
    {
#ifdef debug
	char *n=XMLString::transcode(chars);
	XMLString::trim(n);
	cout << "    chars '" << n << "'" << endl;
	XMLString::release(&n);
#endif

      if (parser->actionStack.size())
	parser->actionStack.front()->text(chars,length);
      // else ???
    }

    void endElement(const XMLCh* const name) 
    {
      char *n=XMLString::transcode(name);
#ifdef debug
      cout << "  end element " << n << endl;
#endif

      typename TagActionList::iterator iAction=parser->tagList.find(n);

      if (iAction!=parser->tagList.end())
      {
	(*iAction).second->endTag();
	parser->actionStack.pop_front();
      }
      else
	cerr << "unknown TAG: " << n << endl;

      XMLString::release(&n);
    }

    // -----------------------------------------------------------------------
    //  Implementations of the SAX ErrorHandler interface
    // -----------------------------------------------------------------------
    void warning(const SAXParseException& exc) 
    {
      throw exc;
    }

    void error(const SAXParseException& exc) 
    {
      throw exc;
    }

    void fatalError(const SAXParseException& exc) 
    {
      throw exc;
    }
  };


public:
  XMLParser(Callback *cb)
  {
    // ??? move this ?
    // Initialize the XML4C2 system
    try
    {
      XMLPlatformUtils::Initialize();
    }

    catch (const XMLException& toCatch)
    {
      cerr << "Error during initialization! :\n"
	   << toCatch.getMessage() << endl;
      throw;
    }


    preferredSyntax=AST::TREE;

    this->cb=cb;
    registerTagAction(tagList,new InstanceTagAction(this));
    registerTagAction(tagList,new PresentationTagAction(this));
    registerTagAction(tagList,new DomainsTagAction(this));
    registerTagAction(tagList,new DomainTagAction(this));
    registerTagAction(tagList,new VariablesTagAction(this));
    registerTagAction(tagList,new VariableTagAction(this));
    registerTagAction(tagList,new RelationsTagAction(this));
    registerTagAction(tagList,new RelationTagAction(this));
    registerTagAction(tagList,new PredicatesTagAction(this));
    registerTagAction(tagList,new PredicateTagAction(this));

    registerTagAction(tagList,new ParametersTagAction(this));
    registerTagAction(tagList,new ListTagAction(this));
    registerTagAction(tagList,new ExpressionTagAction(this));

    registerTagAction(tagList,new FunctionalTagAction(this));
    registerTagAction(tagList,new InfixTagAction(this));
    registerTagAction(tagList,new PostfixTagAction(this));

    registerTagAction(tagList,new ConstraintsTagAction(this));
    registerTagAction(tagList,new ConstraintTagAction(this));

    registerTagAction(tagList,new ExtensionTagAction(this));

    // mathml
    registerTagAction(tagList,new MathTagAction(this));
    registerTagAction(tagList,new ApplyTagAction(this));
    registerTagAction(tagList,new CITagAction(this));
    registerTagAction(tagList,new TrueTagAction(this));
    registerTagAction(tagList,new FalseTagAction(this));

    for(int i=0;i<UndefinedType;++i)
      nextId[i]=0;

    symbolDict["global:allDifferent"]=SymbolInfo(0,GlobalConstraintType);
    symbolDict["global:weightedSumGreaterThan"]=
      SymbolInfo(1,GlobalConstraintType);
    symbolDict["global:weightedSumEqualTo"]=SymbolInfo(2,GlobalConstraintType);
    symbolDict["global:weightedSumDifferentFrom"]=
      SymbolInfo(3,GlobalConstraintType);

  }


  Callback *getCallback()
  {
    return cb;
  }

  /**
   * get the parent tag action that is n levels higher in the current
   * branch of the XML parse tree
   */
  TagAction *getParentTagAction(int n=1)
  {
    if (n<0 || n>=actionStack.size())
      return NULL;

    return actionStack[n];
  }




  /**
   * choose the representation of an expression that the parser must
   * use to transmit an expression to the solver
   *
   * @param syntax 
   *   @arg AST::TREE    transmit as an Abstract Syntax Tree (AST)
   *   @arg AST::PREFIX  transmit as a string (prefix notation)
   *   @arg AST::INFIX_C transmit as a string (C infix notation)
   *   @arg AST::POSTFIX transmit as a string (postfix notation)
   *   @arg AST::MATHML  transmit as a string (MathML expression) 
   *
   */
  void setPreferredExpressionRepresentation(AST::Syntax syntax)
  {
    preferredSyntax=syntax;
  }


  /**
   * ??? must be reworked (externalize Xerces initialization) and so on
   *
   * so far, this is a simple copy from a SAX parser example
   */
  int parse(istream &in)
  {
    //
    //  Create a SAX parser object. Then, according to what we were told on
    //  the command line, set it to validate or not.
    //
    SAXParser* parser = new SAXParser;
    //parser->setValidationScheme(false);
    parser->setDoNamespaces(false);
    parser->setDoSchema(false);
    parser->setValidationSchemaFullChecking(false);

    //
    //  Create the handler object and install it as the document and error
    //  handler for the parser-> Then parse the file and catch any exceptions
    //  that propagate out
    //
    int errorCode = 0;
    int errorCount = 0;
    try
    {
      StreamInputSource inputStream(in);

      SAXCSPHandler handler(*cb,this);
      parser->setDocumentHandler(&handler);
      parser->setErrorHandler(&handler);
      parser->parse(inputStream);
      errorCount = parser->getErrorCount();
    }
    catch (const SAXParseException &e)
    {
      cerr << "On line " << e.getLineNumber() 
	   << ", column " << e.getColumnNumber() << endl;
      cerr << e.getMessage() << endl;
      errorCode = 5;
    }
    catch (const OutOfMemoryException&)
    {
      cerr << "OutOfMemoryException" << endl;
      errorCode = 5;
    }
    catch (const XMLException& toCatch)
    {
      cerr << "\nAn error occurred\n  Error: "
	   << toCatch.getMessage()
	   << "\n" << endl;
      errorCode = 4;
    }
    
    if(errorCode) {
      XMLPlatformUtils::Terminate();
      return errorCode;
    }

    //
    //  Delete the parser itself.  Must be done prior to calling
    //  Terminate, below.
    //
    delete parser;

    // And call the termination method
    XMLPlatformUtils::Terminate();

    //cout << "Error count=" << errorCount << endl;
  }

  void parse(char *filename)
  {
    ifstream in(filename);
	if(!in) { cout << "cannot open file:"  << filename << endl; exit(1); }

    parse(in);
  }

}; // class XMLParser

} // namespace


// Local Variables:
// mode: C++
// End:

#endif

