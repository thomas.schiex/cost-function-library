#include <stdio.h>
#include <stdlib.h>

#define MAX_CARS 1000
#define TRUE 1
#define FALSE 0
#define WM_MAX_ARITY 50

typedef struct
{
	long long val;

	long long var;
} literal;

typedef struct
{

	long long weight;

	long long elim;

	literal TLiterals[WM_MAX_ARITY];

	long long literalsTotal;

} clause;

typedef struct
{

	clause * clauses;

	long long totalClauses;
	
	long long totalVariables;
	
	long long totalWeight;
	
	long long Top;

} problem;

long long genRand(long long max)
/* returns random number in range of 0 to max-1 */
{
   long long n;
   n=rand()/(int)(((unsigned)RAND_MAX + 1) / max);  /* n is random number in range of 0 - max-1 */
   return(n);
}

/*  long long abs(long long v) */
/*  { */
/*  	if(v<0) return (v*(-1)); */
/*  	else return v; */
/*  } */

void createProblem(problem *p,long long num_cla,long long num_var,long long top)
{
	long long i;

	p->totalClauses = num_cla;
	p->totalVariables = num_var;
	p->Top=top;
	p->totalWeight=0;
	
	p->clauses =(clause *)malloc(sizeof(clause)*p->totalClauses);

	if (p->clauses==NULL)
	{
		printf("\nInsufficient memory\n");
		exit(-1);
	}

	for (i=0;i<p->totalClauses;i++)
	{
		p->clauses[i].weight=0;
		p->clauses[i].elim=FALSE;
	}
	//printf("\n %lld %lld %lld \n",p->totalVariables,p->totalClauses,p->Top);

}

void destroyProblem(problem *p)
{
	free(p->clauses);
}

long long numberClauses(problem *p)
{
	 long long c,nc=0;
	 
	for (c=0;c<p->totalClauses;c++)
	{
		if(p->clauses[c].elim==FALSE) nc++;
	}
	return nc;

}

void generateCNF(problem *p)
{
	long long c,l;
	
	printf("p cnf %lld %lld\n",p->totalVariables,numberClauses(p));
	
	for (c=0;c<p->totalClauses;c++)
	{
		if(p->clauses[c].elim==FALSE)
		{
		for(l=0;l<p->clauses[c].literalsTotal;l++)
		{
			if(p->clauses[c].TLiterals[l].val==FALSE) printf("-");
			printf("%lld ",p->clauses[c].TLiterals[l].var+1);
		}
		
		printf("0\n");
		}
		
	}
}

void generateWCNF(problem *p,long long w_wcnf)
{
	long long c,l;
	
	// Si w_wcnf==TRUE el problema tiene un Top inicial
	if(w_wcnf==TRUE) printf("p wcnf %lld %lld %lld\n",p->totalVariables,numberClauses(p),p->Top);
	// sino, es un WCNF de toda la vida
	else printf("p wcnf %lld %lld\n",p->totalVariables,numberClauses(p));
	
	for (c=0;c<p->totalClauses;c++)
	{
		if(p->clauses[c].elim==FALSE)
		{

		printf("%lld ",p->clauses[c].weight);
		for(l=0;l<p->clauses[c].literalsTotal;l++)
		{
			if(p->clauses[c].TLiterals[l].val==FALSE) printf("-");
			printf("%lld ",p->clauses[c].TLiterals[l].var+1);
		}
		printf("0\n");
		}
		
	}
}

void generateWCNF_RW(problem *p,long long w_wcnf)
{
	long long c,l,w;
	
	// Si w_wcnf==TRUE el problema tiene un Top inicial
	if(w_wcnf==TRUE) printf("p wcnf %lld %lld %lld\n",p->totalVariables,numberClauses(p),p->Top);
	// sino, es un WCNF de toda la vida
	else printf("p wcnf %lld %lld\n",p->totalVariables,numberClauses(p));
	
	for (c=0;c<p->totalClauses;c++)
	{
		if(p->clauses[c].elim==FALSE)
		{

		p->clauses[c].weight=genRand(10)+1;
		printf("%lld ",p->clauses[c].weight);
		for(l=0;l<p->clauses[c].literalsTotal;l++)
		{
			if(p->clauses[c].TLiterals[l].val==FALSE) printf("-");
			printf("%lld ",p->clauses[c].TLiterals[l].var+1);
		}
		printf("0\n");
		}
		
	}
}

void generateWCNF_SIM(problem *p)
{
	long long c,l,w;
	
	// Si w_wcnf==TRUE el problema tiene un Top inicial
	// sino, es un WCNF de toda la vida
	printf("p cnf %lld %lld\n",p->totalVariables,p->totalWeight);
	
	for (c=0;c<p->totalClauses;c++)
	{
		if(p->clauses[c].elim==FALSE)
		{

		for(w=0;w<p->clauses[c].weight;w++)
		{
		for(l=0;l<p->clauses[c].literalsTotal;l++)
		{
			if(p->clauses[c].TLiterals[l].val==FALSE) printf("-");
			printf("%lld ",p->clauses[c].TLiterals[l].var+1);
		}
		printf("0\n");
		}
		
		}
		
	}
}

long long existsRepeatedClause(problem *p,long long n_c,long long isCNF)
{
	long long c,l,l2,enc,eq;
	for (c=0;c<n_c;c++)
	{
		if(p->clauses[c].elim==FALSE && p->clauses[c].literalsTotal==p->clauses[n_c].literalsTotal)
		{
			eq=0;
			for(l=0;l<p->clauses[c].literalsTotal;l++)
			{
				enc=FALSE;
				for(l2=0;l2<p->clauses[n_c].literalsTotal && enc==FALSE;l2++)
				{
					if(p->clauses[c].TLiterals[l].var==p->clauses[n_c].TLiterals[l2].var && p->clauses[c].TLiterals[l].val==p->clauses[n_c].TLiterals[l2].val)
					{
						enc=TRUE;
					}
				}
				if(enc==TRUE) eq++;
			
			}
			if(eq==p->clauses[c].literalsTotal)
			{
				p->clauses[n_c].elim=TRUE;
				if(isCNF==FALSE)
				{
					p->clauses[c].weight+=p->clauses[n_c].weight;
				}
				return TRUE;
			}

			
		}
		
	}
	return FALSE;
	
}

void generateNO_REPEAT(problem *p,long long isCNF, long long w_wcnf)
{
	long long c,l;
	
	// si el problema inicial es CNF: Se eliminan las clausulas repetidas.
	// si el problema inicial es WCNF: Se suman los pesos y se unifican en una �nica clausula.
	
	for (c=0;c<p->totalClauses;c++)
	{
		if(p->clauses[c].elim==FALSE)
		{
			existsRepeatedClause(p,c,isCNF);
		}
		
	}
	if(isCNF) generateCNF(p);
	else generateWCNF(p,w_wcnf);
}

void printLiteral(long long v,long long val,long long w)
{
	if(val==TRUE)
	{
		printf("+%lld*x%lld ",w,v+1);
	}
	else
	{
		printf("-%lld*x%lld ",w,v+1);
	}
}

long long nVB(long long v)
{
	if(v==FALSE) return TRUE;
	else return FALSE;
}

void generatePseudoBoolean(problem *p)
{
	long long num_vars,c,v,l,res;
	
	// Generate Objective
	num_vars=p->totalVariables;
	printf("min:  ");
	for (c=0;c<p->totalClauses;c++)
	{
		if(p->clauses[c].weight<p->Top && p->clauses[c].literalsTotal>1)
		{
			printLiteral(num_vars,TRUE,p->clauses[c].weight);
			num_vars++;
		}
		else if(p->clauses[c].weight<p->Top && p->clauses[c].literalsTotal==1)
		{
			printLiteral(p->clauses[c].TLiterals[0].var,nVB(p->clauses[c].TLiterals[0].val),p->clauses[c].weight);
		}
	}
	printf(";\n");
	
	// Clauses
	
	num_vars=p->totalVariables;
	for (c=0;c<p->totalClauses;c++)
	{
		
		if(p->clauses[c].weight<p->Top  && p->clauses[c].literalsTotal>1)
		{
			res=1;
			printLiteral(num_vars,TRUE,1);
			num_vars++;

			for(l=0;l<p->clauses[c].literalsTotal;l++)
			{
				if(p->clauses[c].TLiterals[l].val==FALSE) res--;
				
			    printLiteral(p->clauses[c].TLiterals[l].var,p->clauses[c].TLiterals[l].val,1);
			}
			printf(">= ");
			if(res>=0) printf("+");
			printf("%lld;\n",res);
		}
		else if(p->clauses[c].literalsTotal>1 && p->clauses[c].weight>=p->Top)
		{
			res=1;
			for(l=0;l<p->clauses[c].literalsTotal;l++)
			{
				if(p->clauses[c].TLiterals[l].val==FALSE) res--;
				
			    printLiteral(p->clauses[c].TLiterals[l].var,p->clauses[c].TLiterals[l].val,1);
			}
			printf(">= ");
			if(res>=0) printf("+");
			printf("%lld;\n",res);
			
		}
	}
}

void generateMAXONES(problem *p,long long w_wcnf)
{
	long long c,newtop=0,l,i,nbparam;
	for (c=0;c<p->totalClauses;c++)
	{
		newtop++;
	}
	newtop+=p->totalVariables;

	printf("c PSEUDOBOOLEAN %lld\n",newtop);
	// Si w_wcnf==TRUE el problema tiene un Top inicial
	if(w_wcnf==TRUE) printf("p wcnf %lld %lld %lld\n",p->totalVariables,newtop,newtop);
	// sino, es un WCNF de toda la vida
	else printf("p wcnf %lld %lld\n",p->totalVariables,newtop);
	
	for(i=0;i<p->totalVariables;i++)
	{
		printf("1 %lld 0\n",(i+1));
	}
	for (c=0;c<p->totalClauses;c++)
	{
		printf("%lld ",newtop);
		for(l=0;l<p->clauses[c].literalsTotal;l++)
		{
			if(p->clauses[c].TLiterals[l].val==FALSE) printf("-");
			printf("%lld ",p->clauses[c].TLiterals[l].var+1);
		}
		printf("0\n");
		
	}
}


long long readProblem(FILE * f, problem *p,char **argv)
{
	long long i,isCnf,num_var=0,num_cla=0,top=0,actual,n_l,var,bType,gen,ct,w_wcnf,nbparam;
	char aux1[MAX_CARS],aux2[MAX_CARS],line[MAX_CARS];
	
	isCnf = strtol(argv[2],NULL,0);
	gen = strtol(argv[3],NULL,0);
	w_wcnf = strtol(argv[4],NULL,0);
	
	fgets(line, MAX_CARS, f);

	while (line[0] != 'p')
	{
		if(isCnf==FALSE && (line[0]=='c' && line[1]==' ' && line[2]=='P' && line[3]=='S' && line[4]=='E' && line[5]=='U' && line[6]=='D' && line[7]=='O'))
		{
			sscanf(line, "%s %s %lld", aux1, aux2,&num_cla);
			top=num_cla;
		}
		fgets(line, MAX_CARS, f);
	}


	if(isCnf)
	{
		sscanf(line, "%s %s %lld %lld", aux1, aux2, &num_var, &num_cla);
		top=num_cla;
	}
	else
	{
	nbparam = sscanf(line, "%s %s %lld %lld %lld", aux1, aux2, &num_var, &num_cla, &top);
	if (nbparam != EOF && nbparam == 5) top=num_cla;
	//sscanf(line, "%s %s %lld %lld %lld", aux1, aux2, &num_var, &num_cla,&top);
	}
	

	createProblem(p,num_cla,num_var,top);
	if(top==0) ct=TRUE;
	else ct=FALSE;


    	for (i=0;i<p->totalClauses;i++)
	{
		n_l=0;
		if(isCnf)
		{

			actual=1;

			p->clauses[i].weight=actual; // is a cnf file: assign weight 1 to all clauses

		}
		else
		{
			// is a wcnf file: read the weight from the file
			fscanf(f,"%lld",&actual);
			p->clauses[i].weight=actual;
		}

		p->totalWeight=p->totalWeight+actual;

		// for each clause, read all associated literals

		fscanf(f, "%lld", &actual);
		while (actual != 0)
		{

			var=abs(actual);
			if (actual<0) bType=FALSE;
			else bType=TRUE;
			
			// Create a new literal for the clause

			p->clauses[i].TLiterals[n_l].var=var-1;
			p->clauses[i].TLiterals[n_l].val=bType;

			fscanf(f, "%lld", &actual);

			n_l++;

		} // end while

		p->clauses[i].literalsTotal=n_l;

	}  // end for
	if(ct==TRUE) p->Top=p->totalWeight;

	if(gen==0) generateWCNF(p,w_wcnf);
	else if(gen==1) generatePseudoBoolean(p);
	else if(gen==2) generateMAXONES(p,w_wcnf);
	else if(gen==3) generateNO_REPEAT(p,isCnf,w_wcnf);
	else if(gen==4) generateWCNF_RW(p,w_wcnf);
	else if(gen==5) generateWCNF_SIM(p);
	destroyProblem(p);
	return TRUE;

}

int main(int argc,char ** argv)
{
	FILE* fileIn;
	problem p;
	
	if(argc!=5)
	{
		printf("\n Incorrect number of parameters. Usage: wcnf2msat [inputfile] [0:WCNF|1:CNF] [0:WCNF|1:MSAT|2:MAXONE|3:NO_REP|4:WCNF|5:SIM] [0:NW_WCNF|1:W_WCNF]\n");
		exit(-1);
	}
	
	//printf("\n Input file: %s ... \n",argv[1]);
	fileIn = fopen(argv[1], "r");
	
	if(fileIn == NULL)
	{
		printf("\n Incorrect number of parameters. Usage: wcnf2msat [inputfile] [0:WCNF|1:CNF] [0:WCNF|1:MSAT|2:MAXONE|3:NO_REP|4:WCNF|5:SIM] [0:NW_WCNF|1:W_WCNF]\n");
		exit(-1);
	}
		
	//printf("\n Translating... \n\n");
	
	readProblem(fileIn,&p,argv);
	
	fclose(fileIn);
	
}
