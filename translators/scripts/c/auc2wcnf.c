#include <stdio.h>
#include <stdlib.h>

#define MAX_CARS 500
#define TRUE 1
#define FALSE 0
#define NONE -1

typedef struct
{
	long int num_goods;
	long int num_bids;
	long int *bids;
	long int **matrix;
	long int max_cost;
} problem;

void createProblem(problem *p)
{
	long int i,j; 

	p->matrix=(long int **)malloc(sizeof(long int *)*p->num_bids);
	if(p->matrix==NULL)
		exit(-1);

	for(i=0;i<p->num_bids;i++) 
	{ 
		p->matrix[i]=(long int *)malloc(sizeof(long int)*p->num_goods);
		if(p->matrix[i]==NULL) exit(-1);

		for (j=0;j<p->num_goods;j++)
		{

			p->matrix[i][j]=NONE;

		}
	}
	
	p->bids=(long int *)malloc(sizeof(long int)*p->num_bids);
	
	for(i=0;i<p->num_bids;i++)
	{
		p->bids[i]=0; // costes de los bids
	}
	
	p->max_cost=0;

}

void destroyProblem(problem *p)
{
	long int k;
	
	for(k=0; k<p->num_bids; k++) 
	{ 
		free(p->matrix[k]);
	}

	free(p->matrix);
	
	free(p->bids);
}

void ignore_empty_lines(char *line,FILE * f)
{
	fgets(line, MAX_CARS, f);
	while (line[0] == '\0')
	{	
		fgets(line, MAX_CARS, f);
	}	
}

void read_spaces(char *line,long int *i)
{
	while (line[*i]==' ' || line[*i]=='	')
	{
		*i=*i+1;
	}
}

long int read_numero(char *line,long int *i)
{
	long int ini,num,j;
	char w[MAX_CARS];
	
	for(j=0;j<MAX_CARS;j++) w[j]='\0';
	
	ini=*i;
	
	while (line[*i]!=' ' && line[*i]!='	' && line[*i]!='#')
	{
		w[*i-ini]=(char)line[*i];
		*i=*i+1;
	}
	
	num = strtol(w,NULL,0);
	return num;
}


void extract_info(char *line,problem *p)
{
	long int i,n;
	
	i=0;
	
	while (line[i]!='#')
	{
		read_spaces(line,&i);
		if(line[i]!='#')
		{
			n=read_numero(line,&i);
		}
	}
}

void extract_bid(char *line,problem *p,long int bid)
{
	long int i,n,cost,items;
	
	i=0;
	
	read_spaces(line,&i);
	
	n=read_numero(line,&i); // number of bid
	
	read_spaces(line,&i);
	
	cost=read_numero(line,&i); // cost of bid
	
	p->bids[bid]=cost;
	
	p->max_cost=p->max_cost+cost;
	
	// read items
	items=0;
	while (line[i]!='#')
	{
		read_spaces(line,&i);
		if(line[i]!='#')
		{
			n=read_numero(line,&i);
			p->matrix[bid][items]=n;
			items++;
		}
	}
}


long int conflicts_bid(problem *p,long int bid,long int show)
{
	long int goods,j=0,k,bids2,goods2,enc,n_c=0;
	long int i,*bidUsed;

	bidUsed=(long int *)malloc(sizeof(long int)*p->num_bids);
	if(bidUsed==NULL) exit(-1);

	for(i=0;i<p->num_bids;i++)
	{
		bidUsed[i]=FALSE;
	}
	
	goods=p->matrix[bid][j];
	while(goods!=NONE && j<p->num_goods)
	{
		for(bids2=bid+1;bids2<p->num_bids;bids2++)
		{
			enc=FALSE;
			k=0;
			goods2=p->matrix[bids2][k];
			
			while(goods2!=NONE && k<p->num_goods)
			{
				if(goods2==goods && bidUsed[bids2]==FALSE)
				{
					k=p->num_goods;
					goods2=NONE;
					n_c++;
					bidUsed[bids2]=TRUE;
					if(show==TRUE)
					{
						printf("%ld -%ld -%ld 0\n",p->max_cost,bid+1,bids2+1);
					}
				}
				else
				{
				k++;
				goods2=p->matrix[bids2][k];
				}
				
			}
		}
		j++;
		goods=p->matrix[bid][j];
	}
	
	free(bidUsed);
	return n_c;
}

long int generateProblem(problem *p,long int show)
{
	long int n_c=0,i;
	
	for(i=0;i<p->num_bids;i++)
	{
		if(show==TRUE)
		{
			printf("%ld %ld 0\n",p->bids[i],i+1);
		}
		n_c++;
	}
	
	for(i=0;i<p->num_bids;i++)
	{
		n_c=n_c+conflicts_bid(p,i,show);
	}
	
	return n_c;
}

int readProblem(FILE * f, problem *p,char **argv)
{
	
	char line[MAX_CARS];
	
	char aux1[20],aux2[20];
	
	long int i,j,v1,v2,c,n_c,auxN=0;
	
	fgets(line, MAX_CARS, f);
	
	// Initial comments with a starting "%"
	
	while (line[0] != 'g')
	{	
		fgets(line, MAX_CARS, f);
		if(line[0]!='g') printf("c %s",line);
	}
	// goods NUM_OF_GOODS
	sscanf(line, "%s %d", aux1,&p->num_goods);
	
	// bids NUM_OF_BIDS
	fgets(line, MAX_CARS, f);
	sscanf(line, "%s %d", aux2,&p->num_bids);
	
	// dummy NUM_OF_GOODS
	fgets(line, MAX_CARS, f);
	sscanf(line, "%s %d", aux1,&auxN);
	p->num_goods+=auxN;
	ignore_empty_lines(&line[0],f);
	
	
	createProblem(p);
			
	fgets(line, MAX_CARS, f);
	
	// read bids
	for(i=0;i<p->num_bids;i++)
	{
		//extract_info(line,p);
		extract_bid(line,p,i);
		fgets(line, MAX_CARS, f);
	}
		
	// generate the problem in WCNF format
	p->max_cost=p->max_cost+1;
	n_c=generateProblem(p,FALSE);
	printf("c PSEUDOBOOLEAN %d\n",p->max_cost);
	printf("p wcnf %ld %ld\n",p->num_bids,n_c);
	n_c=generateProblem(p,TRUE);
			
	destroyProblem(p);
		
	return TRUE;

}

int main(long int argc,char ** argv)
{
	FILE* fileIn;
	problem p;
	
	if(argc!=2)
	{
		printf("\n Incorrect number of parameters. Usage: auc2cnf [inputfile] \n");
		exit(-1);
	}
	
	fileIn = fopen(argv[1], "r");
	
	if(fileIn == NULL)
	{
		printf("\n Incorrect input file name. Usage: auc2cnf [inputfile]\n");
		exit(-1);
	}
			
	readProblem(fileIn,&p,argv);
	
	fclose(fileIn);
	
}
