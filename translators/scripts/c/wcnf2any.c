#include <stdio.h>
#include <stdlib.h>

#define MAX_CARS 1000
#define TRUE 1
#define FALSE 0
#define WM_MAX_ARITY 50

typedef struct
{
	int val;

	int var;
} literal;

typedef struct
{

	int weight;

	int elim;

	literal TLiterals[WM_MAX_ARITY];

	int literalsTotal;

} clause;

typedef struct
{

	clause * clauses;

	int totalClauses;
	
	int totalVariables;
	
	int totalWeight;
	
	int Top;

} problem;

int genRand(int max)
/* returns random number in range of 0 to max-1 */
{
   int n;
   n=rand()/(int)(((unsigned)RAND_MAX + 1) / max);  /* n is random number in range of 0 - max-1 */
   return(n);
}

int abs(int v)
{
	if(v<0) return (v*(-1));
	else return v;
}

void createProblem(problem *p,int num_cla,int num_var,int top)
{
	int i;

	p->totalClauses = num_cla;
	p->totalVariables = num_var;
	p->Top=top;
	p->totalWeight=0;
	
	p->clauses =(clause *)malloc(sizeof(clause)*p->totalClauses);

	if (p->clauses==NULL)
	{
		printf("\nInsufficient memory\n");
		exit(-1);
	}

	for (i=0;i<p->totalClauses;i++)
	{
		p->clauses[i].weight=0;
		p->clauses[i].elim=FALSE;
	}
	//printf("\n %d %d %d \n",p->totalVariables,p->totalClauses,p->Top);

}

void destroyProblem(problem *p)
{
	free(p->clauses);
}

int numberClauses(problem *p)
{
	 int c,nc=0;
	 
	for (c=0;c<p->totalClauses;c++)
	{
		if(p->clauses[c].elim==FALSE) nc++;
	}
	return nc;

}

void generateCNF(problem *p)
{
	int c,l;
	
	printf("p cnf %d %d\n",p->totalVariables,numberClauses(p));
	
	for (c=0;c<p->totalClauses;c++)
	{
		if(p->clauses[c].elim==FALSE)
		{
		for(l=0;l<p->clauses[c].literalsTotal;l++)
		{
			if(p->clauses[c].TLiterals[l].val==FALSE) printf("-");
			printf("%d ",p->clauses[c].TLiterals[l].var+1);
		}
		
		printf("0\n");
		}
		
	}
}

void generateWCNF(problem *p,int w_wcnf)
{
	int c,l;
	
	// Si w_wcnf==TRUE el problema tiene un Top inicial
	if(w_wcnf==TRUE) printf("p wcnf %d %d %d\n",p->totalVariables,numberClauses(p),p->Top);
	// sino, es un WCNF de toda la vida
	else printf("p wcnf %d %d\n",p->totalVariables,numberClauses(p));
	
	for (c=0;c<p->totalClauses;c++)
	{
		if(p->clauses[c].elim==FALSE)
		{

		printf("%d ",p->clauses[c].weight);
		for(l=0;l<p->clauses[c].literalsTotal;l++)
		{
			if(p->clauses[c].TLiterals[l].val==FALSE) printf("-");
			printf("%d ",p->clauses[c].TLiterals[l].var+1);
		}
		printf("0\n");
		}
		
	}
}

void generateWCNF_RW(problem *p,int w_wcnf)
{
	int c,l,w;
	
	// Si w_wcnf==TRUE el problema tiene un Top inicial
	if(w_wcnf==TRUE) printf("p wcnf %d %d %d\n",p->totalVariables,numberClauses(p),p->Top);
	// sino, es un WCNF de toda la vida
	else printf("p wcnf %d %d\n",p->totalVariables,numberClauses(p));
	
	for (c=0;c<p->totalClauses;c++)
	{
		if(p->clauses[c].elim==FALSE)
		{

		p->clauses[c].weight=genRand(10)+1;
		printf("%d ",p->clauses[c].weight);
		for(l=0;l<p->clauses[c].literalsTotal;l++)
		{
			if(p->clauses[c].TLiterals[l].val==FALSE) printf("-");
			printf("%d ",p->clauses[c].TLiterals[l].var+1);
		}
		printf("0\n");
		}
		
	}
}

void generateWCNF_SIM(problem *p)
{
	int c,l,w;
	
	// Si w_wcnf==TRUE el problema tiene un Top inicial
	// sino, es un WCNF de toda la vida
	printf("p cnf %d %d\n",p->totalVariables,p->totalWeight);
	
	for (c=0;c<p->totalClauses;c++)
	{
		if(p->clauses[c].elim==FALSE)
		{

		for(w=0;w<p->clauses[c].weight;w++)
		{
		for(l=0;l<p->clauses[c].literalsTotal;l++)
		{
			if(p->clauses[c].TLiterals[l].val==FALSE) printf("-");
			printf("%d ",p->clauses[c].TLiterals[l].var+1);
		}
		printf("0\n");
		}
		
		}
		
	}
}

int existsRepeatedClause(problem *p,int n_c,int isCNF)
{
	int c,l,l2,enc,eq;
	for (c=0;c<n_c;c++)
	{
		if(p->clauses[c].elim==FALSE && p->clauses[c].literalsTotal==p->clauses[n_c].literalsTotal)
		{
			eq=0;
			for(l=0;l<p->clauses[c].literalsTotal;l++)
			{
				enc=FALSE;
				for(l2=0;l2<p->clauses[n_c].literalsTotal && enc==FALSE;l2++)
				{
					if(p->clauses[c].TLiterals[l].var==p->clauses[n_c].TLiterals[l2].var && p->clauses[c].TLiterals[l].val==p->clauses[n_c].TLiterals[l2].val)
					{
						enc=TRUE;
					}
				}
				if(enc==TRUE) eq++;
			
			}
			if(eq==p->clauses[c].literalsTotal)
			{
				p->clauses[n_c].elim=TRUE;
				if(isCNF==FALSE)
				{
					p->clauses[c].weight+=p->clauses[n_c].weight;
				}
				return TRUE;
			}

			
		}
		
	}
	return FALSE;
	
}

void generateNO_REPEAT(problem *p,int isCNF, int w_wcnf)
{
	int c,l;
	
	// si el problema inicial es CNF: Se eliminan las clausulas repetidas.
	// si el problema inicial es WCNF: Se suman los pesos y se unifican en una �nica clausula.
	
	for (c=0;c<p->totalClauses;c++)
	{
		if(p->clauses[c].elim==FALSE)
		{
			existsRepeatedClause(p,c,isCNF);
		}
		
	}
	if(isCNF) generateCNF(p);
	else generateWCNF(p,w_wcnf);
}

void printLiteral(int v,int val,int w)
{
	if(val==TRUE)
	{
		printf("+%d*x%d ",w,v+1);
	}
	else
	{
		printf("-%d*x%d ",w,v+1);
	}
}

int nVB(int v)
{
	if(v==FALSE) return TRUE;
	else return FALSE;
}

void generatePseudoBoolean(problem *p)
{
	int num_vars,c,v,l,res;
	
	// Generate Objective
	num_vars=p->totalVariables;
	printf("min:  ");
	for (c=0;c<p->totalClauses;c++)
	{
		if(p->clauses[c].weight<p->Top && p->clauses[c].literalsTotal>1)
		{
			printLiteral(num_vars,TRUE,p->clauses[c].weight);
			num_vars++;
		}
		else if(p->clauses[c].weight<p->Top && p->clauses[c].literalsTotal==1)
		{
			printLiteral(p->clauses[c].TLiterals[0].var,nVB(p->clauses[c].TLiterals[0].val),p->clauses[c].weight);
		}
	}
	printf(";\n");
	
	// Clauses
	
	num_vars=p->totalVariables;
	for (c=0;c<p->totalClauses;c++)
	{
		
		if(p->clauses[c].weight<p->Top  && p->clauses[c].literalsTotal>1)
		{
			res=1;
			printLiteral(num_vars,TRUE,1);
			num_vars++;

			for(l=0;l<p->clauses[c].literalsTotal;l++)
			{
				if(p->clauses[c].TLiterals[l].val==FALSE) res--;
				
			    printLiteral(p->clauses[c].TLiterals[l].var,p->clauses[c].TLiterals[l].val,1);
			}
			printf(">= ");
			if(res>=0) printf("+");
			printf("%d;\n",res);
		}
		else if(p->clauses[c].literalsTotal>1 && p->clauses[c].weight>=p->Top)
		{
			res=1;
			for(l=0;l<p->clauses[c].literalsTotal;l++)
			{
				if(p->clauses[c].TLiterals[l].val==FALSE) res--;
				
			    printLiteral(p->clauses[c].TLiterals[l].var,p->clauses[c].TLiterals[l].val,1);
			}
			printf(">= ");
			if(res>=0) printf("+");
			printf("%d;\n",res);
			
		}
	}
}

void generateMAXONES(problem *p,int w_wcnf)
{
	int c,newtop=0,l,i,nbparam;
	for (c=0;c<p->totalClauses;c++)
	{
		newtop++;
	}
	newtop+=p->totalVariables;

	printf("c PSEUDOBOOLEAN %d\n",newtop);
	// Si w_wcnf==TRUE el problema tiene un Top inicial
	if(w_wcnf==TRUE) printf("p wcnf %d %d %d\n",p->totalVariables,newtop,newtop);
	// sino, es un WCNF de toda la vida
	else printf("p wcnf %d %d\n",p->totalVariables,newtop);
	
	for(i=0;i<p->totalVariables;i++)
	{
		printf("1 %d 0\n",(i+1));
	}
	for (c=0;c<p->totalClauses;c++)
	{
		printf("%d ",newtop);
		for(l=0;l<p->clauses[c].literalsTotal;l++)
		{
			if(p->clauses[c].TLiterals[l].val==FALSE) printf("-");
			printf("%d ",p->clauses[c].TLiterals[l].var+1);
		}
		printf("0\n");
		
	}
}


int readProblem(FILE * f, problem *p,char **argv)
{
	int i,isCnf,num_var=0,num_cla=0,top=0,actual,n_l,var,bType,gen,ct,w_wcnf,nbparam;
	char aux1[MAX_CARS],aux2[MAX_CARS],line[MAX_CARS];
	
	isCnf = strtol(argv[2],NULL,0);
	gen = strtol(argv[3],NULL,0);
	w_wcnf = strtol(argv[4],NULL,0);
	
	fgets(line, MAX_CARS, f);

	while (line[0] != 'p')
	{
		if(isCnf==FALSE && (line[0]=='c' && line[1]==' ' && line[2]=='P' && line[3]=='S' && line[4]=='E' && line[5]=='U' && line[6]=='D' && line[7]=='O'))
		{
			sscanf(line, "%s %s %d", aux1, aux2,&num_cla);
			top=num_cla;
		}
		fgets(line, MAX_CARS, f);
	}


	if(isCnf)
	{
		sscanf(line, "%s %s %d %d", aux1, aux2, &num_var, &num_cla);
		top=num_cla;
	}
	else
	{
	nbparam = sscanf(line, "%s %s %d %d %d", aux1, aux2, &num_var, &num_cla, &top);
	if (nbparam != EOF && nbparam == 5) top=num_cla;
	//sscanf(line, "%s %s %d %d %d", aux1, aux2, &num_var, &num_cla,&top);
	}
	

	createProblem(p,num_cla,num_var,top);
	if(top==0) ct=TRUE;
	else ct=FALSE;


    	for (i=0;i<p->totalClauses;i++)
	{
		n_l=0;
		if(isCnf)
		{

			actual=1;

			p->clauses[i].weight=actual; // is a cnf file: assign weight 1 to all clauses

		}
		else
		{
			// is a wcnf file: read the weight from the file
			fscanf(f,"%d",&actual);
			p->clauses[i].weight=actual;
		}

		p->totalWeight=p->totalWeight+actual;

		// for each clause, read all associated literals

		fscanf(f, "%d", &actual);
		while (actual != 0)
		{

			var=abs(actual);
			if (actual<0) bType=FALSE;
			else bType=TRUE;
			
			// Create a new literal for the clause

			p->clauses[i].TLiterals[n_l].var=var-1;
			p->clauses[i].TLiterals[n_l].val=bType;

			fscanf(f, "%d", &actual);

			n_l++;

		} // end while

		p->clauses[i].literalsTotal=n_l;

	}  // end for
	if(ct==TRUE) p->Top=p->totalWeight;

	if(gen==0) generateWCNF(p,w_wcnf);
	else if(gen==1) generatePseudoBoolean(p);
	else if(gen==2) generateMAXONES(p,w_wcnf);
	else if(gen==3) generateNO_REPEAT(p,isCnf,w_wcnf);
	else if(gen==4) generateWCNF_RW(p,w_wcnf);
	else if(gen==5) generateWCNF_SIM(p);
	destroyProblem(p);
	return TRUE;

}

int main(int argc,char ** argv)
{
	FILE* fileIn;
	problem p;
	
	if(argc!=5)
	{
		printf("\n Incorrect number of parameters. Usage: wcnf2msat [inputfile] [0:WCNF|1:CNF] [0:WCNF|1:MSAT|2:MAXONE|3:NO_REP|4:WCNF|5:SIM] [0:NW_WCNF|1:W_WCNF]\n");
		exit(-1);
	}
	
	//printf("\n Input file: %s ... \n",argv[1]);
	fileIn = fopen(argv[1], "r");
	
	if(fileIn == NULL)
	{
		printf("\n Incorrect number of parameters. Usage: wcnf2msat [inputfile] [0:WCNF|1:CNF] [0:WCNF|1:MSAT|2:MAXONE|3:NO_REP|4:WCNF|5:SIM] [0:NW_WCNF|1:W_WCNF]\n");
		exit(-1);
	}
		
	//printf("\n Translating... \n\n");
	
	readProblem(fileIn,&p,argv);
	
	fclose(fileIn);
	
}
