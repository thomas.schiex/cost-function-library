
# Translator from wcsp format to optimal solution diversity

# Find a set of m solutions with minimum pairwise hamming distance greater than h

# Usage: awk -f wcsp2diverse.awk problem.wcsp m h > problem_m_h.wcsp

function min(x,y) {
    if (x < y) return(x); 
    else return(y);
}

function max(x,y) {
    if (x > y) return(x); 
    else return(y);
}

function perror(i,message) {
  print message;
  print "line " NR ": " $0;
  error = i;
  exit(i);
}

BEGIN {
    RS = "@"; # special character that should never appear in the wcsp file

	m = ARGV[2];
	h = ARGV[3];
	ARGV[2]="";
	ARGV[3]="";
	ARGC=2;
}

{
	# generate problem name
	name = $1;
	nbvar = $2;
	nbval = $3;
	nbconstr = $4;
	ub = $5;

	n = nbvar * m + nbvar*m*(m-1)/2;
	d = max(nbval, 2);
	e = nbconstr * m + nbvar*m*(m-1)/2 + m*(m-1)/2;
	top = (ub-1)*m +1;
	print name "_" m "_" h, n,d,e,top;
 
	# generate variables and domains
	for (i=0; i<nbvar; i++) {
		domsize[i] = $(6+i);
		printf(" %d", domsize[i]);
	}
	for (s=1; s<m; s++) {
		for (i=0; i<nbvar; i++) {
			printf(" %d", domsize[i]);
		}
	}
	for (s1=0; s1<m; s1++) {
		for (s2=s1+1; s2<m; s2++) {
			for (i=0; i<nbvar; i++) {
				printf(" 2");
			}
		}
	}
	print "";
 
	# generate shared constraints from original wcsp
	pos = 6 + nbvar;
	while (pos <= NF) {
		arity = $pos;
		defcost = $(pos + arity + 1);
		if (defcost >= ub) defcost = top;
		nbtuple = $(pos + arity + 2);
		printf("%d", -arity);
		for (a=0; a<arity; a++) {
			printf(" %d", $(pos + 1 + a));
		}
		print " " defcost " " nbtuple;
		for (p = pos + arity + 3; p < pos + arity + 3 + nbtuple * (arity + 1); p += arity + 1) {
			for (k=0; k<arity; k++) {
				printf("%d ", $(p + k));
			}
			cost = $(p + arity);
			if (cost >= ub) cost = top;
			print cost;
		}
		pos += arity + 3 + nbtuple * (arity + 1);
	}

	# duplicate shared constraints from original wcsp(m-1) times
	for (s=1; s<m; s++) {
		nbc = 1;
		pos = 6 + nbvar;
		while (pos <= NF) {
			arity = $pos;
			defcost = $(pos + arity + 1);
			nbtuple = $(pos + arity + 2);
			printf("%d", arity);
			for (a=0; a<arity; a++) {
				printf(" %d", s*nbvar + $(pos + 1 + a));
			}
			print " " defcost " " (-nbc);
			pos += arity + 3 + nbtuple * (arity + 1);
			nbc++;
		}
	}

	# add ternary constraints diff_x_y = (x != y)
	pos = nbvar * m;
	for (s1=0; s1<m; s1++) {
		for (s2=s1+1; s2<m; s2++) {
			for (i=0; i<nbvar; i++) {
				print 3, pos++, s1*nbvar + i, s2*nbvar + i, top, domsize[i]*domsize[i];
				for (u=0; u<domsize[i]; u++) {
					for (v=0; v<domsize[i]; v++) {
						print (u==v)?0:1,u,v,0;
					}
				}
			}
		}
	}

	# add diversity constraints
	pos = nbvar * m;
	for (s1=0; s1<m; s1++) {
		for (s2=s1+1; s2<m; s2++) {
			printf("%d", nbvar);
			for (i=0; i<nbvar; i++) {
				printf(" %d", pos++);
			}
			print "",-1,"wamong hard",top,1,1,h,nbvar;
		}
	}
}
