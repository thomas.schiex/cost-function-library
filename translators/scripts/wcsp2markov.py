#!/usr/bin/python

# translates a WCSP file to a MARKOV field
# costs are translated by exponentiation, sticking between 0 and 1


import sys
import string
from numpy import *

class TokenizedFile:
	def __init__(self,f):
		self.file = f
		self.toklist = []
	def gettok(self):
		if (len(self.toklist) == 0):
			self.toklist = self.file.readline().split()
		return self.toklist.pop(0)

if (len(sys.argv) != 2):
        print "Usage: ", sys.argv[0], " <wcsp format file>\n" 

t = TokenizedFile(open(sys.argv[1],'r'))

print "MARKOV"
t.gettok()		# Skip pb name
n = long(t.gettok())	# nb of variables
print n
t.gettok()		# Skip max domain size
e = long(t.gettok())	# nb of cost functions
ub = long(t.gettok())

ds = [0,] * n		#list of domain sizes  
for i in range(n):
	ds[i] = long(t.gettok())
	print ds[i],
print
print e
maxcost = 0

cfd = [0] * e		# cost functions dimensions
cfc = [0] * e		# cost function cost table
for i in xrange(e):
	r = long(t.gettok())		# arity
	dimen = []
	if (r == 0):	# transform zero-arity cost functions into unary cost functions with the same default cost
		print 1,
		print 0,
		dimen.append(ds[0])
	else:
		print r,
		for j in xrange(r):
			v = long(t.gettok())
			dimen.append(ds[v])
			print v,		# variable index
	print
	default = long(t.gettok())	# default cost
#	maxcost = max(maxcost,default)
	nbtup = long(t.gettok())		# number of tuples
	cft = ones(dimen, dtype = int64)
	cfd[i] = cft.size
	cft *= default			# array with default cost everywhere
	for j in xrange(nbtup):
		idx = []
		for k in xrange(r):
			idx.append(long(t.gettok()))
		cost = long(t.gettok())
#		maxcost = max(maxcost,cost)
		cft[tuple(idx)] = cost
	maxcost = max(maxcost, max(cft.flat))
	cfc[i] = cft

for i in xrange(e):
	print cfd[i]
	for j in cfc[i].flat: 
		if (j >= ub): print 0.0
		elif (j == 0): print 1.0
		else: print exp(-j*log(10000)/float(maxcost))
	print
	
