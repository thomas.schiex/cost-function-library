QPLIB

A selection of quadratic Boolean discrete problem instances without any constraints originated from

http://qplib.zib.de


Use option -qpmult in toulbar2 when loading qpbo format:
toulbar2 -qpmult=0.5 problem.qpbo

Shifting value reported at problem loading time can be used to convert optimum found for other formats like wcnf or wcsp, i.e. the correct optimum is equal to -(optimum + shifting value).

Warning, instances in qpbo (unconstrained) format cannot represent linear constraints. They are a relaxation of the original instances.

