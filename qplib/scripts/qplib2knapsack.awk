
# convert .qplib into .qpbo format

# warning: works only for QBL with only one linear constraint!

BEGIN {
 problem = "";
 arity = 0;
 scope = "";
 weight = "";
 negweight = "";
 defunarycost = 0;
 obj = 0;
 e1 = 0;
 e2 = 0;
 okconstr = 0;
 constr = "";
 inf = 1e100;
}

FNR==3&&/maximize/{
  maximize = 1;
}

FNR==4 {
 n = 0+$1;
}

FNR==5 {
 m = 0+$1;
 if (m>1) {
   print "Error: more than one linear constraint!" > "/dev/stderr";
   exit(-1);
 }
}

FNR==6 {
 e2 = 0+$1;
}

okconstr && FNR>6 && NF==3 && !/#/ {
 arity++;
 scope = scope " " 0+$2-1;
 weight = weight " " int($3);
 negweight = negweight " " (-int($3));
}

okconstr && FNR>6 && /#/ && /value for infinity/ {
 inf = $1;
}

okconstr && FNR>6 && /#/ && /default left-hand-side value/ && $1 != "-" inf && $1 != inf {
 capacity = int($1);
 constr = constr "\n" arity "" scope " -1 knapsack " capacity "" weight	;
}

okconstr && FNR>6 && /#/ && /default right-hand-side value/ && $1 != "-" inf && $1 != inf {
 capacity = int($1);
 constr = constr "\n" arity "" scope " -1 knapsack " (-capacity) "" negweight;
}

FNR>6 && /#/ && /number of linear terms in all constraints/ && $1 > 0 {
  okconstr = 1;
}

!okconstr && FNR>6 && NF==3 && !/#/ {
 if ($1>$2) {
   problem = problem "\n" $2 " " $1 " " $3;
 } else if ($1<$2) {
   problem = problem "\n" $1 " " $2 " " $3;
 } else {
   problem = problem "\n" $1 " " $2 " " 0.5*$3;
 }
}

!okconstr && FNR>6 && NF==2 && !/#/ {
 e1++;
 vars[$1] = 1;
 problem = problem "\n" $1 " " $1 " " $2;
}

FNR>6 && /#/ && /default value for linear coefficients in objective/ {
 defunarycost = $1;
}

FNR>6 && /#/ && /objective constant/ {
 obj += $1;
}

END {
 print n + ((obj)?1:0), ((maximize)?-1:1) * (e2 + n + ((obj)?1:0)) "" problem;
 if (e1 < n) {
   for (i=1; i<=n; i++) {
     if (!(i in vars)) {
       print i,i,defunarycost;
     }
   }
 }
 if (obj) print n+1, n+1, obj;
 if (okconstr) print constr > "/dev/stderr";
}

