#! /bin/tcsh

(awk -f ./qplib2knapsack.awk $1 >! ../instances/qpbo/${1:r:t}.qpbo) >&! "../instances/wcsp/${1:r:t}_constr"
toulbar2 -precision=8 -qpmult=0.5 ../instances/qpbo/${1:r:t}.qpbo -nopre -k=0 -z=../instances/wcsp/${1:r:t}_.wcsp -z=1
awk '/knapsack/{print $0}' "../instances/wcsp/${1:r:t}_constr" >> ../instances/wcsp/${1:r:t}_.wcsp
set NBCONSTR = `awk 'BEGIN{n=0} /knapsack/{n++} END{print n}' "../instances/wcsp/${1:r:t}_constr"`
awk 'FNR==1{$4 += '$NBCONSTR'} {print $0}' ../instances/wcsp/${1:r:t}_.wcsp >! ../instances/wcsp/${1:r:t}.wcsp
rm -f ../instances/wcsp/${1:r:t}_*

