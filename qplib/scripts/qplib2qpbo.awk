
# convert .qplib into .qpbo format

BEGIN {
 problem = "";
 defunarycost = 0;
 obj = 0;
 e1 = 0;
}

FNR==3&&/maximize/{
  maximize = 1;
}

FNR==4 {
 n = $1;
}

FNR==5 {
 e2 = $1;
}

FNR>5 && NF==3 && !/#/ {
 if ($1>$2) {
   problem = problem "\n" $2 " " $1 " " $3;
 } else if ($1<$2) {
   problem = problem "\n" $1 " " $2 " " $3;
 } else {
   problem = problem "\n" $1 " " $2 " " 0.5*$3;
 }
}

FNR>5 && NF==2 && !/#/ {
 e1++;
 vars[$1] = 1;
 problem = problem "\n" $1 " " $1 " " $2;
}

FNR>5 && /#/ && /default value for linear coefficients in objective/ {
 defunarycost = $1;
}

FNR>5 && /#/ && /objective constant/ {
 obj += $1;
}

END {
 print n + ((obj)?1:0), ((maximize)?-1:1) * (e2 + n + ((obj)?1:0)) "" problem;
 if (e1 < n) {
   for (i=1; i<=n; i++) {
     if (!(i in vars)) {
       print i,i,defunarycost;
     }
   }
 }
 if (obj) print n+1, n+1, obj;
}

