FNR==1 {
	n = $1;
	m = $2;
	e = 0;
	print "maximize";
}
	

FNR>1 {
	e++;
	edge1[e] = $1;
	edge2[e] = $2;
	weight[e] = $3;
	if ($3<0) printf("%d x%d_%d", $3, $1, $2);
	else if (FNR==2) printf("%d x%d_%d", $3, $1, $2);
	else printf("+%d x%d_%d", $3, $1, $2);
}

END {
	print "";
	print "Subject to";
	for (e=1; e<=m; e++) {
		print "x" edge1[e] "_" edge2[e] " - x" edge1[e] " + x" edge2[e] " >= 0";
		print "x" edge1[e] "_" edge2[e] " - x" edge2[e] " + x" edge1[e] " >= 0";
		print "x" edge1[e] "_" edge2[e] " - x" edge2[e] " - x" edge1[e] " <= 0";
		print "x" edge1[e] "_" edge2[e] " + x" edge2[e] " + x" edge1[e] " <= 2";
	}
	print "Binary";
	for (e=1; e<=n; e++) {
		printf(" x%d\n",e);
	}
	for (e=1; e<=m; e++) {
		printf(" x%d_%d\n",edge1[e],edge2[e]);
	}
	print "End";
}
