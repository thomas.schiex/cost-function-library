
# warning! must use toulbar2 option -qpmult=1

FNR==1 {
	n = $1;
	m = $2 + 1;
	print -n,-m;
	t = 0;
}

FNR>1 {
	if (($1 ":" $2) in edge) print FNR ": ERROR!!! " $0 > "/dev/stderr"; 
	if (($2 ":" $1) in edge) print FNR ": ERROR!!! " $0 > "/dev/stderr"; 
	edge[$1 ":" $2]++;
	edge[$2 ":" $1]++;
	if ($1 < $2) print $1,$2,-$3/2;
	else if ($1 > $2) print $2,$1,-$3/2;
	else print FNR ": ERROR!!! " $0 > "/dev/stderr"; 
	t += $3/2;
}

END {
	print 1,1,t;
}
