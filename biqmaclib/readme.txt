BIQMACLIB

A selection of quadratic Boolean discrete problem instances originated from

http://biqmac.uni-klu.ac.at/biqmaclib.html

Use default options in toulbar2 when loading these qpbo files:
toulbar2 problem.qpbo

except for maxcut instances where option -qpmult=1 is required
toulbar2 -qpmult=1 problem.qpbo

Original data file  in sparse format can be read directly using:
toulbar2 problem.sparse --qpbo_ext='.sparse'

Shifting value reported at problem loading time can be used to convert optimum found for other formats like wcnf or wcsp, i.e. the correct optimum is equal to (optimum - shifting value).

