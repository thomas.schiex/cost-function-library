
Large weighted mincut randomly generated instances using

GENRMF -- Maxflow generator in DIMACS format.


These instances are solved by Virtual Arc Consistency.

