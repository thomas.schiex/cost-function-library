
# WEIGHTED MIN-CUT to MAX-2SAT encoding

# assumes variable 1 in partition 0
# assumes variable N in partition 1

# ./genrmf.src/genrmf -a 4 -b 16 -c1 1 -c2 100 -seed 1 | awk -f wmincut2wcsp.awk > mincut4.wcsp
/parameters/ {
  c2 = $12;
  a = $6;
  b = $8;
}

/^p max / {
  n = $3;
  e = $4;
  top = c2*a*a*4*a*b + 1;
  print FILENAME,n,2,e+2,top;
  printf("%d",2);
  for (i=2;i<=n;i++) {
	printf(" 2");
  }
  print "";
}

/^a / {
  c++;
  print 2, $2 - 1, $3 - 1, 0, 1;
  print 0,1,$4;
}

END {
  print 1,0,0,1;
  print 1,top;
  print 1,n-1,0,1;
  print 0,top;
}
