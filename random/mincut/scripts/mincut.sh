#!/bin/tcsh

@ a = $1
while (($a <= $1))
  echo $a
  cd $a
  @ b = $a * $a
  @ n = 1
  while (($n <= 50))
    ( echo "toc" ; echo "exit" ) | matlab -r mincut${a}_${n} > mincut${a}_${n}.res
    @ n += 1
  end
  cd ..
  @ a += 1
end

#    ./genrmf.src/genrmf -a $a -b $b -c1 1 -c2 100 -seed $n | awk -f mincut2matlab.awk > ! mincut${a}_${n}.m

#     ./genrmf.src/genrmf -a $a -b $b -c1 1 -c2 100 -seed $n | awk -f mincut2wcsp.awk > ! mincut${a}_${n}.wcsp

#    ./genrmf.src/genrmf -a $a -b $b -c1 1 -c2 100 -seed $n | awk -f mincut2matlab.awk > ! mincut${a}_${n}.m
