
# WEIGHTED MIN-CUT to matrix encoding

# assumes variable 1 in partition 0
# assumes variable N in partition 1

# ./genrmf.src/genrmf -a 4 -b 16 -c1 1 -c2 100 -seed 1 | awk -f wmincut2matlab.awk > ! mincut4.m

# echo "exit" | time matlab -r mincut4

/parameters/ {
  c2 = $12;
  a = $6;
  b = $8;
}

/^p max / {
  n = $3;
  e = $4;
  top = c2*a*a*4*a*b + 1;
}

/^a / {
  c++;
  m[$2,$3] = $4;
}

END {
  printf("G_dir=[");
  for (i=1; i<=n; i++) {
	for (j=1; j<=n; j++) {
	  printf(" %d", m[i,j]);
	}
	if (i < n) printf(";");
  }
  print "];";
  print "F_cut_dir = sfo_fn_cutfun(G_dir);";
  print "V_G = 1:" n ";";
  print "tic";
  print "A" n " = sfo_s_t_mincut(F_cut_dir,V_G,1," n ");";
  print "toc";
  print "";
}
