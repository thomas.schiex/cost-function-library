# Submitter: Tian Sang (University of Washington)
# Domain: Grid networks, from 12x12 to 50x50 with varying level of determinism
# Type: Bayes for PRE

    * roughly, 50%, 75%, or 90% of the parameters are 0/1
    * 320 Instances
    * Between 144 and 2,500 binary variables 

# treewidth: ~12-50
# Evidence by assigning value 1 to leaf node
#

networks, list of networks used for exact PR/MAR, and a subset of networks used for the other tasks.
