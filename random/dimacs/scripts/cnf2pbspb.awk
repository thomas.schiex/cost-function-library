/^p / {
  var = $3;
  nb = $4;
}

FNR != NR && FNR == 1 {
  printf("# PBType : GE\n");
  printf("# PBGoal : %d\n", $1 + 1);
  printf("# PBObj : MAX\n");
  printf("# NumCoef : %d\n", nb);

  for (i=var+1; i<=nb+var; i++) {
    printf("v%d c1\n", i);
  }
}
