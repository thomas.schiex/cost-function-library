BEGIN {
  ok = 0;
}

ok {
  nb++;
  for (i=1; i<NF; i++) {
    if ($i < 0) printf("~y%d", -$i);
    else printf("y%d", $i);
    printf(" + ");
  }
  printf("~z%d >= 1\n", nb);
}


/^p / {
  var = $3;
  nb = $4;
  ok = 1;

  printf("max: ");
  for (i=1; i<nb; i++) {
    printf("z%d + ", i);
  }
  printf("z%d\n", nb);
  nb = 0;
}
