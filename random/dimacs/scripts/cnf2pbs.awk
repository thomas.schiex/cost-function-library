BEGIN {
  ok = 0;
}

ok {
  nb++;
  for (i=1; i<NF; i++) {
    printf("%d ", $i);
  }
  printf("-%d 0\n",nb+var);
}

/^p / {
  var = $3;
  nb = $4;
  ok = 1;

  printf("p cnf %d %d\n",var+nb,nb);
  nb = 0;
}
