A Library of Mixed-Integer and Continuous Nonlinear Programming Instances (MINLPLib)

Degree-four model for low autocorrelated binary sequences
This instance arises in theoretical physics. Determining a ground
state in the so-called Bernasconi model amounts to minimizing a
degree-four energy function over variables taking values in
{+1,-1}. Here, the energy function is expressed in 0/1 variables. The
model contains symmetries, leading to multiple optimum solutions.

See http://www.minlplib.org/
