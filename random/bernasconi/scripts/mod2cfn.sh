#!/bin/tcsh

# usage: ./mod2cfn.sh problem.mod > problem.cfn

# warning! remove by hand before all comment/blank lines and the objective variable in the input mod file
# keep only decision variable definition and the objective function

sed -z 's/\n/ /g' $1 | sed -E 's/[ ]*[*][ ]*b/*b/g' | sed -E 's/[ \n]+[+][ \n]+/\n/g' | sed -E 's/[ \n]+[-][ \n]+/\n-/g' | awk 'FNR==1{last=$NF;NF=NF-1;gsub(" binary >= 0, <= 1;", ": 2,",$0);gsub("var b","b",$0);print "{"; print "problem: {name: autocorr_bern, mustbe: <1000000},";print "variables: [";print $0;print "],";print "functions: [";$0=last} /[*]b/{sub("[*]b"," b",$0);cost=$1;$1="[scope: [";a=1+gsub("[*]b",",b",$0);$FN=$FN " ], defaultcost: 0, costs: [";for (i=1;i<=a;i++) $NF=$NF " 1,"; $NF=$NF " " cost " ]],"} {print $0} END{print "]}"}'

