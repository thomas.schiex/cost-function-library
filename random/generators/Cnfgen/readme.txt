Files in this directory are intended to promote and assist the
conduct of reproducible and comparable experiments in CNF satisfiability
testing and/or approximation.

The program mkcnf.c (sparc executable suplied is mkcnf) generates
a ``random'' constant-clause-length CNF formula in Dimacs challenge
format, on stdout by default.  Execute the program with no arguments
to get a usage message.

The above program has a "-f" option to "force" the formula to be satisfiable.
However, it uses a method that does not bias the generated literals
to be satisfiable by the "hidden assignment" that the formula is
guaranteed satisfy the output formula.
Therefore, these formulas may not be excessively easy for guessing
approaches.

Two csh shell scripts are included.  These are important adjuncts
to accurate experimentation, because of the way they generate seeds
for mkcnf and record them to facilitate "accounting".
The exact seed appears in a comment in the generated formula.
The last 6 digits of the seed appear in the the output file name.
The numbers of variables and clauses also make up part of that name.

The seed is COMPUTED FROM (not equal to!) a used-supplied "start",
plus the number of variables and the number of clauses.
(see names in this directory as examples).
This makes sure that a formula with more clauses, but other parameters
equal, does not contain the shorter formula as a prefix.
Therefore, runs that keep the number of variables fixed and
vary just the number of clauses operate on independent formulas
(for practical purposes), and are more statistically meaningful.
(Some previously reported results lacked this independence, and
thereby forced the "percent satisfiable" to decrease as clauses increased.)

The program mkcnf.c is loosely based on mwff.c, which was contributed
to the Dimacs Challenge by Bart Selman of AT&T.
The main change in functionality is the method for forcing formulas to be
satisfiable, as mentioned above.
The interface is also much different.
Finally, there are minor changes in the way values returned by random()
are scaled.  I believe the new scaling removes some minor biasing.

mkcnf.c calls the Unix function random() to generate pseudo-random numbers.
If you recompile the source, there are 6 formulas in this directory
to permit you to check that you are generating the same formulas
as the contributed program, from the same arguments.
They were produced by the following commands:

 	mkcnf3.sh   100 10 1 3 >& sample-mkcnf3.log
	mkcnf3-F.sh 100 10 1 3 >& sample-mkcnf3-F.log

--Allen Van Gelder, University of California, Santa Cruz  avg@cs.ucsc.edu
