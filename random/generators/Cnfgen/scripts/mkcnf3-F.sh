# mkcnf3-F.sh :
#	make a series of SATISFIABLE 3-cnf formulas in DIMACS CNF format.


set len = 3
# Please change shell name and initial comment if you change len !

# maxnum must be a power of 10 and the number of 0's must agree with
# the string "%0??d\n" in the later printf (retain leading 0 in that string).
set maxnum = 1000000

if ($#argv < 2 || $#argv > 4) then
	echo Usage: $0 'vars clauses [ start [ numFmlas ] ]'
        echo ' e.g.   ' $0 '50 215 1 10000'
	echo '	default numFmlas = 1; default start = 1;'
	echo '	seed is computed; appears in fmla comment; affects file name.'
	if ($#argv == 0) exit 0
	exit 1
endif

set vars = $1
set clauses = $2
set seed0 = 1
set num = 1
if ($#argv > 2) set seed0 = $3
if ($#argv > 3) set num = $4

if ($num > $maxnum) then
	echo 'ERROR: script generates at most ' $maxnum 'unique file names.'
	echo 'Edit a modified script to increase this bound--see comments.'
	exit 2
	endif

@ seedc = $seed0
@ final = $seed0 + $num - 1

while($seedc <= $final)
    set seed = ` echo $1 $2 $seedc $maxnum | awk -e ' { printf("%06d\n",\\\
			(($1 + 53*$2) * 133 + $3)%($4) ); } ' `
    if ($seedc == $seed0) then
	echo -n 'Initial seed:' $seed ' Generating' $num 'formulas. '
	echo    'Vars:' $vars Clauses: $clauses Start: $seed0
	endif
    set fmla = cnf3-F.${vars}.${clauses}.${seed}
    mkcnf ${vars} ${clauses} ${len} ${seed} -f > ${fmla}
    @ seedc += 1
    end

exit 0



