/* mkcnf.c  modified from mfyumi.c  by AVG, to generate Dimacs cnf
	sat problems.

   Usage: mkcnf #vars #clauses clauseLen [seed] [-f | -u] [out_file]

   default seed = 55; seed must be present if there are more args.
   -f forces fmla to be sat, -u means unforced, default is unforced.
   out_file  is the file, rather than stdout, upon which to write fmla.
*/

/* mfyumi.c : original program is mf.c           Yumi K. Tsuji  3/23/92
   ... obsolete documentation deleted ...
*/

/* based on mf.c by Bart Selman of AT&T
   (contributed to 1993 Dimacs challenge).
*/

/* However, this program is in ANSI C, and generates Dimacs CNF format.

   It has an original feature to generate "forced-satisfiable" cnf
   without making them unnecessarily easy (Selman reported that "forcing"
   led to exceedingly easy formulas).

   Allen Van Gelder, University of California, Santa Cruz, June 1993
   avg@cs.ucsc.edu
**************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#define MAXLINE 200		/* max number char for file name */

/********* standard prototypes ****/
long	random(void);
long	srandom(long);

/********* local prototypes ****/
static void	init_assign_values(void);
static void	init_clause_values(void);
static void	clear_clause(void);
static void	output_wff(char wff_file[]);
static int	store_clause(int last_lit);
static void	make_clause(int forced);
static int	not_in_clause(int var);
static int	clause_sat_odd(void);
static int	get_clause(int ptr, int var);

double          BIG = 2147483648.0;	/* 2**31  */

typedef struct lit_str
{
   int             lit;
   int             next;
   int             next_save;	/* permanent copy */
   int             lits_remain;	/* num lits remaining */
   int             lits_remain_save;	/* permanent copy */
}              *lit_str_ptr;	/* lit_str_ptr is a pointer to structure
				 * lit_str */

typedef struct var_str
{
   int             name;
   short int       value;
   int             make;
   int             critical;
   int             diff;
   int             first;
   int             first_save;	/* permanent copy */
   int             max_diff_list;
   int             forced;	/* 1 if forced by propagation */
   int             picked;	/* 1 if value of variable has been set in
				 * choices */
}              *var_str_ptr;

typedef struct choices_str
{
   int             value;
}              *choices_str_ptr;

typedef struct forced_str
{
   int             value;
}              *forced_str_ptr;

typedef struct prop_array_str
{
   int             value;
}              *prop_array_str_ptr;

typedef struct cl_lit_str
{
   short int       lit;
   int             pos;
}              *cl_lit_str_ptr;	/* cl_lit_str_ptr is a pointer to structure
				 * cl_lit_str; use to hold temp clause */

static lit_str_ptr     wff;	/* wff is global array containing the wff
				 * first lit at location 0 */
static var_str_ptr     assign;	/* assign is global array containing the
				 * assignment variable name from 1 to N
				 * stored atlocations 1 to N */

 /* from 1 to N ; first entry is count */
static cl_lit_str_ptr  clause;	/* clause is global array containing a clause
				 * length in location 0 first lit at location
				 * 1 */

static unsigned int    nvars,		/* number of variables */
                nclauses,	/* number of clauses */
                nlits,		/* number of literals  */
                clause_length;	/* number of lits per clause */
static int	seed;		/* seed used to initialize randomizer */
static int             cvRatio = 0;    /* #clauses/#vars ratio times 100. */

static   int	forced, reveal;

static    char            wff_file[MAXLINE];
static    char            pgm_name[MAXLINE];

/* The main routine. */
int	main(int argc, char* argv[])
{
   int             i;
   int             last_lit;

   setbuf(stdout, NULL);
   if (argc < 4)
      {
      printf(
	"Usage: %s #vars #clauses clauseLen [seed]  [-f | -u] [out_file]\n",
	argv[0]);
      printf("\twhere -f forces a satisfiable formula, ");
      printf("-u (default) is unforced;\n");
      printf("\tand   default seed is 55 (reqd if more args);\n");
      printf("\tand   default out_file is stdout.\n");
      exit(0);
      }
   if (argc == 1)
      {
      exit(0);
      }
   if (argc < 4)
      {
      exit(1);
      }

   nvars = atoi(argv[1]);
   nclauses = atoi(argv[2]);
   clause_length = atoi(argv[3]);
   cvRatio = (nclauses * 100 + 50) / nvars;
   strcpy(pgm_name, argv[0]);

   /* initializes rand #s */
   if (argc > 4)
   {
      seed = atoi(argv[4]);
   } else
      seed = 55;

   srandom(seed);

   wff_file[0] = 0;
   forced = 0;
   reveal = 0;
   if (argc > 5)
	{
	if (argv[5][0] != '-')
		{
		strcpy(wff_file, argv[5]);
		}
	else if (argv[5][1] == 'f')
		{
		forced = 1;
		}
	else if (argv[5][1] == 'r')
		{
		forced = 1;
		reveal = 1;
		}
	else if (argv[5][1] == 'a')
		{
		forced = 1;
		reveal = -1;
		}
	else if (argv[5][1] == 'n')
		{
		forced = 1;
		reveal = -2;
		}
	else if (argv[5][1] == 'p')
		{
		forced = 1;
		reveal = -3;
		}
	else
		{
		forced = 0;
		}
	}

   if (argc > 6)
   {
      strcpy(wff_file, argv[6]);
   }


   /* allocate memory for the wff */
   nlits = nclauses * clause_length;
   wff = (lit_str_ptr) malloc(nlits * (sizeof(struct lit_str)));
   /* allocate memory for the assignment */
   assign = (var_str_ptr) malloc((nvars + 1) * (sizeof(struct var_str)));
   /* allocate memory for temporary storage of a clause */
   clause = (cl_lit_str_ptr) malloc((nvars + 1) *
				    (sizeof(struct cl_lit_str)));
   init_assign_values();
   init_clause_values();

   last_lit = -1;
   for (i = 1; i <= nclauses; i++)
   {
      clear_clause();
      make_clause(forced);
      last_lit = store_clause(last_lit);
   }
   if (argc > 1)
      output_wff("");		/* just output to stdout */
   else
   {
      output_wff(wff_file);
   }
   return 0;
}


static void	init_assign_values(void)
/* initializes assigns values randomly */
{
   int             i;
   double	rand_num;

   /* initialize assign */
   for (i = 0; i <= nvars; i++)
   {
      rand_num = random();
      if (reveal < 0)
         {
         if (reveal == -1 && i%2 ==0)
                   assign[i].value = 1;
         else if (reveal == -1 && i%2 ==1)
                   assign[i].value = -1;
         else if (reveal == -2)
                   assign[i].value = -1;
         else /* (reveal == -3) */
                   assign[i].value = 1;
         }
      else if (2.0*rand_num >= BIG)
	 assign[i].value = 1;
      else
	 assign[i].value = -1;

      if (reveal == 1 && i == 0)
         fprintf(stderr, "The forced assignment is:\n");
      if (reveal == 1 && i > 0)
	    {
	    fprintf(stderr, "%7d", i*assign[i].value);
	    if (i%10 == 0 || i==nvars)
		    fprintf(stderr, "\n");
	    }

      assign[i].make = 0;
      assign[i].critical = 0;
      assign[i].diff = 0;
   }
}


static void	init_clause_values(void)
/* initializes clause entries to 0 */
{
   int             i;

   for (i = 0; i <= nvars; i++)
      clause[i].lit = 0;
}

static void	clear_clause(void)
{
   int             i;

   for (i = 0; i <= clause_length; i++)
      clause[i].lit = 0;
}


static void	output_wff(char wff_file[])
{
   int             i, j, pntr, tmp;

   FILE*	fp;

   if (wff_file[0] == '\0')
      fp = stdout;
   else
      fp = fopen(wff_file, "w");
   fprintf(fp, "c %s random cnf with seed %d, max clause length %d\n",
		pgm_name, seed, clause_length);
   if (forced > 0)
	{
	fprintf(fp, "c FORCED SATISFIABLE\n");
	}
   fprintf(fp, "c \n");
   fprintf(fp, "p cnf %d  %d\n", nvars, nclauses);

   pntr = 0;
   for (i = 1; i <= nclauses; i++)
   {
      tmp = get_clause(pntr, 1);
      for (j = 1; j <= clause[0].lit; j++)
	 fprintf(fp, "%d  ", clause[j].lit);
      fprintf(fp, "0\n");
      pntr = pntr + clause[0].lit;
   }

   if (wff_file[0] != '\0')
      fclose(fp);
}


static int	store_clause(int last_lit)
/* returns position of last lit in wff array */
{
   int             i, lit;

   /* first lit is oversized */
   lit = clause[1].lit;
   if (lit > 0)
      wff[(last_lit + 1)].lit = lit + nvars;
   else
      wff[(last_lit + 1)].lit = lit - nvars;

   for (i = 2; i <= clause[0].lit; i++)
      wff[(last_lit + i)].lit = clause[i].lit;

   return (last_lit + clause[0].lit);
}

 /* initialize assign */


static void	make_clause(int forced)
/* generate random clause */
/* if forced > 0 then ensure it is satisfied in an ODD NUMBER OF LITS
   by assignment all 1's  */
{
   int             i, var;

   clause[0].lit = clause_length;
   for (i = 1; i <= clause_length; i++)
   {
      /* pick var not yet in clause */
      while (1 == 1)
      {
	 var = floor(((double) random() / BIG) * nvars);
	 ++var;
	 if (var == (nvars + 1))
	    --var;
	 if (not_in_clause(var) == 1)
	    break;
      }
      clause[i].lit = var;
   }

   while (1 == 1)
   {
      for (i = 1; i <= clause_length; i++)
	 /* set random sign */
	 if (2.0 * random() >= BIG)
	    clause[i].lit = (-1 * clause[i].lit);
      if ((forced < 1) || (clause_sat_odd() == 1))
	 break;
   }
}


static int	not_in_clause(int var)
/* 1 if not in; otherwise 0 */
{
   int             i;

   for (i = 1; i <= clause[0].lit; i++)
      if (abs(clause[i].lit) == var)
	 return 0;
   return 1;
}



static int	clause_sat_odd(void)
/* returns 1 if satisfied in an ODD NUMBER OF LITERALS, 0 otherwise */
{
   int             i;
   int	numtrue;

   numtrue = 0;
   for (i = 1; i <= clause[0].lit; i++)
	{
	if (assign[abs(clause[i].lit)].value * clause[i].lit > 0)
		numtrue ++;
	}
   if ((numtrue % 2) == 1)
	return 1;
   else
	return 0;
}


static int	get_clause(int ptr, int var)
/* gets clause and returns pointer to next one that contains var */
{
   int  lit, ptr_next, len_clause;

   ptr_next = 0;
   len_clause = 0;
   lit = wff[ptr].lit;
   /* first lit is oversized */
   lit = lit - ((lit / abs(lit)) * nvars);
   if (abs(lit) == var)
      ptr_next = wff[ptr].next;
   ++len_clause;
   clause[len_clause].lit = lit;
   ++ptr;
   if (ptr == nlits)
   {
      clause[0].lit = len_clause;
      return ptr_next;
   }
   lit = wff[ptr].lit;
   if (abs(lit) == var)
      ptr_next = wff[ptr].next;
   while (abs(lit) <= nvars)
   {
      ++len_clause;
      clause[len_clause].lit = lit;
      ++ptr;
      if (ptr == nlits)
      {
	 clause[0].lit = len_clause;
	 return ptr_next;
      }
      lit = wff[ptr].lit;
      if (abs(lit) == var)
	 ptr_next = wff[ptr].next;
   }
   clause[0].lit = len_clause;
   return ptr_next;
}
