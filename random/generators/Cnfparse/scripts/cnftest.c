/*****************************************************************/
/*****************************************************************/
/*                                                               */
/*  cnftest.c   Main Program for cnftest-to test cnfparse.{c,h,o}*/
/*                                                               */
/* Yumi K. Tsuji and Allen Van Gelder, UC Santa Cruz, 930517     */
/*								 */
/*****************************************************************/
/*****************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cnfparse.h"

char	inputFileName[256];

static void     checkArgs(int argc, char* argv[]);
static void     processInput(int argc, char* argv[]);
static void     readInput(char fileName[]);
static void     printCnf(void);


int	main(int argc, char *argv[])
{
    processInput(argc, argv);	/* may exit on error */
    printCnf();
    return 0;
}

/*****************************************************/
/*static void processInput(int argc, char *argv[])   */
/*---------------------------------------------------*/
/* Reads the input file and set the global vars:     */
/*   cnf, as in cnfparse.h			    */
/*****************************************************/

static void
processInput(int argc, char *argv[])
{
    setbuf(stdout, NULL);
    checkArgs(argc, argv);
    readInput(inputFileName);
}

static void
checkArgs(int argc, char *argv[])
{
    if (argc < 2)
	{
	strcpy(inputFileName, "-");
	}
    else
	{
	strcpy(inputFileName, argv[1]);
	}
    if (argc > 2)
	fprintf(stderr,
		"ignoring execess arguments and proceeding with first.\n");
}

static void
readInput(char fileName[])
{
    int	    retn;
    FILE*   fp;

    if (strcmp(fileName, "-"))
	fp = fopen(fileName, "r");
    else
	fp = stdin;
    if (fp == NULL)
    {
	fprintf(stderr, "couldn't open file %s\n", fileName);
	exit(1);
    }
    setbuf(stdout, NULL);
    fflush(stdout);
    retn = loadCnf(fp, stdout);
    if (retn != 0)
    {
	fprintf(stderr, "parse error in reading %s\n", inputFileName);
	exit(retn);
    }
    fclose(fp);
}

static void     printCnf(void)
{
    long    curNum, curOffset, curLit, nLits;
    long*   curClause;

    printf("p cnf %ld %ld\n", cnf.numVars, cnf.numClauses);

    for(curNum = 1; curNum <= cnf.numClauses; curNum++)
	{
	curOffset = cnf.offsets[curNum];
	curClause = cnf.clauseSpace + curOffset;
	nLits = curClause[0];
	for (curLit = 1; curLit <= nLits; curLit++)
		{
		printf("%ld\t", curClause[curLit]);
		}
	printf("%ld\n", 0);
	}
}
