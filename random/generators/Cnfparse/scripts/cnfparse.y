%{

/*****************************************************************/
/*****************************************************************/
/*                                                               */
/* cnfparse.y  -  parser for Dimacs Challenge CNF format         */
/* input is comment lines, problem line, then formula in the form*/
/* of a sequence of signed integers, with 0 terminating each     */
/* clause.   See cnfparse.h for information on usage.            */
/*                                                               */
/* main output is an IList representing the clauses, in the      */
/* format: len1, L11, ..., L1_len1, len2, L21, ..., L2_len2, ... */
/* Example: 5 -7 0 -5 0 -7 8 -3 0 becomes the IList              */
/* 2, 5, -7, 1, -5, 3, -7, 8, -3.                                */
/*                                                               */
/* Other outputs are counts of 1-, 2-, 3-, and longer clauses,   */
/* and length of the longest clause.  All outputs are stored in  */
/* locations provided by a call to parseInit. (See cnfparse.h)   */
/*                                                               */
/* Yumi K. Tsuji and Allen Van Gelder, UC Santa Cruz, 930517     */
/*                                                               */
/* Modified: 930607 ykt    realloc_clauseSp function             */
/*                                                               */  
/*****************************************************************/
/*****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cnfparse.h"


struct cnfStruct cnf;

extern int yydebug;

long	yylineno;
long	curClause, curOffset, nextOffset, curLitPos;
int	lexState, junkFound;

FILE*	inputFile;
FILE*   echo_comment;
FILE*   errorFile;

int	yyparse(void);
int	yylex(void);
long	import(char* s);
static void realloc_clauseSp(void);
static void setupExport(void);

#define	strEq(s, t) ( !strcmp((s), (t)) )
%}

%union
{
   long  ival;
   float fval;
   char* sval;
}





%start sentence

%token <sval> COMMENT
%token PROBLEM_P PROBLEM_CNF
%token <ival> INT
%token < sval > LITERAL ZERO
%type < ival > lit litList clause cnf sentence
%left '-'

%%
sentence: comments problem cnf
		{
		cnf.clauseSpace = (long*)realloc(cnf.clauseSpace,
					curOffset * sizeof(long));
		cnf.allocSpaces = curOffset;
		$$ = 0;
		YYACCEPT;	/* end of file */
		}
	|
		{
		$$ = 0;
		YYABORT;
		}
	;

comments : comments comment | ;

comment : COMMENT
		{ if (echo_comment != NULL)
			fprintf(echo_comment, "%s", $1);
		}

problem: PROBLEM_P PROBLEM_CNF INT INT
		{
    	        cnf.numVars = $3;
		cnf.numClauses = $4;
		lexState = 1;
		if (echo_comment != NULL)
			fprintf(echo_comment, "c p cnf %ld %ld\n",
				cnf.numVars, cnf.numClauses);
		cnf.count1 = cnf.count2 = cnf.count3 = cnf.countLong = 0;
		cnf.longest = 0;
		cnf.numLitOccs = 0;
		cnf.allocSpaces = 4*(cnf.numClauses+1);
		cnf.clauseSpace = (long*)malloc(cnf.allocSpaces*sizeof(long));
		cnf.offsets = (long*)malloc((cnf.numClauses+2)*sizeof(long));
		cnf.export_num = (long*)malloc((cnf.numVars+1)*sizeof(long));
		bzero( (char*)cnf.export_num, (cnf.numVars+1)*sizeof(long) );
		cnf.offsets[0] = 0;
		cnf.clauseSpace[0] = 0;
		cnf.offsets[1] = 1;
		curOffset = 1;
		curLitPos = 1;
		curClause = 1;
		}

cnf:
	clause
		{
		$$ = 0;
		}
	|
	cnf clause
		{
		$$ = 0;
		}
	;

clause:	litList ZERO
		{
		if ($1 == 1)
			cnf.count1++;
		else if ($1 == 2)
			cnf.count2++;
		else if ($1 == 3)
		    cnf.count3++;
		else if ($1 == 4)
		    cnf.count4++;
		else if ($1 == 5)
		    cnf.count5++;
		else if ($1 == 6)
		    cnf.count6++;
		else if ($1 > 6)
			cnf.countLong++;
		if (cnf.longest < $1)
			cnf.longest = $1;
		nextOffset = curOffset + $1 + 1;
		if (nextOffset >= cnf.allocSpaces)
		    realloc_clauseSp();
		cnf.clauseSpace[curOffset] = $1;  /* lits in clause */
		curClause ++;
		curOffset = nextOffset;
		curLitPos = 1;
		cnf.offsets[curClause] = curOffset;
		$$ = 0;
		}
		;

litList: litList lit
		{
		$$ = curLitPos - 1;
		}
	|
		{
		$$ = curLitPos -1;
		}
	;


lit:	LITERAL
		{
      	        $$ = import($1);
		if ((curOffset+curLitPos) >= cnf.allocSpaces)
		    realloc_clauseSp();		  
		cnf.clauseSpace[curOffset+curLitPos] = $$;
		curLitPos ++;
		cnf.numLitOccs ++;
		}
	| '-' LITERAL
		{
		$$ = (- import($2));
		if ((curOffset+curLitPos) >= cnf.allocSpaces)
		    realloc_clauseSp();		  		  
		cnf.clauseSpace[curOffset+curLitPos] = $$;
		curLitPos ++;
		cnf.numLitOccs ++;
		}
	;

%%

long	import(char* s)
	{
	long	i = atoi(s);
	if (i > cnf.numVars)
		{
		fprintf(errorFile, "Variable out of range: %ld, line %ld\n",
			i, yylineno);
		}
	cnf.export_num[i] = 1;
	return i;
	}

long	exportLit(long i)
	{
	if (i < 0)
		return -(cnf.export_num[-i]);
	else
		return cnf.export_num[i];
	}

int	loadCnf(FILE* inputF, FILE* echoC)
	{
	int retn;

	inputFile = inputF;
	echo_comment = echoC;
	if (echo_comment == NULL)
		errorFile = stderr;
	else
		errorFile = echo_comment;
	lexState = 0;
	junkFound = 0;
	yylineno = 1;
	retn = yyparse();
	if (curClause != cnf.numClauses + 1)
		{
		fprintf(errorFile, "ERROR Clauses loaded not = 'p' line.\n");
		retn = -1;
		}
	if (junkFound)
		{
		fprintf(errorFile,
			"ERROR Junk found in input has been printed.\n");
		retn = -2;
		}
	if (! feof(inputFile))
		{
		fprintf(errorFile,
			"WARNING Clauses loaded, but EOF not reached.\n");
		}
	setupExport();
	return retn;
	}

/********************* parser subroutines follow  *******/

static void
setupExport(void)
	{
	long	maxvar = cnf.numVars;
	long	ex, i;

	ex = 0;
	for (i = 1; i <= maxvar; i++)
		if (cnf.export_num[i] > 0)
			{
			ex++;
			cnf.export_num[i] = ex;
			}
	cnf.export_num[0] = ex;
	}


static void
realloc_clauseSp(void)
{
  cnf.allocSpaces = cnf.allocSpaces * 2;
  cnf.clauseSpace = (long*)realloc(cnf.clauseSpace,
				   cnf.allocSpaces * sizeof(long));
  if (cnf.clauseSpace == NULL)
      {
	printf("cnfparse: realloc failed, ");
	printf("line %d.\n", yylineno);
	exit(1);
      }
}

/********************* lexical analyzer (scanner) code follows  *******/

static char	 literalBuffer[512];

int preambleToken(void)
{
extern YYSTYPE  yylval;
int	  c;
long	theInt;

c = getc(inputFile);
ungetc(c, inputFile);
if (c == 'c')
	{
	fgets(literalBuffer, 510, inputFile);
	yylineno ++;
	yylval.sval = literalBuffer;
	return COMMENT;
	}

/* read one word from presumed "problem line". */
fscanf(inputFile, "%s", literalBuffer);
if (strEq(literalBuffer, "p"))
	{
	return PROBLEM_P;
	}
else if (strEq(literalBuffer, "cnf"))
	{
	return PROBLEM_CNF;
	}
/* else */
	{
	sscanf(literalBuffer, "%ld", &theInt);
	yylval.ival = theInt;
	return INT;
	}
}


int yylex(void)
{
extern YYSTYPE  yylval;
int	  c;

if (lexState == 0)
	return preambleToken();

if (curClause > cnf.numClauses)
	{
	/* act like it is end-of-file; consume whitespace and maybe \n. */
	fscanf(inputFile, "%500[ \t\n]", literalBuffer);
	return 0;
	}

while(1)
    {
    /* all ascii chars <= ' ' are "white space"  */
    while ((c = getc(inputFile)) <= ' ' && c >= '\0')
	{
	if (c == '\n') yylineno++;
	}

    if (c < 0)
	return 0;

    if (c == '-')
	return (c);

    if (c >= '0' && c <= '9')
	{
	/* a LITERAL (var name) is an unsigned positive integer.
	   (Code permits easy relaxation of this rule.)
	   a ZERO is the unsigned integer 0.
	   Buffer it, and  return ptr to buffer. */
	ungetc(c, inputFile);
	fscanf(inputFile, "%127[0-9]", literalBuffer);
	literalBuffer[127] = '\0';
	yylval.sval = literalBuffer;
	if (literalBuffer[0] == '0' && atoi(literalBuffer) == 0)
		return (ZERO);
	else
		return (LITERAL);
	}

    /* act like lex; just output the garbage and keep scanning. */
    putc(c, errorFile);
    junkFound = 1;
    }
}

void yyerror(char* s)
{
    fprintf(errorFile, "%s; line=%ld\n", s, yylineno);
}

