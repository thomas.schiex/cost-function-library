/*****************************************************************/
/*								 */
/* cnfparse.h  -  interface to the parser for  CNF version of    */
/* Dimacs Challenge format					 */
/* input is comment lines, problem line, then formula in the form*/
/* of a sequence of signed integers, with 0 terminating each     */
/* clause.  More details are given below.			 */
/*								 */
/* loadCnf(infile, commentfile) loads from infile into the       */
/*	struct named cnf, as specified below.			 */
/*	See prototype below. Negative return value denotes error,*/
/*	otherwise the return value is that of yyparse().	 */
/*	Parser will abort (with core dump) if it cannot get	 */
/*	enough memory to even get started.  It will exit with	 */
/*	an error message if it needs to expand memory and cannot.*/
/*								 */
/* 'commentfile' should be the file pointer on which to print    */
/* the preamble statements, or NULL.  If it is non-NULL, all	 */
/* comments are echoed exactly; the "p" line is prefixed by "c p"*/
/* and is echoed with spacing possibly changed.			 */
/*								 */
/* If 'commentfile' is NULL, errors will be printed on stderr,   */
/* else they will be printed on 'commentfile'.			 */
/*								 */
/*								 */
/* exportLit(i) maps the internal variable number i into a long	 */
/*	int between 1 and the actual number of variables in the	 */
/*	formula.  This mapping preserves the original order,	 */
/*	preserves the sign (-i gets mapped to -(exportVar(i))),	 */
/*	and gets rid of "gaps" in variable numbers.		 */
/*	exportLit(0) returns the actual number of		 */
/*	variables that occurred--i.e., the max possible return	 */
/*	value for exportLit(i).					 */
/*								 */
/*								 */
/* The main output is the global struct named cnf, given below.  */
/* format of the cnf.clauseSpace array is:			 */
/*	0, len1, L11, ..., L1_len1, len2, L21, ..., L2_len2, ... */
/* That is, each clause begins with its NUMBER of literals, then */
/* follows with the actual literals.				 */
/* Note that position 0 is unused.				 */
/* Example: input 5 -7 0 -5 0 -7 8 -3 0 becomes the array	 */
/* 0, 2, 5, -7, 1, -5, 3, -7, 8, -3.				 */
/*								 */
/* Array cnf.offsets tells where each clause is located as an    */
/* offset into cnf.clauseSpace.  E.g., the example turns out as: */
/* 0, 1, 4, 6.							 */
/* Again, note that clause number 0 is unused.			 */
/* Programming note: clause i can be easily accessed with	 */
/*	long* aClause = cnf.clauseSpace + cnf.offsets[i];	 */
/*	long  nlits   = aClause[0];				 */
/*	long  lit1    = aClause[1];				 */
/*	long  litLast = aClause[nlits];				 */
/* etc.								 */
/*								 */
/* Other outputs are counts of 1-, 2-, ..., 6-clauses, and	 */
/* count of clauses longer than 6, plus				 */
/* length of the longest clause, number of declared variables,   */
/* number of declared clauses, and number of literal occurrences.*/
/*								 */
/* Yumi K. Tsuji and Allen Van Gelder, UC Santa Cruz, 930612     */
/*								 */
/*****************************************************************/

/*********************************************************************/
/*								     */
/* Input format:  dimacs challenge cnf format			     */
/* Preamble gives comments and (required) "problem line".	     */
/* A CNF formula as a series of clauses:			     */
/*								     */
/*   "formula variable"s must be positive integers in the range 1-n, */
/*   where "n" was specified on the "problem line".		     */
/*   Example: (by Mike Trick, 'c' and 'p' are in column 1)	     */
/*		c Sample CNF format				     */
/*		c						     */
/*		p cnf 4 3					     */
/*		1 3 -4 0					     */
/*		4 0  2 3 0					     */
/*								     */
/*********************************************************************/

  
/* Storage for cnf is global in cnfparse.y.
   Say "extern struct cnfStruct cnf; struct cnfStruct *cnfPtr = &cnf;"
   to access it with either "cnf.numVars" or "cnfPtr->numVars". */

/* Storage for clauseSpace[] and offsets[] are obtained from malloc()
   during loadCnf() and may be released with free() if you copy cnf to
   your own preferred structure. */

int	loadCnf(FILE* inputFile, FILE* echo_comment);
long	exportLit(long internalLit);

extern struct cnfStruct {
	long	numVars;
	long	numClauses;
	long	numLitOccs;
	long	longest;
	long	count1;
	long	count2;
	long	count3;
	long	count4;
	long	count5;
	long	count6;	
	long	countLong;
	long	allocSpaces;
	long*	clauseSpace;
	long*	offsets;
	long*	export_num;
	}
	cnf;

