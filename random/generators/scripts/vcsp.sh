#!/bin/sh

# usage: vcsp.sh n d c(%) t(%) nbinstances

AWKPATH=../translators
n=1
while expr $n \<= $5 ; do
  ./random_vcsp vcsp$1_$2_$3_$4_$n.ds $1 $2 $3 $4 1 valued 0 25 1 25 10 25 100 25 1000
  awk -f ../translators/ds2cpi.awk vcsp$1_$2_$3_$4_$n.ds | awk -f ../translators/cp2wcsp.awk > vcsp$1_$2_$3_$4_$n.wcsp
  n=`expr $n + 1`
done
