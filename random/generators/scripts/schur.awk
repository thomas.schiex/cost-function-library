BEGIN {
  N = 140;
  B = 5;

  print "SCHUR_" N "_" B, 1;
  for (i=1;i<=N;i++) {
	printf("X%d", i);
	for (v=1;v<=B;v++) {
	  printf(" %d", v);
	}
	print "";
  }

  for (i=1;i<=N;i++) {
	for (j=i+1;j<=N;j++) {
	  for (k=j+1;k<=N;k++) {
		if (i+j == k) {
		  print "hard(!(X" i "==X" j " && X" i "==X" k " && X" j "==X" k "))";
#		  print "hard(alldiff(X" i ",X" j ",X" k "))";
		}
	  }
	}
  }
  exit;
}
