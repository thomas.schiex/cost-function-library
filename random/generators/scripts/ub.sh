#!/bin/sh

# usage: ub.sh WCSP/benchs/maxcsp/sparseloose
 
# script file with one argument (a directory)
# for executing toolbar on every problem in wcsp format
# in order to store optimum (or good upper bounds) in *.ub files

timelimit=1

echo "file upperbound"
for e in $1/*.wcsp ; do
  dir=`dirname $e`
  base=`basename $e .wcsp`  
  ub="${dir}/${base}.ub" 
  if [[ -e $ub ]] ; then
    initub=`cat $ub`
    if (($initub < 2000000000)) ; then
      nothing=0
#    echo "$e $initub $initub"
    else
      initub=`incop.sh $e`
      printf "$e $initub "
      toolbar -t${timelimit} -u${initub} $e | awk 'BEGIN{ub='${initub}'} /^Optimum/{ub=$2} /^Best bound/{if ($3 < ub) ub=$3} /^Lower bound/{ub=$3} END{print ub}' | tee $ub
    fi
  else
    initub=`incop.sh $e`
    printf "$e $initub "
    toolbar -t${timelimit} -u${initub} $e | awk 'BEGIN{ub='${initub}'} /^Optimum/{ub=$2} /^Best bound/{if ($3 < ub) ub=$3} /^Lower bound/{ub=$3} END{print ub}' | tee $ub
  fi
done
