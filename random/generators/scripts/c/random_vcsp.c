/* Automatic generation of Random Binary Valued CSP */
/* S. de Givry @ 1995 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define TOP 2000000

static int premiere_fois = 0;

/* tableaux contenant la distribution des couts */
static int prioritemax =1;
static int cout[1024];
static int pccout[1024];
static int nbcontr[1024];
static int nbcontr100[1024];

#define endl fputs("\n",ptfile)

/* +++++++++++++++++++++++++ */
/* random(maxi) |-> [0,maxi[ */
/* +++++++++++++++++++++++++ */
int randomax (maxi)
int maxi;
{ long heure;

 /* Le code qui suit initialise le generateur de nombre aleatoire  a une valeur
    precise pour obtenir la meme suite de nombres aleatoires si DEMO est 
    presente dans l'environnement d'appel a` l'execution. Cependant, cette
    liste de nombre est identique pour chaque creation de processus, ce qui
    donne un caractere tres repetitifs aux taches. Ainsi, en ne definissant
    pas DEMO, on initialise le randm avec une valeur pseudo-aleatoire qui
    est l'heure calendaire multipliee par le pid du processus .
  */
  if (!premiere_fois) {
    premiere_fois = 1;
    if (!getenv("DEMO")) {
      heure = (long) time(NULL);
      srand48((unsigned) heure * getpid());
    }
  }

  return lrand48()%maxi;
}

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* cree un csp binaire aleatoire dans un fichier texte (a l'exception du titre) */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
void make_rnd_vcsp(ptfile,n,k,d,t,valued)
     FILE* ptfile;
     int n,k,d,t,valued;
{
  int *edges; /* contient toute les contraintes possibles */
  int *good; /* contient tout les couples permis */
  int nbedges,nbgood;
  int i,j,a,b;
  int pos,posedge,numedge;
  int numgood,posgood;
  int totaledges,totalgood;
  char nom[100];
  int val;
  int priorite = 0;
  int seuilprio = 0;

  /* reservation de memoire dynamique pour edges et good */
  if (!(edges = (int *) malloc(n*n*sizeof(int)))) {
    fprintf(stderr,"pas assez de memoire !");
    exit (1);
  }
  if (!(good = (int *) malloc(k*k*sizeof(int)))) {
    fprintf(stderr,"pas assez de memoire !");
    exit (1);
  }

  /* ecrit la declaration des variables et de leur domaine */
  for (i=0; i<n; i++) {
    sprintf(nom,"X%d",i);
    fputs(nom,ptfile);
    for (a=0; a<k; a++) {
      sprintf(nom," %d",a);
      fputs(nom,ptfile);
    }
    endl;
  }    

  /* initialise le triangle strictement superieur de la matrice a 0 */
  for (i=0; i<n; i++) {
    for (j=i+1; j<n; j++) {
      *(edges+i*n+j)=0;
    }
  }

  nbedges = n*(n-1)/2;
  /* calcul du nombre de contraintes (avec un arrondi aleatoire) */
  totaledges = d*nbedges/100; 
  totaledges += (d<100 && d>0 && (d*nbedges)%100>=50)?1:0;
  /* printf("ttedges=%d\n",totaledges); */
  /* cree toutes les contraintes */
  for (numedge=0; numedge<totaledges; numedge++) {
    posedge = randomax(nbedges); /* choix d'une arete */
    pos = 0; /* recherche de son emplacement dans la matrice */
    j=0;
    for (i=0; i<n; i++) {
      for (j=i+1; j<n; j++) {
	if (!*(edges+i*n+j)) {
	  if (pos==posedge) {
	    break;
	  }
	  pos++;
	}
      }
      if (j<n && pos==posedge && !*(edges+i*n+j)) {
	break;
      }
    }
    
    if (j>=n || i>=n || !(pos==posedge) || *(edges+i*n+j)) {
      fprintf(stderr,"Erreur dans la matrice edges.");
      exit (1);
    }
    /* mise a jour */
    nbedges--;
    *(edges+i*n+j)=1;

    /* ecrit l'entete de la contrainte */
    if (valued) {
      if (numedge-seuilprio >= nbcontr100[priorite]) {
	if (priorite<prioritemax-1) {
	  priorite++;
	  seuilprio = numedge;
	} else {
	  fprintf(stderr,"erreur dans l'algo !!");
	  exit(0);
	}
      }
      val = cout[priorite];
      sprintf(nom,"#\nC%d %d extension X%d X%d\n",numedge,val,i,j);
    }
    else {
      sprintf(nom,"#\nC%d extension X%d X%d\n",numedge,i,j);
    }

    fputs(nom,ptfile);

    /* initialise la matrice des couples permis */
    for (a=0; a<k; a++) {
      for (b=0; b<k; b++) {
	*(good+a*k+b) = 0;
      }
    }
    
    nbgood = k*k;
    /* calcul du nombre de contraintes (avec un arrondi aleatoire) */
    totalgood = (100-t)*nbgood/100; 
    totalgood += (t<100 && t>0 && ((100-t)*nbgood)%100>=50)?1:0;
    /* printf("ttgood=%d\n",totalgood); */
    /* cree tous les couples permis */
    for (numgood=0; numgood<totalgood; numgood++) {
      posgood = randomax(nbgood); /* choix d'un couple permis */
      pos = 0; /* recherche de son emplacement dans la matrice */
      b=0;
      for (a=0; a<k; a++) {
	for (b=0; b<k; b++) {
	  if (!*(good+a*k+b)) {
	    if (pos==posgood) {
	      break;
	    }
	    pos++;
	  }
	}
	if (b<k && pos==posgood && !*(good+a*k+b)) {
	  break;
	}
      }
      if (a>=k || !(pos==posgood) || *(good+a*k+b)) {
	fprintf(stderr,"Erreur dans la matrice good.");
	exit (1);
      }
      /* mise a jour */
      nbgood--;
      *(good+a*k+b) = 1;

      /* ecrit le couple permis */
      sprintf(nom,"%d %d\n",a,b);
      fputs(nom,ptfile);
    }
  } 
  /* libere la memoire dynamique */
  free(edges);
  free(good);
}

/* +++++++++++++++++++++++++++++++++ */
/*  cree les instances des problemes */
/* +++++++++++++++++++++++++++++++++ */
void cree_instances(ptfile, nbprob, nbconstr, nbconstrtot)
     FILE* ptfile;
     int nbprob, nbconstr, nbconstrtot;
{
  int numprob;
  int *reserve;
  int efface,elem;
  int reste,place,recherche;
  int seuil = 0;
  int priorite;

  if (!(reserve=(int *)calloc(nbconstrtot,sizeof(int)))) {
    fprintf(stderr,"erreur reservation memoire!");
    exit(0);
  }

  /* generation des problemes */
  for (numprob=0; numprob<nbprob; numprob++) {
    /* genere l'entete */
    fprintf(ptfile,"%% %d",numprob);

    seuil = 0;

    for (priorite=0; priorite<prioritemax; priorite++) {

      nbconstr = nbcontr[priorite];
      nbconstrtot = nbcontr100[priorite];

      /* initialisation de la structure noncyclique */
      for (efface=0; efface<nbconstrtot; efface++) {
	reserve[efface] = 0;
      }
      reste = nbconstrtot;
      
      
      /* cree la suite */
      for (elem=0; elem<nbconstr; elem++) {
	place = randomax(reste);
	recherche=0;
	while (!(place==0 && reserve[recherche]==0)) {
	  if (reserve[recherche]!=0) {
	    recherche++;
	  }
	  else {
	    if (place!=0) {
	      place--;
	      recherche++;
	    }
	  }
	}
	fprintf(ptfile," C%d",recherche+seuil);
	reserve[recherche] = 1;
	reste--;
      }

      seuil += nbconstrtot;
    }
    /* genere la fin */
    fprintf(ptfile,"\n");
  }
  /* puts("Problemes crees."); */
  free(reserve);
}

/* =================== */
/* programme principal */
/* =================== */
int main(argc, argv)
int argc;
char* argv[];
{
  FILE* ptfile;
  int valued=0;
  char titre[255];
  int i;
  int infty = TOP;
  int n,d,c,t;
  int nbtotalcontr,nbtotalcontr100;
  int sommecontr,sommecontr100;
  int priorite;

  if (argc < 9) {
    puts("Command: genrndbvcsp name n d c t nbinst type pchard [% cost]*");
    puts("   name = name of csp and csp's file or else stdout");
    puts("   n = number of variables");
    puts("   d = size of domains");
    puts("   c = connectivity of the constraint graph");
    puts("   t = tightness of constraints");
    puts("   nbinst = number of instances to create");
    puts("   type = valued or classic");
    puts("   pchard = pourcentage of hard constraints if type = valued");
    puts("   [% cout]* : list of percent (%) distribution of valued constraints (cost)");
    puts("Example: genrndbvcsp VCSP_20_10_50_60_12 20 10 50 60 100 valued 20 20 1000 20 100 20 10 20 1");
    puts("Caution! The sum of all percents (hard and soft constraints) must be equal to 100 %.");
  }
  else {
    n = atoi(argv[2]);
    d = atoi(argv[3]);
    c = atoi(argv[4]);
    t = atoi(argv[5]);
    nbtotalcontr = n * (n-1) /2 ;
    nbtotalcontr100 = nbtotalcontr;
    nbtotalcontr = c * nbtotalcontr / 100;
    /* printf("%d, %d, %d..\n",c,nbtotalcontr,(c*nbtotalcontr100)%100); */
    nbtotalcontr += (c<100 && c>0 && (((c*nbtotalcontr100)%100) >= 50))?1:0;
    sommecontr = 0;
    sommecontr100 = 0;

    /* test si ce sera un VCSP ou CSP */
    valued = 0;
    if (strstr(argv[7],"valued") != NULL) {
      valued = 1;
      infty = 1;
      /* lecture de la distribution des couts */
      cout[0] = 1;
      pccout[0] = atoi(argv[8]);      
      nbcontr100[0] = pccout[0] * nbtotalcontr100 / 100;
      sommecontr100 += nbcontr100[0];
      nbcontr[0] = pccout[0] * nbtotalcontr / 100;
      sommecontr += nbcontr[0];
      prioritemax = 1;
      for (i=9; i<argc; i+=2) {
	cout[prioritemax] = atoi(argv[i+1]);
	pccout[prioritemax] = atoi(argv[i]);
	nbcontr[prioritemax] = pccout[prioritemax] * nbtotalcontr / 100; 
	sommecontr += nbcontr[prioritemax];
	nbcontr100[prioritemax] = pccout[prioritemax] * nbtotalcontr100 / 100; 
	sommecontr100 += nbcontr100[prioritemax];
	infty += nbcontr[prioritemax] * cout[prioritemax];
	prioritemax++;
      }
      nbcontr[prioritemax-1] += nbtotalcontr - sommecontr; 
      infty += (nbtotalcontr - sommecontr) * cout[prioritemax-1];
      nbcontr100[prioritemax-1] += nbtotalcontr100 - sommecontr100; 
      /* mise a jou de l'infini */
      cout[0] = infty;
    } else {
      cout[0] = 0;
      pccout[0] = 100;
      nbcontr[0] = nbtotalcontr; 
      nbcontr100[0] = nbtotalcontr100; 
      prioritemax = 1;
    }

    if (strcmp(argv[1],"stdout") != 0) {
      /* ouverture du fichier en ecriture */
      if (!(ptfile=fopen(argv[1],"w"))) {
	fprintf(stderr,"erreur a l'ouverture du fichier !!!");
      }
      /* ecriture du titre du (V)CSP */
      strcpy(titre,argv[1]);
      strcat(titre," ");
      strcat(titre,argv[7]);
      fputs(titre,ptfile);endl;
      /* cree les variables, les domaines et les contraintes */
      make_rnd_vcsp(ptfile,n,d,100,t,valued);
      /* cree les instances */
      cree_instances(ptfile,atoi(argv[6]),nbtotalcontr,nbtotalcontr100);
      fclose(ptfile);
    }
    else {
      /* ATTENTION! ne s'occupe pas d'ecrire le titre du (V)CSP */
      /* cree les variables, les domaines et les contraintes */
      make_rnd_vcsp(stdout,n,d,100,t,valued);
      /* cree les instances */
      cree_instances(stdout,atoi(argv[6]),nbtotalcontr,nbtotalcontr100);
    }

    fprintf(stderr, "Infinity = %d\n", infty);
  }

#ifdef DEBUG
  printf("%d, %d\n",nbtotalcontr,nbtotalcontr100);
  for (priorite=0; priorite<prioritemax; priorite++) {
    printf("%d : cout=%d, nbcontr=%d, nbcontr100=%d\n",priorite,cout[priorite],nbcontr[priorite],nbcontr100[priorite]);
  }
#endif
  
    
  /* puts("Fini !"); */
  return 0;
}
