/* This is a n-queens problem where some cells are penalized. The goal
is to find a solution to the n-queens problems such that the number of
queens  in  penalized  cells  is  minimal.  The  program  takes  three
paramaters: n the size of the problem, e the number of penalized cells
per row, s the seed. The output is in wcsp format. */

#include <stdio.h>

int abs(int i){if(i<0) return -1*i; else return i;}

int main(int argc, char *argv[])
{
  int n,e,i,j,k,l,r,s,pos;
  int *T;

  if(argc == 1)
    {
      printf("generates random weighted n-queens problems\n");
      printf("of size n, with e randomly chosen penalized cells\n");
      printf("per row, with seed s\n");
      printf("USAGE: wqueens n e s");
      printf("OUTPUT in .wcsp format\n");
      return 0;
    }
  if(argc != 4){printf("error arguments %i\n",argc); return 0;}
  n=atoi(argv[1]);
  e=atoi(argv[2]);
  s=atoi(argv[3]);
  
  printf("WQUEENS-");
  printf("%i",n);
  printf("-");
  printf("%i",e);
  printf("-");
  printf("%i %i %i %i %i\n",s,n,n,((n*(n-1))/2)+n, n+1);

  for(i=0;i<n;i++)printf("%i ",n);
  printf("\n");


  for(i=0;i<n;i++)for(j=i+1;j<n;j++)
    {
      r=0;
      for(k=0;k<n;k++)for(l=0;l<n;l++)
	if(k==l || abs(i-j)==abs(k-l)) r++;
     
      printf("2 %i %i 0 %i\n",i,j,r);
      for(k=0;k<n;k++)for(l=0;l<n;l++)
	if(k==l || abs(i-j)==abs(k-l)) 
	  printf("%i %i %i\n",k,l,n+1);
    }

  if(e<=0 || e>n) return 1;

  srand(s);
  T= (int *) malloc(sizeof(int) * n);
  for(i=0;i<n;i++)
    {
      for(k=0;k<n;k++)T[k]=0;
      j=0;
      while(j<e)
	{
	  pos=rand()%(n-j);
	  j++;
	  for(k=0;k<n;k++)
	    if(T[k]==0)
	      {
		if(pos==0) {T[k]=1; k=n;}
		else pos--;
	      }
	}
      printf("1 %i 0 %i\n",i,e);
      for(k=0;k<n;k++)if(T[k]==1)printf("%i 1\n",k);
    }
  free(T);
  return 0;
}
