/* generates connected random binary CSPs to be solved as Max-CSP 
   in .wcsp format. */


#include <stdio.h>

int ****T;
int **G;

int *V;
int cnt;

void see_G(n)
{
  int i,j;
  for(i=0;i<n;i++)
    {
      printf("%i: ",i);
      for(j=0;j<n;j++)if(G[i][j]==1)printf("%i ",j);
      printf("\n");
    }
}

void unconnected(int n, int v, int *cnt)
{
  int i;

  V[v]=1;
  (*cnt)++;
  for(i=0;i<n; i++)if(G[v][i]==1 && V[i]==0)unconnected(n,i,cnt);
}

void generate_constraint(int i, int j, int d, int n_nogoods)
{
 int k,l,pares_posibles,dado;

 G[i][j]=1; G[j][i]=1;

 pares_posibles=d*d; /*number of nogoods candidates */

 while(n_nogoods>0)
   {
     dado=rand()%pares_posibles;
     for(k=0;k<d;k++)for(l=0;l<d;l++)
       if(T[i][j][k][l]==0)
	 {
	   if(dado==0)
	     {
	       T[i][j][k][l]=1;
	       T[j][i][l][k]=1;
	       n_nogoods--;
	       pares_posibles--;
	       k=d;l=d;
	     }
	   dado--;
	 }
   }
}





int main(int argc, char *argv[])
{
  int n,d,e,t,s;
  int i,j,k,l,e2,pares_posibles,dado;

  /* reading the arguments */

  if(argc == 1)
    {
      printf("generates a connected binary random CSP\n");
      printf("with n variables, e constraints having t nogoods each,");
      printf("with seed s\n");
      printf("\nUSAGE: maxCSP n d e t s\n");
      printf("OUTPUT in .wcsp format\n");
      return 0;
    }
  if(argc != 6){printf("error arguments %i\n",argc); return 0;}
  n=atoi(argv[1]);
  d=atoi(argv[2]);
  e=atoi(argv[3]);
  t=atoi(argv[4]);
  s=atoi(argv[5]);
  
  if(e<n || e>((n*(n-1))/2) || t>(d*d))
    {printf("error arguments %i\n",argc); return 0;}

  /* Memory allocation */

  V=(int *) malloc(sizeof(int) * n);
  
  G= (int **) malloc(sizeof(int *) * n);
  for(i=0;i<n;i++) G[i]= (int *) malloc(sizeof(int) * n);
  
  
  T= (int ****) malloc(sizeof(int ***) * n);
  for(i=0;i<n;i++) T[i]= (int ***) malloc(sizeof(int **) * n);
  for(i=0;i<n;i++)for(j=0;j<n;j++)
    T[i][j]= (int **) malloc(sizeof(int *) * d);
  for(i=0;i<n;i++)for(j=0;j<n;j++)
    for(k=0;k<d;k++)
      T[i][j][k]= (int *) malloc(sizeof(int) * d);

  /* Problem generation */
  
  srand(s);

  do {
    for(i=0;i<n;i++)V[i]=0;

    for(i=0;i<n;i++)for(j=0;j<n;j++)G[i][j]=0;
    
    for(i=0;i<n;i++)for(j=0;j<n;j++)
      for(k=0;k<d;k++)for(l=0;l<d;l++)
	T[i][j][k][l]=0;
    
    pares_posibles=(n*(n-1))/2;
    e2=e;
    while(e2>0)
      {
	dado=rand()%pares_posibles;
	for(i=0;i<n;i++)for(j=i+1;j<n;j++)if(G[i][j]==0)
	  {
	    if(dado==0)
	      {
		generate_constraint(i,j,d,t);
		pares_posibles--;
		e2--;
		i=n;j=n;
	      }
	    dado--;
	  }
      }
    cnt=0;
    unconnected(n,0,&cnt);
  } while (cnt!=n);

  /* Writing out the problem in wcsp format */

  printf("MAXCSP-");
  printf("%i",n);
  printf("-");
  printf("%i",d);
  printf("-");
  printf("%i",e);
  printf("-");
  printf("%i",t);
  printf("-");
  printf("%i %i %i %i %i\n",s,n,d,e,e+1);
  
  for(i=0;i<n;i++)printf("%i ",d);
  printf("\n");



  for(i=0;i<n;i++)for(j=i+1;j<n;j++)if(G[i][j]==1)
    {
      printf("2 %i %i 0 %i\n",i,j,t);
      for(k=0;k<d;k++)for(l=0;l<d;l++)
	if(T[i][j][k][l]==1) printf("%i %i 1\n",k,l);
    }

  /* deallocating dynamic memory */

  for(i=0;i<n;i++)for(j=0;j<n;j++)for(k=0;k<d;k++)
    free(T[i][j][k]);
  for(i=0;i<n;i++)for(j=0;j<n;j++) free(T[i][j]);
  for(i=0;i<n;i++) free(T[i]);
  free(T);
  
  for(i=0;i<n;i++) free(G[i]);
  free(G);

  free(V);
  return 1;
}




