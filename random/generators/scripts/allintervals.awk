## Soft AllInterval series generator
## see CSPLib problem 007

## usage:
## awk -f allintervals.awk > allintervals.cp
## gawk -f cp2wcsp.awk allintervals.cp > allintervals.wcsp

BEGIN {
  N = 20; ## problem size (number of x_i variables)
  U = 5; ## generate random unary costs in [0,U[
  B = 1; ## default cost for violation of soft alldifferent (each pair of equal variables adds this cost to the total cost)

# problem name
  print "AllIntervals" N;
# x_i variables
  for (i= 1; i<=N ; i++) {
	printf("x%d", i);
	for (d= 0; d<=N-1 ; d++) {
	  printf(" %d", d);
	}
	print "";
  }
# d_i variables
  for (i= 1; i<=N-1 ; i++) {
	printf("d%d", i);
	for (d= 1; d<=N-1 ; d++) {
	  printf(" %d", d);
	}
	print "";
  }
# d_i = |x_i - x_{i+1}|
  for (i= 1; i<=N-1 ; i++) {
	print "hard(d" i " == abs(x" i " - x" i+1 "))";
  }
# random unary costs
  for (i= 1; i<=N ; i++) {
	print "x" i, 0;
	for (d= 0; d<=N-1 ; d++) {
	  print d,int(U * rand());
	}
  }
# soft alldifferent on x_i variables using binary cost functions
  for (i= 1; i<=N ; i++) {
	for (j= i+1; j<=N ; j++) {
	  print "soft(" B ", x" i " != x" j ")";
	}
  }
# soft alldifferent on d_i variables using binary cost functions
  for (i= 1; i<=N-1 ; i++) {
	for (j= i+1; j<=N-1 ; j++) {
	  print "soft(" B ", d" i " != d" j ")";
	}
  }
}
