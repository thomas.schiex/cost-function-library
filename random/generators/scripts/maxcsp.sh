#!/bin/sh

# usage: maxcsp.sh n d c(%) t(%) nbinstances

AWKPATH=../translators
n=1
while expr $n \<= $5 ; do
  ./random_vcsp vcsp$1_$2_$3_$4_$n.ds $1 $2 $3 $4 1 valued 0 100 1
  awk -f ../translators/ds2cpi.awk vcsp$1_$2_$3_$4_$n.ds | awk -f ../translators/cp2wcsp.awk > vcsp$1_$2_$3_$4_$n.wcsp
  n=`expr $n + 1`
done
