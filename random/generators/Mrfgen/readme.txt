Notebook for random pairwise MRF generation.

Can generate two kind of instances:

-Dense random instances: pairwise potentials are sampled uni-
formly from [0, s]. The unary potentials are
sampled uniformly from [0, 1] using fixed precision num-
bers with a choosen digits precision.

-Erdos-Renyi instances: egdes are sampled with probability p.
Pairwise and unary potentials are then sampled like the random
instances for each edges within the graph. 
