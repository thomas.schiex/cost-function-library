BEGIN {
  nb = 0;
  sum = 0;
}

FNR == NR && FNR == 1 {
  nbvar = $1;
  nbclause = $2;
}

FNR == NR && FNR > 1 {
  nb++;
  w[nb] = $2;
  sum += $2;
}

FNR != NR && FNR == 1 {
  printf("# PBType : GE\n");
  printf("# PBGoal : %d\n", $1 + 1);
  printf("# PBObj : MAX\n");
  printf("# NumCoef : %d\n", nb);

  for (i=1; i<=nb; i++) {
    printf("v%d c%d\n", i+nbvar, w[i]);
  }
}
