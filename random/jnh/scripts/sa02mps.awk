function format(s1,s2,s3,s4) 
{
  printf(" %-2s %-8s  %-8s  %12s\n", s1,s2,s3,s4);
  return;
}

BEGIN {
    verbose = 0;
 
    ok = 0;
}

ok {
  nb++;

  if (($1 + 2) != NF) {
    if (verbose) print "*** ERROR: current line does contain enough literals";
  }

  rhs[nb] = 0;
  cost[nb] = $2;
  for (i=3; i<=NF; i++) {
    if ($i < 0) {
	defined[-$i] = 1;
	matrix[-$i,nb]--; 
	if (matrix[-$i,nb] == 0) {
	    if (verbose) print "*** WARNING: " nb " is a tautologie due to " $i "!";
	}
	rhs[nb]--;
    } else {
	defined[$i] = 1;
	matrix[$i,nb]++;
	if (matrix[$i,nb] == 0) {
	    if (verbose) print "*** WARNING: " nb " is a tautologie due to " $i "!";
	}
    }
  }
}


NR == 1 {
  var = $1;
  nb = $2;
  ok = 1;

  print "NAME          " FILENAME;

  print "ROWS";
  print " N  obj";
  for (i=1; i<=nb; i++) {
    print " G  c" i;
  }
  
  nbcopy = nb;
  nb = 0;
}

END {
  if (nb != nbcopy) {
    if (verbose) print "*** ERROR: read not enough clauses!!!",nb,nbcopy;
  }

  print "COLUMNS";
  for (i=1; i<=var; i++) {
    for (j=1; j<=nb; j++) {
      if (matrix[i,j] != 0) {
	format("", "y" i, "c" j, matrix[i,j]);
      }
    }
  }
  for (i=1; i<=nb; i++) {
    format("", "z" i, "c" i, -1);
    format("", "z" i, "obj", cost[i]);
  }   

  print "RHS";
  for (i=1; i<=nb; i++) {
    format("", "RHS", "c" i, rhs[i]);
  }

  print "BOUNDS";
  for (i=1; i<=var; i++) {
      if (defined[i] == 1) {
	  format("BV", "BND", "y" i);
      } else {
	  if (verbose) print "*** WARNING! Variable " i " not used!";
      }
  }
  for (i=1; i<=nb; i++) {
    format("UP", "BND", "z" i, 1);
  }

  print "ENDATA";
}
