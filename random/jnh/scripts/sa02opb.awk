BEGIN {
  nb = 0;
}

FNR == NR && FNR > 1 {
  nb++;
  w[nb] = $2;
}

FNR != NR && FNR == 1 {
  printf("max: ");
  for (i=1; i<nb; i++) {
    printf("%d * z%d + ", w[i], i);
  }
  printf("%d * z%d\n", w[nb], nb);
  nb = 0;
}

FNR != NR && FNR > 1 {
  nb++;
  for (i=3; i<=NF; i++) {
    if ($i < 0) printf("~y%d", -$i);
    else printf("y%d", $i);
    printf(" + ");
  }
  printf("~z%d >= 1\n", nb);
}
