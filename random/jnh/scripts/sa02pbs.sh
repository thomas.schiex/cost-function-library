
for e in *.sa0 ; do 
  f=`basename $e .sa0`
  echo "$f.pbs ..."
  nawk -f sat2pbs.awk $e $e > $f.pbs
  nawk -f sat2pbseq.awk $e $e > $f.pbseq
  nawk -f sat2pbspb.awk $e $f.lb > $f.pbs.pb
  ln -fs $f.pbs.pb $f.pbseq.pb
done
