
# translate sat format to sa0 format
# in sa0 format, each clause is located on a single line of text

for e in *.sat ; do 
  f=`basename $e .sat`
  echo $f.sa0
  nawk -f sat2sa0.awk $e > $f.sa0
done
