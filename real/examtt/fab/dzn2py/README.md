# dzn2py tool (dzn2py.sh and dzn2py_control.py)

# Desc

  The dzn2py.sh script makes .py.xz data file from .dzn.xz data file. Then
  it controls the .py produced file, by calling the dzn2py_control.py code.

  The resulting .py data file is ready to be imported by
  (exam_tt_main.py + exam_tt_pb.py) code.

- Produced data files :

  data/InstancesDZN/*.dzn.xz ----dzn2py.sh----> instances/python/*.py.xz

# Example 

- command :

      ./dzn2py.sh D1-3-18.dzn.xz

- result :

  ./D1-3-18.py.xz file produced from ../../data/InstancesDZN/D1-3-18.dzn.xz file

  The D1-3-18.py data file is ready to be imported by 
  (exam_tt_main.py + exam_tt_pb.py) code.

