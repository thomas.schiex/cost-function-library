#!/bin/bash

# $1=foo.dzn.xz -> foo.py.xz

fdzn=`echo $1|sed 's/.xz//'`
fpy=`echo $fdzn.py|sed 's/.dzn//'`

printf "Generates $fdzn.xz -> $fpy.xz ...\n" 

cp ../../data/InstancesDZN/$1 . 
unxz ./$1 ; # => $fdzn

sed 's/\[|/[\[/' $fdzn > ./tmp1
sed 's/|\]/]\]/' ./tmp1 > ./tmp2
sed 's/|/],\[/' ./tmp2 > ./$fpy
xz -z ./$fpy
rm -f ./$fdzn ./tmp1 ./tmp2

printf "Controls ...\n"

cp ./$fpy.xz ./data.py.xz
unxz ./data.py.xz
python dzn2py_control.py # (data.py file controled)
rm -f data.py data.pyc ;

printf "End.\n"

