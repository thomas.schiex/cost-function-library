# Examination Timetabling

# Ref

Local Search and Constraint Programming for a Real-World Examination Timetabling Problem
Michele Battistutta, Sara Ceschia, Fabio De Cesco, Luca Di Gaspero, Andrea Schaerf, and Elena Topan
In Proc. of CP-AI-OR, Vienna, Austria, September 2020.

Data from:
https://bitbucket.org/satt/examtimetablinguniuddata/src/master/

# Desc

- Existing minizinc : exam_tt.mzn and .dzn data file

  - exam_tt.mzn :
    
    At https://bitbucket.org/satt/examtimetablinguniuddata/src/master/

  - .dzn data files : ./data/InstancesDZN/*.dzn.xz
    
    From https://bitbucket.org/satt/examtimetablinguniuddata/src/master/
    under CPAIOR2020/InstancesDZN

  - .json description files : ./data/InstancesJSON/*.json

    From https://bitbucket.org/satt/examtimetablinguniuddata/src/master/
    under CPAIOR2020/InstancesJSON

- toulbar2 : exam_tt_main.py, exam_tt_pb.py and data.py

  - ./scripts/exam_tt_main.py, exam_tt_pb.py

  - data.py : ./instances/python/*.py.xz

    Generated from ./*.dzn.xz by using ./fab/dzn2py tool

