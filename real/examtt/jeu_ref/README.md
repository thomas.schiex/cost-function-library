# JEU toybis

# Desc

The toybis dataset is a reference instance that can be used to compare
behaviors of different codes of the problem (regression testing...).
'toybis' is a 'variant' of 'toy' modified in order to have a single solution.

The expected single solution is :

      1 6 5 7 6 6 11 7 14 0 19 1 10 5 16 5 12 2 13 4 16 3 17 3 8 7

      Cost : 6

# Content :

- data files for exam_tt_main.py, exam_tt_pb.py :

  - toybis.py (copy toybis.py into scripts as data.py) 

  - toybisJSON.py (copy toybisJSON.py into scripts as dataJSON.py) 

- wcsp data file : toybis.wcsp.xz

- output folder : the solution given in different formats (as returned by
  'python3 exam_tt_main.py') :

  - as_tb2-w_file.txt (to be compared to toulbar2 output).

  - JEU_brut_result.txt

  - JEU_result_as_pr.txt

- toybis.dzn : .dzn data file as expected by minizinc (for exam_tt.mzn)

