# Examination Timetabling model with toulbar2

# Content 

- toulbar2 problem : exam_tt_main.py, exam_tt_pb.py

- required : data.py 'from' ../instances/python/*.py.xz

- for results restitution (not mandatory) :

  - tool : result.py

  - dataJSON.py 'from' ../data/InstancesJSON/*.json

# Commands :
  
      JEU=toy

      echo "$JEU" > JEU.txt

      # copy data.py
      cp ../instances/python/${JEU}.py.xz data.py.xz
      unxz data.py.xz

      # build dataJSON.py (not mandatory)
      echo "dataJSON = \\" > ./dataJSON.py
      cat ../data/InstancesJSON/${JEU}.json >> ./dataJSON.py
      sed -i 's/false/False/g' ./dataJSON.py
      sed -i 's/true/True/g' ./dataJSON.py

      python3 exam_tt_main.py

