
  #########################################################################
  #
  #    measure.py was used by exam_tt.py (that has become obsolete)
  #
  #########################################################################


exam_tt.py becomes exam_tt_main.py+exam_tt_pb.py ; measure.py not used ; result.py for results restitution ; correction for scope [SamePeriod[e1][e2], EventPeriod[e1], EventPeriod[e2]]

import time

def timer_init() :
    return (time.time(), None) # (start, end)
    
def timer_stop(timer) :
    (start, end) = timer
    return (start, time.time()) # (start, end)
    
def timer_duration(timer) :
    (start, end) = timer
    return (end - start) # duration

