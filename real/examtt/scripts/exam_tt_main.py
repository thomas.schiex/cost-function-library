# Examination Timetabling problem

import sys
from pytoulbar2 import CFN
from exam_tt_pb import build_exam_tt_problem

result_module_available = True
try:
    from result import brut_result, pr_result, extract_results
except:
    result_module_available = False

OPTION_VERBOSE = True
OPTION_VERBOSE_PLUS = True
OPTION_VERBOSE_PLUS_PLUS = False

from data import * # Events, Periods, Rooms, RoomedEvent, Conflicts,
                   # DistanceWeight, MinDistance, MaxDistance, Precedence,
                   # EventPeriodConstraints, EventRoomConstraints,
                   # RoomPeriodConstraints, RoomsetOverlap

if OPTION_VERBOSE_PLUS_PLUS :
    print("--------- DATA ---------")
    print("Events=", Events, " ; ",
          "Periods=", Periods, " ; ", "Rooms=", Rooms)
    print("RoomedEvent=", RoomedEvent)
    print("Conflicts=", Conflicts)
    print("DistanceWeight=", DistanceWeight)
    print("MinDistance=", MinDistance)
    print("MaxDistance=", MaxDistance)
    print("Precedence=", Precedence)
    print("EventPeriodConstraints=", EventPeriodConstraints)
    print("EventRoomConstraints=", EventRoomConstraints)
    print("RoomPeriodConstraints=", RoomPeriodConstraints)
    print("RoomsetOverlap=", RoomsetOverlap)
    print("------------------------")

Problem = None

# Build Problem

if OPTION_VERBOSE :
    print("\nBuild Problem ...")

Problem = build_exam_tt_problem(MAXCOST_VALUE=10000)

# Save Problem

if True :

    if OPTION_VERBOSE :
        print("\nSave Problem ...")

    #Problem.Dump('data.wcsp')
    Problem.Dump('data.cfn')

# Solve

if True :

    if OPTION_VERBOSE :
        print("\nSolve Problem ...\n")

    res = None

    Problem.Option.verbose = 0

    # tb2 option -vacint : VAC-integrality/Full-EAC variable ordering heuristic
    Problem.Option.FullEAC = True

    Problem.Option.showSolutions = 0

    #Problem.NoPreprocessing()
    #Problem.Option.nbDecisionVars = 2*Events

    # tb2 option -timer=[integer] : CPU time limit in seconds
    #Problem.CFN.timer(3600)

    res = Problem.Solve()

    if result_module_available:

        s = brut_result(res)

        s_pr = pr_result(s, Events, Periods, Rooms)

        if OPTION_VERBOSE_PLUS : #if OPTION_VERBOSE :
            extract_results(s_pr, Rooms)

    else:
        print("\n")
        print("result.py not found",
              "(required to present detailed results)")


#------------------------------------------------------------------------------
#  
#  solve minimize ConflictCost + RoomPreferenceCost + PeriodPreferenceCost + RoomPeriodCost + MinDirectionalDistanceCost + MaxDirectionalDistanceCost + MinUndirectionalDistanceCost + MaxUndirectionalDistanceCost;
#  
#  %solve::int_search(EventPeriod ++ EventRoom, first_fail, indomain_random, complete) minimize ConflictCost + RoomPreferenceCost + PeriodPreferenceCost + MinDirectionalDistanceCost + MaxDirectionalDistanceCost + MinUndirectionalDistanceCost + MaxUndirectionalDistanceCost;
#  
# % output ["Periods = "] ++ [show(EventPeriod[e] - 1) ++ " " | e in 1..Events] 
# %     ++ ["\nRooms = "] ++ [show(EventRoom[e] - 1) ++ " " | e in 1..Events]
# %     ++ ["\nConflictCost = \(ConflictCost)\n" ++ 
# %         "RoomPreferenceCost = \(RoomPreferenceCost)\n" ++ 
# %         "PeriodPreferenceCost = \(PeriodPreferenceCost)\n" ++ 
# %         "MinDirectionalDistanceCost = \(MinDirectionalDistanceCost)\n" ++
# %         "MaxDirectionalDistanceCost = \(MaxDirectionalDistanceCost)\n" ++
# %         "MinUndirectionalDistanceCost = \(MinUndirectionalDistanceCost)\n" ++
# %         "MaxUndirectionalDistanceCost = \(MaxUndirectionalDistanceCost)\n"];
#  
# output ["Periods = \(EventPeriod);\n" ++ 
#          "Rooms = \(EventRoom);\n" ++
#          "- ConflictCost = \(ConflictCost);\n" ++ 
#          "- RoomPreferenceCost = \(RoomPreferenceCost);\n" ++ 
#          "- PeriodPreferenceCost = \(PeriodPreferenceCost);\n" ++ 
#          "- RoomPeriodCost = \(RoomPeriodCost);\n" ++
#   "- DistanceCost = \(MinDirectionalDistanceCost + MaxDirectionalDistanceCost 
#   + MinUndirectionalDistanceCost + MaxUndirectionalDistanceCost);\n" ++
#   + MinDirectionalDistanceCost = \(MinDirectionalDistanceCost);\n" ++
#   "  + MaxDirectionalDistanceCost = \(MaxDirectionalDistanceCost);\n" ++
#   "  + MinUndirectionalDistanceCost = \(MinUndirectionalDistanceCost);\n" ++
#   "  + MaxUndirectionalDistanceCost = \(MaxUndirectionalDistanceCost);\n"
#         ]
#  
#------------------------------------------------------------------------------

