	/	tagsnp wcsp benchmark :

I) background:

This problem occurs in genetics. SNP. Single Nucleotide Polymorphism)
are typos mismatch in DNA sequences between members of. species.
There are important factor responsible of intra species polymorphism.
They may explain, for instance, portion of the heritable risk of
common diseases and can affect respond to pathogens, chemicals, drugs,
vaccines, and other agents. However, their greatest importance in
biomedical research is for comparing regions of the genome between
cohorts (such as with matched cohorts with and without disease).
The study of single nucleotide polymorphisms is also important in
crop and livestock breeding programs. 

TagSNP problem consists of selection of subset of SNP markers along
a chromosome such as the selected markers, called tag SNPs, are mostly
representative of the genetic information of the wall set of markers.
The aims is to capture. maximally informative set of SNP for further
screening of larger population.

For each pair of markers, linkage disequilibrium (LD) measures the
pairwise correlation between the two SNP. LD are. distance measure
obtain from statistical analysis of occurrence of SNP in an initial
cohort. It is normalized and vary in [0-1] scale. 

A marker can represent another marker if and only if it is selected
and the distance between the two markers is higher than. user-defined
maximum threshold.. given threshold - usually 0.8 - allows
to define an undirected Graph where each node is a marker and edges are weightedby the LD distance linking pair nodes. Edges are filtered if LD is lower than
the threshold. Thus, The initial Graph after filtering and decomposition
gives set of connected components corresponding each one to independante intance to solve. 

Thus, in the present benchmark each connected component are processed separately
as an independent problem.

Interestingly, the instance collection covers large diversity, size and topologies
vary from few tens to several hundred of variables with connectivity
spanning from 17% to 93%. 
(cf table in annex 1 describing instances physical caracteristiques (i.e) nombre of markers, density).


II) model description:

For each connected component, we define. binary WCSP from G. Each
variable represents. node/marker. Its domain represents the fact
that the marker is selected or that it is represented by another marker
in the neighborhood of the node as defined by G. Domain size is
equal to the degree in. plus one. 


There is. binary cost function
for each edge of G. The costs take into account several coverage
criteria based on LD defined proposed: 

(0) minimize tagsnp number.
(A ) Maximize average r2 between tagSNPs and untagged SNPs they represent;
(B ) Maximize the lowest r2 between tagSNPs and the untagged SNPs they
connect to;

1 SNP are asocieted to one variable. 
domains of variables contains the putative label of his representative neighbors.
Obviously SNP themselves are also included into the domain constraint .
hence the domain size is equal to the degree in G plus one.

as usual in wcsp the problem is defined as triplet 
{X,D,C}
X is the set of snp. X = {0...N} , N is the vertex number in G.

Di = { 0,1, ......di} value are the putative representation of Xi in G including himself. 
in this case Xi=0

if Xi = 0 then  Ci = U0
unary  constraint are applied on each snp with default cost U0 is defined as follow:
Uo= (N +1)* sum( all cost_criteres)

for minimizing the number of selected tagsnp, 

Uo is setup to an infinity cost to penalize increase of selection in the solution.
this costs is the price to pay if the variable is selected in the solution.
in the opposite case no penalty are declared.

thus 
if (Xi = 0) then Ci = Uo;
 else Ci= null ;


Binary constraint is defined for each edges from the initial graph.
The aims of these constraint is to forbid selection of adjacent node into G.

so if (Xi = 0 and Xj =0 ) the Cij = Uo;


criteria (i) and (2) are respectively taken into account using.
objectify function f_cA et f_cB

less considering criteria (A), in this case we want 
selection most representative tagsnp in terms of r2.

thus this aspect has been integrate into the problem in

fcA(Xi,Xj) = abs( div( (ri -rj/(ri+rj)) ) ; 

considering the fact ri is inclure into the set [0,8,..1].
the resulting space is included in the interval [0-100];



if ( Xi=0 and Xj !=0 | Xj=0 and Xi !=0)

f_cA(Xi,Xj) = abs ( div (rj /(ri-rj)))




Note that this problem is similar to. weighted set covering problem.

The benchmark is composed of 80 instances coming from human chromosome
1 by courtesy of Steve Qin.

An initial upper bound is provided by festa [Qin05],
an incomplete method based on limited exhaustive search followed by
a greedy approach. We use the marker order on the chromosome to build
the tree decomposition. 


ii) model description
--------------------

Each initial node has. corresponding variable in the wcsp model.
the model includes:
unary constraint corresponding to. flag for tagsnp selection.
and binary constraint between each pair of connected snp.
in abstract, each node from the initial graph correspond to the variable set of the instance.
edges of the LD graph correspond to binary constraint between variable.
interestingly, the structure of the initial problem is extremely close from
the resulting wcsp problem.


iii) benchmark organization

the current benchmark includes 82 instances extracted from the graph of
r2 pairwise LD of human chromosome I. Original data comes from HapMap project.
(release 16, March 2005)

each instance is corresponding to. connected component (CC) resulting of 
of the filtered LD graph.. threshold r2=0.8 has been used, 
i.e all edges with. r2 < 0.8 are removed, before CC extraction.

the initial graph includes 178438 snp markers located in 43251 clusters.
most of the can be easily resolve using exhaustive search of solution.
i.e. iterative generate and test of the wall set of putative solution).

82 cluster are too large for finding solution using exhausting search of tagsnp solution.
the listing of festa solution used as initial upper for benchmark figures in the 3th fields 

the non optimal festa solution transformed using our function cost available 
in ub file. optimal solution our best solution found are also provided in *.best.ub files.


for each instances in the current directory are proposed :
i). txt file includes unfiltered connected component : under the form of edge list.
ii). wcsp instances 
iii) respectively festa upper bound files and the best upper bound obtained by wcsp optimisation approach.

for example. instances 1968

1968.txt ==> edges list (format. node1 node2 r2 ) with TAB for field delimiter
1968.wcsp ==> wcsp model
1968.ub ==> festa up
1968.best.ub ==> best upper bound found for the instance
dot/1968.dot ==> LD graph in Graphviz file format (dot)
for visualization zgrviewer 
(http://zvtm.sourceforge.net/zgrviewer.html)

for closed instances a red color layout is applied on selected tagsnp.
=====
the following tab inlcudes: festa initial tagsnp number used for init upper bound 
respectively includes instance name  variable number and tagsnp set size founded using festa with a uncomplet approach.

Cl no.	Cl size	Gr set size	
1968	64	5
2480	55	5
3007	84	4
3567	67	6
3935	57	6
5013	80	4
6656	54	5
8681	95	9
8892	55	5
9727	238	12
9881	42	8
10734	49	7
11053	47	5
12929	85	4
13106	61	5
13809	84	4
14194	251	5
14481	82	8
14976	94	8
15183	119	5
15282	114	9
16864	35	6
17271	107	4
17289	33	6
17904	100	4
17929	119	6
18016	116	4
18603	104	6
18827	80	5
18941	79	4
18948	80	4
19648	91	6
19655	65	5
19674	50	8
19684	45	5
19984	174	11
20037	75	5
20125	113	6
20927	38	6
21758	63	6
22631	99	5
22707	63	6
23129	114	5
23384	63	5
23626	107	5
23659	150	5
25023	40	6
26578	93	8
27116	43	6
27498	258	3
27505	65	5
28188	55	6
28202	64	5
28203	215	6
28454	96	5
29043	50	5
29401	35	6
29624	88	9
30162	48	5
30393	85	4
30395	81	4
30461	464	9
30495	91	5
30557	87	7
30644	78	4
30799	67	5
30933	132	7
31466	123	5
31481	33	6
31675	46	5
31968	43	6
32987	123	9
34916	59	5
35418	77	5
35624	55	5
37188	187	8
37610	114	8
38837	94	4
38928	75	4
39997	124	4
42042	55	5
43205	121	4


tab1: instances number followed by respectively his global size and the tagsnp number founded by uncomplet approach.
