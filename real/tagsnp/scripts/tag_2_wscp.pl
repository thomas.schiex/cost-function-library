#!/usr/bin/perl
# version tester avec simon ok mais sans contrainte lexicographique

use Getopt::Long;
use Pod::Usage;
use Cwd;
use strict;

$[ = 1;			# set array base to 1
$, = ' ';		# set output field separator
$\ = "\n";		# set output record separator

my $file="ABC.txt";
$file="square-anti.txt";
my $verbose;
my $balance=1;
my $CostFactor=100;
my $r = 0.8;
my $n = 0;
my $MinLim;
my $MaxLim;
my $nbre_cons=0;
###
$MinLim=1.0;
$MaxLim=1;
###
my $Ucost;
my %snp;
my %invsnp;
my %Link;
my @line;
my $i;
my $j;
my $top;
my $maxd;
my $c;
my %val;
my %dom;
my $Xi;
my $Xj;
my $CXii;
my $CXij;
my $Nbtuple=0;
######

my $Cout1;
my $Cout2;
my $Cout3;
my $Ucost;

my $TotalEgde=0;
my $TotalEgde2=0;
my $TotalEgde3=0;
my $minDiffcost =1;
my $help;

my $result = GetOptions(
    "file=s"     => \$file,        # string
    "balance=i"   => \$balance,        #balance = coefficient entre critere 1 et critere 3 au sens festa
    "costfactor=i"   => \$CostFactor, # integer precision du score
    "r=f"   => \$r, # seuil de filtrage 
    "help"   => \$help,       # flag  for help display
    "verbose"   => \$verbose       # string
);
my %option = (
	"CostFactor" => $CostFactor,
	"r" => $r, 			# real seuil filtrage
	"balance"	=> $balance,
	"file"      => $file,          # string
    	"help"   => $help,       # flag  for help display
	"verbose"   => $verbose        # string
);

#------------------------------------------------------------
sub usage ( $ )
{
        my ($msg)=@_;
        print STDERR "$msg\n";
        system( "/usr/bin/pod2text $0 ");
        exit(1);
}
##------------------------------------------------------------



#=================
## open file
if($help) {
        print "Help command:\n";
        &usage('empty value for -file option');
}


if( not defined (@ARGV) ) {
        print "error file not found :\n";
        &usage('empty value for -file option');
}


$MinLim=$r;
if ($verbose) {

	print "####################";
	print "file =$file";
	print "seuil r = $r";
	print "balance factor = $balance";
	print "costfactor = $CostFactor";
	print "####################";
}



open( F, $file ) || die("Could not open $file!");

while (<F>) {
	@line = split(' ', $_, -1);
	if ($line[3] >= $r) {	
		if (!(defined $snp{$line[1]})) {
			$snp{$line[1]} = ++$n;
			$invsnp{$n} = $line[1];
		}
		if (!(defined $snp{$line[2]})) {
			$snp{$line[2]} = ++$n;
			$invsnp{$n} = $line[2];
		}
		$Link{$snp{$line[1]}, $snp{$line[2]}} = $line[3];
		$Link{$snp{$line[2]}, $snp{$line[1]}} = $line[3];

		if( $MinLim >  $line[3]) {$MinLim=  $line[3];}
		if( $MaxLim <  $line[3]){ $MaxLim=  $line[3];} 
	}
}

$maxd = 0;
$c = 0;
for ($i = 1; $i <= $n; $i++) {
	$dom{$i} = 1;
	for ($j = 1; $j <= $n; $j++) {
		if ($j != $i && $Link{$i, $j} > 0) {
			$val{$i, $j} = $dom{$i};
			$dom{$i}++;
		}
		if ($j > $i && $Link{$i, $j} > 0) {
			$c++;
			$Cout1 = &Max_Tag_Neighbor($Link{$i, $j},$r,$MaxLim,$CostFactor);
			$Cout3 = &Max_Tag_space($Link{$i, $j},$r,$MaxLim,$CostFactor);
			$TotalEgde2 = $TotalEgde2 + $Cout1 ;
			$TotalEgde3 = $TotalEgde3 + $balance*$Cout3 ;
		}
	}
	if ($dom{$i} > $maxd) {	
		$maxd = $dom{$i};
	}
}
###############
# calcul de top et cout unaire
#
#$verbose =0;
$Ucost =($TotalEgde2+$TotalEgde3);
# ==> cherche le max des cot fini sur chaque contrainte

if ($Ucost == 0 ) { $Ucost =1; };
$top = $Ucost*($n + 1);
#################"
my $counter=0;
print 'tagsnp', $n*2, $maxd, 3*$n + 3*$c, $top;

# taille des domaines :
#------------------

for ($i = 1; $i <= $n; $i++) {
	my $tmp=2;
	printf  '%d ', $tmp;
}
for ($i = 1; $i <= $n; $i++) {
	printf '%d ', $dom{$i};
}
print '';

############################
#

# flag tag snp de 0 n-1 ==> flag tagsnp
# de N a 2N-1  ==>variable de  voisinage
#
for ($i = 1; $i <= $n; $i++) {
	print 1, $i - 1, 0, 1;
	print 0, $Ucost;
	$nbre_cons++;
			if( $verbose ) { print " cout interdit entre flag et sa representation dans sa liste de voisin"; }
			print 2, $i - 1, $i-1+$n, 0, 1;
			print "1 0", $top;
			$nbre_cons++;
}

##########################
# contrainte unaire sur les voisins tuple contenu dans les domaines entre tagsnp et representation de voisinage 
# si flag = 0 et indice
if( $verbose ) {
	print " COUT UNAIRE sur les voisinage :";
}
for ($i = 1; $i <= $n; $i++) {

	print 1,$i+$n-1, 0, $dom{$i}-1;
	$nbre_cons++;

	for ($j = 1; $j <= $n; $j++)
	{
		if ($Link{$i, $j} > 0 ) 
		{
			if ($verbose) { print " val ij = $val{$i,$j} => $i et  $j  sont  relies" }
			print $val{$i,$j}, &Max_Tag_Neighbor($Link{$i, $j},$r,$MaxLim,$CostFactor);
		} else {
			if ($verbose) { print " val ij = $val{$i,$j} => $i et  $j ces noeuds ne sont  pas un voisin" }
		}


	}
}

##################################
# dispertion contrainte binaire entre variable tagsnp 
#critere 3 deux tag snp directement li�es=> penalisation 
for ($i = 1; $i <= $n; $i++) {
	for ($j = $i + 1; $j <= $n; $j++)
	{
		if ($Link{$i, $j} > 0)
		{
			if( $verbose ) { print " COUT DISPERTION entre tag potentiel contrainte binaire entre flag lies dans le graphe de depart:"; }
			print 2, $i - 1, $j - 1, 0, 1;
			$nbre_cons++;
			print "0 0",$balance*&Max_Tag_space($Link{$i, $j},$r,$MaxLim,$CostFactor);

		}
	}
}





#######################
#
# contrainte binaire pour interdir les cout entre variable et voisinage
# flag =1 voisinage = flag ==> cout top
# flag d'un voisin et sa representation => Top
# 

if( $verbose ) {
	print "cout interdit :";
}
for ($i = 1; $i <= $n; $i++) {
	for ($j = $i + 1; $j <= $n; $j++)
	{
		if ($Link{$i, $j} > 0)
		{
			print 2, $i+$n - 1, $j-1, 0, 1 ;
			print $val{$i,$j} ,"1 $top";
			print 2, $j+$n - 1, $i-1, 0, 1 ;
			print $val{$j,$i} ,"1 $top";
			$nbre_cons++;
			$nbre_cons++;
		}
	}
}




if ($verbose) {
       	print "########################"; print "nbre contrainte = $nbre_cons"; 
	print "Max score = $MaxLim";
	print "Min score limit = $MinLim";
}

#################################################################
sub Max_Tag_space 
{
	my $EdgeValue = shift;
	my $MinValue = shift;
	my $MaxValue = shift;
	my $Pres_Factor = shift;
	my $cost = int( $Pres_Factor * ($EdgeValue-$MinValue)/($MaxValue-$MinValue)  );

	return ($cost);
}
sub Max_Tag_Neighbor
{
	my $EdgeValue = shift;
	my $MinValue = shift;
	my $MaxValue = shift;
	my $Pres_Factor = shift;
	my $cost = int( $Pres_Factor * abs(($EdgeValue-$MaxValue)/($MaxValue-$MinValue)) );

	return ($cost);
}



#_________________________________________

__END__

=head1 NAME

 tag_2_wcsp.pl product a wcsp model for tagsnp selection from an adjacant list of the snp pair correlation graph
	input data are pair snp correlation .i.e ( weighted graph ) vertex are snp and edges are r� criters )
Example: 
	 rs13374371 	 rs9436300 	0.00679
	...
	 rs1889926 	 rs7534511 	0.00872


  Usage: 
  tag_2_wcsp.pl -f myedgefile.txt -r 0.8 ==> print on stdout the model 
	
  

=head1 SYNOPSIS

  # Create an WCSP modele  for each componant find in the adjacent matrice 
  tag_2_wcsp.pl -f myfilie -r  0.8  



    generate modele file for toulbar2 ,in wcsp format
    input file contening ajacence liste
    by default first and second colonn are used for nodes determination
    thrith cool is used for delatsq value


=head1 DESCRIPTION

many efficient and peer reviewed algorithms. 


=head3 Graph read Input Parameters [Optional]:

    "file=s"       =>  txt file contening edge files
    "balance=i"    =>  balance = coefficient entre critere 1 et critere 3 au sens festa
			default is one (dispertion and representativeness have the same weight)
    "costfactor=i" =>  integer precision du score defaults 100
    "r=f"          =>  r2 treshlold used for edge filtering
    "help"         =>  flag  for help display
    "verbose"      =>  flag 



=head1 AUTHOR

David Allouche E<lt>allouche@toulouse.inra.frE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2008 by David Allouche

This script is free software; you can redistribute it and/or modify
it under the same terms as Perl itself. 

=cut
