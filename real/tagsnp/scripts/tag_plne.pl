#!/usr/bin/perl
use strict;
use Getopt::Long;
use Pod::Usage;
use Cwd;
use Graph;
use Graph::Undirected;
use Graph::Traversal::BFS;
use File::Basename;

#require "Graphlib.pm";

my $j;
my $man  = 0;
my $help;
my ($file);    #fichier contenant la liste des arretes
my ($sol);     #fichier contenant la liste des tags
my $node1;
my $node2;
my $field3;
my $field4;
my $line;
my $verbose;
my @field;
my $m1       = 0;
my $m2       = 1;
my $r        = 2;
my $treshold = 0.8;
my $deltasq;
my $redge;
my @del_edge;
my $rnode;
my @del_node;
my $edge_filter;
my $edgeNb;
my $nodeNb;
my $nodeliste;
my @singleton;
my $graphviz;
my $tgcolor = "red";
my %shapeTag;
my $jaccard;
my $remove;
my $InPutDir;
my @InPutDirList;
my $OutPutDir = 'out/';
my $lp_solve;
my $Score_pre=100;
my $setcover;


#------------------------------------------------------------
# command line option
#------------------------------------------------------------
my $result = GetOptions(
    "file=s" => \$file,    # string
    "treshold=f" => \$treshold,    # numeric edge filter treshold
    "m1=i"       => \$m1,          # node1 colon rank
    "m2=i"       => \$m2,          # node2 colon rank
    "r=i"        => \$r,           # delta r2 colon rank
    "InPutDir=s"  => \$InPutDir,     # string input path of graph file
    "OutPutDir=s" => \$OutPutDir,    # string input path of graph file
    "verbose"     => \$verbose,       # debug flag
    "help"     => \$help,       # debug flag
    "setcover"     => \$setcover,       # flag for pur set cover
    "lpsolve"     => \$lp_solve# debug flag
   
);
my %option = (
    "file"     => $file,             # string
    "treshold" => $treshold,         # numeric
    "m1"       => $m1,               # numeric
    "m2"       => $m2,               # numeric
    "r"        => $r,                # numeric
    "InPutDir"  => $InPutDir,     # string input path of graph file
    "OutPutDir" => $OutPutDir,    # string input path of graph file
    "lpsolve"     => $lp_solve, 	# d
    "help"     => $help,       # debug flag
    "setcover"     => $setcover,       # flag for pur set cover
    "verbose"   => $verbose       # string
);
 #------------------------------------------------------------
sub usage ( $ )
{
        my ($msg)=@_;
        print STDERR "$msg\n";
        system( "/usr/bin/pod2text $0 ");
        exit(1);
}
##------------------------------------------------------------



#=================
## open file
if($help) {
	print "Help command:\n";
        &usage('empty value for -file option');
}
	

if( not defined (@ARGV) ) {
	print "error file not found :\n";
        &usage('empty value for -file option');
}

unless(-d $OutPutDir){
    mkdir $OutPutDir or die "Couldn't create dir: [$OutPutDir] ($!)";
print " $OutPutDir created \n";
}


#------------------------------------------------------------
if ( defined $InPutDir ) {
    print "$InPutDir scan \n";
    @InPutDirList = `find $InPutDir -name "*.txt"`;
} else
	{
	 print "reading File = $file \n";
	$InPutDirList[0]="$file";
	} 

foreach $file (@InPutDirList) {
    chomp $file;

    # debut modele
    my $g = Graph::Undirected->new( unionfind => 1 );
    my $clustername = basename( "$file", ".txt" );
   print "##################\n";
   print "file : $file \n";
   print "##################\n";
    #=================
    # open file
    if ( defined $file ) {
        $g = read_graph(
            $file,    $m1,    $m2,    $r, $treshold,
            $verbose, $rnode, $redge, $remove
        );
        @singleton = get_singleton($g);
        $g = clean_graph( $g, $verbose, @singleton );
        &print_header( $m1, $m2, $r, $treshold );
        &cc_density( $g, "all nodes" );
    }
    else {
        &usage('empty value for -file option');
    }

    if ($verbose) {

        my @cc;
        @cc = $g->connected_components();
        my @hist_cc_size;

        my $nbcc = @cc;
        print
          "\n# ----- cluster found after singleton cleaning = $nbcc -----\n";
        my $cc_row;
        my $cc_size;
        my $index = 0;

        for $cc_row (@cc) {
            print "CLUSTER $index :$$#cc_row";
            my @tmp = @$cc_row;
            print "@$cc_row\n"
              ;    # affichage tu tableau contenant la composante connexe
            $cc_size = @$cc_row;
            &cc_to_graph( $g, $index, $verbose, $clustername, @tmp );
            $hist_cc_size[$cc_size]++;
            $index++;
        }

    }
if($setcover) {
    &Cplex_set_cover_mod( $g, "$OutPutDir/$clustername.cplex" );
} else 
{
    &Cplex_set_cover_Multi( $g, "$OutPutDir/$clustername.cplex",$treshold,$Score_pre,$verbose,$lp_solve );

}
}

sub Cplex_set_cover_mod {

    my $g = shift;
    my $f = shift;

    open MOD, "> $f" or die "Can't open $f : $!";
######################
    # set covering simple cyplex
    #
############"
    print MOD "Minimize \n obj:";
    my $Snpi = 1;
    my %snp;
    foreach my $v ( $g->vertices ) {
        $snp{$v} = $Snpi;
        print MOD '+x' . $Snpi;
        $Snpi++;
    }
    print MOD "\n";

    # contrainte
    print MOD "Subject To\n";
    foreach my $v ( $g->vertices ) {
        print MOD "c" . $snp{$v} . ":";

        my @voisin    = $g->neighbours($v);
        my $contraint = "+x" . $snp{$v};
        foreach my $vj ( $g->neighbours($v) ) {
            $contraint = $contraint . " +x" . $snp{$vj};
        }

        print MOD $contraint;
        print MOD ">=1";
        print MOD "\n";

    }
    print MOD "\n";

    my $bounds  = "Bounds\n";
    my $integer = "Integer \n";
    foreach my $v ( $g->vertices ) {
        $bounds = $bounds . "  x" . $snp{$v} . "<= 1 \n";

        $integer = $integer . "  x" . $snp{$v} . "\n";
    }
    print MOD $bounds;
    print MOD $integer;
    print MOD "End \n";

    close MOD;

    return ();
}

sub Max_Tag_space {
    my $EdgeValue   = shift;
    my $MinValue    = shift;
    my $Pres_Factor = shift;
    my $cost =
      int( $Pres_Factor * ( $EdgeValue - $MinValue ) / ( 1 - $MinValue ) );

    return ($cost);
}

sub Max_Tag_Neighbor {
    my $EdgeValue   = shift;
    my $MinValue    = shift;
    my $MaxValue    = 1;
    my $Pres_Factor = shift;
    my $cost =
      int( $Pres_Factor *
          abs( ( $EdgeValue - $MaxValue ) / ( $MaxValue - $MinValue ) ) );

    return ($cost);
}

##############################################################################
# tagsnp plne modele pour dispersion et reprsentativité
#########################################
sub Cplex_set_cover_Multi {

    my $g        = shift;
    my $f        = shift;
    my $treshold = shift;
    my $Score_pre = shift;
    my $verbose = shift;
    my $lp_solve= shift;
    my $BigM;    # unirary cost = Somme de tous les critéres
my $el;
	

# big M = somme ( tous_tag + somme_tous representatif + tous_paire dispersition )
# cf ... wcsp ..
    my $ObjectFunction;
    my $ObjectFunction_Header;
    my $ALLconstraint = "Subject To\n";
    my $ALLconstraintVoisin;
    my $Disp_total_score = 0;
    my $Rep_totale_score = 0;
    my $ALLcontraintVoisinDur;
    $ObjectFunction_Header = "Minimize \n obj:";
   if ($lp_solve) {
	  $ObjectFunction_Header = "Min:\n ";
	  $ALLconstraint =" ";
	  $el = ";\n";

	} else
	{$el= "\n";}

    open MOD, "> $f" or die "Can't open $f : $!";
######################
    # set covering simple cyplex
    #
############"
    $ObjectFunction;
    my $ObjectFunctionVoisin;
    my $Snpi = 1;
    my %snp;
    foreach my $v ( $g->vertices ) {
        $snp{$v} = $Snpi;
        $Snpi++;
    }

    #        $ObjectFunction=$ObjectFunction."\n";
    # contrainte
	my $lcount;
    foreach my $v ( $g->vertices ) {
        #$ALLconstraint       = $ALLconstraint . "c" . $snp{$v} . ":";
        $ALLconstraint       .= "c" . $snp{$v} . ":";
        $ALLconstraintVoisin = $ALLconstraintVoisin . "CMV_$snp{$v}:";

        my $contraint       = "+x" . $snp{$v};
        my $contraintVoisin = "x$snp{$v} ";

	my $vcount;
        foreach my $vj ( $g->neighbours($v) ) {
            $contraint = $contraint . " +x" . $snp{$vj};
 if($vcount % 20 eq 1) { $ALLconstraintVoisin.= "\n";}
        $vcount++;

            $contraintVoisin =
              $contraintVoisin . "+R" . $snp{$v} . "_" . $snp{$vj};
 if($vcount % 20 eq 1) { $contraintVoisin.= "\n";}
        }

        $ALLconstraint       = $ALLconstraint . "$contraint >=1 $el";
        $ALLconstraintVoisin = $ALLconstraintVoisin . "$contraintVoisin=1 $el";

    }

    $ALLconstraint       = "$ALLconstraint\n";
    $ALLconstraintVoisin = "$ALLconstraintVoisin\n";

    my $bounds  = "Bounds\n";
    my $inttype;
    my $integer = "Integer \n";
   if( $lp_solve) {
	$inttype ="int ";
	$integer="";
	}
    foreach my $v ( $g->vertices ) {

        $bounds  = $bounds . "  x" . $snp{$v} . "<= 1 $el";
        $integer = $integer . "$inttype  x" . $snp{$v} . "$el";
	if($verbose) { print "correspondance : $v ==> x$snp{$v} $el"; }
    }

    print "\n";
###
    # dispertion
##########"

    my $Repre_total_score = 0;
    my @AoA               = $g->edges;
    my $Nbr_edge          = $#AoA;
    my $E;
    my $value = 0;
    my $stop  = 0;
    my $ObjectFunction_disp;
    my $ObjectFunction_Rep;

    my $name = "r2";
    if ( $stop < 1 ) { $stop = $#AoA }
    for $E ( 0 .. $stop ) {
        $value =
          $g->get_edge_attribute( @{ $AoA[$E] }[0], @{ $AoA[$E] }->[1], $name );

        if($verbose) { print "Edge $E is: @{$AoA[$E]}  r2 value = $value\n"; }

        my $Dscore;
        my $Rscore;
        my $varDisp;
        my $varRep1;
        my $varRep2;

        $Dscore = &Max_Tag_space( $value, $treshold, $Score_pre);
        $Rscore            = &Max_Tag_Neighbor( $value, $treshold, $Score_pre);
        $Disp_total_score  = $Disp_total_score + $Dscore;
        $Repre_total_score = $Repre_total_score + $Rscore;

        #  contrante dure de representation

        $varDisp =
          "d" . $snp{ @{ $AoA[$E] }[0] } . "_" . $snp{ @{ $AoA[$E] }[1] };
        $varRep1 =
          "R" . $snp{ @{ $AoA[$E] }[0] } . "_" . $snp{ @{ $AoA[$E] }[1] };
        $varRep2 =
          "R" . $snp{ @{ $AoA[$E] }[1] } . "_" . $snp{ @{ $AoA[$E] }[0] };

        # print "element i",@{$AoA[$E]}[0]," element j ",@{$AoA[$E]}[1];
        $varDisp =~ s/ //g;

	if($verbose) {
        print "variable dis $value  = $Dscore*$varDisp \n";
	}
	if( $Dscore > 0 ) {
        $ObjectFunction_disp = $ObjectFunction_disp . "\n +$Dscore $varDisp"; }
	if( $Rscore > 0 ) {
        $ObjectFunction_Rep  = $ObjectFunction_Rep . " \n+$Rscore $varRep1";
        $ObjectFunction_Rep  = $ObjectFunction_Rep . " \n+$Rscore $varRep2";
	}

        $ALLconstraint =
            "$ALLconstraint C_$varDisp:x"
          . $snp{ @{ $AoA[$E] }[0] } . "+x"
          . $snp{ @{ $AoA[$E] }[1] }
          . " - $varDisp <= 1 $el\n";
        $ALLconstraint =
            "$ALLconstraint C_$varDisp"
          . "_a: $varDisp - x"
          . $snp{ @{ $AoA[$E] }[0] }
          . "<= 0 $el \n";
        $ALLconstraint =
            "$ALLconstraint C_$varDisp"
          . "_b: $varDisp - x"
          . $snp{ @{ $AoA[$E] }[1] }
          . "<= 0 $el \n";

	$ALLcontraintVoisinDur.="C$varRep1:x$snp{@{$AoA[$E]}[0]} - $varRep2>= 0$el";
	$ALLcontraintVoisinDur.="C$varRep2:x$snp{@{$AoA[$E]}[1]} - $varRep1>= 0$el";

        #D5b: d45 - y5 <= 0
        $bounds  = $bounds . "$varDisp<=1 $el";
        $integer = $integer . "$inttype $varDisp $el";
        $bounds  = $bounds . "$varRep1<=1  $el";
        $integer = $integer . "$inttype $varRep1 $el";
        $bounds  = $bounds . "$varRep2<=1  $el";
        $integer = $integer . "$inttype $varRep2 $el";

    }

    my $total_secondaire = $Repre_total_score + $Disp_total_score;
####################
    # ecriture
    print MOD "$ObjectFunction_Header";
	my $count = 0;
    foreach my $v ( $g->vertices ) {
	if($count > 0) {
        $ObjectFunction = "$ObjectFunction +$total_secondaire x$snp{$v}";
	} else {

        $ObjectFunction = "$ObjectFunction $total_secondaire x$snp{$v}";
	}
	if($count % 20 eq 1) {  $ObjectFunction = "$ObjectFunction \n";}
	$count++;
    }
    print MOD "$ObjectFunction";
    print MOD "$ObjectFunction_Rep";
    print MOD "$ObjectFunction_disp";
    print MOD "$el";

    $ALLconstraint =~s/:+/: /g;
    $ALLconstraintVoisin=~s/:+/: /g;
    $ALLcontraintVoisinDur=~s/:+/: /g;
    print MOD $ALLconstraint;
    print MOD $ALLconstraintVoisin;
    print MOD $ALLcontraintVoisinDur;
    print MOD $bounds;
    print MOD "\n";
    print MOD $integer;
    print MOD "\n";
    if (!$lp_solve) { print MOD "End \n"; }

    close MOD;

    return ();
}

#==============
#print_header
#==============
##############################################"
sub print_header ( $ ) {
    my $M1       = shift;
    my $M2       = shift;
    my $deltasq  = shift;
    my $treshold = shift;

    print "#-----------------------\n";
    print "#default param: \n";
    print "# M1 COL =:$M1 ";
    print "# M2 COL =:$M2 ";
    print "# deltasq COL=:$deltasq \n";
    print "# treshold =:$treshold \n";
    print "#-----------------------\n";
    print "\n";
}

#==============
#read_graph
#==============
############ graph filtring #######################
# read graph
############# graph filtring #######################
sub read_graph 
{
my $file=shift; # file descriptor
my $m1=shift;# node1 colonne rank
my $m2=shift;# node1 colonne rank
my $r=shift;##edge weight colonne rank
my $treshold=shift;# weight treshold
my $verbose=shift;# verbose flag
my $rnode=shift;# node liste to be deleted
my $redge=shift;# edge liste to be deleted
my $g = Graph::Undirected->new( unionfind => 1 );
my @field;
my ($node1,$node2,$deltasq,$field4);
my $line;
my @del_node;
my @del_edge;
my  $nodeNb;
my $edgeNb;
my $edge_filter;
if ( defined $rnode ) {
   @del_node = split ':', $rnode;
    print "#>>>>>>>>>>>>>>"
      . `$#del_node+1`
      . " node(s) will be remove >>>>>>>>>>>\n";
    print "#>>>>>>>>>>>>>> node $rnode  removed >>>>>>>>>>>\n";
}

    open( F, $file ) || die("Could not open $file!");
#------------------------------------------------------------
while ( $line = <F> ) {
    ( $node1, $node2, $deltasq, $field4 ) = split ' ', $line;
    (@field) = split ' ', $line;
    $node1   = $field[$m1];
    $node2   = $field[$m2];
    $deltasq = $field[$r];
############# graph filtring #######################
    #---------------NODE
    my $delete_node_flag;
    my $t;
    my $delete_edge_flag = 0;
    if ( defined $rnode ) {

        my $rn;
        foreach $rn (@del_node) {
            if ( $rn eq $node1 ) {
                $delete_node_flag = 1;
                $delete_edge_flag = 1;
            }
            elsif ( $rn eq $node2 ) {
                $delete_node_flag = 2;
                $delete_edge_flag = 1;
            }
            else {
                $delete_node_flag = 0;
            }
        }
    }

   #---------------EDGE

    if ( defined $redge ) {
        foreach $t (@del_edge) {
            if ( ( $t eq "$node1:$node2" ) or ( $t eq "$node2:$node1" ) ) {
                $delete_edge_flag = 1;
                print "\n<<<<<<<<<<<<<< edge $t removed <<<<<<<<<<<\n";
            }
        }
    }
############# graph construction #######################
    if ( ( !( defined $g->vertex($node1) ) ) and ( $delete_node_flag != 1 ) and ( $deltasq >= $treshold ) ) {
        if ( $verbose > 0 ) { print "#N1 add vertex $node1\n"; }

        if($node1) {
        $g->add_vertex("$node1");
        $g->set_vertex_attribute( $node1, "varcsp", $nodeNb);
        $nodeNb++;
}
    }
    if ( ( !( defined $g->vertex($node2) ) ) and ( $delete_node_flag != 2 ) and ( $deltasq >= $treshold )) {
            if($node2) {
        $g->add_vertex("$node2");
        $g->set_vertex_attribute($node2, "varcsp", $nodeNb);
        $nodeNb++;
}
        if ( $verbose > 0 ) { print "#N2 add vertex $node2\n";
        }
    }

    if ( ( $deltasq >= $treshold ) and ( $delete_edge_flag != 1 ) ) {
        $g->add_path( "$node1", "$node2" );
        $g->set_edge_attribute( $node1, $node2, "r2", $deltasq );
        $edgeNb++;
        if ( $verbose > 0 ) { print "# edge $node1 => $node2 added\n"; }
    }
    else {

        if ($verbose > 0 ) {
            print "#edge filtered $node1:$node2 r=$deltasq\n";
        }
        $edge_filter++;
        $delete_edge_flag = 0;
    }
}

close(F);

if ( defined $redge ) { $g = &remove_edge( $redge, $g ); }

print "#-----------------------\n";
print "#-------INPUT GRAPHE READ----------------\n";
print "# Nodes  =:$nodeNb\n";
print "# edges  =:$edgeNb \n";
print "# Edges filtred Nbre =:$edge_filter \n";
print "#-----------------------\n";
print "#-----------------------\n";

return($g)

}

########################
#==============
# cc_to_graph
#==============
########################

sub cc_to_graph {
my $g = shift;
my $cc_id=shift;
my $verbose=shift;
my $clustername=shift;
my @ccnode=@_;
my $v;
my $c = Graph::Undirected->new( unionfind => 1 );
foreach $v (@ccnode) { $c-> add_edges($g->edges_at($v)); }

my $size =@ccnode;
print"\n";
print  " ccid=$clustername:";
print"cluster $cc_id composition($size):";
&cc_aver_degres($c,$cc_id);
&cc_density($c,$cc_id);
print"\n";
if(defined($verbose)) {
print $c;
print"\n";
}
return($c)
}

###############################################"
sub cc_aver_degres{
my $gragh = shift;
my $cc_id=shift;
my $aver= $gragh->average_degree;
print ":average degrees in cluster:$cc_id=$aver ";
}

#==============
#cc_density
#==============
###############################################"
sub cc_density {
my $g = shift;
my $cc_id=shift;
my $density = $g->density;
print ":density of cluster:$cc_id=$density ";
}

#==============
# get_singleton
#==============
#=====================
#############################
#  GRAPH decomposition en composante connexe
# DFS
########################
sub get_singleton
{
my $g = shift;
my @cc = $g->connected_components();
my @hist_cc_size;
my $nbcc = @cc;
print "# ----- cluster found = $nbcc -----\n";
my $cc_row;
my $cc_size;
my $index = 0;
my @singleton;
my $tmp;

for $cc_row (@cc) {
    $cc_size = @$cc_row;
    if ( $cc_size == 1 ) {
        push( @singleton, @$cc_row);
    }

}
my $i=0;
for my $v (@singleton) {
$i++;
print "singleton:$i:$v\n";
}
print "#$i singletons pushed found \n";
print "\n#==========\n";
return (@singleton)
}
########################
########################
sub clean_graph ($){

my $g = shift;
my $verbose =shift;
my @singleton= @_;
my $nbre_single=@singleton;
my $v ;

for $v (@singleton) {
        if (defined $verbose) { print "node $v delete"; }
         $g=$g->delete_vertex($v);
 }
return($g)
}
########################


#==============
# 
#==============
1;

#_________________________________________

__END__

=head1 NAME

 tag_plne.pl is a wrapper for adjacant list to set covering optimisation.
	input data are pair snp correlation ( weighted graph vertex are snp edges are r² criters )
 Example: 
             rs13374371      rs9436300      0.00679
            ...
             rs1889926       rs7534511      0.00872


  Usage: 
  tag_plne.pl -f myedgefile.txt -t 0.8 ==> produce myegdefile.cplex
  tag_plne.pl -In myedgeDIR -out Myoutdir -t 0.8 -lp ==> produce myegdefile.cplex for each txt file founded in Myedgedir
  

=head1 SYNOPSIS

  # Create an MIP modele  for each componant find in the adjacent matrice 
  tag_plne.pl -f myfilie -treshold  0.8 -lp 



    generate modele file for cplex , lp_solve or qsoft in lp format
    input file contening ajacence liste
    by default first and second colonn are used for nodes determination
    thrith cool is used for delatsq value

    file format can by imposed using the following option :

    -m1  => colonne indice corresponding to the first node
    -m2 => colonne indice corresponding to the second node
    -r  => colonne indice corresponding to 

=head1 DESCRIPTION

many efficient and peer reviewed algorithms. 

=head1 OPTION 
    -treshold => weight cutoff used for graph filtering before 
    -InPutDir"  => input directory for graph (will scan the input directory a modele will be generated for each *txt file founded)
    -OutPutDir"  => output directory for graph defaut (./out)

=head3 Graph read Input Parameters [Optional]:

    "treshold" => $treshold,         # numeric
    "m1"       => $m1,               # numeric default 1
    "m2"       => $m2,               # numeric default 2
    "r"        => $r,                # numeric default 3
    "InPutDir"  => $InPutDir,     # string input path of graph directory
    "OutPutDir" => $OutPutDir,    # string input path of graph file
    "lpsolve"     => $lp_solve,         #  lp_solve format default format compliant with cplex and qsopt
     setcover  ==> $setcover  # flag ==> pur set covering model is used (no secondery criters)
    "verbose"   => $verbose       # string


=head1 AUTHOR

David Allouche E<lt>allouche@toulouse.inra.frE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2008 by David Allouche

This script is free software; you can redistribute it and/or modify
it under the same terms as Perl itself. 

=cut
