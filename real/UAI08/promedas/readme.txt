# Submitter: Vicenc Gomez (University Nijmegen)‏
# Domain: Medical diagnosis, real-world cases, converted from noisy-or
# Type: Markov for MAR/PRE

    * 238 Instances
    * Binary variables 

# QMR-DT like networks; layered noisy-or model

    * Bayesian network model converted to Markov network after performing simplifications (pruning unobserved nodes, negative findings, compact representation of noisy-or, etc) 

# Treewidths range from 1 (tree) to ~60

    * most are too difficult for exact algorithms 
