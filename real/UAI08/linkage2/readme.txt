# Submitter: Dechter group (UC Irvine)‏
# Domain: Genetic linkage
# Type: Markov for MPE
    * 22 instances
    * Max. domain size between 3 and 7 
# Treewidth: ~20-35
#
http://graphmod.ics.uci.edu/uai08/Evaluation/Report/Benchmarks
