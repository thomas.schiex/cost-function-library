---
--- Original README file from  ftp.cert.fr/pub/lemaitre/FullRLFAP.tgz
---

The three  directories CELAR, GRAPH and  SUBCELAR6 contain the definition of
the radio link frequency assignment problems described in (the forthcoming):

Radio Link Frequency Assignment Benchmarks. B. Cabon, S. de Givry,
L. Lobjois, T. Schiex and J.P. Warners. CONSTRAINTS journal. 1998.

The  first directory  holds  the CELAR  instances,   provided by the  Centre
d'Electronique de l'Armement (France).

The second directory  holds the GRAPH instances,  generated using  the GRAPH
generator at Delft University (the Netherlands).

The third directory holds the  SUBCELAR6 instances which have been extracted
from  the  CELAR6 instance  by  the   combinatorial  optimization group   of
ONERA/CERT (France) to prove optimality of the best solution known.

If  you find any  mistake  or ambiguity in  this  data, please report  it to
Thomas.Schiex@toulouse.inra.fr.

---

The  fourth directory holds  the SUBCELAR7  instances which  have been
extracted from  the CELAR7 instance by  the combinatorial optimization
group of ONERA/CERT (France) to compute a problem lower bound.
