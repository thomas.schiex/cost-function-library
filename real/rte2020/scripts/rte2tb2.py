###########################################
# Challenge ROADEF 2020 by RTE
###########################################

# input:
# - problem file in json format
# - CPU-time limit in seconds
# output:
#  - best solution found in format of the challenge

VERBOSITY = -1    # should be -1, use 0 if you want to see default solver output

MATCHING = False   # if True then add two-intervention excess corrections using a greedy matching heuristic (can be very slow to generate)

import sys
def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

import json
import numpy
import lzma

if sys.argv[1].rfind(".xz") != -1:
    f = lzma.open(sys.argv[1])
    json_bytes = f.read()
    stri = json_bytes.decode('utf-8')
    data = json.loads(stri)
else:
    f = open(sys.argv[1])
    data = json.load(f)

CPUTIMELIMIT = int(sys.argv[2])

n = len(data['Interventions'])
T = data['T']
r = len(data['Resources'])
e = len(data['Exclusions'])
s = max(data['Scenarios_number'])
quantile = float(data['Quantile'])
alpha = float(data['Alpha'])
eprint('Nb_interventions:',n,'T:',T,'Nb_resources:',r,'Nb_exclusions:',e,'Max_nb_scenarios:',s,'Quantile:',quantile,'Alpha:',alpha)

# you can redefine quantile and alpha from the command line if needed
if len(sys.argv) > 3:
    quantile = float(sys.argv[3])
if len(sys.argv) > 4:
    alpha = float(sys.argv[4])

from pytoulbar2 import CFN

MAXCOST_VALUE = 1e9
top = int(MAXCOST_VALUE)
#eprint(top)

PRECISION = 1e5   # 1e-5 tolerance

MULTWORKLOAD = int(PRECISION)
def intworkload(i):
    return int(i * MULTWORKLOAD + 0.5)

MULTCOST = 100
def intcost(c):
    return int(c * MULTCOST + 0.5)

Problem = CFN(top, vac = 1)    # vac = 1  ## add this option to perform VAC in preprocessing # vns = -1 ## add this option to perform variable neighborhood search

# make decision variables: one start-time variable for each intervention
vars = []
dom = []
for key, value in data['Interventions'].items():
    vars.append(key)
    tmax = int(value['tmax'])
    realtmax = 1+max(t for t in range(tmax) if t+int(value['Delta'][t]) <= T)
    assert(len([t for t in range(realtmax) if t+int(value['Delta'][t]) <= T]) == realtmax) # assume all domain values before realtmax are authorised
    #eprint(key, tmax, realtmax)
    d = min(T, realtmax)
    dom.append(d)
    Problem.AddVariable(key, ['v'+str(t+1) for t in range(d)])

# create resource constraints
for key, value in data['Resources'].items():
    for t in range(T):
        scope = [i for i,var in enumerate(vars) if key in data['Interventions'][var]['workload']]
        if len(scope) > 0:
            params = ''
            paramsneg = ''
            realscope = []
            totalweight = 0
            fixedweight = 0
            for i in scope:
                nbval = 0
                paramval = ''
                paramvalneg = ''
                maxweight = 0
                for t2 in range(dom[i]):
                    if (t2 <= t) and (t < t2 + int(data['Interventions'][vars[i]]['Delta'][t2])) and (str(t+1) in data['Interventions'][vars[i]]['workload'][key]) and (str(t2+1) in data['Interventions'][vars[i]]['workload'][key][str(t+1)]) and intworkload(data['Interventions'][vars[i]]['workload'][key][str(t+1)][str(t2+1)]) > 0:
                        nbval += 1
                        weight = intworkload(data['Interventions'][vars[i]]['workload'][key][str(t+1)][str(t2+1)])
                        maxweight = max(maxweight, weight)
                        paramval += ' ' + str(t2) + ' ' + str(weight)
                        paramvalneg += ' ' + str(t2) + ' ' + str(-weight)
                if nbval > 0:
                    if dom[i]>1:
                        realscope.append(i)
                        params += ' ' + str(nbval) + paramval
                        paramsneg += ' ' + str(nbval) + paramvalneg
                        totalweight += maxweight
                    else:
                        assert(weight == maxweight)
                        fixedweight += weight
            if len(params) > 0:
                mymin = intworkload(value['min'][t]) - fixedweight
                if mymin > 0:
                    #eprint(realscope, str(mymin) + params)
                    Problem.CFN.wcsp.postKnapsackConstraint(realscope, str(mymin) + params, False, True)
                mymax = intworkload(value['max'][t]) - fixedweight
                assert(mymax >= 0)
                if totalweight > mymax:
                    #eprint(realscope, str(-mymax) + paramsneg)
                    Problem.CFN.wcsp.postKnapsackConstraint(realscope, str(-mymax) + paramsneg, False, True)

#create exclusion constraints
exclusion = {}
for key, value in data['Exclusions'].items():
    assert(len(value) == 3)
    if len(data['Seasons'][value[2]]) >= 1:
        #eprint(data['Seasons'][value[2]])
        var1 = value[0]
        i1 = vars.index(var1)
        var2 = value[1]
        i2 = vars.index(var2)
        scope = [i1, i2]
        exclusion[(i1,i2)] = True
        exclusion[(i2,i1)] = True
        costs = [0]*dom[i1]*dom[i2]
        used = False
        for t1 in range(dom[i1]):
            for t2 in range(dom[i2]):
                t1end = t1 + int(data['Interventions'][var1]['Delta'][t1])
                t2end = t2 + int(data['Interventions'][var2]['Delta'][t2])
                if t1end > t2 and t2end > t1 and any(int(t)>=t1+1 and int(t)<t1end+1 and int(t)>=t2+1 and int(t)<t2end+1 for t in data['Seasons'][value[2]]):
                    used = True
                    costs[t1*dom[i2]+t2] = top
        if used:
            #eprint(scope, costs)
            Problem.AddFunction(scope, costs)

#add individual approximate risk value per intervention
for i,var in enumerate(vars):
    scope = [i]
    costs = [0]*dom[i]
    for t in range(dom[i]):
        mean = 0
        excess = 0
        for d in range(int(data['Interventions'][var]['Delta'][t])):
            if (t+d < T):
                total = 0
                nbs = int(data['Scenarios_number'][t+d])
                scenario = [0]*nbs
                for s in range(nbs):
                    risk = float(data['Interventions'][var]['risk'][str(t+d+1)][str(t+1)][s])
                    assert(risk >= 0.)
                    total += risk
                    scenario[s] = risk
                q = numpy.quantile(scenario, quantile, interpolation='higher')
                #eprint(var, t, d, scenario, q, total/nbs, max(0, q - total / nbs))
                mean += total / nbs
                excess += max(0, q - total / nbs)
        #eprint(mean,excess)
        costs[t] = intcost(alpha * mean + (1 - alpha) * excess)
    #eprint(scope, costs)
    Problem.AddFunction(scope, costs)

#add risk excess error corrections as a two-intervention matching problem solved greedily
if MATCHING:
    pairs = []
    for i,var1 in enumerate(vars):
        for j, var2 in enumerate(vars):
            if j>i and (i,j) not in exclusion:
                scope = [i, j]
                costs = [0]*dom[i]*dom[j]
                used = False
                maxerror = -top
                for t1 in range(dom[i]):
                    for t2 in range(dom[j]):
                        excess = 0
                        excess1 = 0
                        excess2 = 0
                        for t in range(max(t1,t2), min(t1+int(data['Interventions'][var1]['Delta'][t1]), t2+int(data['Interventions'][var2]['Delta'][t2]))):
                                if (t < T):
                                    total1 = 0
                                    total2 = 0
                                    nbs = int(data['Scenarios_number'][t])
                                    scenario = [0]*nbs
                                    scenario1 = [0]*nbs
                                    scenario2 = [0]*nbs
                                    for s in range(nbs):
                                        risk = float(data['Interventions'][var1]['risk'][str(t+1)][str(t1+1)][s])
                                        assert(risk >= 0.)
                                        total1 += risk
                                        scenario1[s] = risk
                                        scenario[s] = risk
                                        risk = float(data['Interventions'][var2]['risk'][str(t+1)][str(t2+1)][s])
                                        assert (risk >= 0.)
                                        total2 += risk
                                        scenario2[s] = risk
                                        scenario[s] += risk
                                    q1 = numpy.quantile(scenario1, quantile, interpolation='higher')
                                    q2 = numpy.quantile(scenario2, quantile, interpolation='higher')
                                    q = numpy.quantile(scenario, quantile, interpolation='higher')
                                    #eprint(var, t, d, scenario, q, total/nbs, max(0, q - total / nbs))
                                    excess += max(0, q - (total1+total2) / nbs)
                                    excess1 += max(0, q1 - total1 / nbs)
                                    excess2 += max(0, q2 - total2 / nbs)
                        #eprint(excess1+excess2 - excess)
                        error = intcost((1 - alpha) * (excess1 + excess2 - excess))
                        maxerror = max(maxerror, error)
                        if (error != 0):
                            used = True
                            costs[t1*dom[j]+t2] = -error
                if used:
                    pairs.append((scope, costs, maxerror))

    #eprint("Pairwise interactions computed..")
    pairs.sort(key = lambda t: t[2], reverse = True)
    #eprint(pairs)
    varused = {}
    # greedy selection of biggest excess estimate errors
    for scope, costs, maxerror in pairs:
        if scope[0] not in varused and scope[1] not in varused:
            #eprint(scope, costs)
            varused[scope[0]] = True
            varused[scope[1]] = True
            Problem.AddFunction(scope, costs)
        if len(varused) == len(vars):
            break

Problem.Option.verbose = VERBOSITY

#Problem.Option.FullEAC = False
Problem.Option.VACthreshold = True
Problem.Option.useRASPS = 1
Problem.Option.RASPSnbBacktracks = 1000
Problem.Option.RASPSreset = True
Problem.Option.RASPSangle = 5

#Problem.Option.showSolutions = 3
#Problem.Dump('data.wcsp')
#Problem.NoPreprocessing()

Problem.CFN.timer(CPUTIMELIMIT)   # CPU-time limit in seconds

try:
    res = Problem.Solve()
except Problem.SolverOut:
    if len(Problem.CFN.solutions()) > 0:
        res = [Problem.CFN.solution(), Problem.CFN.wcsp.getDPrimalBound(), len(Problem.CFN.solutions())]
    else:
        res = None
if res and len(res[0]) == len(vars) == n:
    for i in range(n):
        print(vars[i], 1+res[0][i])
