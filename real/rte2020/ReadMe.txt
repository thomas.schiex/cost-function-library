
ROADEF/EURO Challenge 2020: Maintenance planning optimization problem in collaboration with RTE

Data and RTE solution checker come from:
https://github.com/rte-france/challenge-roadef-2020

Description of the problem and results from the competitors are available at:
https://www.roadef.org/challenge/2020/en/index.php

Usage of python script rte2tb2.py:
>python3 ./scripts/rte2tb2.py ./data/example1.json.xz cputime 1>out 2>example1.sol

