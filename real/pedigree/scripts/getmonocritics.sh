#!/bin/sh

# getmonocritics.sh problem.pre

problem=`basename $1 .pre`

# check if problem is inconsistent
satisfied=`./toolbar -p3 ${problem}.wcsp -u1 | awk '/Optimum/{print $2} /Lower bound/{print $3}'`
if (("$satisfied" == 0)) ; then
 exit
fi

# performs genotype elimination in order to get a reduced list of potential monocritics
critics=`./toolbar -v -p3 ${problem}.wcsp -u2 -o2 -n0 -v3 | awk 'NR==FNR&&/^1 [0-9]* 1 1$/{typed[$2]=1} NR!=FNR&&/^[0-9]*: ([*0-9]* )*$/&&($1+0 in typed){dom=$0;sub(".*: ","",dom);gsub("[*] ","",dom);if (length(dom)>2) print "typed["$1+0"]=1;"}' ${problem}.wcsp -`

# check if there are potential monocritics and not multicritics
if ((${#critics} == 0)) ; then
 echo "no monocritics but multi-critics!"
 exit
fi

#echo "potential critics are" $critics

# find the true monocritics
for e in `awk 'BEGIN{'"${critics}"'} NF==2 && ok{print var"="$1;ok=0} NF==4 && $1==1 && $2 in typed && $3==1 && $4==1 {var=$2;ok=1}' ${problem}.wcsp` ; do 
#    echo "$e"
  ./toolbar -v -p3 ${problem}.wcsp -u2 -R$e | awk -f solution2cp.awk ${problem}.cp - | awk -f getcritics.awk $1 -
done
