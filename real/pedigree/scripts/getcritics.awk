BEGIN {
    n = 0;
}

FNR == NR && NF == 7 && $6 != 0 && $7 != 0 {
    if ($6 < $7) typing[n]= $6 "" $7;
    else typing[n]= $7 "" $6;
}

FNR == NR && NF == 7 {
    name[n] = $2;
    n++;
}

FNR != NR && /solution:/ {
    for (num in typing) {
	if ($(num+3) != typing[num]) {
	    res["individual "name[num]" : typing "typing[num]] = $(num+2);
	}
    }
}

END {
    for (e in res) print e; # " -> "res[e];
}
