#!/bin/sh

# find all different alleles in a pedigree

# nballeles.sh pedigree.pre

awk '{print $6; print $7}' $1 | sort -u
