
# convert a pedigree in pre format into WCSP in cp format

# awk -f pedigree2cp.awk pedigree.pre [number of alleles] [1:model using phase,0:no phase] [initial upper bound or zero (no upper bound)] > pedigree.cp

# use cp2wcsp.awk to convert pedigree.cp into pedigree.wcsp readable by toolbar

# e.g. awk -f pedigree2cp.awk eye.pre 6 0 0 > eye.cp

function min(x,y) {
    if (x < y) return(x); 
    else return(y);
}

function max(x,y) {
    if (x > y) return(x); 
    else return(y);
}

BEGIN {
    nballele = ARGV[2];
    ARGV[2] = "";
    phase = 0;
    if (3 in ARGV) {
	phase = ARGV[3];
	ARGV[3] = "";
    }
    ub = 0;
    if (4 in ARGV) {
	ub = ARGV[4];
	ARGV[4] = "";
    }
    printf("NBALLELE=%d%s%s\n", nballele,(phase)?"PHASE":"",(ub==0)?"":(" " ub));

    for (i=1; i<= nballele; i++) {
	for (j=i; j <= nballele; j++) {
	    low[i "" j] = i;
	    high[i "" j] = j;
	}
    }
    
    individu = 0;
}

NF >= 7 {
    individu++;
    if (phase) {
	printf("f%s", $2);
	for (i=1; i<=nballele; i++) {
	    printf(" %d", i);
	}
	print "";
	printf("m%s", $2);
	for (i=1; i<=nballele; i++) {
	    printf(" %d", i);
	}
	print "";
    }
    mylow = min($6,$7);
    myhigh = max($6,$7);
    if (ub && mylow>0) {
	printf("x%s %d%d\n", $2, mylow, myhigh);
    } else {
	printf("x%s", $2);
	for (i=1; i<=nballele; i++) {
	    for (j=i; j<=nballele; j++) {
		printf(" %d%d", i, j);
	    }
	}
	print "";
	if (myhigh > 0) {
	    print "x" $2,1;
	    for (i=1; i<=nballele; i++) {
		for (j=i; j<=nballele; j++) {
		    if ((mylow==0 && (myhigh==i || myhigh==j)) || (mylow==i && myhigh==j)) {
			print i "" j,0;
		    }
		}
	    }	
	}
    }
    if (phase) {
	print "hard(low(x" $2 "," nballele ") == min(f" $2 ",m" $2 ") && high(x" $2 "," nballele ") == max(f" $2 ",m" $2 "))";
    }
    name[individu] = $2;
    if ($2 in reversename) {
	print "ERROR AT LINE " NF " : individual number declares twice!";
	exit;
    }
    reversename[$2] = individu;
    sex[individu] = $5;
    father[individu] = $3;
    mother[individu] = $4;
}

END {
    for (idx=1; idx<=individu; idx++) {
	if (!((father[idx] == 0) || 
	      (mother[idx] == 0) || 
	      ((sex[reversename[father[idx]]]==1 && sex[reversename[mother[idx]]]==2) || 
	       (sex[reversename[father[idx]]]==2 && sex[reversename[mother[idx]]]==1)))) {
	    print "#warning at line",NR,": parents",father[idx],mother[idx],"of",name[idx],"not from different sex!",sex[reversename[father[idx]]],sex[reversename[mother[idx]]];
	}
	if (father[idx] != 0 && mother[idx] != 0) {
	    if (sex[reversename[father[idx]]]==2) {
		tmp = father[idx];
		father[idx] = mother[idx];
		mother[idx] = tmp;
	    }
	} else if (father[idx] != 0 && mother[idx] == 0) {
	    if (sex[reversename[father[idx]]]==2) {
		tmp = father[idx];
		father[idx] = mother[idx];
		mother[idx] = tmp;
	    }
	} else if (father[idx] == 0 && mother[idx] != 0) {
	    if (sex[reversename[mother[idx]]]==1) {
		tmp = father[idx];
		father[idx] = mother[idx];
		mother[idx] = tmp;
	    }
	}
	if (father[idx] != 0 && mother[idx] != 0) {
	    print "hard((low(x" name[idx] "," nballele ") == low(x" father[idx] "," nballele ") && high(x" name[idx] "," nballele ") == low(x" mother[idx] "," nballele ")) || (low(x" name[idx] "," nballele ") == high(x" father[idx] "," nballele ") && high(x" name[idx] "," nballele ") == low(x" mother[idx] "," nballele ")) || (low(x" name[idx] "," nballele ") == low(x" father[idx] "," nballele ") && high(x" name[idx] "," nballele ") == high(x" mother[idx] "," nballele ")) || (low(x" name[idx] "," nballele ") == high(x" father[idx] "," nballele ") && high(x" name[idx] "," nballele ") == high(x" mother[idx] "," nballele ")) || (low(x" name[idx] "," nballele ") == low(x" mother[idx] "," nballele ") && high(x" name[idx] "," nballele ") == low(x" father[idx] "," nballele ")) || (low(x" name[idx] "," nballele ") == high(x" mother[idx] "," nballele ") && high(x" name[idx] "," nballele ") == low(x" father[idx] "," nballele ")) || (low(x" name[idx] "," nballele ") == low(x" mother[idx] "," nballele ") && high(x" name[idx] "," nballele ") == high(x" father[idx] "," nballele ")) || (low(x" name[idx] "," nballele ") == high(x" mother[idx] "," nballele ") && high(x" name[idx] "," nballele ") == high(x" father[idx] "," nballele ")))";
	    if (phase) {
		print "hard(f" name[idx] " == f" father[idx] " || f" name[idx] " == m" father[idx] ")";
		print "hard(m" name[idx] " == f" mother[idx] " || m" name[idx] " == m" mother[idx] ")";
	    }
	}
	if (father[idx] != 0 && mother[idx] == 0) {
	    print "hard(low(x" name[idx] "," nballele ") == low(x" father[idx] "," nballele ") || low(x" name[idx] "," nballele ") == high(x" father[idx] "," nballele ") || high(x" name[idx] "," nballele ") == low(x" father[idx] "," nballele ") || high(x" name[idx] "," nballele ") == high(x" father[idx] "," nballele "))";
	    if (phase) {
		print "hard(f" name[idx] " == f" father[idx] " || f" name[idx] " == m" father[idx] ")";
	    }
	}
	if (father[idx] == 0 && mother[idx] != 0) {
	    print "hard(low(x" name[idx] "," nballele ") == low(x" mother[idx] "," nballele ") || low(x" name[idx] "," nballele ") == high(x" mother[idx] "," nballele ") || high(x" name[idx] "," nballele ") == low(x" mother[idx] "," nballele ") || high(x" name[idx] "," nballele ") == high(x" mother[idx] "," nballele "))";
	    if (phase) {
		print "hard(m" name[idx] " == f" mother[idx] " || m" name[idx] " == m" mother[idx] ")";
	    }
	}
    }
}
