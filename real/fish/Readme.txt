These two instances model a sub-problem that occurs during the genomic assembly of genomes with one or more whole-genome-duplication events.
