#!/bin/bash

ubGlobal=10000000
nbDays=30
nbNurses=28
nbShifts=3
nbVariables=$(( $nbDays * $nbNurses ))
nbValues=$(( $nbShifts + 1 ))
nbConstraints=$(( $nbNurses*(1 + 2) + $nbDays*(1 + 1 + (2 + 2)) + $nbDays*$nbNurses*2+12 + $nbNurses + $nbNurses + $nbNurses + $nbNurses + $nbNurses + $nbNurses + ((3 + 2 + 1 + 3 + 3 + 5 + 3 + 2 + 1 + 1) + (3 + 1 + 1 + 4 + 3 + 1 + 4 + 2 + 2) + (2 + 3 + 3 + 2 + 4 + 5 + 5 + 5 + 2)) + $nbNurses + $nbNurses))

function writeHeader {
	echo "NRP_IKIGAMI1_30Days_28Nurses_3Shifts" $nbVariables $nbValues $nbConstraints $ubGlobal
	for i in `seq 1 $nbVariables`; do
		echo -n $nbValues" "
	done
	echo ""
}

function writeNurse {
	for nurse in `seq 1 $nbNurses` ; do
		echo -n $nbDays
		for day in `seq 1 $nbDays` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong hard 1000"
		echo "3 0 1 2"
		if [[ $nurse -eq 19 ]] ; then
			echo "0 22"
		else
			echo "0 21"
		fi

		#0-6
		echo -n "7"
		for day in `seq 1 7` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong hard 1000"
		echo "3 0 1 2"
		echo "0 6"
		#8-14
		echo -n "7"
		for day in `seq 8 14` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong hard 1000"
		echo "3 0 1 2"
		echo "0 6"
		#15-21
		echo -n "7"
		for day in `seq 15 21` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong hard 1000"
		echo "3 0 1 2"
		echo "0 6"
		#22-28
		echo -n "7"
		for day in `seq 22 28` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong hard 1000"
		echo "3 0 1 2"
		echo "0 6"
	done
	
	### Day Shift
	MinDay=( 9 10 10 10 10 10 9 8 9 10 9 10 10 10 9 10 9 9 18 10 10 10 9 9 10 9 8 14)
	MaxDay=( 12 13 13 13 13 13 12 11 12 13 12 13 13 13 12 13 12 12 20 13 13 13 12 12 13 12 11 17)
	for nurse in `seq 1 $nbNurses` ; do
		echo -n $nbDays
		idnurse=$(( $nurse - 1))
		for day in `seq 1 $nbDays` ; do	
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 10000"
		echo "1 0"
		echo ${MinDay[$idnurse]}" "${MaxDay[$idnurse]}
	done
	
	### NightShift
	MinNight=( 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 0 4 4 4 4 4 4 4 4 2 )
	MaxNight=( 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 0 5 5 5 5 5 5 5 5 3 )
	for nurse in `seq 1 $nbNurses` ; do
		echo -n $nbDays
		idnurse=$(( $nurse - 1))
		for day in `seq 1 $nbDays` ; do	
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 10000"
		echo "1 1"
		echo ${MinNight[$idnurse]}" "${MaxNight[$idnurse]}
	done
	
}

function writeNurseWithBalancing {
	for nurse in `seq 1 $nbNurses` ; do
		echo -n $nbDays
		for day in `seq 1 $nbDays` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wbalancedamong hard 1000 4"
		echo "3 0 1 2"
		if [[ $nurse -eq 19 ]] ; then
			echo "0 22"
		else
			echo "0 21"
		fi
		#0-6
		echo -n 7
		for day in `seq 1 7` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " hard 1000"
		echo "0 6"
		#8-14
		echo -n 7
		for day in `seq 8 14` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " hard 1000"
		echo "0 6"
		#15-21
		echo -n 7
		for day in `seq 15 21` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " hard 1000"
		echo "0 6"
		#22-28
		echo -n 7
		for day in `seq 22 28` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " hard 1000"
		echo "0 6"
	done
	
	### Day Shift
	MinDay=( 9 10 10 10 10 10 9 8 9 10 9 10 10 10 9 10 9 9 18 10 10 10 9 9 10 9 8 14)
	MaxDay=( 12 13 13 13 13 13 12 11 12 13 12 13 13 13 12 13 12 12 20 13 13 13 12 12 13 12 11 17)
	for nurse in `seq 1 $nbNurses` ; do
		echo -n $nbDays
		idnurse=$(( $nurse - 1))
		for day in `seq 1 $nbDays` ; do	
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 10000"
		echo "1 0"
		echo ${MinDay[$idnurse]}" "${MaxDay[$idnurse]}
	done
	
	### NightShift
	MinNight=( 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 0 4 4 4 4 4 4 4 4 2 )
	MaxNight=( 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 0 5 5 5 5 5 5 5 5 3 )
	for nurse in `seq 1 $nbNurses` ; do
		echo -n $nbDays
		idnurse=$(( $nurse - 1))
		for day in `seq 1 $nbDays` ; do	
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 10000"
		echo "1 1"
		echo ${MinNight[$idnurse]}" "${MaxNight[$idnurse]}
	done
	
}


function writeDaily {
	############ D
	
	### ALL
	AllMinD=( 10 9 9 9 10 12 10 10 9 9 10 10 10 10 10 9 9 10 12 10 10 10 9 9 10 12 10 10 10 9 )
	AllMaxD=( 11 10 9 9 11 14 11 11 10 9 11 11 11 11 11 10 9 11 14 11 11 11 10 9 11 16 11 11 11 10 )
	
	for day in `seq 1 $nbDays` ; do
		echo -n $nbNurses
		idday=$(( $day - 1))
		for nurse in `seq 1 $nbNurses` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 0"
		echo ${AllMinD[$idday]}" "${AllMaxD[$idday]}
	done
	
	### GROUP A
	GroupAMinD=( 3 3 3 3 3 4 3 3 3 3 3 3 3 3 3 3 3 3 4 3 3 3 3 3 3 4 3 3 3 3 )
	GroupAMaxD=( 4 4 3 3 4 5 4 4 4 3 4 4 4 4 4 4 3 4 5 4 4 4 4 3 4 6 4 4 4 4 )
	
	for day in `seq 1 $nbDays` ; do
		echo -n 10
		idday=$(( $day - 1))
		for nurse in `seq 1 10` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 0"
		echo ${GroupAMinD[$idday]}" "${GroupAMaxD[$idday]}
	done
	
	### GROUP B
	GroupBMinD=( 3 3 3 3 3 4 3 3 3 3 3 3 3 3 3 3 3 3 4 3 3 3 3 3 3 4 3 3 3 3 )
	GroupBMaxD=( 4 4 3 3 4 5 4 4 4 3 4 4 4 4 4 4 3 4 5 4 4 4 4 3 4 6 4 4 4 4 )
	
	 	 	
	for day in `seq 1 $nbDays` ; do
		echo -n 9
		idday=$(( $day - 1))
		for nurse in `seq 11 19` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 0"
		echo ${GroupBMinD[$idday]}" "${GroupBMaxD[$idday]}
	done	
	
	### GROUP C
	GroupCMinD=( 3 3 3 3 3 4 3 3 3 3 3 3 3 3 3 3 3 3 4 3 3 3 3 3 3 4 3 3 3 3 )
	GroupCMaxD=( 4 4 3 3 4 5 4 4 4 3 4 4 4 4 4 4 3 4 5 4 4 4 4 3 4 6 4 4 4 4 )
	
	 	 	
	for day in `seq 1 $nbDays` ; do
		echo -n 9
		idday=$(( $day - 1))
		for nurse in `seq 20 28` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 0"
		echo ${GroupCMinD[$idday]}" "${GroupCMaxD[$idday]}
	done
	
	### GROUP 1-7 Aa
	GroupAaMinD=( 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 )
	GroupAaMaxD=( 4 4 3 3 4 4 4 4 4 3 4 4 4 4 4 4 3 4 4 4 4 4 4 3 4 5 4 4 4 4 )
	
	for day in `seq 1 $nbDays` ; do
		echo -n 7
		idday=$(( $day - 1))
		for nurse in `seq 1 7` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 0"
		echo ${GroupAaMinD[$idday]}" "${GroupAaMaxD[$idday]}
	done
	
	### GROUP 11-17 Bb
	GroupBbMinD=( 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 )
	GroupBbMaxD=( 4 4 3 3 4 4 4 4 4 3 4 4 4 4 4 4 3 4 4 4 4 4 4 3 4 5 4 4 4 4 )
	
	for day in `seq 1 $nbDays` ; do
		echo -n 7
		idday=$(( $day - 1))
		for nurse in `seq 11 17` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 0"
		echo ${GroupBbMinD[$idday]}" "${GroupBbMaxD[$idday]}
	done
	
	### GROUP 20-26 Cc
	GroupCcMinD=( 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 )
	GroupCcMaxD=( 4 4 3 3 4 4 4 4 4 3 4 4 4 4 4 4 3 4 4 4 4 4 4 3 4 5 4 4 4 4 )
	
	for day in `seq 1 $nbDays` ; do
		echo -n 7
		idday=$(( $day - 1))
		for nurse in `seq 20 26` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 0"
		echo ${GroupCcMinD[$idday]}" "${GroupCcMaxD[$idday]}
	done
	
	############ N
	
	### ALL
	
	for day in `seq 1 $nbDays` ; do
		echo -n $nbNurses
		for nurse in `seq 1 $nbNurses` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 1"
		echo "4 4"
	done
	
	### GROUP A
	GroupAMinN=( 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 )
	GroupAMaxN=( 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 ) 
	
	for day in `seq 1 $nbDays` ; do
		echo -n 10
		idday=$(( $day - 1))
		for nurse in `seq 1 10` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 1"
		echo ${GroupAMinN[$idday]}" "${GroupAMaxN[$idday]}
	done
	
	### GROUP B
	GroupBMinN=( 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 )
	GroupBMaxN=( 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 ) 
	
	for day in `seq 1 $nbDays` ; do
		echo -n 9
		idday=$(( $day - 1))
		for nurse in `seq 11 19` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 1"
		echo ${GroupBMinN[$idday]}" "${GroupBMaxN[$idday]}
	done
	
	
	### GROUP C
	GroupCMinN=( 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 )
	GroupCMaxN=( 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 ) 
	
	for day in `seq 1 $nbDays` ; do
		echo -n 9
		idday=$(( $day - 1))
		for nurse in `seq 20 28` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 1"
		echo ${GroupCMinN[$idday]}" "${GroupCMaxN[$idday]}
	done
	
	### GROUP Aa
	GroupAaMinN=( 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 )
	GroupAaMaxN=( 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 ) 
	
	for day in `seq 1 $nbDays` ; do
		echo -n 7
		idday=$(( $day - 1))
		for nurse in `seq 1 7` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 1"
		echo ${GroupAaMinN[$idday]}" "${GroupAaMaxN[$idday]}
	done
	
	### GROUP Bb
	GroupBbMinN=( 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 )
	GroupBbMaxN=( 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 ) 
	
	for day in `seq 1 $nbDays` ; do
		echo -n 7
		idday=$(( $day - 1))
		for nurse in `seq 11 17` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 1"
		echo ${GroupBbMinN[$idday]}" "${GroupBbMaxN[$idday]}
	done
	
	
	### GROUP Cc
	GroupCcMinN=( 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 )
	GroupCcMaxN=( 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 ) 
	
	for day in `seq 1 $nbDays` ; do
		echo -n 7
		idday=$(( $day - 1))
		for nurse in `seq 20 26` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 1"
		echo ${GroupCcMinN[$idday]}" "${GroupCcMaxN[$idday]}
	done
	
	### Skilled
	nbSkilled=7
	SkilledNurse=( 1 2 3 11 12 20 21 )
	
	for day in `seq 1 $nbDays` ; do
		echo -n $nbSkilled
		idday=$(( $day - 1))
		for nurse in `seq 1 $nbSkilled` ; do
			posNurse=$(( $nurse - 1))
			idnurse=${SkilledNurse[$posNurse]}
			variable=$(( ($idnurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 0"
		echo "1 3"
	done
	
	for day in `seq 1 $nbDays` ; do
		echo -n $nbSkilled
		idday=$(( $day - 1))
		for nurse in `seq 1 $nbSkilled` ; do
		posNurse=$(( $nurse - 1))
		idnurse=${SkilledNurse[$posNurse]}
			variable=$(( ($idnurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 1"
		echo "1 2"
	done
	
	
	### Recent
	nbRecent=7
	RecentNurse=( 8 9 10 18 19 27 28 )
	
	for day in `seq 1 $nbDays` ; do
		echo -n $nbRecent
		idday=$(( $day - 1 ))
		for nurse in `seq 1 $nbRecent` ; do
			posNurse=$(( $nurse - 1))
			idnurse=${RecentNurse[$posNurse]}
			variable=$(( ($idnurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 0"
		echo "0 7"
	done
	
	for day in `seq 1 $nbDays` ; do
		echo -n $nbRecent
		idday=$(( $day - 1))
		for nurse in `seq 1 $nbRecent` ; do
			posNurse=$(( $nurse - 1))
			idnurse=${RecentNurse[$posNurse]}
			variable=$(( ($idnurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 1"
		echo "0 1"
	done
}


function writeDailyWithNested {
	############ D
	AllMinD=( 		10 9 9 9 10 12 10 10 9 9 10 10 10 10 10 9 9 10 12 10 10 10 9 9 10 12 10 10 10 9 )
	AllMaxD=( 		11 10 9 9 11 14 11 11 10 9 11 11 11 11 11 10 9 11 14 11 11 11 10 9 11 16 11 11 11 10 )
	GroupAMinD=( 	3 3 3 3 3 4 3 3 3 3 3 3 3 3 3 3 3 3 4 3 3 3 3 3 3 4 3 3 3 3 )
	GroupAMaxD=( 	4 4 3 3 4 5 4 4 4 3 4 4 4 4 4 4 3 4 5 4 4 4 4 3 4 6 4 4 4 4 )
	GroupBMinD=( 	3 3 3 3 3 4 3 3 3 3 3 3 3 3 3 3 3 3 4 3 3 3 3 3 3 4 3 3 3 3 )
	GroupBMaxD=(	4 4 3 3 4 5 4 4 4 3 4 4 4 4 4 4 3 4 5 4 4 4 4 3 4 6 4 4 4 4 )
	GroupCMinD=( 	3 3 3 3 3 4 3 3 3 3 3 3 3 3 3 3 3 3 4 3 3 3 3 3 3 4 3 3 3 3 )
	GroupCMaxD=( 	4 4 3 3 4 5 4 4 4 3 4 4 4 4 4 4 3 4 5 4 4 4 4 3 4 6 4 4 4 4 )
	GroupAaMinD=( 	2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 )
	GroupAaMaxD=( 	4 4 3 3 4 4 4 4 4 3 4 4 4 4 4 4 3 4 4 4 4 4 4 3 4 5 4 4 4 4 )
	GroupBbMinD=( 	2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 )
	GroupBbMaxD=( 	4 4 3 3 4 4 4 4 4 3 4 4 4 4 4 4 3 4 4 4 4 4 4 3 4 5 4 4 4 4 )
	GroupCcMinD=( 	2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 )
	GroupCcMaxD=( 	4 4 3 3 4 4 4 4 4 3 4 4 4 4 4 4 3 4 4 4 4 4 4 3 4 5 4 4 4 4 )

	### ALL
	for day in `seq 1 $nbDays` ; do
		idday=$(( $day - 1 ))
		echo -n $nbNurses
		for nurse in `seq 1 $nbNurses` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 whierarchicalamong lin 1 6"
		echo "1 0"
		echo ${AllMinD[$idday]}" "${AllMaxD[$idday]}
		###
		echo "6"
		echo "All >> A"
		echo "A >> Aa"
		echo "All >> B"
		echo "B >> Bb"
		echo "All >> C"
		echo "C >> Cc"
		###
		echo -n "10"
		for nurse in `seq 1 10` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done 
		echo " lin 1 A"
		echo ${GroupAMinD[$idday]}" "${GroupAMaxD[$idday]}
		###
		echo -n "7"
		for nurse in `seq 1 7` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done 
		echo " lin 1 Aa"
		echo ${GroupAaMinD[$idday]}" "${GroupAaMaxD[$idday]}
		###
		echo -n "9"
		for nurse in `seq 11 19` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done 
		echo " lin 1 B"
		echo ${GroupBMinD[$idday]}" "${GroupBMaxD[$idday]}
		###
		echo -n "7"
		for nurse in `seq 11 17` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done 
		echo " lin 1 Bb"
		echo ${GroupBbMinD[$idday]}" "${GroupBbMaxD[$idday]}
		###
		echo -n "9"
		for nurse in `seq 20 28` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done 
		echo " lin 1 C"
		echo ${GroupCMinD[$idday]}" "${GroupCMaxD[$idday]}
		###
		echo -n "7"
		for nurse in `seq 20 26` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done 
		echo " lin 1 Cc"
		echo ${GroupCcMinD[$idday]}" "${GroupCcMaxD[$idday]}
	done
	
	############ N
	AllMinN=( 		4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 )
	AllMaxN=( 		4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 ) 
	GroupAMinN=( 	1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 )
	GroupAMaxN=( 	2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 ) 
	GroupAaMinN=( 	1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 )
	GroupAaMaxN=(	2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 ) 
	GroupBMinN=(	1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 )
	GroupBMaxN=(	2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 ) 
	GroupBbMinN=(	1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 )
	GroupBbMaxN=( 	2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 ) 
	GroupCMinN=( 	1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 )
	GroupCMaxN=( 	2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 ) 
	GroupCcMinN=( 	1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 )
	GroupCcMaxN=( 	2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 ) 
	### ALL
	for day in `seq 1 $nbDays` ; do
		idday=$(( $day - 1 ))
		echo -n $nbNurses
		for nurse in `seq 1 $nbNurses` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 whierarchicalamong lin 1 6"
		echo "1 1"
		echo ${AllMinN[$idday]}" "${AllMaxN[$idday]}
		###
		echo "6"
		echo "All >> A"
		echo "A >> Aa"
		echo "All >> B"
		echo "B >> Bb"
		echo "All >> C"
		echo "C >> Cc"
		###
		echo -n "10"
		for nurse in `seq 1 10` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done 
		echo " lin 1 A"
		echo ${GroupAMinN[$idday]}" "${GroupAMaxN[$idday]}
		###
		echo -n "7"
		for nurse in `seq 1 7` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done 
		echo " lin 1 Aa"
		echo ${GroupAaMinN[$idday]}" "${GroupAaMaxN[$idday]}
		###
		echo -n "9"
		for nurse in `seq 11 19` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done 
		echo " lin 1 B"
		echo ${GroupBMinN[$idday]}" "${GroupBMaxN[$idday]}
		###
		echo -n "7"
		for nurse in `seq 11 17` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done 
		echo " lin 1 Bb"
		echo ${GroupBbMinN[$idday]}" "${GroupBbMaxN[$idday]}
		###
		echo -n "9"
		for nurse in `seq 20 28` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done 
		echo " lin 1 C"
		echo ${GroupCMinN[$idday]}" "${GroupCMaxN[$idday]}
		###
		echo -n "7"
		for nurse in `seq 20 26` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done 
		echo " lin 1 Cc"
		echo ${GroupCcMinN[$idday]}" "${GroupCcMaxN[$idday]}
	done
	
	### Skilled
	nbSkilled=7
	SkilledNurse=( 1 2 3 11 12 20 21 )
	
	for day in `seq 1 $nbDays` ; do
		echo -n $nbSkilled
		idday=$(( $day - 1))
		for nurse in `seq 1 $nbSkilled` ; do
			posNurse=$(( $nurse - 1))
			idnurse=${SkilledNurse[$posNurse]}
			variable=$(( ($idnurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 0"
		echo "1 3"
	done
	
	for day in `seq 1 $nbDays` ; do
		echo -n $nbSkilled
		idday=$(( $day - 1))
		for nurse in `seq 1 $nbSkilled` ; do
		posNurse=$(( $nurse - 1))
		idnurse=${SkilledNurse[$posNurse]}
			variable=$(( ($idnurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 1"
		echo "1 2"
	done
	
	
	### Recent
	nbRecent=7
	RecentNurse=( 8 9 10 18 19 27 28 )
	
	for day in `seq 1 $nbDays` ; do
		echo -n $nbRecent
		idday=$(( $day - 1 ))
		for nurse in `seq 1 $nbRecent` ; do
			posNurse=$(( $nurse - 1))
			idnurse=${RecentNurse[$posNurse]}
			variable=$(( ($idnurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 0"
		echo "0 7"
	done
	
	for day in `seq 1 $nbDays` ; do
		echo -n $nbRecent
		idday=$(( $day - 1))
		for nurse in `seq 1 $nbRecent` ; do
			posNurse=$(( $nurse - 1))
			idnurse=${RecentNurse[$posNurse]}
			variable=$(( ($idnurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1"
		echo "1 1"
		echo "0 1"
	done
}


function writeRequest {
	#dayO=( 12 7 6 8 12 7 12 14 12 14 7 7 7 12 21 )  
	#nurseO=( 1 7 8 8 9 15 17 18 19 19 23 24 26 27 27 )
	Orequest=( 
   #1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 
	0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 #1
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 #2
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#3
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#4
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#5
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#6
	0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#7
	0 0 0 0 0 1 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#8
	0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#9
	0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#10
   #1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 
	0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#11
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#12
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#13
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#14
	0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#15
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#16
	0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#17
	0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#18
	0 0 0 0 0 0 0 0 0 0 0 1 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#19
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#20
   #1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#21
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#22
	0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#23
	0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#24
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#25
	0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#26
	0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0	#27
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0	#28
   #1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 
	)
	for nurse in `seq 1 $nbNurses` ; do
		for day in `seq 1 $nbDays` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			if [[ ${Orequest[$variable]} -eq 0 ]] ; then
				echo "1 "$variable" 0 1"
				echo "2 1000000"
			else
				echo "1 "$variable" 1000000 1"
				echo "2 0"
			fi
		done
	done
	
	Offrequest=( 
   #1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 
	1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 #1
	1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 #2
	0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 #3
	1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 #4
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 #5
	1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 #6
	0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 #7
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 #8
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0 0 0 #9
	0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 #10
   #1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 
	1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 #11
	0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 #12
	1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 #13
	0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 #14
	1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 1 1 #15
	0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 #16
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 #17
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 #18
	0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 #19
	0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 #20
   #1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 #21
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 #22
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 #23
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 #24
	1 1 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 #25
	1 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 #26
	1 1 0 0 0 0 0 0 1 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 #27
	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 #28
   #1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 
	)
	for nurse in `seq 1 $nbNurses` ; do
		for day in `seq 1 $nbDays` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			if [[ ${Offrequest[$variable]} -eq 1 ]] ; then
				echo "1 "$variable" 100 1"
				echo "3 0"
			else
				echo "1 "$variable" 0 0"
			fi
		done
	done
	
	#Other
	variable=$(( (16-1) + (2-1) * $nbDays ))
	echo "1 "$variable" 0 1"
	echo "1 100"
	
	variable=$(( (30-1) + (2-1) * $nbDays ))
	echo "1 "$variable" 0 1"
	echo "1 100"
	
	variable=$(( (25-1) + (8-1) * $nbDays ))
	echo "1 "$variable" 0 1"
	echo "0 100"
	
	variable=$(( (22-1) + (9-1) * $nbDays ))
	echo "1 "$variable" 100 1"
	echo "1 0"
	
	variable=$(( (24-1) + (13-1) * $nbDays ))
	echo "1 "$variable" 100 1"
	echo "1 0"
	
	variable=$(( (28-1) + (15-1) * $nbDays ))
	echo "1 "$variable" 100 1"
	echo "1 0"
	
	variable=$(( (11-1) + (16-1) * $nbDays ))
	echo "1 "$variable" 100 1"
	echo "1 0"
	
	variable=$(( (22-1) + (16-1) * $nbDays ))
	echo "1 "$variable" 100 1"
	echo "1 0"
	
	variable=$(( (24-1) + (23-1) * $nbDays ))
	echo "1 "$variable" 100 1"
	echo "0 0"
	
	variable=$(( (6-1) + (25-1) * $nbDays ))
	echo "1 "$variable" 100 1"
	echo "1 0"
	
	variable=$(( (24-1) + (25-1) * $nbDays ))
	echo "1 "$variable" 100 1"
	echo "1 0"
	
	variable=$(( (5-1) + (26-1) * $nbDays ))
	echo "1 "$variable" 100 1"
	echo "1 0"
	
	
		
}


function afterNight {
	for nurse in `seq 1 $nbNurses` ; do
		echo -n $nbDays
		for day in `seq 1 $nbDays` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wregular"
		echo "2"
		echo "1 0 0"
		echo "2 0 0 1 0"
		echo "8"
		echo "0 0 0 0"
		echo "0 1 1 0"
		echo "0 2 0 0"
		echo "0 3 0 0"
		echo "1 0 0 1000000"
		echo "1 1 1 1000000"
		echo "1 2 0 0"
		echo "1 3 0 0"
	done
}

function maxOff {
	for nurse in `seq 1 $nbNurses` ; do
		echo -n $nbDays
		for day in `seq 1 $nbDays` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wregular"
		echo "6"
		echo "1 0 0"
		echo "6 0 0 1 0 2 0 3 0 4 0 5 0"
		echo "24"
		echo "0 0 0 0"
		echo "0 1 0 0"
		echo "0 2 0 0"
		echo "0 3 1 0"
		echo "1 0 0 0"
		echo "1 1 0 0"
		echo "1 2 0 0"
		echo "1 3 2 0"
		echo "2 0 0 0"
		echo "2 1 0 0"
		echo "2 2 0 0"
		echo "2 3 3 0"
		echo "3 0 0 0"
		echo "3 1 0 0"
		echo "3 2 0 0"
		echo "3 3 4 0"
		echo "4 0 0 0"
		echo "4 1 0 0"
		echo "4 2 0 0"
		echo "4 3 5 0"
		echo "5 0 0 0"
		echo "5 1 0 0"
		echo "5 2 0 0"
		echo "5 3 5 1000000"
	done
}

function maxOn {
	for nurse in `seq 1 $nbNurses` ; do
		echo -n $nbDays
		for day in `seq 1 $nbDays` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wregular"
		echo "7"
		echo "1 0 0"
		echo "7 0 0 1 0 2 0 3 0 4 0 5 0 6 0"
		echo "28"
		echo "0 0 1 0"
		echo "0 1 1 0"
		echo "0 2 1 0"
		echo "0 3 0 0"
		echo "1 0 2 0"
		echo "1 1 2 0"
		echo "1 2 2 0"
		echo "1 3 0 0"
		echo "2 0 3 0"
		echo "2 1 3 0"
		echo "2 2 3 0"
		echo "2 3 0 0"
		echo "3 0 4 0"
		echo "3 1 4 0"
		echo "3 2 4 0"
		echo "3 3 0 0"
		echo "4 0 5 0"
		echo "4 1 5 0"
		echo "4 2 5 0"
		echo "4 3 0 0"
		echo "5 0 6 0"
		echo "5 1 6 0"
		echo "5 2 6 0"
		echo "5 3 0 0"
		echo "6 0 6 1000000"
		echo "6 1 6 1000000"
		echo "6 2 6 1000000"
		echo "6 3 0 0"
	done
}

function maxD {
	for nurse in `seq 1 $nbNurses` ; do
		echo -n $nbDays
		for day in `seq 1 $nbDays` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wregular"
		echo "5"
		echo "1 0 0"
		echo "5 0 0 1 0 2 0 3 0 4 0"
		echo "20"
		echo "0 0 1 0"
		echo "0 1 0 0"
		echo "0 2 0 0"
		echo "0 3 0 0"
		echo "1 0 2 0"
		echo "1 1 0 0"
		echo "1 2 0 0"
		echo "1 3 0 0"
		echo "2 0 3 0"
		echo "2 1 0 0"
		echo "2 2 0 0"
		echo "2 3 0 0"
		echo "3 0 4 0"
		echo "3 1 0 0"
		echo "3 2 0 0"
		echo "3 3 0 0"
		echo "4 0 4 1000000"
		echo "4 1 0 0"
		echo "4 2 0 0"
		echo "4 3 0 0"
	done
}

function cutWork {
	for nurse in `seq 1 $nbNurses` ; do
		echo -n $nbDays
		for day in `seq 1 $nbDays` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wregular"
		echo "5"
		echo "1 0 0"
		echo "5 0 0 1 0 2 0 3 0 4 0"
		echo "20"
		echo "0 0 0 0"
		echo "0 1 0 0"
		echo "0 2 0 0"
		echo "0 3 1 0"
		echo "1 0 2 0"
		echo "1 1 2 0"
		echo "1 2 2 0"
		echo "1 3 1 0"
		echo "2 0 0 0"
		echo "2 1 0 0"
		echo "2 2 0 0"
		echo "2 3 3 0"
		echo "3 0 4 0"
		echo "3 1 4 0"
		echo "3 2 4 0"
		echo "3 3 1 0"
		echo "4 0 0 0"
		echo "4 1 0 0"
		echo "4 2 0 0"
		echo "4 3 3 10000"
		
	done
}

function minNightDist {
	for nurse in `seq 1 $nbNurses` ; do
		echo -n $nbDays
		for day in `seq 1 $nbDays` ; do
			variable=$(( ($nurse-1)*$nbDays + ($day-1) ))
			echo -n " "$variable
		done
		echo " -1 wregular"
		echo "5"
		echo "1 0 0"
		echo "5 0 0 1 0 2 0 3 0 4 0"
		echo "20"
		echo "0 0 0 0"
		echo "0 1 1 0"
		echo "0 2 0 0"
		echo "0 3 0 0"
		echo "1 0 2 0"
		echo "1 1 1 10000"
		echo "1 2 2 0"
		echo "1 3 2 0"
		echo "2 0 3 0"
		echo "2 1 1 10000"
		echo "2 2 3 0"
		echo "2 3 3 0"
		echo "3 0 4 0"
		echo "3 1 1 10000"
		echo "3 2 4 0"
		echo "3 3 4 0"
		echo "4 0 0 0"
		echo "4 1 1 10000"
		echo "4 2 0 0"
		echo "4 3 0 0"
	done
}

function writeDODO {
	echo "5"
	echo "1 0 0"
	echo "5 0 0 1 0 2 0 3 0 4 0"
	echo "20"
	echo "0 0 1 0"
	echo "0 1 4 0"
	echo "0 2 4 0"
	echo "0 3 4 0"
	echo "1 0 4 0"
	echo "1 1 4 0"
	echo "1 2 4 0"
	echo "1 3 2 0"
	echo "2 0 3 0"
	echo "2 1 4 0"
	echo "2 2 4 0"
	echo "2 3 4 0"
	echo "3 0 4 0"
	echo "3 1 4 0"
	echo "3 2 4 0"
	echo "3 3 4 1000000"
	echo "4 0 4 0"
	echo "4 1 4 0"
	echo "4 2 4 0"
	echo "4 3 4 0"	
}

function writeExtraRequest { # 3
	# NURSE * NBDAYS + DAYS 
	
	####################################################################
	### ExtraRequest Nurse A01
	variable=$(( (1-1) * $nbDays + (1-1)))
	echo "1 "$variable" 0 2"
	echo "0 1000000"
	echo "1 1000000"
	
	variable=$(( (1-1) * $nbDays + (3-1)))
	echo "1 "$variable" 0 1"
	echo "1 1000000"
	
	variable=$(( (1-1) * $nbDays + (4-1)))
	echo "1 "$variable" 0 1"
	echo "1 1000000"
	
	### ExtraRequest Nurse A02
	variable1=$(( (2-1) * $nbDays + (1-1)))
	variable2=$(( (2-1) * $nbDays + (2-1)))
	variable3=$(( (2-1) * $nbDays + (3-1)))
	variable4=$(( (2-1) * $nbDays + (4-1)))
	variable5=$(( (2-1) * $nbDays + (5-1)))
	echo "5 "$variable1" "$variable2" "$variable3" "$variable4" "$variable5" -1 wamong hard 1000000"
	echo "1 3"
	echo "0 4"
	
	echo "4 "$variable1" "$variable2" "$variable3" "$variable4" -1 wregular"
	writeDODO	
	
	### ExtraRequest Nurse A03
	variable1=$(( (3-1) * $nbDays + (1-1)))
	variable2=$(( (3-1) * $nbDays + (2-1)))
	echo "2 "$variable1" "$variable2" 0 1"
	echo "0 0 1000000"
	
	### ExtraRequest Nurse A04
	variable1=$(( (4-1) * $nbDays + (1-1)))
	variable2=$(( (4-1) * $nbDays + (2-1)))
	variable3=$(( (4-1) * $nbDays + (3-1)))
	variable4=$(( (4-1) * $nbDays + (4-1)))

	echo "4 "$variable1" "$variable2" "$variable3" "$variable4" -1 wregular"
	writeDODO	
	
	echo "1 "$variable1" 0 2"
	echo "0 1000000"
	echo "1 1000000"
	
	echo "1 "$variable2" 0 1"
	echo "1 1000000"
	
	### ExtraRequest Nurse A05
	variable1=$(( (5-1) * $nbDays + (1-1)))
	variable2=$(( (5-1) * $nbDays + (2-1)))
	variable3=$(( (5-1) * $nbDays + (3-1)))
	variable4=$(( (5-1) * $nbDays + (4-1)))
	variable5=$(( (5-1) * $nbDays + (5-1)))
	variable6=$(( (5-1) * $nbDays + (6-1)))
	echo "6 "$variable1" "$variable2" "$variable3" "$variable4" "$variable5" "$variable6" -1 wamong hard 1000000"
	echo "3 0 1 2"
	echo "0 5"
	
	echo "3 "$variable1" "$variable2" "$variable3" 0 2"
	echo "0 0 0 1000000"
	echo "3 0 3 1000000"
	
	echo "1 "$variable1" 0 1"
	echo "1 1000000"
	 
	### ExtraRequest Nurse A06
	variable1=$(( (6-1) * $nbDays + (1-1)))
	variable2=$(( (6-1) * $nbDays + (2-1)))
	variable3=$(( (6-1) * $nbDays + (3-1)))
	variable4=$(( (6-1) * $nbDays + (4-1)))
	variable5=$(( (6-1) * $nbDays + (5-1)))

	echo "4 "$variable1" "$variable2" "$variable3" "$variable4" -1 wregular"
	writeDODO
	
	echo "5 "$variable1" "$variable2" "$variable3" "$variable4" "$variable5" -1 wamong hard 1000000"
	echo "1 3"
	echo "0 4"
	
	echo "1 "$variable1" 0 2"
	echo "0 1000000"
	echo "1 1000000"
	
	echo "1 "$variable2" 0 1"
	echo "1 1000000"
	
	echo "1 "$variable3" 0 1"
	echo "1 1000000"
	
	### ExtraRequest Nurse A07
	variable1=$(( (7-1) * $nbDays + (1-1)))
	variable2=$(( (7-1) * $nbDays + (2-1)))
	variable3=$(( (7-1) * $nbDays + (3-1)))
	variable4=$(( (7-1) * $nbDays + (4-1)))
	echo "4 "$variable1" "$variable2" "$variable3" "$variable4" -1 wamong hard 1000000"
	echo "1 3"
	echo "0 3"
	
	echo "1 "$variable1" 0 1"
	echo "1 1000000"	

	echo "1 "$variable2" 0 1"
	echo "1 1000000"
	
	### ExtraRequest Nurse A08
	variable=$(( (8-1) * $nbDays + (1-1)))
	echo "1 "$variable" 0 1"
	echo "1 1000000"
	
	variable1=$(( (8-1) * $nbDays + (1-1)))
	variable2=$(( (8-1) * $nbDays + (2-1)))
	variable3=$(( (8-1) * $nbDays + (3-1)))
	echo "3 "$variable1" "$variable2" "$variable3" 0 2"
	echo "0 0 0 1000000"
	echo "3 0 3 1000000"
	
	### ExtraRequest Nurse A09
	variable1=$(( (9-1) * $nbDays + (1-1)))
	variable2=$(( (9-1) * $nbDays + (2-1)))
	variable3=$(( (9-1) * $nbDays + (3-1)))
	variable4=$(( (9-1) * $nbDays + (4-1)))
	echo "4 "$variable1" "$variable2" "$variable3" "$variable4" -1 wamong hard 1000000"
	echo "3 0 1 2"
	echo "0 3"
	
	### ExtraRequest Nurse A10
	variable1=$(( (10-1) * $nbDays + (1-1)))
	variable2=$(( (10-1) * $nbDays + (2-1)))
	echo "2 "$variable1" "$variable2" 0 9"
	echo "0 0 1000000"
	echo "0 1 1000000"
	echo "0 2 1000000"
	echo "1 0 1000000"
	echo "1 1 1000000"
	echo "1 2 1000000"
	echo "2 0 1000000"
	echo "2 1 1000000"
	echo "2 2 1000000"
	####################################################################
	
	####################################################################
	### ExtraRequest Nurse B11
	variable1=$(( (11-1) * $nbDays + (1-1)))
	variable2=$(( (11-1) * $nbDays + (2-1)))
	variable3=$(( (11-1) * $nbDays + (3-1)))
	variable4=$(( (11-1) * $nbDays + (4-1)))
	variable5=$(( (11-1) * $nbDays + (5-1)))
	echo "5 "$variable1" "$variable2" "$variable3" "$variable4" "$variable5" -1 wamong hard 1000000"
	echo "1 3"
	echo "0 4"
	
	echo "1 "$variable2" 0 1"
	echo "1 1000000"
	
	echo "1 "$variable3" 0 1"
	echo "1 1000000"
	### ExtraRequest Nurse B12
	variable1=$(( (12-1) * $nbDays + (1-1)))
	variable2=$(( (12-1) * $nbDays + (2-1)))
	variable3=$(( (12-1) * $nbDays + (3-1)))
	echo "3 "$variable1" "$variable2" "$variable3" 0 2"
	echo "0 0 0 1000000"
	echo "3 0 3 1000000"
	
	### ExtraRequest Nurse B13
	variable1=$(( (12-1) * $nbDays + (1-1)))
	variable2=$(( (12-1) * $nbDays + (2-1)))
	variable3=$(( (12-1) * $nbDays + (3-1)))
	echo "3 "$variable1" "$variable2" "$variable3" 0 1"
	echo "3 3 3 1000000"
	
	### ExtraRequest Nurse B14
	variable1=$(( (14-1) * $nbDays + (1-1)))
	variable2=$(( (14-1) * $nbDays + (2-1)))
	variable3=$(( (14-1) * $nbDays + (3-1)))
	variable4=$(( (14-1) * $nbDays + (4-1)))

	echo "4 "$variable1" "$variable2" "$variable3" "$variable4" -1 wregular"
	writeDODO
	
	echo "4 "$variable1" "$variable2" "$variable3" "$variable4" -1 wamong hard 1000000"
	echo "1 3"
	echo "0 3"
	
	echo "1 "$variable1" 0 1"
	echo "1 1000000"
	
	echo "1 "$variable2" 0 1"
	echo "1 1000000"
	### ExtraRequest Nurse B15
	variable1=$(( (15-1) * $nbDays + (1-1)))
	echo "1 "$variable1" 0 2"
	echo "0 1000000"
	echo "1 1000000"
	
	variable=$(( (15-1) * $nbDays + (3-1)))
	echo "1 "$variable" 0 1"
	echo "1 1000000"

	variable=$(( (15-1) * $nbDays + (4-1)))
	echo "1 "$variable" 0 1"
	echo "1 1000000"
	
	### ExtraRequest Nurse B16
	variable1=$(( (16-1) * $nbDays + (1-1)))
	variable2=$(( (16-1) * $nbDays + (2-1)))
	echo "2 "$variable1" "$variable2" 0 1"
	echo "0 0 1000000"
	
	### ExtraRequest Nurse B17
	variable1=$(( (17-1) * $nbDays + (1-1)))
	variable2=$(( (17-1) * $nbDays + (2-1)))
	variable3=$(( (17-1) * $nbDays + (3-1)))
	variable4=$(( (17-1) * $nbDays + (4-1)))

	echo "4 "$variable1" "$variable2" "$variable3" "$variable4" -1 wregular"
	writeDODO
	
	echo "4 "$variable1" "$variable2" "$variable3" "$variable4" -1 wamong hard 1000000"
	echo "1 3"
	echo "0 3"
	
	echo "1 "$variable1" 0 1"
	echo "1 1000000"
	
	echo "1 "$variable2" 0 1"
	echo "1 1000000"
	
	### ExtraRequest Nurse B18
	variable1=$(( (18-1) * $nbDays + (1-1)))
	variable2=$(( (18-1) * $nbDays + (2-1)))
	variable3=$(( (18-1) * $nbDays + (3-1)))
	echo "3 "$variable1" "$variable2" "$variable3" 0 1"
	echo "0 0 0 1000000"
	
	echo "1 "$variable1" 1000000 1"
	echo "3 0"
	
	### ExtraRequest Nurse B19
	variable1=$(( (19-1) * $nbDays + (1-1)))
	variable2=$(( (19-1) * $nbDays + (2-1)))
	variable3=$(( (19-1) * $nbDays + (3-1)))
	variable4=$(( (19-1) * $nbDays + (4-1)))
	variable5=$(( (19-1) * $nbDays + (5-1)))
	echo "5 "$variable1" "$variable2" "$variable3" "$variable4" "$variable5" -1 wamong hard 1000000"
	echo "3 0 1 2"
	echo "0 4"
	
	echo "2 "$variable1" "$variable2" 0 1"
	echo "0 0 1000000" 
	####################################################################
	
	####################################################################
	### ExtraRequest Nurse C20
	variable1=$(( (20-1) * $nbDays + (1-1)))
	variable2=$(( (20-1) * $nbDays + (2-1)))
	variable3=$(( (20-1) * $nbDays + (3-1)))
	variable4=$(( (20-1) * $nbDays + (4-1)))
	variable5=$(( (20-1) * $nbDays + (5-1)))
	variable6=$(( (20-1) * $nbDays + (6-1)))
	echo "6 "$variable1" "$variable2" "$variable3" "$variable4" "$variable5" "$variable6" -1 wamong hard 1000000"
	echo "3 0 1 2"
	echo "0 5"
	
	echo "3 "$variable1" "$variable2" "$variable3" 0 2"
	echo "0 0 0 1000000"
	echo "3 0 3 1000000"
	
	### ExtraRequest Nurse C21
	variable1=$(( (21-1) * $nbDays + (1-1)))
	variable2=$(( (21-1) * $nbDays + (2-1)))
	variable3=$(( (21-1) * $nbDays + (3-1)))
	variable4=$(( (21-1) * $nbDays + (4-1)))
	variable5=$(( (21-1) * $nbDays + (5-1)))
	echo "5 "$variable1" "$variable2" "$variable3" "$variable4" "$variable5" -1 wamong hard 1000000"
	echo "1 3"
	echo "0 4"
	
	echo "4 "$variable1" "$variable2" "$variable3" "$variable4" -1 wregular"
	writeDODO	
	
	echo "2 "$variable1" "$variable2" 0 1"
	echo "0 3 1000000"
	
	### ExtraRequest Nurse C22
	variable1=$(( (22-1) * $nbDays + (1-1)))
	variable2=$(( (22-1) * $nbDays + (2-1)))
	variable3=$(( (22-1) * $nbDays + (3-1)))
	variable4=$(( (22-1) * $nbDays + (4-1)))
	variable5=$(( (22-1) * $nbDays + (5-1)))
	variable6=$(( (22-1) * $nbDays + (6-1)))
	echo "6 "$variable1" "$variable2" "$variable3" "$variable4" "$variable5" "$variable6" -1 wamong hard 1000000"
	echo "3 0 1 2"
	echo "0 5"
	
	echo "3 "$variable1" "$variable2" "$variable3" 0 2"
	echo "0 0 0 1000000"
	echo "3 0 3 1000000"
	
	echo "1 "$variable1" 0 1"
	echo "1 1000000"
	
	### ExtraRequest Nurse C23
	variable1=$(( (23-1) * $nbDays + (1-1)))
	variable2=$(( (23-1) * $nbDays + (2-1)))
	variable3=$(( (23-1) * $nbDays + (3-1)))
	variable4=$(( (23-1) * $nbDays + (4-1)))
	echo "4 "$variable1" "$variable2" "$variable3" "$variable4" -1 wamong hard 1000000"
	echo "1 3"
	echo "0 3"
	
	echo "1 "$variable1" 0 1"
	echo "0 1000000"
	
	### ExtraRequest Nurse C24
	variable1=$(( (24-1) * $nbDays + (1-1)))
	variable2=$(( (24-1) * $nbDays + (2-1)))
	variable3=$(( (24-1) * $nbDays + (3-1)))
	variable4=$(( (24-1) * $nbDays + (4-1)))

	echo "4 "$variable1" "$variable2" "$variable3" "$variable4" -1 wregular"
	writeDODO
	
	echo "4 "$variable1" "$variable2" "$variable3" "$variable4" -1 wamong hard 1000000"
	echo "1 3"
	echo "0 3"
	
	echo "1 "$variable1" 0 1"
	echo "1 1000000"
	
	echo "1 "$variable2" 0 1"
	echo "1 1000000"
	
	### ExtraRequest Nurse C25
	variable1=$(( (25-1) * $nbDays + (1-1)))
	echo "1 "$variable1" 0 2"
	echo "0 1000000"
	echo "1 1000000"
	
	variable2=$(( (25-1) * $nbDays + (2-1)))
	echo "1 "$variable2" 0 2"
	echo "0 1000000"
	echo "1 1000000"
	
	echo "2 "$variable1" "$variable2" 0 9"
	echo "0 0 1000000"
	echo "0 1 1000000"
	echo "0 2 1000000"
	echo "1 0 1000000"
	echo "1 1 1000000"
	echo "1 2 1000000"
	echo "2 0 1000000"
	echo "2 1 1000000"
	echo "2 2 1000000"
	
	variable=$(( (25-1) * $nbDays + (3-1)))
	echo "1 "$variable" 0 1"
	echo "1 1000000"
	
	variable=$(( (25-1) * $nbDays + (4-1)))
	echo "1 "$variable" 0 1"
	echo "1 1000000"
	### ExtraRequest Nurse C26
	variable1=$(( (26-1) * $nbDays + (1-1)))
	variable2=$(( (26-1) * $nbDays + (2-1)))
	variable3=$(( (26-1) * $nbDays + (3-1)))
	variable4=$(( (26-1) * $nbDays + (4-1)))
	variable5=$(( (26-1) * $nbDays + (5-1)))

	echo "4 "$variable1" "$variable2" "$variable3" "$variable4" -1 wregular"
	writeDODO
	
	echo "5 "$variable1" "$variable2" "$variable3" "$variable4" "$variable5" -1 wamong hard 1000000"
	echo "1 3"
	echo "0 4"
	
	echo "1 "$variable1" 0 2"
	echo "0 1000000"
	echo "1 1000000"
	
	echo "1 "$variable2" 0 1"
	echo "1 1000000"
	
	echo "1 "$variable3" 0 1"
	echo "1 1000000"
	
	### ExtraRequest Nurse C27
	variable1=$(( (27-1) * $nbDays + (1-1)))
	echo "1 "$variable1" 0 2"
	echo "0 1000000"
	echo "1 1000000"
	
	variable2=$(( (27-1) * $nbDays + (2-1)))
	echo "1 "$variable2" 0 2"
	echo "0 1000000"
	echo "1 1000000"
	
	echo "2 "$variable1" "$variable2" 0 9"
	echo "0 0 1000000"
	echo "0 1 1000000"
	echo "0 2 1000000"
	echo "1 0 1000000"
	echo "1 1 1000000"
	echo "1 2 1000000"
	echo "2 0 1000000"
	echo "2 1 1000000"
	echo "2 2 1000000"
	
	variable=$(( (27-1) * $nbDays + (3-1)))
	echo "1 "$variable" 0 1"
	echo "1 1000000"
	
	variable=$(( (27-1) * $nbDays + (4-1)))
	echo "1 "$variable" 0 1"
	echo "1 1000000"
	
	### ExtraRequest Nurse C28
	variable1=$(( (28-1) * $nbDays + (1-1)))
	variable2=$(( (28-1) * $nbDays + (2-1)))
	variable3=$(( (28-1) * $nbDays + (3-1)))
	variable4=$(( (28-1) * $nbDays + (4-1)))
	variable5=$(( (28-1) * $nbDays + (5-1)))
	echo "5 "$variable1" "$variable2" "$variable3" "$variable4" "$variable5" -1 wamong hard 1000000"
	echo "3 0 1 2"
	echo "0 4"
	
	echo "3 "$variable1" "$variable2" "$variable3" 0 1"
	echo "0 0 0 1000000" 
	####################################################################
}

function writeFreeWE {
	for nurse in `seq 1 $nbNurses` ; do
		echo -n 12
		for week in `seq 1 4` ; do
			variable1=$(( ($nurse-1) * $nbDays + (1+7*($week-1)-1)))
			variable2=$(( ($nurse-1) * $nbDays + (2+7*($week-1)-1)))
			variable3=$(( ($nurse-1) * $nbDays + (3+7*($week-1)-1)))
			echo -n " "$variable1" "$variable2" "$variable3
		done
		echo " -1 wregular"
	echo "15"
	echo "1 0 0"
	echo "15 0 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 14 0"
	echo "60"
	echo "0 0 3 0"
	echo "0 1 1 0"
	echo "0 2 3 0"
	echo "0 3 3 0"
	echo "1 0 2 0"
	echo "1 1 2 0"
	echo "1 2 2 0"
	echo "1 3 2 0"
	echo "2 0 0 0"
	echo "2 1 0 0"
	echo "2 2 0 0"
	echo "2 3 0 0"
	echo "3 0 2 0"
	echo "3 1 2 0"
	echo "3 2 2 0"
	echo "3 3 4 0"
	echo "4 0 0 0"
	echo "4 1 0 0"
	echo "4 2 0 0"
	echo "4 3 5 0"
	echo "5 0 8 0"
	echo "5 1 6 0"
	echo "5 2 8 0"
	echo "5 3 8 0"
	echo "6 0 7 0"
	echo "6 1 7 0"
	echo "6 2 7 0"
	echo "6 3 7 0"
	echo "7 0 5 0"
	echo "7 1 5 0"
	echo "7 2 5 0"
	echo "7 3 5 0"
	echo "8 0 7 0"
	echo "8 1 7 0"
	echo "8 2 7 0"
	echo "8 3 9 0"
	echo "9 0 5 0"
	echo "9 1 5 0"
	echo "9 2 5 0"
	echo "9 3 10 0"
	echo "10 0 13 0"
	echo "10 1 11 0"
	echo "10 2 13 0"
	echo "10 3 13 0"
	echo "11 0 12 0"
	echo "11 1 12 0"
	echo "11 2 12 0"
	echo "11 3 12 0"
	echo "12 0 10 0"
	echo "12 1 10 0"
	echo "12 2 10 0"
	echo "12 3 10 0"
	echo "13 0 12 0"
	echo "13 1 12 0"
	echo "13 2 12 0"
	echo "13 3 14 0"
	echo "14 0 10 0"
	echo "14 1 10 0"
	echo "14 2 10 0"
	echo "14 3 10 1000000"
	done	
}

function writeHolyDay {
	for nurse in `seq 1 $nbNurses` ; do
		echo -n "9"
		variable=$(( ($nurse-1) * $nbDays + (2-1)))
		echo -n " "$variable
		variable=$(( ($nurse-1) * $nbDays + (3-1)))
		echo -n " "$variable	
		variable=$(( ($nurse-1) * $nbDays + (4-1)))
		echo -n " "$variable
		variable=$(( ($nurse-1) * $nbDays + (9-1)))
		echo -n " "$variable
		variable=$(( ($nurse-1) * $nbDays + (10-1)))
		echo -n " "$variable
		variable=$(( ($nurse-1) * $nbDays + (16-1)))
		echo -n " "$variable
		variable=$(( ($nurse-1) * $nbDays + (17-1)))
		echo -n " "$variable
		variable=$(( ($nurse-1) * $nbDays + (23-1)))
		echo -n " "$variable
		variable=$(( ($nurse-1) * $nbDays + (24-1)))
		echo -n " "$variable
		echo " -1 wregular"
		
	echo "9"
	echo "1 0 0"
	echo "9"
	echo "0 1000000"
	echo "1 1000000"
	echo "2 1000000"
	echo "3 1000000"
	echo "4 1000000"
	echo "5 1000000"
	echo "6 1000000"
	echo "7 1000000"
	echo "8 0"
	echo "36"
	echo "0 0 2 0"
	echo "0 1 2 0"
	echo "0 2 2 0"
	echo "0 3 1 0"
	echo "1 0 4 0"
	echo "1 1 4 0"
	echo "1 2 4 0"
	echo "1 3 8 0"
	echo "2 0 4 0"
	echo "2 1 4 0"
	echo "2 2 4 0"
	echo "2 3 3 0"
	echo "3 0 5 0"
	echo "3 1 5 0"
	echo "3 2 5 0"
	echo "3 3 8 0"
	echo "4 0 5 0"
	echo "4 1 5 0"
	echo "4 2 5 0"
	echo "4 3 5 0"
	echo "5 0 6 0"
	echo "5 1 6 0"
	echo "5 2 6 0"
	echo "5 3 7 0"
	echo "6 0 5 0"
	echo "6 1 5 0"
	echo "6 2 5 0"
	echo "6 3 5 0"
	echo "7 0 5 0"
	echo "7 1 5 0"
	echo "7 2 5 0"
	echo "7 3 8 0"
	echo "8 0 8 0"
	echo "8 1 8 0"
	echo "8 2 8 0"
	echo "8 3 8 0"
	done
}


writeHeader
#writeNurse
writeNurseWithBalancing
#writeDaily
writeDailyWithNested
writeRequest
afterNight
maxOff
maxOn
maxD
cutWork
minNightDist
writeExtraRequest
writeFreeWE
writeHolyDay
