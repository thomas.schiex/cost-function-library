#!/bin/bash

ubGlobal=10000000
nbDays=28
nbNurses=16
nbShifts=3
nbVariables=$(( $nbDays * $nbNurses ))
nbValues=$(( $nbShifts + 1 ))
nbConstraints=$(( $nbDays*$nbShifts + $nbNurses*($nbShifts+1) + $nbNurses*2 + $nbNurses*($nbDays-1) + $nbNurses*($nbDays-2) + 2*$nbNurses ))

function writeHeader {
	echo "VALOUXIS_28Days_16Nurses_3Shifts" $nbVariables $nbValues $nbConstraints $ubGlobal
	for i in `seq 1 $nbVariables`; do
		echo -n $nbValues" "
	done
	echo ""
}

function writeDaily { #nbShifts*nbDays
	for day in `seq 1 $nbDays`; do
		idDay=$(( $day - 1 ))
		echo -n $nbNurses
		for nurse in `seq 1 $nbNurses`; do
			idNurse=$(( $nurse - 1))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 100000"
		echo "1 0"
		dayWeek=$(($idDay % 7))
		if [[ $dayWeek -lt 5 ]] ; then 
			echo "4 4"
		else 
			echo "3 3"
		fi
	done
	
	for day in `seq 1 $nbDays`; do
		idDay=$(( $day - 1 ))
		echo -n $nbNurses
		for nurse in `seq 1 $nbNurses`; do
			idNurse=$(( $nurse - 1))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 10000"
		echo "1 1"
		dayWeek=$(($idDay % 7))
		if [[ $dayWeek -lt 5 ]] ; then 
			echo "4 4"
		else 
			echo "3 3"
		fi
	done
	
	for day in `seq 1 $nbDays`; do
		idDay=$(( $day - 1 ))
		echo -n $nbNurses
		for nurse in `seq 1 $nbNurses`; do
			idNurse=$(( $nurse - 1))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 100000"
		echo "1 2"
		echo "2 2"
	done
}

function writeNurse { #nbNurses*($nbShifts+1)
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n $nbDays 
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1000"
		echo "1 0"
		echo "5 8"
	done
	
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n $nbDays 
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1000"
		echo "1 1"
		echo "5 8"
	done
	
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n $nbDays 
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1000"
		echo "1 2"
		echo "2 5"
	done
	
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n $nbDays 
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1000"
		echo "1 3"
		echo "10 13"
	done
}

function writeWE {
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n "4" 
		for week in `seq 0 3`; do 
			day=$(( 7*$week + 7 ))
			idDay=$(( $day - 1))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1000"
		echo "1 3"
		echo "1 4"
	done
}

function writePrec { #$nbNurses*($nbDays-1)
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))
		nbDayMone=$(( $nbDays - 1 ))
		for day in `seq 1 $nbDayMone`; do
			variable1=$(( $idNurse * $nbDays + ($day - 1) ))
			variable2=$(( $idNurse * $nbDays + ($day) ))
			echo "2 "$variable1" "$variable2" 0 3"
			echo "2 0 1000"
			echo "2 1 1000"
			echo "1 0 1000"
		done
	done
}

function writeIsolOff { #$nbNurses*($nbDays-2)
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))
		nbDayMtwo=$(( $nbDays - 2 ))
		for day in `seq 1 $nbDayMtwo`; do
			variable1=$(( $idNurse * $nbDays + ($day - 1) ))
			variable2=$(( $idNurse * $nbDays + ($day) ))
			variable3=$(( $idNurse * $nbDays + ($day + 1) ))
			echo "3 "$variable1" "$variable2" "$variable3" 0 9"
			echo "0 3 0 1000"
			echo "0 3 1 1000"
			echo "0 3 2 1000"
			echo "1 3 0 1000"
			echo "1 3 1 1000"
			echo "1 3 2 1000"
			echo "2 3 0 1000"
			echo "2 3 1 1000"
			echo "2 3 2 1000"
		done
	done
}

function writeIsolOn {
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n $nbDays 
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wregular"
		echo "6"
		echo "1 0 0"
		echo "6 0 0 1 0 2 0 3 0 4 0 5 0"
		echo "24"
		echo "0 0 0 0"
		echo "0 1 0 0"
		echo "0 2 0 0"
		echo "0 3 1 0"
		echo "1 0 2 0"
		echo "1 1 2 0"
		echo "1 2 2 0"
		echo "1 3 1 0"
		echo "2 0 3 0"
		echo "2 1 3 0"
		echo "2 2 3 0"
		echo "2 3 1 1000"
		echo "3 0 4 0"
		echo "3 1 4 0"
		echo "3 2 4 0"
		echo "3 3 1 100"
		echo "4 0 5 0"
		echo "4 1 5 0"
		echo "4 2 5 0"
		echo "4 3 1 10"
		echo "5 0 5 0"
		echo "5 1 5 0"
		echo "5 2 5 0"
		echo "5 3 1 0"
	done
}

function writeNight {
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n $nbDays 
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wregular"
		echo "4"
		echo "1 0 0"
		echo "4 0 0 1 0 2 0 3 0"
		echo "16"
		echo "0 0 0 0"
		echo "0 1 0 0"
		echo "0 2 1 0"
		echo "0 3 0 0"
		echo "1 0 0 0"
		echo "1 1 0 0"
		echo "1 2 2 0"
		echo "1 3 0 0"
		echo "2 0 0 0"
		echo "2 1 0 0"
		echo "2 2 3 0"
		echo "2 3 0 0"
		echo "3 0 0 0"
		echo "3 1 0 0"
		echo "3 2 3 1000"
		echo "3 3 0 0"
	done	
}

function writeWork {
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n $nbDays 
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wregular"
		echo "5"
		echo "1 0 0"
		echo "5 0 0 1 0 2 0 3 0 4 0"
		echo "20"
		echo "0 0 1 0"
		echo "0 1 1 0"
		echo "0 2 1 0"
		echo "0 3 0 0"
		echo "1 0 2 0"
		echo "1 1 2 0"
		echo "1 2 2 0"
		echo "1 3 0 0"
		echo "2 0 3 0"
		echo "2 1 3 0"
		echo "2 2 3 0"
		echo "2 3 0 0"
		echo "3 0 4 0"
		echo "3 1 4 0"
		echo "3 2 4 0"
		echo "3 3 0 0"
		echo "4 0 4 1000"
		echo "4 1 4 1000"
		echo "4 2 4 1000"
		echo "4 3 0 0"
	done	
}

writeHeader
writeDaily
writeNurse
writeWE
writePrec
writeIsolOff
writeIsolOn
writeNight
writeWork
