#!/bin/bash

ubGlobal=10000000
nbDays=7
nbNurses=27
nbShifts=3
nbVariables=$(( $nbDays * $nbNurses ))
nbValues=$(( $nbShifts + 1 ))
nbConstraints=$(( $nbShifts*$nbDays + 2*$nbNurses + $nbNurses*($nbDays-1) + $nbDays*$nbNurses ))

function writeHeader {
	echo "LLR_7Days_27Nurses_3Shifts" $nbVariables $nbValues $nbConstraints $ubGlobal
	for i in `seq 1 $nbVariables`; do
		echo -n $nbValues" "
	done
	echo ""
}

function writeDaily {
	for day in `seq 1 $nbDays`; do
		idDay=$(( $day - 1 ))
		echo -n $nbNurses
		for nurse in `seq 1 $nbNurses`; do
			idNurse=$(( $nurse - 1))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 10000"
		echo "1 0"
		echo "6 6"
	done
	
	for day in `seq 1 $nbDays`; do
		idDay=$(( $day - 1 ))
		echo -n $nbNurses
		for nurse in `seq 1 $nbNurses`; do
			idNurse=$(( $nurse - 1))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 10000"
		echo "1 1"
		echo "6 6"
	done
	
	for day in `seq 1 $nbDays`; do
		idDay=$(( $day - 1 ))
		echo -n $nbNurses
		for nurse in `seq 1 $nbNurses`; do
			idNurse=$(( $nurse - 1))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 10000"
		echo "1 2"
		echo "3 3"
	done
}

function writeNurse { #nbNurses*($nbShifts+1)
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n $nbDays 
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 10000"
		echo "1 2"
		echo "0 1"
	done
	
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n $nbDays 
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 10000"
		echo "1 3"
		echo "2 7"
	done
}

function writePrec { #$nbNurses*($nbDays-1)
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))
		nbDayMone=$(( $nbDays - 1 ))
		for day in `seq 1 $nbDayMone`; do
			variable1=$(( $idNurse * $nbDays + ($day - 1) ))
			variable2=$(( $idNurse * $nbDays + ($day) ))
			echo "2 "$variable1" "$variable2" 0 8"
			echo "2 0 10000"
			echo "1 0 6"
			echo "0 0 5"
			echo "0 1 5"
			echo "0 2 5"
			echo "3 3 5"
			echo "3 0 5"
			echo "3 1 1"
			
		done
	done
}

function writeRequest {
	request=(
	   #2		3		4		5		6		7		8
		2		-1		-1		-1		-1		-1		0	#1
		-1		1		-1		-1		-1		-1		-1	#2
		0		-1		-1		-1		2		-1		-1	#3
		0		0		-1		-1		-1		-1		-1	#4
		-1		-1		-1		-1		-1		-1		-1	#5
	   #2		3		4		5		6		7		8
		-1		-1		1		0		0		-1		3	#6
		0		-1		-1		-1		-1		-1		2	#7
		1		1		-1		-1		-1		-1		-1	#8
		-1		-1		-1		-1		-1		-1		-1	#9
		-1		-1		1		-1		3		3 		3	#10
	   #2		3		4		5		6		7		8
		0		-1		-1		-1		-1		-1		-1	#11
		-1		3		-1		-1		-1		-1		-1	#12
		-1		-1		1		-1		-1		-1		-1	#13
		3		3		3		3		3		3		3 	#14
		-1		-1		-1		-1		-1		-1		2	#15
	   #2		3		4		5		6		7		8 
		0		-1		1		2		-1		-1		-1	#16
		-1		-1		1		-1		-1		-1		2	#17
		-1		-1		-1		-1		-1		-1		-1	#18
		0		-1		3		1		-1		-1		-1	#19
		-1		-1		-1		-1		2		-1		-1	#20
	   #2		3		4		5		6		7		8 
		-1		-1		2		-1		-1		3		-1	#21
		-1		-1		1		-1		-1		-1		2	#22
		3		3		3		3		3		3		3	#23
		-1		-1		-1		-1		-1		-1		-1	#24
		-1		-1		0		1		-1		3		3	#25
	   #2		3		4		5		6		7		8 
		0		-1		1		-1		-1		-1		-1 	#26
		1		0		-1		-1		-1		-1		3	#27
	   #2		3		4		5		6		7		8 
	)
	
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			if [[ ${request[$variable]} -eq -1 ]]; then 
				echo "1 "$variable" 0 0"
			else
				echo "1 "$variable" 10 1"
				echo ${request[$variable]}" 0"
			fi
		done
	done
}

writeHeader
writeDaily
writeNurse
writePrec
writeRequest
