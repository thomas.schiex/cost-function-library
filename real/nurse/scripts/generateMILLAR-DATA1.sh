#!/bin/bash

ubGlobal=10000000
nbDays=14
nbNurses=8
nbShifts=2
nbVariables=$(( $nbDays * $nbNurses ))
nbValues=$(( $nbShifts + 1 ))
nbConstraints=$(( $nbDays*$nbShifts + $nbNurses*($nbShifts+1) + $nbNurses*($nbDays-1) + $nbNurses*3 + $nbNurses))

function writeHeader {
	echo "MILLAR_14Days_8Nurses_2Shifts" $nbVariables $nbValues $nbConstraints $ubGlobal
	for i in `seq 1 $nbVariables`; do
		echo -n $nbValues" "
	done
	echo ""
}

function writeDaily { #nbShifts*nbDays
	for day in `seq 1 $nbDays`; do
		idDay=$(( $day - 1 ))
		for shift_ in `seq 1 $nbShifts`; do
			idShift=$(( $shift_ - 1 ))
			echo -n $nbNurses
			for nurse in `seq 1 $nbNurses`; do
				idNurse=$(( $nurse - 1))
				variable=$(( $idNurse * $nbDays + $idDay ))
				echo -n " "$variable
			done
			echo " -1 wamong lin 100"
			echo "1 "$idShift
			echo "2 2"
		done
	done
}

function writeNurse { #nbNurses*($nbShifts+1)
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n $nbDays 
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 100"
		echo "1 0"
		echo "3 4"
	done
	
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n $nbDays 
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1000"
		echo "1 1"
		echo "3 4"
	done
	
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n $nbDays 
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 1000"
		echo "1 2"
		echo "7 7"
	done
}

function writePrec { #$nbNurses*($nbDays-1)
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))
		nbDayMone=$(( $nbDays - 1 ))
		for day in `seq 1 $nbDayMone`; do
			variable1=$(( $idNurse * $nbDays + ($day - 1) ))
			variable2=$(( $idNurse * $nbDays + ($day) ))
			echo "2 "$variable1" "$variable2" 0 1"
			echo "1 0 100"
		done
	done
}

function writeMaxNight { #$nbNurses
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n $nbDays 
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wregular"
		echo "4"
		echo "1 0 0"
		echo "4 0 0 1 0 2 0 3 0"
		echo "12"
		echo "0 0 0 0"
		echo "0 1 1 0"
		echo "0 2 0 0"
		echo "1 0 0 0"
		echo "1 1 2 0"
		echo "1 2 0 0"
		echo "2 0 0 0"
		echo "2 1 3 0"
		echo "2 2 0 0"
		echo "3 0 0 0"
		echo "3 1 3 100"
		echo "3 2 0 0"
	done
}

function writeMaxOff { #$nbNurses
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n $nbDays 
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wregular"
		echo "5"
		echo "1 0 0"
		echo "5 0 0 1 0 2 0 3 0 4 0"
		echo "15"
		echo "0 0 0 0"
		echo "0 1 0 0"
		echo "0 2 1 0"
		echo "1 0 0 0"
		echo "1 1 0 0"
		echo "1 2 2 0"
		echo "2 0 0 0"
		echo "2 1 0 0"
		echo "2 2 3 0"
		echo "3 0 0 0"
		echo "3 1 0 0"
		echo "3 2 4 0"
		echo "4 0 0 0"
		echo "4 1 0 0"
		echo "4 2 4 100"
	done
}

function writeOnReg { #$nbNurses
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n $nbDays 
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wregular"
		echo "6"
		echo "1 0 0"
		echo "6 0 0 1 0 2 0 3 0 4 0 5 0"
		echo "18"
		echo "0 0 1 0"
		echo "0 1 1 0"
		echo "0 2 0 0"
		echo "1 0 2 0"
		echo "1 1 2 0"
		echo "1 2 0 100"
		echo "2 0 3 0"
		echo "2 1 3 0"
		echo "2 2 0 0"
		echo "3 0 4 0"
		echo "3 1 4 0"
		echo "3 2 0 0"
		echo "4 0 4 100"
		echo "4 1 4 100"
		echo "4 2 5 0"
		echo "5 0 1 100"
		echo "5 1 1 100"
		echo "5 2 0 0"
	done
}

function writeWE {
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n "4 "
		variableS1=$(( $idNurse * $nbDays + 5 ))
		variableD1=$(( $idNurse * $nbDays + 6 ))
		variableS2=$(( $idNurse * $nbDays + 5 + 7 ))
		variableD2=$(( $idNurse * $nbDays + 6 + 7))
		echo  $variableS1" "$variableD1" "$variableS2" "$variableD2" 1000 16"
		echo "2 2 0 0 0"
		echo "2 2 0 1 0"
		echo "2 2 0 2 0"
		echo "2 2 1 0 0"
		echo "2 2 1 1 0"
		echo "2 2 1 2 0"
		echo "2 2 2 0 0"
		echo "2 2 2 1 0"
		echo "0 0 2 2 0"
		echo "0 1 2 2 0" 
		echo "0 2 2 2 0"
		echo "1 0 2 2 0"
		echo "1 1 2 2 0"
		echo "1 2 2 2 0"
		echo "2 0 2 2 0"
		echo "2 1 2 2 0"
	done
}

writeHeader
writeDaily
writeNurse
writePrec
writeMaxNight
writeMaxOff
writeOnReg
writeWE
