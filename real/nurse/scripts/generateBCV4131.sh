#!/bin/bash

top=10000000
nbDays=29
nbNurses=13
nbShifts=4

nbVariables=$(( $nbDays*$nbNurses )) 
nbCells=$(( $nbDays*$nbNurses )) 
nbValues=$(( $nbShifts + 1 ))

nbConstraints=$(( $nbDays*$nbShifts + $nbNurses*$nbDays + $nbNurses + 8 + 12 + 12 + 12 + 4*12 + 3*12 + (12*1+4*4+8*7) + (7 + 0 + 0 + 0 + 8 + 1 +1 ) +  48 + 12))


### header
function writeHeader {
	echo "BCV4-13-1" $nbVariables $nbValues $nbConstraints $top
	for i in `seq 1 $nbCells`; do
		echo -n $nbValues" "
	done
	echo ""
	
}

### daily
function writeDaily {
	for day in `seq 1 $nbDays`; do
		idDay=$(($day -1))

		### V
		echo -n $nbNurses" "
		for nurse in `seq 1 $nbNurses`; do
			idNurse=$(($nurse -1))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n $variable" ";
		done	
		echo "-1 wamong lin" $top
		echo "1 0"
		echo "1 1"

		### D
		echo -n $nbNurses" " 
		for nurse in `seq 1 $nbNurses`; do
			idNurse=$(($nurse -1))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n $variable" ";
		done	
		echo "-1 wamong lin" $top
		echo "1 1"
		echo "3 3"

		### DH
		echo -n $nbNurses" "
		for nurse in `seq 1 $nbNurses`; do
			idNurse=$(($nurse -1))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n $variable" ";
		done	
		echo "-1 wamong lin" $top
		echo "1 2"
		echo "1 1"

		### L
		echo -n $nbNurses" " 
		for nurse in `seq 1 $nbNurses`; do
			idNurse=$(($nurse -1))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n $variable" ";
		done	
		echo "-1 wamong lin" $top
		echo "1 3"
		echo "1 1"
	done
}

nurseMax=( 19 19 16 16 16 16 16 16 16 16 16 16 16 )

function writeNurse {
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(($nurse -1))

		echo -n $nbDays" "
		for day in `seq 1 $nbDays`; do
			idDay=$(($day -1))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n $variable" ";
		done
		echo "-1 wamong lin 160"
		echo "4 0 1 2 3"
		echo "0 "${nurseMax[$idNurse]}
	done
}

function writeOnlyDH {
	for day in `seq 1 $nbDays`; do
		idDay=$(($day -1))
		idNurse=$((1 -1))
		
		variable=$(( $idNurse * $nbDays + $idDay ))
		echo "1 "$variable" 10000 2"
		echo "2 0"
		echo "4 0"
	done
}

function writeNoDH {
	nurse=$1
	cost=$2

	for day in `seq 1 $nbDays`; do
		idDay=$(($day -1))
		idNurse=$(($nurse -1))
		
		variable=$(( $idNurse * $nbDays + $idDay ))
		echo "1 "$variable" 0 1"
		echo "2 "$cost
	done
}
			
function writeNoWeekEndHeadNurse {
	for day in `seq 1 $nbDays`; do
		idDay=$(( $day - 1 ))
		idNurse=$((1 -1))

		wday=$(( $idDay % 7 ))
		variable=$(( $idNurse * $nbDays + $idDay ))

		if [[ $wday -eq 5 ]]; then
			echo "1 "$variable" 200 1" 
			echo "4 0"
		fi

		if [[ $wday -eq 6 ]]; then
			echo "1 "$variable" 230 1" 
			echo "4 0"
		fi 
	done 
}

function writeWConsMin2Max4 {
	nurse=$1
	idNurse=$(($nurse -1))

	echo -n $nbDays" "
	for day in `seq 1 $nbDays`; do
		idDay=$(( $day - 1 ))
		variable=$(( $idNurse * $nbDays + $idDay ))
		echo -n $variable" "
	done 
	echo "-1 wregular"
	echo "5"
	echo "1 0 0"
	echo "5 0 0 1 0 2 0 3 0 4 0"
	echo "25"
	echo "0 0 1 0"
	echo "0 1 1 0"
	echo "0 2 1 0"
	echo "0 3 1 0"
	echo "0 4 0 0"
	echo "1 0 2 0"
	echo "1 1 2 0"
	echo "1 2 2 0"
	echo "1 3 2 0"
	echo "1 4 0 5"
	echo "2 0 3 0"
	echo "2 1 3 0"
	echo "2 2 3 0"
	echo "2 3 3 0"
	echo "2 4 0 0"
	echo "3 0 4 0"
	echo "3 1 4 0"
	echo "3 2 4 0"
	echo "3 3 4 0"
	echo "3 4 0 0"
	echo "4 0 4 20"
	echo "4 1 4 20"
	echo "4 2 4 20"
	echo "4 3 4 20"
	echo "4 4 0 0"
}

function writeOConsMax3 {
	nurse=$1
	idNurse=$(($nurse -1))

	echo -n $nbDays" "
	for day in `seq 1 $nbDays`; do
		idDay=$(( $day - 1 ))
		variable=$(( $idNurse * $nbDays + $idDay ))
		echo -n $variable" "
	done 
	echo "-1 wregular"
	echo "4"
	echo "1 0 0"
	echo "4 0 0 1 0 2 0 3 0"
	echo "20"
	echo "0 0 0 0"
	echo "0 1 0 0"
	echo "0 2 0 0"
	echo "0 3 0 0"
	echo "0 4 1 0"
	echo "1 0 0 0"
	echo "1 1 0 0"
	echo "1 2 0 0"
	echo "1 3 0 0"
	echo "1 4 2 0"
	echo "2 0 0 0"
	echo "2 1 0 0"
	echo "2 2 0 0"
	echo "2 3 0 0"
	echo "2 4 3 0"
	echo "3 0 0 0"
	echo "3 1 0 0"
	echo "3 2 0 0"
	echo "3 3 0 0"
	echo "3 4 3 20"
}

function writeOConsMin2Max5 {
	nurse=$1
	idNurse=$(($nurse -1))

	echo -n $nbDays" "
	for day in `seq 1 $nbDays`; do
		idDay=$(( $day - 1 ))
		variable=$(( $idNurse * $nbDays + $idDay ))
		echo -n $variable" "
	done 
	echo "-1 wregular"
	echo "6"
	echo "1 0 0"
	echo "6 0 0 1 0 2 0 3 0 4 0 5 0"
	echo "30"
	echo "0 0 0 0"
	echo "0 1 0 0"
	echo "0 2 0 0"
	echo "0 3 0 0"
	echo "0 4 1 0"
	echo "1 0 0 1"
	echo "1 1 0 1"
	echo "1 2 0 1"
	echo "1 3 0 1"
	echo "1 4 2 0"
	echo "2 0 0 0"
	echo "2 1 0 0"
	echo "2 2 0 0"
	echo "2 3 0 0"
	echo "2 4 3 0"
	echo "3 0 0 0"
	echo "3 1 0 0"
	echo "3 2 0 0"
	echo "3 3 0 0"
	echo "3 4 4 0"
	echo "4 0 0 0"
	echo "4 1 0 0"
	echo "4 2 0 0"
	echo "4 3 0 0"
	echo "4 4 5 0"
	echo "5 0 0 0"
	echo "5 1 0 0"
	echo "5 2 0 0"
	echo "5 3 0 0"
	echo "5 4 5 20"
}

function writeMaxWork {
	nurse=$1
	maxW=$2
	
	idNurse=$(($nurse -1))

	echo -n $nbDays" "
	for day in `seq 1 $nbDays`; do
		idDay=$(($day -1))
		variable=$(( $idNurse * $nbDays + $idDay ))
		echo -n $variable" ";
	done
	echo "-1 wamong lin 10"
	echo "4 0 1 2 3"
	echo "0 "$maxW
	
}

function writeShiftLimit { #nurse V D DH L
	nurse=$1
	limitV=$2
	limitD=$3
	limitDH=$4
	limitL=$5
	
	idNurse=$(($nurse -1))
	
	echo -n $nbDays" "
	for day in `seq 1 $nbDays`; do
		idDay=$(($day -1))
		variable=$(( $idNurse * $nbDays + $idDay ))
		echo -n $variable" ";
	done
	echo "-1 wamong lin 30"
	echo "1 0"
	echo "0 "$limitV
	
	echo -n $nbDays" "
	for day in `seq 1 $nbDays`; do
		idDay=$(($day -1))
		variable=$(( $idNurse * $nbDays + $idDay ))
		echo -n $variable" ";
	done
	echo "-1 wamong lin 30"
	echo "1 1"
	echo "0 "$limitD

	echo -n $nbDays" "
	for day in `seq 1 $nbDays`; do
		idDay=$(($day -1))
		variable=$(( $idNurse * $nbDays + $idDay ))
		echo -n $variable" ";
	done
	echo "-1 wamong lin 30"
	echo "1 2"
	echo "0 "$limitDH

	echo -n $nbDays" "
	for day in `seq 1 $nbDays`; do
		idDay=$(($day -1))
		variable=$(( $idNurse * $nbDays + $idDay ))
		echo -n $variable" ";
	done
	echo "-1 wamong lin 30"
	echo "1 3"
	echo "0 "$limitL	
}

function writeShiftCons { #nurse
	nurse=$1
	idNurse=$(($nurse -1))

	#max 3 cons D
	echo -n $nbDays" "
	for day in `seq 1 $nbDays`; do
		idDay=$(( $day - 1 ))
		variable=$(( $idNurse * $nbDays + $idDay ))
		echo -n $variable" "
	done 
	echo "-1 wregular"
	echo "4"
	echo "1 0 0"
	echo "4 0 0 1 0 2 0 3 0"
	echo "20"
	echo "0 0 0 0" 
	echo "0 1 1 0"
	echo "0 2 0 0"
	echo "0 3 0 0"
	echo "0 4 0 0"
	echo "1 0 0 0" 
	echo "1 1 2 0"
	echo "1 2 0 0"
	echo "1 3 0 0"
	echo "1 4 0 0"
	echo "2 0 0 0" 
	echo "2 1 3 0"
	echo "2 2 0 0"
	echo "2 3 0 0"
	echo "2 4 0 0"
	echo "3 0 0 0" 
	echo "3 1 3 5"
	echo "3 2 0 0"
	echo "3 3 0 0"
	echo "3 4 0 0"
	
	
	#max 2 cons V
	echo -n $nbDays" "
	for day in `seq 1 $nbDays`; do
		idDay=$(( $day - 1 ))
		variable=$(( $idNurse * $nbDays + $idDay ))
		echo -n $variable" "
	done 
	echo "-1 wregular"
	echo "3"
	echo "1 0 0"
	echo "3 0 0 1 0 2 0"
	echo "15"
	echo "0 0 1 0" 
	echo "0 1 0 0"
	echo "0 2 0 0"
	echo "0 3 0 0"
	echo "0 4 0 0"
	echo "1 0 2 0" 
	echo "1 1 0 0"
	echo "1 2 0 0"
	echo "1 3 0 0"
	echo "1 4 0 0"
	echo "2 0 2 5" 
	echo "2 1 0 0"
	echo "2 2 0 0"
	echo "2 3 0 0"
	echo "2 4 0 0"
	
	#max 2 cons L
	echo -n $nbDays" "
	for day in `seq 1 $nbDays`; do
		idDay=$(( $day - 1 ))
		variable=$(( $idNurse * $nbDays + $idDay ))
		echo -n $variable" "
	done 
	echo "-1 wregular"
	echo "3"
	echo "1 0 0"
	echo "3 0 0 1 0 2 0"
	echo "15"
	echo "0 0 0 0" 
	echo "0 1 0 0"
	echo "0 2 0 0"
	echo "0 3 1 0"
	echo "0 4 0 0"
	echo "1 0 0 0" 
	echo "1 1 0 0"
	echo "1 2 0 0"
	echo "1 3 2 0"
	echo "1 4 0 0"
	echo "2 0 0 0" 
	echo "2 1 0 0"
	echo "2 2 0 0"
	echo "2 3 2 5"
	echo "2 4 0 0"
}

function writeWeekLimitVDL { #nurse #WeekV #WeekD #WeekL
	nurse=$1
	limitV=$2
	limitD=$3
	limitL=$4
	
	idNurse=$(($nurse -1))
	
	for week in `seq 1 4`; do
		echo -n "7 "
		
		idWeek=$(($week -1))
		
		for day in `seq 1 7`; do
			idDay=$(($day -1))
			variable=$(( $idNurse * $nbDays + $idDay + $idWeek*7 ))
			echo -n $variable" "
		done
		echo "-1 wamong lin 30"
		echo "1 0"
		echo "0 "$limitV
	done
	
	for week in `seq 1 4`; do
		echo -n "7 "
		
		idWeek=$(($week -1))
		
		for day in `seq 1 7`; do
			idDay=$(($day -1))
			variable=$(( $idNurse * $nbDays + $idDay + $idWeek*7 ))
			echo -n $variable" "
		done
		echo "-1 wamong lin 30"
		echo "1 1"
		echo "0 "$limitD
	done
	
	for week in `seq 1 4`; do
		echo -n "7 "
		
		idWeek=$(($week -1))
		
		for day in `seq 1 7`; do
			idDay=$(($day -1))
			variable=$(( $idNurse * $nbDays + $idDay + $idWeek*7 ))
			echo -n $variable" "
		done
		echo "-1 wamong lin 30"
		echo "1 3"
		echo "0 "$limitL
	done
}

function writeWeekLimitDL { #nurse  #WeekD #WeekL
	nurse=$1
	limitD=$2
	limitL=$3
	
	idNurse=$(($nurse -1))
	
	for week in `seq 1 4`; do
		echo -n "7 "
		
		idWeek=$(($week -1))
		
		for day in `seq 1 7`; do
			idDay=$(($day -1))
			variable=$(( $idNurse * $nbDays + $idDay + $idWeek*7 ))
			echo -n $variable" "
		done
		echo "-1 wamong lin 30"
		echo "1 1"
		echo "0 "$limitD
	done
	
	for week in `seq 1 4`; do
		echo -n "7 "
		
		idWeek=$(($week -1))
		
		for day in `seq 1 7`; do
			idDay=$(($day -1))
			variable=$(( $idNurse * $nbDays + $idDay + $idWeek*7 ))
			echo -n $variable" "
		done
		echo "-1 wamong lin 30"
		echo "1 3"
		echo "0 "$limitL
	done
}

function writeWeekLimitD { #nurse #WeekD
	nurse=$1
	limitD=$2
	
	idNurse=$(($nurse -1))
	
	for week in `seq 1 4`; do
		echo -n "7 "
		
		idWeek=$(($week -1))
		
		for day in `seq 1 7`; do
			idDay=$(($day -1))
			variable=$(( $idNurse * $nbDays + $idDay + $idWeek*7 ))
			echo -n $variable" "
		done
		echo "-1 wamong lin 30"
		echo "1 1"
		echo "0 "$limitD
	done
}

function writeMaxMonday { #nurse #limit
	nurse=$1
	limit=$2
	
	idNurse=$(($nurse -1))
	
	echo -n "5 "
	for week in `seq 1 5`; do
		idWeek=$(($week -1))
		idDay=$((1 -1))
		variable=$(( $idNurse * $nbDays + $idDay + $idWeek*7 ))
		echo -n $variable" "
	done
	echo "-1 wamong lin 30"
	echo "4 0 1 2 3"
	echo "0 "$limit
}

function writeMaxFriday { #nurse #limit
	nurse=$1
	limit=$2
	
	idNurse=$(($nurse -1))
	
	echo -n "4 "
	for week in `seq 1 4`; do
		idWeek=$(($week -1))
		idDay=$((5 -1))
		variable=$(( $idNurse * $nbDays + $idDay + $idWeek*7 ))
		echo -n $variable" "
	done
	echo "-1 wamong lin 30"
	echo "4 0 1 2 3"
	echo "0 "$limit
}

function writeMaxSaturday { #nurse #limit
	nurse=$1
	limit=$2
	
	idNurse=$(($nurse -1))
	
	echo -n "4 "
	for week in `seq 1 4`; do
		idWeek=$(($week -1))
		idDay=$((6 -1))
		variable=$(( $idNurse * $nbDays + $idDay + $idWeek*7 ))
		echo -n $variable" "
	done
	echo "-1 wamong lin 30"
	echo "4 0 1 2 3"
	echo "0 "$limit
}

function writeMaxSunday { #nurse #limit
	nurse=$1
	limit=$2
	
	idNurse=$(($nurse -1))
	
	echo -n "4 "
	for week in `seq 1 4`; do
		idWeek=$(($week -1))
		idDay=$((7 -1))
		variable=$(( $idNurse * $nbDays + $idDay + $idWeek*7 ))
		echo -n $variable" "
	done
	echo "-1 wamong lin 30"
	echo "4 0 1 2 3"
	echo "0 "$limit
}

function writeBothWeekEndDay { #nurse
	nurse=$1
	
	idNurse=$(($nurse - 1))
	
	for week in `seq 1 4`; do
		idWeek=$(($week - 1))
		sat=$(( $idNurse * $nbDays + 7*$idWeek + 5))
		sun=$(( $idNurse * $nbDays + 7*$idWeek + 6))
		echo "2 "$sat" "$sun" 0 8" 
		echo "4 0 1"
		echo "4 1 1" 
		echo "4 2 1"
		echo "4 3 1"
		echo "0 4 1"
		echo "1 4 1"
		echo "2 4 1"
		echo "3 4 1"
	done
}

function writeNoConsWE { #nurse
	nurse=$1
	
	idNurse=$(($nurse -1))

	echo -n 8 " "
	for week in `seq 1 4`; do
		idWeek=$(($week - 1))
		sat=$(( $idNurse * $nbDays + 7*$idWeek + 5))
		sun=$(( $idNurse * $nbDays + 7*$idWeek + 6))
		echo $sat" "$sun" "
	done 
	echo "-1 wregular"
	echo "9"
	echo "1 0 0"
	echo "9 0 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0"
	echo "45"
	echo "0 0 1 0"
	echo "0 1 1 0"
	echo "0 2 1 0"
	echo "0 3 1 0"
	echo "0 4 3 0"
	echo "1 0 2 0"
	echo "1 1 2 0"
	echo "1 2 2 0"
	echo "1 3 2 0"
	echo "1 4 2 0"
	echo "3 0 2 0"
	echo "3 1 2 0"
	echo "3 2 2 0"
	echo "3 3 2 0"
	echo "3 4 0 0"
	echo "2 0 4 0"
	echo "2 1 4 0"
	echo "2 2 4 0"
	echo "2 3 4 0"
	echo "2 4 5 0"
	echo "4 0 6 0"
	echo "4 1 6 0"
	echo "4 2 6 0"
	echo "4 3 6 0"
	echo "4 4 6 0"
	echo "5 0 6 0"
	echo "5 1 6 0"
	echo "5 2 6 0"
	echo "5 3 6 0"
	echo "5 4 2 0"
	echo "6 0 7 0"
	echo "6 1 7 0"
	echo "6 2 7 0"
	echo "6 3 7 0"
	echo "6 4 8 0"
	echo "7 0 6 50"
	echo "7 1 6 50"
	echo "7 2 6 50"
	echo "7 3 6 50"
	echo "7 4 6 50"
	echo "8 0 6 50"
	echo "8 1 6 50"
	echo "8 2 6 50"
	echo "8 3 6 50"
	echo "8 4 6 0"
}


writeHeader
writeDaily

##Nurse 1 HN
writeOnlyDH						#29
writeNoWeekEndHeadNurse			#8		#37

### Nurse 2 spe 
writeNoDH 2 10000				#29
writeWConsMin2Max4 2			#1		30
writeOConsMax3 2				#1		31
writeMaxWork 2 15				#1		32
writeShiftLimit 2 8 12 29 8		#4		36
writeShiftCons 2				#3		39
writeWeekLimitVDL 2 3 3 3 		#12		51
writeMaxFriday 2 3		 		#1		52
writeMaxSaturday 2 2	 		#1		52
writeMaxSunday 2 3		 		#1		52
writeBothWeekEndDay 2			#4		56
writeNoConsWE 2


### Nurse 3
writeNoDH 3 1					#29
writeWConsMin2Max4 3			#1		30
writeOConsMin2Max5 3			#1		31
writeMaxWork 3 16				#1		32
writeShiftLimit 3 7 16 7 7		#4		36
writeShiftCons 3				#3		39
writeWeekLimitD 3 4		 		#4		43
writeBothWeekEndDay 3			#4		47
writeNoConsWE 3

### Nurse 4
writeNoDH 4 10000				#29
writeWConsMin2Max4 4			#1		30
writeOConsMin2Max5 4			#1		31
writeMaxWork 4 16				#1		32
writeShiftLimit 4 7 16 7 7		#4		36
writeShiftCons 4				#3		39
writeWeekLimitD 4 4		 		#4		43
writeBothWeekEndDay 4			#4 		47
writeNoConsWE 4

### Nurse 5
writeNoDH 5 10000				#29
writeWConsMin2Max4 5			#1		30
writeOConsMin2Max5 5			#1		31
writeMaxWork 5 16				#1		32
writeShiftLimit 5 7 16 7 7		#4		36
writeShiftCons 5				#3		39
writeWeekLimitD 5 4		 		#4		43
writeBothWeekEndDay 5			#4		47
writeNoConsWE 5

### Nurse 6
writeNoDH 6 10000				#29
writeWConsMin2Max4 6			#1		30
writeOConsMin2Max5 6			#1		31
writeMaxWork 6 16				#1		32
writeShiftLimit 6 7 16 7 7		#4		36
writeShiftCons 6				#3		39
writeWeekLimitD 6 4		 		#4		43
writeBothWeekEndDay 6			#4		47
writeNoConsWE 6

### Nurse 7
writeNoDH 7 10000				#29
writeWConsMin2Max4 7			#1		30
writeOConsMin2Max5 7			#1		31
writeMaxWork 7 16				#1		32
writeShiftLimit 7 7 16 7 7		#4		36
writeShiftCons 7				#3		39
writeWeekLimitDL 7 4 3	 		#8		47
writeMaxFriday 7 2		 		#1		48
writeMaxMonday 7 2		 		#1		49
writeBothWeekEndDay 7			#4		53
writeNoConsWE 7

### Nurse 8
writeNoDH 8 10000				#29
writeWConsMin2Max4 8			#1		30
writeOConsMin2Max5 8			#1		31
writeMaxWork 8 16				#1		32
writeShiftLimit 8 7 16 7 7		#4		36
writeShiftCons 8				#3		39
writeWeekLimitDL 8 4 3	 		#8		47
writeMaxFriday 8 2		 		#1		48
writeMaxMonday 8 2		 		#1		49
writeBothWeekEndDay 8			#4		53
writeNoConsWE 8

### Nurse 9
writeNoDH 9 10000				#29
writeWConsMin2Max4 9			#1		30
writeOConsMin2Max5 9			#1		31
writeMaxWork 9 16				#1		32
writeShiftLimit 9 7 16 7 7		#4		36
writeShiftCons 9				#3		39
writeWeekLimitDL 9 4 3	 		#8		47
writeMaxFriday 9 2		 		#1		48
writeMaxMonday 9 2		 		#1		49
writeBothWeekEndDay 9			#4		53
writeNoConsWE 9

### Nurse 10
writeNoDH 10 10000				#29
writeWConsMin2Max4 10			#1		30
writeOConsMin2Max5 10			#1		31
writeMaxWork 10 16				#1		32
writeShiftLimit 10 7 16 7 7		#4		36
writeShiftCons 10				#3		39
writeWeekLimitDL 10 4 3	 		#8		47
writeMaxFriday 10 2		 		#1		48
writeMaxMonday 10 2		 		#1		49
writeBothWeekEndDay 10			#4		53
writeNoConsWE 10

### Nurse 11
writeNoDH 11 1					#29
writeWConsMin2Max4 11			#1		30
writeOConsMin2Max5 11			#1		31
writeMaxWork 11 16				#1		32
writeShiftLimit 11 7 16 7 7		#4		36
writeShiftCons 11				#3		39
writeWeekLimitDL 11 4 3	 		#8		47
writeMaxFriday 11 2		 		#1		48
writeMaxMonday 11 2		 		#1		49
writeBothWeekEndDay 11			#4		53
writeNoConsWE 11

### Nurse 12
writeNoDH 12 1					#29
writeWConsMin2Max4 12			#1		30
writeOConsMin2Max5 12			#1		31
writeMaxWork 12 16				#1		32
writeShiftLimit 12 7 16 7 7		#4		36
writeShiftCons 12				#3		39
writeWeekLimitDL 12 4 3	 		#8		47
writeMaxFriday 12 2		 		#1		48
writeMaxMonday 12 2		 		#1		49
writeBothWeekEndDay 12			#4		53
writeNoConsWE 12

### Nurse 13
writeNoDH 13 1					#29
writeWConsMin2Max4 13			#1		30
writeOConsMin2Max5 13			#1		31
writeMaxWork 13 16				#1		32
writeShiftLimit 13 7 16 7 7		#4		36
writeShiftCons 13				#3		39
writeWeekLimitDL 13 4 3	 		#8		47
writeMaxFriday 13 2		 		#1		48
writeMaxMonday 13 2		 		#1		49
writeBothWeekEndDay 13			#4		53
writeNoConsWE 13


writeNurse

