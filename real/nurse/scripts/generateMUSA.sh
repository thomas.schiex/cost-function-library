#!/bin/bash

ubGlobal=10000000
nbDays=14
nbNurses=11
nbShifts=1
nbVariables=$(( $nbDays * $nbNurses ))
nbValues=$(( $nbShifts + 1 ))
nbConstraints=$(( $nbDays*3 + $nbDays*$nbNurses + $nbNurses*2 + 7 + 7))

function writeHeader {
	echo "MUSA_14Days_11Nurses_1Shifts" $nbVariables $nbValues $nbConstraints $ubGlobal
	for i in `seq 1 $nbVariables`; do
		echo -n $nbValues" "
	done
	echo ""
}

function writeDaily {
	## RN ##
	for day in `seq  1 $nbDays`; do 
		idday=$(( $day - 1 ))
		variable1=$(( (1-1)*$nbDays + ($day-1) ))
		variable2=$(( (2-1)*$nbDays + ($day-1) ))
		variable3=$(( (3-1)*$nbDays + ($day-1) ))
		echo "3 "$variable1" "$variable2" "$variable3" 0 7"
		echo "0 0 1 5"
		echo "0 1 0 5"
		echo "1 0 0 5"
		echo "1 1 0 10"
		echo "1 0 1 10"
		echo "0 1 1 10"
		echo "1 1 1 17"
	done
	## LPN ##
	for day in `seq  1 $nbDays`; do 
		idday=$(( $day - 1 ))
		variable1=$(( (4-1)*$nbDays + ($day-1) ))
		variable2=$(( (5-1)*$nbDays + ($day-1) ))
			echo "2 "$variable1" "$variable2" 0 3"
			echo "1 1 12"
			echo "0 1 5"
			echo "1 0 5"
	done
	## NA ##
	for day in `seq  1 $nbDays`; do 
		idday=$(( $day - 1 ))
		echo -n "6 "
		for nurse in `seq 6 11`; do
			variable=$(( ($nurse-1) * $nbDays + $idday ))
			echo -n $variable" " 
		done
		echo "-1 wamong lin 17"
		echo "1 0"
		echo "2 6"
	done
}

function writeRequest {
	
	request=(
	#1 		2 		3 		4 		5 		6 		7 		8 		9 		0 		1 		2 		3 		4
	 0 		0 		0		0 		0 		0 		0 		0 		0 		0 		0 		0 		5 		5 		#1
	 0 		0 		0 		0 		0 		5 		5 		10000 	10000 	10000 	10000 	10000 	10000 	10000 	#2
	 10000 	10000 	10000 	10000 	10000 	3 		3 		0 		0 		0 		0 		0 		0 		0		#3
	 0 		0 		0		0 		0 		0 		0 		0 		0 		0 		0 		0 		3		3 		#4
	 0 		0 		0		0 		0 		5		5 		0 		0 		0 		0 		0 		0 		0 		#5
	#1 		2 		3 		4 		5 		6 		7 		8 		9 		0 		1 		2 		3 		4
	 0 		0 		0		0 		0 		0 		0 		0 		0 		0 		0 		0 		5		5 		#6
	 0 		0 		0		0 		0 		0 		0 		0 		0 		0 		0 		0 		5		5 		#7
	 0 		0 		0		0 		0 		0 		0 		0 		0 		0 		0 		0 		3		3 		#8
	 0 		0 		0		0 		0 		5		5 		0 		0 		0 		0 		0 		0 		0 		#9
	 0 		0 		0		0 		0 		5		5 		0 		0 		0 		0 		0 		0 		0 		#10
	 0 		0 		0		0 		0 		0 		0 		0 		0 		0 		0 		0 		3		3 		#11
	#1 		2 		3 		4 		5 		6 		7 		8 		9 		0 		1 		2 		3 		4
	)
	
	
	for day in `seq  1 $nbDays`; do 
		idday=$(( $day - 1 ))
		for nurse in `seq 1 11`; do
			idnurse=$(( $nurse - 1))
			variable=$(( $idnurse*$nbDays + $idday ))
			echo "1 "$variable" 0 1"
			echo "0 "${request[$variable]}
		done
	done
}

function writeNurse {
		#	 	1		2		3		4		5		6		7		8		9		10		11
	dayReq=( 	10		5		4		6		10		10		10		4		10		10		2 )
	offReq=( 	4		14		14		8		4		4		4		10		4		4		12 )
	
	for nurse in `seq 1 11`; do
		idnurse=$(( $nurse - 1))
		echo -n $nbDays" ";
		for day in `seq  1 $nbDays`; do 
			idday=$(( $day - 1 ))
			variable=$(( $idnurse*$nbDays + $idday ))
			echo -n $variable" "
		done
		echo "-1 wamong lin 9"
		echo "1 0"
		echo "0 "${dayReq[$idnurse]}
	done
	
	for nurse in `seq 1 11`; do
		idnurse=$(( $nurse - 1))
		echo -n $nbDays" ";
		for day in `seq  1 $nbDays`; do 
			idday=$(( $day - 1 ))
			variable=$(( $idnurse*$nbDays + $idday ))
			echo -n $variable" "
		done
		echo "-1 wamong lin 9"
		echo "1 1"
		echo "0 "${offReq[$idnurse]}
	done	
}

function cons2off {
	echo "3"
	echo "1 0 0"
	echo "3 0 0 1 0 2 0"
	echo "6"
	echo "0 0 0 0"
	echo "0 1 1 0"
	echo "1 0 0 0"
	echo "1 1 2 0"
	echo "2 0 0 0"
	echo "2 1 2 5"
}

function writeCons2Off {
	nurseOff=( 1 2 5 6 7 9 10 )
	for nursepos in `seq 0 6`; do 
		idnurse=$((${nurseOff[$nursePos]} - 1))
		if [[ ${nurseOff[$nursePos]} -ne 2 ]]; then 
			echo -n "14 "
			for day in `seq  1 $nbDays`; do 
				idday=$(( $day - 1 ))
				variable=$(( $idnurse*$nbDays + $idday ))
				echo -n $variable" "
			done
		else
			echo -n "7 "
			for day in `seq  1 7`; do 
				idday=$(( $day - 1 ))
				variable=$(( $idnurse*$nbDays + $idday ))
				echo -n $variable" "
			done
		fi
		echo "-1 wregular"
		cons2off
	done
}

function writeWeekSpecial {
	nurseOff=( 	1 2 5 6 7 9 10 )
	week=(		2 1 1 2 2 1 1 )
	
	for nursepos in `seq 0 6`; do 
		idnurse=$((${nurseOff[$nursePos]} - 1))
		echo -n "7 "
		if [[ ${week[$nursePos]} -eq 1 ]] ; then
			for day in `seq  1 7`; do 
				idday=$(( $day - 1 ))
				variable=$(( $idnurse*$nbDays + $idday ))
				echo -n $variable" "
			done
		else
			for day in `seq  8 14`; do 
				idday=$(( $day - 1 ))
				variable=$(( $idnurse*$nbDays + $idday ))
				echo -n $variable" "
			done
		fi
		echo "-1 wamong lin 5"
		echo "1 0"
		echo "0 5"
	done
}

writeHeader
writeDaily
writeRequest
writeNurse
writeCons2Off
writeWeekSpecial
