#!/bin/bash

ubGlobal=10000000
nbDays=7
nbNurses=14
nbShifts=2
nbVariables=$(( $nbDays * $nbNurses ))
nbValues=$(( $nbShifts + 1 ))
nbConstraints=$((  3*$nbDays + $nbNurses*$nbDays + 2*$nbNurses + 2 + 6 ))

function writeHeader {
	echo "OZKARAHAN_7ays_14Nurses_2Shifts" $nbVariables $nbValues $nbConstraints $ubGlobal
	for i in `seq 1 $nbVariables`; do
		echo -n $nbValues" "
	done
	echo ""
}

function writeDaily {
	### AID
	for day in `seq  1 $nbDays`; do 
		idday=$(( $day - 1 ))
		variable1=$(( (12-1)*$nbDays + $idday ))
		variable2=$(( (13-1)*$nbDays + $idday ))
		variable3=$(( (14-1)*$nbDays + $idday ))
		echo "3 "$variable1"  "$variable2" "$variable3" 0 15"
		echo "0 0 0 400" 
		echo "0 0 1 200" 
		echo "0 0 2 200" 
		echo "0 1 0 200" 
		echo "0 2 0 200"
		echo "1 0 0 200" 
		echo "2 0 0 200"		
		echo "1 1 1 200" 
		echo "1 2 2 200"
		echo "2 1 2 200"
		echo "2 2 1 200"
		echo "1 1 2 200"
		echo "1 2 1 200"
		echo "2 1 1 200"
		echo "2 2 2 200"
	done
	
	### NURSE
	for day in `seq  1 $nbDays`; do 
		idday=$(( $day - 1 ))
		echo -n "11 "
		for nurse in `seq 1 11`; do
			idnurse=$(( $nurse - 1))
			variable=$(( $idnurse*$nbDays + $idday ))
			echo -n $variable" "
		done
		echo "-1 wamong lin 200"
		echo "1 0"
		if [[ $day -lt 6 ]] ; then
			echo "4 4"
		else 
			echo "3 3"
		fi
	done
	
	for day in `seq  1 $nbDays`; do 
		idday=$(( $day - 1 ))
		echo -n "11 "
		for nurse in `seq 1 11`; do
			idnurse=$(( $nurse - 1))
			variable=$(( $idnurse*$nbDays + $idday ))
			echo -n $variable" "
		done
		echo "-1 wamong lin 200"
		echo "1 1"
		echo "2 2"
	done	
		
}

function writeNurse {
	dayReq=(	0 0 0 3 4 2 3 4 4 3 3 2 3 2 )
	nightReq=(	5 6 3 0 0 0 0 0 0 0 0 0 0 0 )
	
	for nurse in `seq 1 $nbNurses`; do
		idnurse=$(( $nurse - 1))
		if [[ ${dayReq[$idnurse]} -eq 0 ]]; then 
			for day in `seq  1 $nbDays`; do 
				idday=$(( $day - 1 ))
				variable=$(( $idnurse*$nbDays + $idday ))
				echo "1 "$variable" 0 1"
				echo "0 500"
			done
		fi
		if [[ ${nightReq[$idnurse]} -eq 0 ]]; then 
			for day in `seq  1 $nbDays`; do 
				idday=$(( $day - 1 ))
				variable=$(( $idnurse*$nbDays + $idday ))
				echo "1 "$variable" 0 1"
				echo "1 500"
			done
		fi
	done
	
	for nurse in `seq 1 $nbNurses`; do
		idnurse=$(( $nurse - 1))
		if [[ ${dayReq[$idnurse]} -ne 0 ]]; then 
			echo -n $nbDays" "
			for day in `seq  1 $nbDays`; do 
				idday=$(( $day - 1 ))
				variable=$(( $idnurse*$nbDays + $idday ))
				echo -n $variable" "
			done
			echo "-1 wamong lin 300"
			echo "1 0"
			echo ${dayReq[$idnurse]}" "$nbDays
			
			echo -n $nbDays" "
			for day in `seq  1 $nbDays`; do 
				idday=$(( $day - 1 ))
				variable=$(( $idnurse*$nbDays + $idday ))
				echo -n $variable" "
			done
			echo "-1 wamong lin 500"
			echo "1 0"
			echo "0 "${dayReq[$idnurse]}
		fi		
	done
	
	for nurse in `seq 1 $nbNurses`; do
		idnurse=$(( $nurse - 1))
		if [[ ${nightReq[$idnurse]} -ne 0 ]]; then 
			echo -n $nbDays" "
			for day in `seq  1 $nbDays`; do 
				idday=$(( $day - 1 ))
				variable=$(( $idnurse*$nbDays + $idday ))
				echo -n $variable" "
			done
			echo "-1 wamong lin 300"
			echo "1 1"
			echo ${nightReq[$idnurse]}" "$nbDays
			
			echo -n $nbDays" "
			for day in `seq  1 $nbDays`; do 
				idday=$(( $day - 1 ))
				variable=$(( $idnurse*$nbDays + $idday ))
				echo -n $variable" "
			done
			echo "-1 wamong lin 500"
			echo "1 1"
			echo "0 "${nightReq[$idnurse]}
		fi		
	done	
}

function writeRequest {
	day1=2
	nurse1=2
	variable1=$(( ($nurse1 - 1)*$nbDays + ($day1 - 1)))
	
	echo "1 "$variable1" 100 1"
	echo "2 0"
	
	day2=3
	nurse2=5
	variable2=$(( ($nurse2 - 1)*$nbDays + ($day2 - 1)))
	
	echo "1 "$variable2" 100 1"
	echo "2 0"	
}

function writeWE {
	nurses=( 3 4 7 8 9 13 )
	for nursePos in `seq 0 5`; do
		variable1=$(( (${nurses[$nursePos]} - 1)*$nbDays  + 5 ))
		variable2=$(( (${nurses[$nursePos]} - 1)*$nbDays  + 6 ))
		echo "2 "$variable1" "$variable2" 400 1"
		echo "2 2 0"
	done 
	
}
writeHeader
writeDaily
writeNurse
writeRequest
writeWE
