#!/bin/bash

ubGlobal=10000000
nbDays=28
nbNurses=13
nbShifts=2
nbVariables=$(( $nbDays * $nbNurses ))
nbValues=$(( $nbShifts + 1 ))
nbConstraints=$(( 4*$nbDays + 4*$nbNurses + $nbNurses*($nbDays-1) + $nbNurses*($nbDays-2) + $nbNurses + $nbNurses ))

function writeHeader {
	echo "AZAIEZ_28Days_13Nurses_2Shifts" $nbVariables $nbValues $nbConstraints $ubGlobal
	for i in `seq 1 $nbVariables`; do
		echo -n $nbValues" "
	done
	echo ""
}

function writeDaily {
	### ALL
	for day in `seq 1 $nbDays`; do
		idDay=$(( $day - 1 ))
		echo -n $nbNurses
		for nurse in `seq 1 $nbNurses`; do
			idNurse=$(( $nurse - 1))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 10000"
		echo "1 0"
		echo "3 13"
	done
	
	for day in `seq 1 $nbDays`; do
		idDay=$(( $day - 1 ))
		echo -n $nbNurses
		for nurse in `seq 1 $nbNurses`; do
			idNurse=$(( $nurse - 1))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 10000"
		echo "1 1"
		echo "3 13"
	done
	
	S0=( 2 4 5 6 9 ) # => 5
	S1=( 1 3 7 10 11 12 13 ) # => 7
	S2=( 8 ) # => 1
	
	for day in `seq 1 $nbDays`; do
		idDay=$(( $day - 1 ))
		echo -n "5"
		for nurse in `seq 0 4`; do
			idNurse=$(( ${S0[$nurse]} - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo "-1 wamong lin 10000"
		echo "1 0"
		echo "1 5"
	done

	for day in `seq 1 $nbDays`; do
		idDay=$(( $day - 1 ))
		echo -n "7"
		for nurse in `seq 0 6`; do
			idNurse=$(( ${S1[$nurse]} - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo "-1 wamong lin 10000"
		echo "1 1"
		echo "1 7"
	done	
}

function writeNurse {
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n $nbDays 
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 5"
		echo "1 0"
		echo "8 8"
	done
	
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n $nbDays 
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 5"
		echo "1 1"
		echo "7 7"
	done
	
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n $nbDays 
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 10000"
		echo "1 2"
		echo "12 14"
	done
	
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n $nbDays 
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 20"
		echo "2 0 1"
		echo "0 15"
	done
}

function writePrec { #$nbNurses*($nbDays-1)
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))
		nbDayMone=$(( $nbDays - 1 ))
		for day in `seq 1 $nbDayMone`; do
			variable1=$(( $idNurse * $nbDays + ($day - 1) ))
			variable2=$(( $idNurse * $nbDays + ($day) ))
			echo "2 "$variable1" "$variable2" 0 2"
			echo "1 0 10000"
			echo "0 1 3"
		done
	done
}

function writeIsol { #$nbNurses*($nbDays-2)
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))
		nbDayMtwo=$(( $nbDays - 2 ))
		for day in `seq 1 $nbDayMtwo`; do
			variable1=$(( $idNurse * $nbDays + ($day - 1) ))
			variable2=$(( $idNurse * $nbDays + ($day) ))
			variable3=$(( $idNurse * $nbDays + ($day + 1) ))
			echo "3 "$variable1" "$variable2" "$variable3" 0 6"
			echo "2 0 2 1"
			echo "2 1 2 1"
			echo "0 2 0 1"
			echo "0 2 1 1"
			echo "1 2 0 1"
			echo "1 2 1 1"
		done
	done
}

function writeWE {
	weday=( 6 7 13 14 20 21 27 28 )
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))
		echo -n "8"
		for daypos in `seq 0 7`; do
			idDay=$(( ${weday[$daypos]} - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wamong lin 10000"
		echo "1 2"
		echo "0 4"
	done
}

function writeStretche {
	for nurse in `seq 1 $nbNurses`; do
		idNurse=$(( $nurse - 1))	
		echo -n $nbDays 
		for day in `seq 1 $nbDays`; do 
			idDay=$(( $day - 1 ))
			variable=$(( $idNurse * $nbDays + $idDay ))
			echo -n " "$variable
		done
		echo " -1 wregular"
		echo "5"
		echo "1 0 0 "
		echo "5 0 0 1 0 2 0 3 0 4 0 "
		echo "15"
		echo "0 0 1 0"
		echo "0 1 1 0"
		echo "0 2 0 0"
		echo "1 0 2 0"
		echo "1 1 2 0"
		echo "1 2 0 0"
		echo "2 0 3 0"
		echo "2 1 3 0"
		echo "2 2 0 0"
		echo "3 0 4 0"
		echo "3 1 4 0"
		echo "3 2 0 0"
		echo "4 0 4 10000"
		echo "4 1 4 10000"
		echo "4 2 0 0"
	done
}

writeHeader
writeDaily
writeNurse
writePrec
writeIsol
writeWE
writeStretche
