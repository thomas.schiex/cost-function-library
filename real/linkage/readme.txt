----------------------------------------------------------------------------
pedigree*.uai instances are from:

Probablistic Inference Evaluation of UAI'08

http://graphmod.ics.uci.edu/uai08/Evaluation/Report

Linkage 2

    * Submitter: Dechter group (UC Irvine)
    * Domain: Genetic linkage
    * Type: Markov for MPE
          o 22 instances
          o Max. domain size between 3 and 7 
    * Treewidth: ~20-35 

----------------------------------------------------------------------------
sim.* instances are extracted from:

Haplotype Inference in General Pedigrees using the Cluster Variation Method
Cornelis A Albers, Tom Heskes, Hilbert J Kappen
Genetics, Vol. 177, 1101-1116, October 2007

http://www.snn.ru.nl/~bertk/cvmhaplo.pdf

sim.1 corresponds to pedigree.I with 53 individuals and 20 markers (SNP).
sim.2 corresponds to pedigree.II with 368 individuals and 8 markers (SNP).

E.g. sim.1.53.20.4.uai sim.1.53.20.4.uai.evid were generated with quaternary cost functions using:
awk -f pre2uai_quater.awk sim.1.par sim.1.pre > sim.1.53.20.4.uai

They were converted by toulbar2 into wcsp format:
toulbar2 sim.1.53.20.4.uai sim.1.53.20.4.uai.evid -z
mv problem_original.wcsp sim.1.53.20.4.wcsp

Finally, toulbar2 v0.9.4 preprocessed them using cost function decomposition and i-bounded variable elimination:

toulbar2 sim.1.53.20.4.uai sim.1.53.20.4.uai.evid -p=*i*
mv problem.wcsp sim.1.53.20.4_reduce*i*.wcsp

----------------------------------------------------------------------------
bovin* instances are from INRA (real data of bovine chromosome 3)

Whole data has 7864 individuals and 460 markers (SNP).
A selection of the 20 first markers has been extracted.

@ Aurelie Favier, Simon de Givry
afavier@toulouse.inra.fr
degivry@toulouse.inra.fr
