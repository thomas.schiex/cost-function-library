# usage: awk -f pre2uai_ter.awk nom.par nom.pre  > nom.uai

# creation des fichiers nom.uai et nom.uai.evid

#ATTENTION: les phenotypes non observés ont une valeur qcq

# modelisation avec des contraintes au plus quaternaires

BEGIN{
	#msg="erreur"
	if(ARGC==3)
	{
	c=0;
	n=0;
	g=0;
	f=0;
	l=0;
	i=0;
	markers=0;
	}
	
	
}
# n := nombre d'individus
# f := nombre de fondateurs
# g := nombre d'evidences
# l := nombre de loci
# nbp := nombre de contraintes (probabilités conditionnelles)
# individu[i]=j := l'individu i a pour identifiant j
# id[j]=i := l'identifiant le l'individu i est j
# father[i]=j := le pere de i est j
# mother[i]=j := la mere de i est j

# snp1[i] := tableau d'allele pour l'individu i du 1er chrs
# snp2[i] := tableau d'allele pour l'individu i du 2nd chrs


#################################
# Traitement du fichier nom.par #
#################################

FILENAME==ARGV[1] && (NR ==1){
#	print "A " ARGV[1] " " $0

	markers = $1
}

FILENAME==ARGV[1] && (NR > 3) && (NR < 4+2*markers) && (NR%2 == 1) {	#recuperer les frequence allelique de chaque marqueur
	# freq[l,0]=P(allele de l=1)
	freq[NR-4-c,0]=$1;
	# freq[l,1]=P(allele de l=2)
	freq[NR-4-c,1]=$2;
	c++;
	
}

FILENAME==ARGV[1] && (NR == 5+markers*2){
	for(i=1; i<=NF; i++)
	{
		#Avec H1:
			#recombinaison[i] = le taux de recombinaison entre le marqueur i-1 et i
		recombinaison[i+1]=$i;
		# Sans H1:
			#recombinaison[l]=taux de recombinaison entre locus[l] et locus[l-1]
		#recombinaison[locus[i+1]]=$i
	}
}

#################################
# Traitement du fichier nom.pre #
#################################

FILENAME==ARGV[2]{
	individu[++n] = $2;
	id[$2] = n;
	if ($3!=0 && $4==0) $3 = 0;
	else if ($3==0 && $4!=0) $4 = 0;
	father[$2] = $3;
	mother[$2] = $4;
	if($3==0 && $4==0)
		fondateur[++f]=$2;
	#markers = (NF-6)/2;
	decalage = NF - 2*markers + 1;
	for (i=NF-2*markers+1; i<=NF; i++) {
		if (i % 2 == 0) snp1[$2,1+int((i-decalage)/2)] = $i;
		else snp2[$2,1+int((i-decalage)/2)] = $i;
#		if (i % 2 == 0) snp1[$2,int((i-5)/2)] = $i;
#		else snp2[$2,int((i-5)/2)] = $i;
		if ($i>0) g++;
	}
}




# pour chaque marqueur et pour chaque individu, 5 variables:
# variable allele paternel: indice 0
# variable allele maternel: indice 1
# variable selection allele paternel: indice 2
# variable selection allele maternel: indice 3
# variable phenotype: indice 4
END{
	if(ARGC==3){

	print "BAYES"

	#nombre de variables:
	print 5*n*markers 

	#la taille du domaine de chaque variable	
	domaine=""
	for(i=1; i<=5*n*markers; i++)
		if(i%5==0) # domaine des variable PHENOTYPE
			domaine = domaine "3 "
		else
			domaine = domaine "2 "
	print domaine

	# nombre de probabilités pour les fondateurs: 2*F*markers
	# nombre de probabilités pour les recombinaisons : 2*NF*markers
	# nombre de probabibiltés conditionnées: 4*NF*markers + evidence/2


	#nombre de probabilites (contraintes)
	#print (2*f*markers)" + " (2*(n-f)*markers)" + " (4*(n-f)*markers) " + " g/2*markers
	print (2*f*markers)+(2*(n-f)*markers)+ (4*(n-f)*markers) + g/2


	# pour les probabilités des fondateurs (hors variable phenotype):
#	print "# fondateurs:" #par ordre des fondateurs trouves (par marqueurs)

	for(m=1; m<=markers; m++)	
		for(i=1; i<=f; i++)
		{
			print "1 " 5*(m-1)*n+ 5*(id[fondateur[i]]-1)+0;
			print "1 " 5*(m-1)*n+ 5*(id[fondateur[i]]-1)+1;
		}

#	print "# segregations:"
	# pour les probabilités de recombinaison entre les variables de segregations:
	for(i=1; i<=n; i++)#cas pour le 1er marqueur (dans l'ordre sur le chrs)
		if ( father[individu[i]]!=0 || mother[individu[i]]!=0) #non fondateurs
		{
			# Utilisation de l'hypothese H1:
			print "1 " 5*(i-1)+2			#segregation paternel
			print "1 " 5*(i-1)+3			#segregation maternel

			# Sans l'hypothese H1: (faire...)
			#print "1 " 4*(locus[1] (??) -1)*n + 4*(i-1)+3
			#print "1 " 4*(locus[1] (??) -1)*n + 4*(i-1)+4


		}
	for(m=2; m<=markers; m++)
		for(i=1; i<=n; i++)			
			if ( father[individu[i]]!=0 || mother[individu[i]]!=0) #non fondateurs
			{	
				# Avec H1:
				print "2 " 5*(m-2)*n+ 5*(i-1)+2 " " 5*(m-1)*n+ 5*(i-1)+2		#segregation paternel
				print "2 " 5*(m-2)*n+ 5*(i-1)+3 " " 5*(m-1)*n+ 5*(i-1)+3		#segregation maternel
			}

#	print "# phenotypes:"# seuls les phenotypes observés sont necessaires
	for(m=1; m<=markers; m++)
		for(i=1; i<=n; i++)	
			if(snp1[individu[i],m]!=0 && snp2[individu[i],m]!=0)
				print "3 " 5*(m-1)*n+ 5*(i-1)+0 " " 5*(m-1)*n+ 5*(i-1)+1 " " 5*(m-1)*n+ 5*(i-1)+4

#	print "# conditionnees:"
	# pour les probabilités conditionnelles:
	for(m=1; m<=markers; m++)
		for(i=1; i<=n; i++) {
			if ( father[individu[i]]!=0 )
			{
				# 3 GPp SC GC
				print "3 " 5*(m-1)*n+ 5*(id[father[individu[i]]]-1)+0 " " 5*(m-1)*n+ 5*(i-1)+2 " " 5*(m-1)*n+ 5*(i-1)+0;
				# 3 GMp SC GC
				print "3 " 5*(m-1)*n+ 5*(id[father[individu[i]]]-1)+1 " " 5*(m-1)*n+ 5*(i-1)+2 " " 5*(m-1)*n+ 5*(i-1)+0;
			} else {
				print "1 0";
				print "1 0";
			}
			if ( mother[individu[i]]!=0 )
			{
				# 3 GPm SC GC
				print "3 " 5*(m-1)*n+ 5*(id[mother[individu[i]]]-1)+0 " " 5*(m-1)*n+ 5*(i-1)+3 " " 5*(m-1)*n+ 5*(i-1)+1;
				# 3 GMm SC GC
				print "3 " 5*(m-1)*n+ 5*(id[mother[individu[i]]]-1)+1 " " 5*(m-1)*n+ 5*(i-1)+3 " " 5*(m-1)*n+ 5*(i-1)+1;
			} else {
				print "1 0";
				print "1 0";
			}
		}



	# TABLES DE VALEURS (dans le meme ordre que les probabilites enoncees!)

#	print "# Probabilites des fondateurs:"
	for(m=1; m<=markers; m++)
		for(i=1; i<=f; i++)
		{
			print "2"
			# récupérer les fréquences allélique du marqueur correspondant
			print freq[m,0] " " freq[m,1] #" (" 5*(m-1)*n+ 5*(id[fondateur[i]]-1)+1 ")"
			print "2"
			print freq[m,0] " " freq[m,1] #" (" 5*(m-1)*n+ 5*(id[fondateur[i]]-1)+2 ")"
		
		}

	# pour les probabilités de recombinaison:
#	print "# Probabilites de segregation: "
	for(i=1; i<=n; i++)
		if ( father[individu[i]]!=0 || mother[individu[i]]!=0) # non fondateurs 
		{
			print "2" 
			print "0.5 0.5" # par défaut
			print "2" 
			print "0.5 0.5" # par défaut  
		}

	for(m=2; m<=markers; m++)
		for(i=1; i<=n; i++)			
			if ( father[individu[i]]!=0 || mother[individu[i]]!=0) #non fondateurs
			{	
				# Avec H1:
				print "4" 
				print 1-recombinaison[m] " " recombinaison[m] 
				print recombinaison[m] " " 1-recombinaison[m]
				print "4"  
				print 1-recombinaison[m] " " recombinaison[m]
				print recombinaison[m] " " 1-recombinaison[m]
			}


#	print "# Probabilites des phenotypes:"
	for(m=1; m<=markers; m++)
		for(i=1; i<=n; i++)	
		if(snp1[individu[i],m]!=0 && snp2[individu[i],m]!=0)
		{
			print "12 "
			print "1 0 0"
			print "0 1 0"
			print "0 1 0"
			print "0 0 1" 
		}


	for(m=1; m<=markers; m++)
		for(i=1; i<=n; i++) {
			if ( father[individu[i]]!=0 )
			{
					# 3 GPp SC GC
					print "8"
					print "1 0"
#					print "0.5 0.5"
					print "1 1"
					print "0 1"
#					print "0.5 0.5"
					print "1 1"

					# 3 GMp SC GC
					print "8"
#					print "0.5 0.5"
					print "1 1"
					print "1 0"
#					print "0.5 0.5"
					print "1 1"
					print "0 1"
			} else {
				print "2";
				print "0.5 0.5";
				print "2";
				print "0.5 0.5";
			}
			if ( mother[individu[i]]!=0 )
			{
					# 3 GPp SC GC
					print "8"
					print "1 0"
#					print "0.5 0.5"
					print "1 1"
					print "0 1"
#					print "0.5 0.5"
					print "1 1"

					# 3 GMp SC GC
					print "8"
#					print "0.5 0.5"
					print "1 1"
					print "1 0"
#					print "0.5 0.5"
					print "1 1"
					print "0 1"
			} else {
				print "2";
				print "0.5 0.5";
				print "2";
				print "0.5 0.5";
			}
		}

	###########################################
	# Ecriture des evidence dans nom.uai.evid #
	###########################################
	name = FILENAME 
	gsub(/[a-zA-Z]$/,"",name);
	gsub(/[a-zA-Z]$/,"",name);
	gsub(/[a-zA-Z]$/,"",name);
	name=name "uai.evid"

	print g/2 > name
	for(m=1; m<=markers; m++)
		for(i=1; i<=n; i++)
		{
			if(snp1[individu[i],m]!=0 && snp2[individu[i],m]!=0)
				print 5*(m-1)*n+ 5*(i-1)+4 " " (snp1[individu[i],m] + snp2[individu[i],m])-2 >> name
		}




	}#fin du  If du début
	else
		print "ERREUR"
}
