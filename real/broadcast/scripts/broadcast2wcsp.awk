BEGIN{top=1000000; tuples=""; wmax=1000}
FNR==1{d=$NF;for (i=0;i<d;i++) {ntuple++; tuples = tuples "\n" i " " i " " top}}
FNR==2{p=$NF}
FNR==3{c=$NF}
FNR>=4&&FNR<4+p{start[$2]=$3;end[$2]=$4;for (i=1;i<$2;i++) if (start[i] < end[$2] && end[i]>start[$2]) e++;}
FNR>=4+p&&FNR<4+p+c{ntuple++;tuples = tuples "\n" $2-1 " " $3-1 " " top}
FNR==4+p+c{print "chinesebroadcast",p,d,e+p*d,top;for (i=1;i<=p;i++)printf(" %d",d);print ""}
FNR>=4+p+c{print 1,$3-1,0,1;print $2-1,wmax-$NF}
END{for (i=1;i<=p;i++)for (j=1;j<i;j++) if (start[j] < end[i] && end[j]>start[i]) print 2,i-1,j-1,0,ntuple,tuples}

