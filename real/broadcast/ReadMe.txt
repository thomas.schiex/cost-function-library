***
Ma F. et al. (2016) Optimizing Shortwave Radio Broadcast Resource Allocation via Pseudo-Boolean Constraint Solving and Local Search. In: Rueher M. (eds) Principles and Practice of Constraint Programming. CP 2016.
***

﻿1.
The first three lines describe the size of the instance:
number_of_transmitter D:  D denoting the number of transmission devices.
number_of_program P:  P denoting the number of programs,
number_of_constraint C: C denoting the number of constraints.
For example, an instance with 50 transmission devices, 2 programs and 301 constraints will be:
number_of_transmitter 50
number_of_program 2
number_of_constraint 301 
2.
The next P lines contain program information. Each program has the following format:
program ID BT ET,
ID is a number indicating the program. BT is the start time of this program, while ET is the end time.
For example, "program 2 1200 2000" means that the second program will start at 12:00 and end at 20:00.
3.
Following the program lines are constraints information, which have the following format.
constraint u v
u and v are between 1 to D, representing transmission devices. The constraint states that transmission device u and
transmission device v conflict with each other.
For example, "constraint 19 33" means that transmission device 19 conflicts with transmission devices 33.
4.
The last part of the instance is the edge information with the following format.
edge d p w
d is a number from 1 to D denoting transmission device id, while p is a number from 1 to P denoting program id,
w is a number denoting weight. The edge information means that program p can be broadcasted by 
transmission device d with weight w, where w is the number of qualified sites.
For example, edge 21 2 36 means that program 2 can be broadcasted by the transmission device 21, covering 36 qualified sites.

