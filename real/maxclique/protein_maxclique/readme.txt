The protein structure alignment problem is to compute a score of similarity between two proteins based on a particular knowledge of their respective tri-dimensional structure. Each protein is described by a sequence of amino acids and by a set of binary relations representing pairs of amino acids which are close together in the tri-dimensional structure. The problem is to find a matching from a subsequence of amino acids of the first protein to another subsequence of amino acids of the second protein such that it maximizes the number of binary relations which are present in both subsequences and their respective amino acid pairs match together in the matching.

This problem has been transformed into the maximum clique problem as described in the following paper. The original instances, in file cmoclique.tar.gz (DIMACS binary format), come from:

D. Strickland, E. Barnes, J. Sokol
Optimal protein structure alignment using maximum cliques
Operations Research, 53(3) 389--402, 2005.

Instance sandiaprotein corresponds to instance 1bpi-1knt in the paper.

Translation to weighted Max-SAT "wcnf" format used bin2asc program (DIMACS binary to ascii format) and maxclique2wcnf.sh script (DIMACS ascii to wcnf format) available in the benchs/translators directory.

Notice that the maximum clique size can be deduced from the optimum value of wcnf instances by subtracting from the number of Boolean variables.

Translation to pseudo-Boolean "msat" format used wcsp2any program (wcsp2any file.wcnf 0 1 0 > file.msat) also available in the benchs/translators directory.
