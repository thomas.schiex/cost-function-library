Protein side-chain positioning problem as described in

Yanover, Chen, and Yair Weiss.
Approximate inference and protein-folding.
Advances in neural information processing systems 15 (NIPS 2002), pages 1481-1488.

